<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOfferListResponse StructType
 * @subpackage Structs
 */
class GetOfferListResponse extends AbstractStructBase
{
    /**
     * The GetOfferListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage
     */
    public $GetOfferListResult;
    /**
     * Constructor method for GetOfferListResponse
     * @uses GetOfferListResponse::setGetOfferListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage $getOfferListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage $getOfferListResult = null)
    {
        $this
            ->setGetOfferListResult($getOfferListResult);
    }
    /**
     * Get GetOfferListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage|null
     */
    public function getGetOfferListResult()
    {
        return isset($this->GetOfferListResult) ? $this->GetOfferListResult : null;
    }
    /**
     * Set GetOfferListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage $getOfferListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListResponse
     */
    public function setGetOfferListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage $getOfferListResult = null)
    {
        if (is_null($getOfferListResult) || (is_array($getOfferListResult) && empty($getOfferListResult))) {
            unset($this->GetOfferListResult);
        } else {
            $this->GetOfferListResult = $getOfferListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
