<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOrderListResponse StructType
 * @subpackage Structs
 */
class GetOrderListResponse extends AbstractStructBase
{
    /**
     * The GetOrderListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OrderListMessage
     */
    public $GetOrderListResult;
    /**
     * Constructor method for GetOrderListResponse
     * @uses GetOrderListResponse::setGetOrderListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderListMessage $getOrderListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderListMessage $getOrderListResult = null)
    {
        $this
            ->setGetOrderListResult($getOrderListResult);
    }
    /**
     * Get GetOrderListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderListMessage|null
     */
    public function getGetOrderListResult()
    {
        return isset($this->GetOrderListResult) ? $this->GetOrderListResult : null;
    }
    /**
     * Set GetOrderListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderListMessage $getOrderListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderListResponse
     */
    public function setGetOrderListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderListMessage $getOrderListResult = null)
    {
        if (is_null($getOrderListResult) || (is_array($getOrderListResult) && empty($getOrderListResult))) {
            unset($this->GetOrderListResult);
        } else {
            $this->GetOrderListResult = $getOrderListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
