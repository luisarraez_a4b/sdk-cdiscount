<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetRelaysFileSubmissionResultResponse StructType
 * @subpackage Structs
 */
class GetRelaysFileSubmissionResultResponse extends AbstractStructBase
{
    /**
     * The GetRelaysFileSubmissionResultResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage
     */
    public $GetRelaysFileSubmissionResultResult;
    /**
     * Constructor method for GetRelaysFileSubmissionResultResponse
     * @uses GetRelaysFileSubmissionResultResponse::setGetRelaysFileSubmissionResultResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage $getRelaysFileSubmissionResultResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage $getRelaysFileSubmissionResultResult = null)
    {
        $this
            ->setGetRelaysFileSubmissionResultResult($getRelaysFileSubmissionResultResult);
    }
    /**
     * Get GetRelaysFileSubmissionResultResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage|null
     */
    public function getGetRelaysFileSubmissionResultResult()
    {
        return isset($this->GetRelaysFileSubmissionResultResult) ? $this->GetRelaysFileSubmissionResultResult : null;
    }
    /**
     * Set GetRelaysFileSubmissionResultResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage $getRelaysFileSubmissionResultResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetRelaysFileSubmissionResultResponse
     */
    public function setGetRelaysFileSubmissionResultResult(\A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage $getRelaysFileSubmissionResultResult = null)
    {
        if (is_null($getRelaysFileSubmissionResultResult) || (is_array($getRelaysFileSubmissionResultResult) && empty($getRelaysFileSubmissionResultResult))) {
            unset($this->GetRelaysFileSubmissionResultResult);
        } else {
            $this->GetRelaysFileSubmissionResultResult = $getRelaysFileSubmissionResultResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetRelaysFileSubmissionResultResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
