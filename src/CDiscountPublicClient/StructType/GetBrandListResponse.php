<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetBrandListResponse StructType
 * @subpackage Structs
 */
class GetBrandListResponse extends AbstractStructBase
{
    /**
     * The GetBrandListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\BrandListMessage
     */
    public $GetBrandListResult;
    /**
     * Constructor method for GetBrandListResponse
     * @uses GetBrandListResponse::setGetBrandListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\BrandListMessage $getBrandListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\BrandListMessage $getBrandListResult = null)
    {
        $this
            ->setGetBrandListResult($getBrandListResult);
    }
    /**
     * Get GetBrandListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\BrandListMessage|null
     */
    public function getGetBrandListResult()
    {
        return isset($this->GetBrandListResult) ? $this->GetBrandListResult : null;
    }
    /**
     * Set GetBrandListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\BrandListMessage $getBrandListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetBrandListResponse
     */
    public function setGetBrandListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\BrandListMessage $getBrandListResult = null)
    {
        if (is_null($getBrandListResult) || (is_array($getBrandListResult) && empty($getBrandListResult))) {
            unset($this->GetBrandListResult);
        } else {
            $this->GetBrandListResult = $getBrandListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetBrandListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
