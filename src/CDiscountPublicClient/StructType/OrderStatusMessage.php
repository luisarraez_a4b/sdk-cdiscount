<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderStatusMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderStatusMessage
 * @subpackage Structs
 */
class OrderStatusMessage extends ServiceBaseAPIMessage
{
    /**
     * The Status
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Status;
    /**
     * Constructor method for OrderStatusMessage
     * @uses OrderStatusMessage::setStatus()
     * @param string $status
     */
    public function __construct($status = null)
    {
        $this
            ->setStatus($status);
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ExternalOrderStatus::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ExternalOrderStatus::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $status
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage
     */
    public function setStatus($status = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ExternalOrderStatus::valueIsValid($status)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ExternalOrderStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ExternalOrderStatus::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
