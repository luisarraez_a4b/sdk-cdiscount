<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductListByIdentifierResponse StructType
 * @subpackage Structs
 */
class GetProductListByIdentifierResponse extends AbstractStructBase
{
    /**
     * The GetProductListByIdentifierResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage
     */
    public $GetProductListByIdentifierResult;
    /**
     * Constructor method for GetProductListByIdentifierResponse
     * @uses GetProductListByIdentifierResponse::setGetProductListByIdentifierResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage $getProductListByIdentifierResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage $getProductListByIdentifierResult = null)
    {
        $this
            ->setGetProductListByIdentifierResult($getProductListByIdentifierResult);
    }
    /**
     * Get GetProductListByIdentifierResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage|null
     */
    public function getGetProductListByIdentifierResult()
    {
        return isset($this->GetProductListByIdentifierResult) ? $this->GetProductListByIdentifierResult : null;
    }
    /**
     * Set GetProductListByIdentifierResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage $getProductListByIdentifierResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListByIdentifierResponse
     */
    public function setGetProductListByIdentifierResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage $getProductListByIdentifierResult = null)
    {
        if (is_null($getProductListByIdentifierResult) || (is_array($getProductListByIdentifierResult) && empty($getProductListByIdentifierResult))) {
            unset($this->GetProductListByIdentifierResult);
        } else {
            $this->GetProductListByIdentifierResult = $getProductListByIdentifierResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListByIdentifierResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
