<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFulfilmentSupplyOrderReportListResponse StructType
 * @subpackage Structs
 */
class GetFulfilmentSupplyOrderReportListResponse extends AbstractStructBase
{
    /**
     * The GetFulfilmentSupplyOrderReportListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage
     */
    public $GetFulfilmentSupplyOrderReportListResult;
    /**
     * Constructor method for GetFulfilmentSupplyOrderReportListResponse
     * @uses GetFulfilmentSupplyOrderReportListResponse::setGetFulfilmentSupplyOrderReportListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage $getFulfilmentSupplyOrderReportListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage $getFulfilmentSupplyOrderReportListResult = null)
    {
        $this
            ->setGetFulfilmentSupplyOrderReportListResult($getFulfilmentSupplyOrderReportListResult);
    }
    /**
     * Get GetFulfilmentSupplyOrderReportListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage|null
     */
    public function getGetFulfilmentSupplyOrderReportListResult()
    {
        return isset($this->GetFulfilmentSupplyOrderReportListResult) ? $this->GetFulfilmentSupplyOrderReportListResult : null;
    }
    /**
     * Set GetFulfilmentSupplyOrderReportListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage $getFulfilmentSupplyOrderReportListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderReportListResponse
     */
    public function setGetFulfilmentSupplyOrderReportListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage $getFulfilmentSupplyOrderReportListResult = null)
    {
        if (is_null($getFulfilmentSupplyOrderReportListResult) || (is_array($getFulfilmentSupplyOrderReportListResult) && empty($getFulfilmentSupplyOrderReportListResult))) {
            unset($this->GetFulfilmentSupplyOrderReportListResult);
        } else {
            $this->GetFulfilmentSupplyOrderReportListResult = $getFulfilmentSupplyOrderReportListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderReportListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
