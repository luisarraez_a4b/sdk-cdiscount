<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductMatchingFileMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductMatchingFileMessage
 * @subpackage Structs
 */
class ProductMatchingFileMessage extends ServiceBaseAPIMessage
{
    /**
     * The PackageId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $PackageId;
    /**
     * The ProductMatchingList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductMatching
     */
    public $ProductMatchingList;
    /**
     * Constructor method for ProductMatchingFileMessage
     * @uses ProductMatchingFileMessage::setPackageId()
     * @uses ProductMatchingFileMessage::setProductMatchingList()
     * @param int $packageId
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductMatching $productMatchingList
     */
    public function __construct($packageId = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductMatching $productMatchingList = null)
    {
        $this
            ->setPackageId($packageId)
            ->setProductMatchingList($productMatchingList);
    }
    /**
     * Get PackageId value
     * @return int|null
     */
    public function getPackageId()
    {
        return $this->PackageId;
    }
    /**
     * Set PackageId value
     * @param int $packageId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage
     */
    public function setPackageId($packageId = null)
    {
        // validation for constraint: int
        if (!is_null($packageId) && !(is_int($packageId) || ctype_digit($packageId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($packageId, true), gettype($packageId)), __LINE__);
        }
        $this->PackageId = $packageId;
        return $this;
    }
    /**
     * Get ProductMatchingList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductMatching|null
     */
    public function getProductMatchingList()
    {
        return isset($this->ProductMatchingList) ? $this->ProductMatchingList : null;
    }
    /**
     * Set ProductMatchingList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductMatching $productMatchingList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage
     */
    public function setProductMatchingList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductMatching $productMatchingList = null)
    {
        if (is_null($productMatchingList) || (is_array($productMatchingList) && empty($productMatchingList))) {
            unset($this->ProductMatchingList);
        } else {
            $this->ProductMatchingList = $productMatchingList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
