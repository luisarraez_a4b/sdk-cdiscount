<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IdentifierRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:IdentifierRequest
 * @subpackage Structs
 */
class IdentifierRequest extends AbstractStructBase
{
    /**
     * The IdentifierType
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $IdentifierType;
    /**
     * The ValueList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $ValueList;
    /**
     * Constructor method for IdentifierRequest
     * @uses IdentifierRequest::setIdentifierType()
     * @uses IdentifierRequest::setValueList()
     * @param string $identifierType
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $valueList
     */
    public function __construct($identifierType = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $valueList = null)
    {
        $this
            ->setIdentifierType($identifierType)
            ->setValueList($valueList);
    }
    /**
     * Get IdentifierType value
     * @return string|null
     */
    public function getIdentifierType()
    {
        return $this->IdentifierType;
    }
    /**
     * Set IdentifierType value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\IdentifierTypeEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\IdentifierTypeEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $identifierType
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\IdentifierRequest
     */
    public function setIdentifierType($identifierType = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\IdentifierTypeEnum::valueIsValid($identifierType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\IdentifierTypeEnum', is_array($identifierType) ? implode(', ', $identifierType) : var_export($identifierType, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\IdentifierTypeEnum::getValidValues())), __LINE__);
        }
        $this->IdentifierType = $identifierType;
        return $this;
    }
    /**
     * Get ValueList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getValueList()
    {
        return isset($this->ValueList) ? $this->ValueList : null;
    }
    /**
     * Set ValueList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $valueList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\IdentifierRequest
     */
    public function setValueList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $valueList = null)
    {
        if (is_null($valueList) || (is_array($valueList) && empty($valueList))) {
            unset($this->ValueList);
        } else {
            $this->ValueList = $valueList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\IdentifierRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
