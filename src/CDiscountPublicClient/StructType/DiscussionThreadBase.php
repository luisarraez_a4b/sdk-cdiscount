<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DiscussionThreadBase StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscussionThreadBase
 * @subpackage Structs
 */
class DiscussionThreadBase extends AbstractStructBase
{
    /**
     * The CloseDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CloseDate;
    /**
     * The CreationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $CreationDate;
    /**
     * The Id
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Id;
    /**
     * The LastUpdatedDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LastUpdatedDate;
    /**
     * The Messages
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfMessage
     */
    public $Messages;
    /**
     * The Status
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Status;
    /**
     * The Subject
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Subject;
    /**
     * Constructor method for DiscussionThreadBase
     * @uses DiscussionThreadBase::setCloseDate()
     * @uses DiscussionThreadBase::setCreationDate()
     * @uses DiscussionThreadBase::setId()
     * @uses DiscussionThreadBase::setLastUpdatedDate()
     * @uses DiscussionThreadBase::setMessages()
     * @uses DiscussionThreadBase::setStatus()
     * @uses DiscussionThreadBase::setSubject()
     * @param string $closeDate
     * @param string $creationDate
     * @param int $id
     * @param string $lastUpdatedDate
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfMessage $messages
     * @param string $status
     * @param string $subject
     */
    public function __construct($closeDate = null, $creationDate = null, $id = null, $lastUpdatedDate = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfMessage $messages = null, $status = null, $subject = null)
    {
        $this
            ->setCloseDate($closeDate)
            ->setCreationDate($creationDate)
            ->setId($id)
            ->setLastUpdatedDate($lastUpdatedDate)
            ->setMessages($messages)
            ->setStatus($status)
            ->setSubject($subject);
    }
    /**
     * Get CloseDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCloseDate()
    {
        return isset($this->CloseDate) ? $this->CloseDate : null;
    }
    /**
     * Set CloseDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $closeDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public function setCloseDate($closeDate = null)
    {
        // validation for constraint: string
        if (!is_null($closeDate) && !is_string($closeDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($closeDate, true), gettype($closeDate)), __LINE__);
        }
        if (is_null($closeDate) || (is_array($closeDate) && empty($closeDate))) {
            unset($this->CloseDate);
        } else {
            $this->CloseDate = $closeDate;
        }
        return $this;
    }
    /**
     * Get CreationDate value
     * @return string|null
     */
    public function getCreationDate()
    {
        return $this->CreationDate;
    }
    /**
     * Set CreationDate value
     * @param string $creationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public function setCreationDate($creationDate = null)
    {
        // validation for constraint: string
        if (!is_null($creationDate) && !is_string($creationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($creationDate, true), gettype($creationDate)), __LINE__);
        }
        $this->CreationDate = $creationDate;
        return $this;
    }
    /**
     * Get Id value
     * @return int|null
     */
    public function getId()
    {
        return $this->Id;
    }
    /**
     * Set Id value
     * @param int $id
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->Id = $id;
        return $this;
    }
    /**
     * Get LastUpdatedDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLastUpdatedDate()
    {
        return isset($this->LastUpdatedDate) ? $this->LastUpdatedDate : null;
    }
    /**
     * Set LastUpdatedDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $lastUpdatedDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public function setLastUpdatedDate($lastUpdatedDate = null)
    {
        // validation for constraint: string
        if (!is_null($lastUpdatedDate) && !is_string($lastUpdatedDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastUpdatedDate, true), gettype($lastUpdatedDate)), __LINE__);
        }
        if (is_null($lastUpdatedDate) || (is_array($lastUpdatedDate) && empty($lastUpdatedDate))) {
            unset($this->LastUpdatedDate);
        } else {
            $this->LastUpdatedDate = $lastUpdatedDate;
        }
        return $this;
    }
    /**
     * Get Messages value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfMessage|null
     */
    public function getMessages()
    {
        return isset($this->Messages) ? $this->Messages : null;
    }
    /**
     * Set Messages value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfMessage $messages
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public function setMessages(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfMessage $messages = null)
    {
        if (is_null($messages) || (is_array($messages) && empty($messages))) {
            unset($this->Messages);
        } else {
            $this->Messages = $messages;
        }
        return $this;
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionState::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionState::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $status
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public function setStatus($status = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionState::valueIsValid($status)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\DiscussionState', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionState::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        return $this;
    }
    /**
     * Get Subject value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSubject()
    {
        return isset($this->Subject) ? $this->Subject : null;
    }
    /**
     * Set Subject value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $subject
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public function setSubject($subject = null)
    {
        // validation for constraint: string
        if (!is_null($subject) && !is_string($subject)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subject, true), gettype($subject)), __LINE__);
        }
        if (is_null($subject) || (is_array($subject) && empty($subject))) {
            unset($this->Subject);
        } else {
            $this->Subject = $subject;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionThreadBase
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
