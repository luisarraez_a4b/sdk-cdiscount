<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OfferFilter StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferFilter
 * @subpackage Structs
 */
class OfferFilter extends AbstractStructBase
{
    /**
     * The OfferPoolId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $OfferPoolId;
    /**
     * The SellerProductIdList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $SellerProductIdList;
    /**
     * Constructor method for OfferFilter
     * @uses OfferFilter::setOfferPoolId()
     * @uses OfferFilter::setSellerProductIdList()
     * @param int $offerPoolId
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList
     */
    public function __construct($offerPoolId = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList = null)
    {
        $this
            ->setOfferPoolId($offerPoolId)
            ->setSellerProductIdList($sellerProductIdList);
    }
    /**
     * Get OfferPoolId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getOfferPoolId()
    {
        return isset($this->OfferPoolId) ? $this->OfferPoolId : null;
    }
    /**
     * Set OfferPoolId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $offerPoolId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferFilter
     */
    public function setOfferPoolId($offerPoolId = null)
    {
        // validation for constraint: int
        if (!is_null($offerPoolId) && !(is_int($offerPoolId) || ctype_digit($offerPoolId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($offerPoolId, true), gettype($offerPoolId)), __LINE__);
        }
        if (is_null($offerPoolId) || (is_array($offerPoolId) && empty($offerPoolId))) {
            unset($this->OfferPoolId);
        } else {
            $this->OfferPoolId = $offerPoolId;
        }
        return $this;
    }
    /**
     * Get SellerProductIdList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getSellerProductIdList()
    {
        return isset($this->SellerProductIdList) ? $this->SellerProductIdList : null;
    }
    /**
     * Set SellerProductIdList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferFilter
     */
    public function setSellerProductIdList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList = null)
    {
        if (is_null($sellerProductIdList) || (is_array($sellerProductIdList) && empty($sellerProductIdList))) {
            unset($this->SellerProductIdList);
        } else {
            $this->SellerProductIdList = $sellerProductIdList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
