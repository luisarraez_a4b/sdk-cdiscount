<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OfferListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferListMessage
 * @subpackage Structs
 */
class OfferListMessage extends ServiceBaseAPIMessage
{
    /**
     * The OfferList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer
     */
    public $OfferList;
    /**
     * Constructor method for OfferListMessage
     * @uses OfferListMessage::setOfferList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer $offerList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer $offerList = null)
    {
        $this
            ->setOfferList($offerList);
    }
    /**
     * Get OfferList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer|null
     */
    public function getOfferList()
    {
        return isset($this->OfferList) ? $this->OfferList : null;
    }
    /**
     * Set OfferList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer $offerList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage
     */
    public function setOfferList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer $offerList = null)
    {
        if (is_null($offerList) || (is_array($offerList) && empty($offerList))) {
            unset($this->OfferList);
        } else {
            $this->OfferList = $offerList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
