<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitFulfilmentOnDemandSupplyOrderResponse StructType
 * @subpackage Structs
 */
class SubmitFulfilmentOnDemandSupplyOrderResponse extends AbstractStructBase
{
    /**
     * The SubmitFulfilmentOnDemandSupplyOrderResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage
     */
    public $SubmitFulfilmentOnDemandSupplyOrderResult;
    /**
     * Constructor method for SubmitFulfilmentOnDemandSupplyOrderResponse
     * @uses SubmitFulfilmentOnDemandSupplyOrderResponse::setSubmitFulfilmentOnDemandSupplyOrderResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentOnDemandSupplyOrderResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentOnDemandSupplyOrderResult = null)
    {
        $this
            ->setSubmitFulfilmentOnDemandSupplyOrderResult($submitFulfilmentOnDemandSupplyOrderResult);
    }
    /**
     * Get SubmitFulfilmentOnDemandSupplyOrderResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage|null
     */
    public function getSubmitFulfilmentOnDemandSupplyOrderResult()
    {
        return isset($this->SubmitFulfilmentOnDemandSupplyOrderResult) ? $this->SubmitFulfilmentOnDemandSupplyOrderResult : null;
    }
    /**
     * Set SubmitFulfilmentOnDemandSupplyOrderResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentOnDemandSupplyOrderResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentOnDemandSupplyOrderResponse
     */
    public function setSubmitFulfilmentOnDemandSupplyOrderResult(\A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentOnDemandSupplyOrderResult = null)
    {
        if (is_null($submitFulfilmentOnDemandSupplyOrderResult) || (is_array($submitFulfilmentOnDemandSupplyOrderResult) && empty($submitFulfilmentOnDemandSupplyOrderResult))) {
            unset($this->SubmitFulfilmentOnDemandSupplyOrderResult);
        } else {
            $this->SubmitFulfilmentOnDemandSupplyOrderResult = $submitFulfilmentOnDemandSupplyOrderResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentOnDemandSupplyOrderResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
