<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductModel StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductModel
 * @subpackage Structs
 */
class ProductModel extends AbstractStructBase
{
    /**
     * The CategoryCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CategoryCode;
    /**
     * The Definition
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition
     */
    public $Definition;
    /**
     * The ModelId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $ModelId;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The ProductXmlStructure
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductXmlStructure;
    /**
     * The VariationValueList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription
     */
    public $VariationValueList;
    /**
     * The Version
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Version;
    /**
     * Constructor method for ProductModel
     * @uses ProductModel::setCategoryCode()
     * @uses ProductModel::setDefinition()
     * @uses ProductModel::setModelId()
     * @uses ProductModel::setName()
     * @uses ProductModel::setProductXmlStructure()
     * @uses ProductModel::setVariationValueList()
     * @uses ProductModel::setVersion()
     * @param string $categoryCode
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition $definition
     * @param int $modelId
     * @param string $name
     * @param string $productXmlStructure
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription $variationValueList
     * @param string $version
     */
    public function __construct($categoryCode = null, \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition $definition = null, $modelId = null, $name = null, $productXmlStructure = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription $variationValueList = null, $version = null)
    {
        $this
            ->setCategoryCode($categoryCode)
            ->setDefinition($definition)
            ->setModelId($modelId)
            ->setName($name)
            ->setProductXmlStructure($productXmlStructure)
            ->setVariationValueList($variationValueList)
            ->setVersion($version);
    }
    /**
     * Get CategoryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCategoryCode()
    {
        return isset($this->CategoryCode) ? $this->CategoryCode : null;
    }
    /**
     * Set CategoryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $categoryCode
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public function setCategoryCode($categoryCode = null)
    {
        // validation for constraint: string
        if (!is_null($categoryCode) && !is_string($categoryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($categoryCode, true), gettype($categoryCode)), __LINE__);
        }
        if (is_null($categoryCode) || (is_array($categoryCode) && empty($categoryCode))) {
            unset($this->CategoryCode);
        } else {
            $this->CategoryCode = $categoryCode;
        }
        return $this;
    }
    /**
     * Get Definition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition|null
     */
    public function getDefinition()
    {
        return isset($this->Definition) ? $this->Definition : null;
    }
    /**
     * Set Definition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition $definition
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public function setDefinition(\A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition $definition = null)
    {
        if (is_null($definition) || (is_array($definition) && empty($definition))) {
            unset($this->Definition);
        } else {
            $this->Definition = $definition;
        }
        return $this;
    }
    /**
     * Get ModelId value
     * @return int|null
     */
    public function getModelId()
    {
        return $this->ModelId;
    }
    /**
     * Set ModelId value
     * @param int $modelId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public function setModelId($modelId = null)
    {
        // validation for constraint: int
        if (!is_null($modelId) && !(is_int($modelId) || ctype_digit($modelId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($modelId, true), gettype($modelId)), __LINE__);
        }
        $this->ModelId = $modelId;
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Get ProductXmlStructure value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductXmlStructure()
    {
        return isset($this->ProductXmlStructure) ? $this->ProductXmlStructure : null;
    }
    /**
     * Set ProductXmlStructure value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productXmlStructure
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public function setProductXmlStructure($productXmlStructure = null)
    {
        // validation for constraint: string
        if (!is_null($productXmlStructure) && !is_string($productXmlStructure)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productXmlStructure, true), gettype($productXmlStructure)), __LINE__);
        }
        if (is_null($productXmlStructure) || (is_array($productXmlStructure) && empty($productXmlStructure))) {
            unset($this->ProductXmlStructure);
        } else {
            $this->ProductXmlStructure = $productXmlStructure;
        }
        return $this;
    }
    /**
     * Get VariationValueList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription|null
     */
    public function getVariationValueList()
    {
        return isset($this->VariationValueList) ? $this->VariationValueList : null;
    }
    /**
     * Set VariationValueList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription $variationValueList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public function setVariationValueList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription $variationValueList = null)
    {
        if (is_null($variationValueList) || (is_array($variationValueList) && empty($variationValueList))) {
            unset($this->VariationValueList);
        } else {
            $this->VariationValueList = $variationValueList;
        }
        return $this;
    }
    /**
     * Get Version value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVersion()
    {
        return isset($this->Version) ? $this->Version : null;
    }
    /**
     * Set Version value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $version
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public function setVersion($version = null)
    {
        // validation for constraint: string
        if (!is_null($version) && !is_string($version)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        if (is_null($version) || (is_array($version) && empty($version))) {
            unset($this->Version);
        } else {
            $this->Version = $version;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModel
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
