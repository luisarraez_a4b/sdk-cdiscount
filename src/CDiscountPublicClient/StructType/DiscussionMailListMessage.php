<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DiscussionMailListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscussionMailListMessage
 * @subpackage Structs
 */
class DiscussionMailListMessage extends ServiceBaseAPIMessage
{
    /**
     * The DiscussionMailList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionMail
     */
    public $DiscussionMailList;
    /**
     * Constructor method for DiscussionMailListMessage
     * @uses DiscussionMailListMessage::setDiscussionMailList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionMail $discussionMailList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionMail $discussionMailList = null)
    {
        $this
            ->setDiscussionMailList($discussionMailList);
    }
    /**
     * Get DiscussionMailList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionMail|null
     */
    public function getDiscussionMailList()
    {
        return isset($this->DiscussionMailList) ? $this->DiscussionMailList : null;
    }
    /**
     * Set DiscussionMailList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionMail $discussionMailList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage
     */
    public function setDiscussionMailList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionMail $discussionMailList = null)
    {
        if (is_null($discussionMailList) || (is_array($discussionMailList) && empty($discussionMailList))) {
            unset($this->DiscussionMailList);
        } else {
            $this->DiscussionMailList = $discussionMailList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
