<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for VariationDescription StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:VariationDescription
 * @subpackage Structs
 */
class VariationDescription extends AbstractStructBase
{
    /**
     * The VariantValueDescription
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $VariantValueDescription;
    /**
     * The VariantValueId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $VariantValueId;
    /**
     * Constructor method for VariationDescription
     * @uses VariationDescription::setVariantValueDescription()
     * @uses VariationDescription::setVariantValueId()
     * @param string $variantValueDescription
     * @param int $variantValueId
     */
    public function __construct($variantValueDescription = null, $variantValueId = null)
    {
        $this
            ->setVariantValueDescription($variantValueDescription)
            ->setVariantValueId($variantValueId);
    }
    /**
     * Get VariantValueDescription value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getVariantValueDescription()
    {
        return isset($this->VariantValueDescription) ? $this->VariantValueDescription : null;
    }
    /**
     * Set VariantValueDescription value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $variantValueDescription
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription
     */
    public function setVariantValueDescription($variantValueDescription = null)
    {
        // validation for constraint: string
        if (!is_null($variantValueDescription) && !is_string($variantValueDescription)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($variantValueDescription, true), gettype($variantValueDescription)), __LINE__);
        }
        if (is_null($variantValueDescription) || (is_array($variantValueDescription) && empty($variantValueDescription))) {
            unset($this->VariantValueDescription);
        } else {
            $this->VariantValueDescription = $variantValueDescription;
        }
        return $this;
    }
    /**
     * Get VariantValueId value
     * @return int|null
     */
    public function getVariantValueId()
    {
        return $this->VariantValueId;
    }
    /**
     * Set VariantValueId value
     * @param int $variantValueId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription
     */
    public function setVariantValueId($variantValueId = null)
    {
        // validation for constraint: int
        if (!is_null($variantValueId) && !(is_int($variantValueId) || ctype_digit($variantValueId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($variantValueId, true), gettype($variantValueId)), __LINE__);
        }
        $this->VariantValueId = $variantValueId;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
