<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ServiceOrderListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ServiceOrderListMessage
 * @subpackage Structs
 */
class ServiceOrderListMessage extends ServiceBaseAPIMessage
{
    /**
     * The ServiceOrderList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder
     */
    public $ServiceOrderList;
    /**
     * Constructor method for ServiceOrderListMessage
     * @uses ServiceOrderListMessage::setServiceOrderList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder $serviceOrderList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder $serviceOrderList = null)
    {
        $this
            ->setServiceOrderList($serviceOrderList);
    }
    /**
     * Get ServiceOrderList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder|null
     */
    public function getServiceOrderList()
    {
        return isset($this->ServiceOrderList) ? $this->ServiceOrderList : null;
    }
    /**
     * Set ServiceOrderList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder $serviceOrderList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage
     */
    public function setServiceOrderList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder $serviceOrderList = null)
    {
        if (is_null($serviceOrderList) || (is_array($serviceOrderList) && empty($serviceOrderList))) {
            unset($this->ServiceOrderList);
        } else {
            $this->ServiceOrderList = $serviceOrderList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
