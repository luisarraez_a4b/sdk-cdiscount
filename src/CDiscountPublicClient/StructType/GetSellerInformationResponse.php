<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetSellerInformationResponse StructType
 * @subpackage Structs
 */
class GetSellerInformationResponse extends AbstractStructBase
{
    /**
     * The GetSellerInformationResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage
     */
    public $GetSellerInformationResult;
    /**
     * Constructor method for GetSellerInformationResponse
     * @uses GetSellerInformationResponse::setGetSellerInformationResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage $getSellerInformationResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage $getSellerInformationResult = null)
    {
        $this
            ->setGetSellerInformationResult($getSellerInformationResult);
    }
    /**
     * Get GetSellerInformationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage|null
     */
    public function getGetSellerInformationResult()
    {
        return isset($this->GetSellerInformationResult) ? $this->GetSellerInformationResult : null;
    }
    /**
     * Set GetSellerInformationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage $getSellerInformationResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerInformationResponse
     */
    public function setGetSellerInformationResult(\A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage $getSellerInformationResult = null)
    {
        if (is_null($getSellerInformationResult) || (is_array($getSellerInformationResult) && empty($getSellerInformationResult))) {
            unset($this->GetSellerInformationResult);
        } else {
            $this->GetSellerInformationResult = $getSellerInformationResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerInformationResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
