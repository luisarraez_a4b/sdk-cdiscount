<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for HeaderMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:HeaderMessage
 * @subpackage Structs
 */
class HeaderMessage extends AbstractStructBase
{
    /**
     * The Context
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ContextMessage
     */
    public $Context;
    /**
     * The Localization
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage
     */
    public $Localization;
    /**
     * The Security
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public $Security;
    /**
     * The Version
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Version;
    /**
     * Constructor method for HeaderMessage
     * @uses HeaderMessage::setContext()
     * @uses HeaderMessage::setLocalization()
     * @uses HeaderMessage::setSecurity()
     * @uses HeaderMessage::setVersion()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ContextMessage $context
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage $localization
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext $security
     * @param string $version
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ContextMessage $context = null, \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage $localization = null, \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext $security = null, $version = null)
    {
        $this
            ->setContext($context)
            ->setLocalization($localization)
            ->setSecurity($security)
            ->setVersion($version);
    }
    /**
     * Get Context value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ContextMessage|null
     */
    public function getContext()
    {
        return $this->Context;
    }
    /**
     * Set Context value
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ContextMessage $context
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage
     */
    public function setContext(\A4BGroup\Client\CDiscountPublicClient\StructType\ContextMessage $context = null)
    {
        $this->Context = $context;
        return $this;
    }
    /**
     * Get Localization value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage|null
     */
    public function getLocalization()
    {
        return $this->Localization;
    }
    /**
     * Set Localization value
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage $localization
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage
     */
    public function setLocalization(\A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage $localization = null)
    {
        $this->Localization = $localization;
        return $this;
    }
    /**
     * Get Security value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext|null
     */
    public function getSecurity()
    {
        return isset($this->Security) ? $this->Security : null;
    }
    /**
     * Set Security value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext $security
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage
     */
    public function setSecurity(\A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext $security = null)
    {
        if (is_null($security) || (is_array($security) && empty($security))) {
            unset($this->Security);
        } else {
            $this->Security = $security;
        }
        return $this;
    }
    /**
     * Get Version value
     * @return string|null
     */
    public function getVersion()
    {
        return $this->Version;
    }
    /**
     * Set Version value
     * @param string $version
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage
     */
    public function setVersion($version = null)
    {
        // validation for constraint: string
        if (!is_null($version) && !is_string($version)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        $this->Version = $version;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
