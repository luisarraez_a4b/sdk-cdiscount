<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for LocalizationMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:LocalizationMessage
 * @subpackage Structs
 */
class LocalizationMessage extends AbstractStructBase
{
    /**
     * The Country
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Country;
    /**
     * The CultureName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CultureName;
    /**
     * The Currency
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Currency;
    /**
     * The DecimalPosition
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $DecimalPosition;
    /**
     * The Language
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Language;
    /**
     * Constructor method for LocalizationMessage
     * @uses LocalizationMessage::setCountry()
     * @uses LocalizationMessage::setCultureName()
     * @uses LocalizationMessage::setCurrency()
     * @uses LocalizationMessage::setDecimalPosition()
     * @uses LocalizationMessage::setLanguage()
     * @param string $country
     * @param string $cultureName
     * @param string $currency
     * @param int $decimalPosition
     * @param string $language
     */
    public function __construct($country = null, $cultureName = null, $currency = null, $decimalPosition = null, $language = null)
    {
        $this
            ->setCountry($country)
            ->setCultureName($cultureName)
            ->setCurrency($currency)
            ->setDecimalPosition($decimalPosition)
            ->setLanguage($language);
    }
    /**
     * Get Country value
     * @return string|null
     */
    public function getCountry()
    {
        return $this->Country;
    }
    /**
     * Set Country value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Country::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Country::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $country
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage
     */
    public function setCountry($country = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\Country::valueIsValid($country)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\Country', is_array($country) ? implode(', ', $country) : var_export($country, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\Country::getValidValues())), __LINE__);
        }
        $this->Country = $country;
        return $this;
    }
    /**
     * Get CultureName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCultureName()
    {
        return isset($this->CultureName) ? $this->CultureName : null;
    }
    /**
     * Set CultureName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $cultureName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage
     */
    public function setCultureName($cultureName = null)
    {
        // validation for constraint: string
        if (!is_null($cultureName) && !is_string($cultureName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cultureName, true), gettype($cultureName)), __LINE__);
        }
        if (is_null($cultureName) || (is_array($cultureName) && empty($cultureName))) {
            unset($this->CultureName);
        } else {
            $this->CultureName = $cultureName;
        }
        return $this;
    }
    /**
     * Get Currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->Currency;
    }
    /**
     * Set Currency value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Currency::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Currency::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $currency
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\Currency::valueIsValid($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\Currency', is_array($currency) ? implode(', ', $currency) : var_export($currency, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\Currency::getValidValues())), __LINE__);
        }
        $this->Currency = $currency;
        return $this;
    }
    /**
     * Get DecimalPosition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getDecimalPosition()
    {
        return isset($this->DecimalPosition) ? $this->DecimalPosition : null;
    }
    /**
     * Set DecimalPosition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $decimalPosition
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage
     */
    public function setDecimalPosition($decimalPosition = null)
    {
        // validation for constraint: int
        if (!is_null($decimalPosition) && !(is_int($decimalPosition) || ctype_digit($decimalPosition))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($decimalPosition, true), gettype($decimalPosition)), __LINE__);
        }
        if (is_null($decimalPosition) || (is_array($decimalPosition) && empty($decimalPosition))) {
            unset($this->DecimalPosition);
        } else {
            $this->DecimalPosition = $decimalPosition;
        }
        return $this;
    }
    /**
     * Get Language value
     * @return string|null
     */
    public function getLanguage()
    {
        return $this->Language;
    }
    /**
     * Set Language value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Language::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Language::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $language
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage
     */
    public function setLanguage($language = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\Language::valueIsValid($language)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\Language', is_array($language) ? implode(', ', $language) : var_export($language, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\Language::getValidValues())), __LINE__);
        }
        $this->Language = $language;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\LocalizationMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
