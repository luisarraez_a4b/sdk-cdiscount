<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderClaimListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderClaimListMessage
 * @subpackage Structs
 */
class OrderClaimListMessage extends ServiceBaseAPIMessage
{
    /**
     * The OrderClaimList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderClaim
     */
    public $OrderClaimList;
    /**
     * Constructor method for OrderClaimListMessage
     * @uses OrderClaimListMessage::setOrderClaimList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderClaim $orderClaimList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderClaim $orderClaimList = null)
    {
        $this
            ->setOrderClaimList($orderClaimList);
    }
    /**
     * Get OrderClaimList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderClaim|null
     */
    public function getOrderClaimList()
    {
        return isset($this->OrderClaimList) ? $this->OrderClaimList : null;
    }
    /**
     * Set OrderClaimList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderClaim $orderClaimList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage
     */
    public function setOrderClaimList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderClaim $orderClaimList = null)
    {
        if (is_null($orderClaimList) || (is_array($orderClaimList) && empty($orderClaimList))) {
            unset($this->OrderClaimList);
        } else {
            $this->OrderClaimList = $orderClaimList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
