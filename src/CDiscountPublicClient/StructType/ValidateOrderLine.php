<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidateOrderLine StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ValidateOrderLine
 * @subpackage Structs
 */
class ValidateOrderLine extends AbstractStructBase
{
    /**
     * The AcceptationState
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $AcceptationState;
    /**
     * The ProductCondition
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ProductCondition;
    /**
     * The SellerProductId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SellerProductId;
    /**
     * The TypeOfReturn
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $TypeOfReturn;
    /**
     * Constructor method for ValidateOrderLine
     * @uses ValidateOrderLine::setAcceptationState()
     * @uses ValidateOrderLine::setProductCondition()
     * @uses ValidateOrderLine::setSellerProductId()
     * @uses ValidateOrderLine::setTypeOfReturn()
     * @param string $acceptationState
     * @param string $productCondition
     * @param string $sellerProductId
     * @param string $typeOfReturn
     */
    public function __construct($acceptationState = null, $productCondition = null, $sellerProductId = null, $typeOfReturn = null)
    {
        $this
            ->setAcceptationState($acceptationState)
            ->setProductCondition($productCondition)
            ->setSellerProductId($sellerProductId)
            ->setTypeOfReturn($typeOfReturn);
    }
    /**
     * Get AcceptationState value
     * @return string|null
     */
    public function getAcceptationState()
    {
        return $this->AcceptationState;
    }
    /**
     * Set AcceptationState value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $acceptationState
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderLine
     */
    public function setAcceptationState($acceptationState = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::valueIsValid($acceptationState)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\AcceptationStateEnum', is_array($acceptationState) ? implode(', ', $acceptationState) : var_export($acceptationState, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::getValidValues())), __LINE__);
        }
        $this->AcceptationState = $acceptationState;
        return $this;
    }
    /**
     * Get ProductCondition value
     * @return string|null
     */
    public function getProductCondition()
    {
        return $this->ProductCondition;
    }
    /**
     * Set ProductCondition value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $productCondition
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderLine
     */
    public function setProductCondition($productCondition = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::valueIsValid($productCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ProductConditionEnum', is_array($productCondition) ? implode(', ', $productCondition) : var_export($productCondition, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::getValidValues())), __LINE__);
        }
        $this->ProductCondition = $productCondition;
        return $this;
    }
    /**
     * Get SellerProductId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSellerProductId()
    {
        return isset($this->SellerProductId) ? $this->SellerProductId : null;
    }
    /**
     * Set SellerProductId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sellerProductId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderLine
     */
    public function setSellerProductId($sellerProductId = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductId) && !is_string($sellerProductId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductId, true), gettype($sellerProductId)), __LINE__);
        }
        if (is_null($sellerProductId) || (is_array($sellerProductId) && empty($sellerProductId))) {
            unset($this->SellerProductId);
        } else {
            $this->SellerProductId = $sellerProductId;
        }
        return $this;
    }
    /**
     * Get TypeOfReturn value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTypeOfReturn()
    {
        return isset($this->TypeOfReturn) ? $this->TypeOfReturn : null;
    }
    /**
     * Set TypeOfReturn value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\AskingForReturnType::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\AskingForReturnType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $typeOfReturn
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderLine
     */
    public function setTypeOfReturn($typeOfReturn = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\AskingForReturnType::valueIsValid($typeOfReturn)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\AskingForReturnType', is_array($typeOfReturn) ? implode(', ', $typeOfReturn) : var_export($typeOfReturn, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\AskingForReturnType::getValidValues())), __LINE__);
        }
        if (is_null($typeOfReturn) || (is_array($typeOfReturn) && empty($typeOfReturn))) {
            unset($this->TypeOfReturn);
        } else {
            $this->TypeOfReturn = $typeOfReturn;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderLine
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
