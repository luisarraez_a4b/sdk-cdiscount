<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetGlobalConfigurationResponse StructType
 * @subpackage Structs
 */
class GetGlobalConfigurationResponse extends AbstractStructBase
{
    /**
     * The GetGlobalConfigurationResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage
     */
    public $GetGlobalConfigurationResult;
    /**
     * Constructor method for GetGlobalConfigurationResponse
     * @uses GetGlobalConfigurationResponse::setGetGlobalConfigurationResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage $getGlobalConfigurationResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage $getGlobalConfigurationResult = null)
    {
        $this
            ->setGetGlobalConfigurationResult($getGlobalConfigurationResult);
    }
    /**
     * Get GetGlobalConfigurationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage|null
     */
    public function getGetGlobalConfigurationResult()
    {
        return isset($this->GetGlobalConfigurationResult) ? $this->GetGlobalConfigurationResult : null;
    }
    /**
     * Set GetGlobalConfigurationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage $getGlobalConfigurationResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetGlobalConfigurationResponse
     */
    public function setGetGlobalConfigurationResult(\A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage $getGlobalConfigurationResult = null)
    {
        if (is_null($getGlobalConfigurationResult) || (is_array($getGlobalConfigurationResult) && empty($getGlobalConfigurationResult))) {
            unset($this->GetGlobalConfigurationResult);
        } else {
            $this->GetGlobalConfigurationResult = $getGlobalConfigurationResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetGlobalConfigurationResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
