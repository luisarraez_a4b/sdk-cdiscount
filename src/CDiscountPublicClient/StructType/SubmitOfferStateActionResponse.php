<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitOfferStateActionResponse StructType
 * @subpackage Structs
 */
class SubmitOfferStateActionResponse extends AbstractStructBase
{
    /**
     * The SubmitOfferStateActionResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateReportMessage
     */
    public $SubmitOfferStateActionResult;
    /**
     * Constructor method for SubmitOfferStateActionResponse
     * @uses SubmitOfferStateActionResponse::setSubmitOfferStateActionResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateReportMessage $submitOfferStateActionResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateReportMessage $submitOfferStateActionResult = null)
    {
        $this
            ->setSubmitOfferStateActionResult($submitOfferStateActionResult);
    }
    /**
     * Get SubmitOfferStateActionResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateReportMessage|null
     */
    public function getSubmitOfferStateActionResult()
    {
        return isset($this->SubmitOfferStateActionResult) ? $this->SubmitOfferStateActionResult : null;
    }
    /**
     * Set SubmitOfferStateActionResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateReportMessage $submitOfferStateActionResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferStateActionResponse
     */
    public function setSubmitOfferStateActionResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateReportMessage $submitOfferStateActionResult = null)
    {
        if (is_null($submitOfferStateActionResult) || (is_array($submitOfferStateActionResult) && empty($submitOfferStateActionResult))) {
            unset($this->SubmitOfferStateActionResult);
        } else {
            $this->SubmitOfferStateActionResult = $submitOfferStateActionResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferStateActionResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
