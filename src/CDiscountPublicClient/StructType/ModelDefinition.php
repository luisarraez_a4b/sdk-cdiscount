<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ModelDefinition StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ModelDefinition
 * @subpackage Structs
 */
class ModelDefinition extends AbstractStructBase
{
    /**
     * The ListProperties
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     */
    public $ListProperties;
    /**
     * The MandatoryModelProperties
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $MandatoryModelProperties;
    /**
     * The MultipleFreeTextProperties
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     */
    public $MultipleFreeTextProperties;
    /**
     * The SingleFreeTextProperties
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1
     */
    public $SingleFreeTextProperties;
    /**
     * Constructor method for ModelDefinition
     * @uses ModelDefinition::setListProperties()
     * @uses ModelDefinition::setMandatoryModelProperties()
     * @uses ModelDefinition::setMultipleFreeTextProperties()
     * @uses ModelDefinition::setSingleFreeTextProperties()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $listProperties
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $mandatoryModelProperties
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $multipleFreeTextProperties
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $singleFreeTextProperties
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $listProperties = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $mandatoryModelProperties = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $multipleFreeTextProperties = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $singleFreeTextProperties = null)
    {
        $this
            ->setListProperties($listProperties)
            ->setMandatoryModelProperties($mandatoryModelProperties)
            ->setMultipleFreeTextProperties($multipleFreeTextProperties)
            ->setSingleFreeTextProperties($singleFreeTextProperties);
    }
    /**
     * Get ListProperties value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function getListProperties()
    {
        return isset($this->ListProperties) ? $this->ListProperties : null;
    }
    /**
     * Set ListProperties value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $listProperties
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition
     */
    public function setListProperties(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $listProperties = null)
    {
        if (is_null($listProperties) || (is_array($listProperties) && empty($listProperties))) {
            unset($this->ListProperties);
        } else {
            $this->ListProperties = $listProperties;
        }
        return $this;
    }
    /**
     * Get MandatoryModelProperties value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getMandatoryModelProperties()
    {
        return isset($this->MandatoryModelProperties) ? $this->MandatoryModelProperties : null;
    }
    /**
     * Set MandatoryModelProperties value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $mandatoryModelProperties
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition
     */
    public function setMandatoryModelProperties(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $mandatoryModelProperties = null)
    {
        if (is_null($mandatoryModelProperties) || (is_array($mandatoryModelProperties) && empty($mandatoryModelProperties))) {
            unset($this->MandatoryModelProperties);
        } else {
            $this->MandatoryModelProperties = $mandatoryModelProperties;
        }
        return $this;
    }
    /**
     * Get MultipleFreeTextProperties value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function getMultipleFreeTextProperties()
    {
        return isset($this->MultipleFreeTextProperties) ? $this->MultipleFreeTextProperties : null;
    }
    /**
     * Set MultipleFreeTextProperties value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $multipleFreeTextProperties
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition
     */
    public function setMultipleFreeTextProperties(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $multipleFreeTextProperties = null)
    {
        if (is_null($multipleFreeTextProperties) || (is_array($multipleFreeTextProperties) && empty($multipleFreeTextProperties))) {
            unset($this->MultipleFreeTextProperties);
        } else {
            $this->MultipleFreeTextProperties = $multipleFreeTextProperties;
        }
        return $this;
    }
    /**
     * Get SingleFreeTextProperties value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1|null
     */
    public function getSingleFreeTextProperties()
    {
        return isset($this->SingleFreeTextProperties) ? $this->SingleFreeTextProperties : null;
    }
    /**
     * Set SingleFreeTextProperties value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $singleFreeTextProperties
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition
     */
    public function setSingleFreeTextProperties(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 $singleFreeTextProperties = null)
    {
        if (is_null($singleFreeTextProperties) || (is_array($singleFreeTextProperties) && empty($singleFreeTextProperties))) {
            unset($this->SingleFreeTextProperties);
        } else {
            $this->SingleFreeTextProperties = $singleFreeTextProperties;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ModelDefinition
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
