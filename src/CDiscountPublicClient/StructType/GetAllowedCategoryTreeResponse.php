<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAllowedCategoryTreeResponse StructType
 * @subpackage Structs
 */
class GetAllowedCategoryTreeResponse extends AbstractStructBase
{
    /**
     * The GetAllowedCategoryTreeResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage
     */
    public $GetAllowedCategoryTreeResult;
    /**
     * Constructor method for GetAllowedCategoryTreeResponse
     * @uses GetAllowedCategoryTreeResponse::setGetAllowedCategoryTreeResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllowedCategoryTreeResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllowedCategoryTreeResult = null)
    {
        $this
            ->setGetAllowedCategoryTreeResult($getAllowedCategoryTreeResult);
    }
    /**
     * Get GetAllowedCategoryTreeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage|null
     */
    public function getGetAllowedCategoryTreeResult()
    {
        return isset($this->GetAllowedCategoryTreeResult) ? $this->GetAllowedCategoryTreeResult : null;
    }
    /**
     * Set GetAllowedCategoryTreeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllowedCategoryTreeResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllowedCategoryTreeResponse
     */
    public function setGetAllowedCategoryTreeResult(\A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllowedCategoryTreeResult = null)
    {
        if (is_null($getAllowedCategoryTreeResult) || (is_array($getAllowedCategoryTreeResult) && empty($getAllowedCategoryTreeResult))) {
            unset($this->GetAllowedCategoryTreeResult);
        } else {
            $this->GetAllowedCategoryTreeResult = $getAllowedCategoryTreeResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllowedCategoryTreeResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
