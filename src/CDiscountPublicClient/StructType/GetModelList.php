<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetModelList StructType
 * @subpackage Structs
 */
class GetModelList extends AbstractStructBase
{
    /**
     * The headerMessage
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage
     */
    public $headerMessage;
    /**
     * The modelFilter
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ModelFilter
     */
    public $modelFilter;
    /**
     * Constructor method for GetModelList
     * @uses GetModelList::setHeaderMessage()
     * @uses GetModelList::setModelFilter()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ModelFilter $modelFilter
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage = null, \A4BGroup\Client\CDiscountPublicClient\StructType\ModelFilter $modelFilter = null)
    {
        $this
            ->setHeaderMessage($headerMessage)
            ->setModelFilter($modelFilter);
    }
    /**
     * Get headerMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage|null
     */
    public function getHeaderMessage()
    {
        return isset($this->headerMessage) ? $this->headerMessage : null;
    }
    /**
     * Set headerMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetModelList
     */
    public function setHeaderMessage(\A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage = null)
    {
        if (is_null($headerMessage) || (is_array($headerMessage) && empty($headerMessage))) {
            unset($this->headerMessage);
        } else {
            $this->headerMessage = $headerMessage;
        }
        return $this;
    }
    /**
     * Get modelFilter value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ModelFilter|null
     */
    public function getModelFilter()
    {
        return isset($this->modelFilter) ? $this->modelFilter : null;
    }
    /**
     * Set modelFilter value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ModelFilter $modelFilter
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetModelList
     */
    public function setModelFilter(\A4BGroup\Client\CDiscountPublicClient\StructType\ModelFilter $modelFilter = null)
    {
        if (is_null($modelFilter) || (is_array($modelFilter) && empty($modelFilter))) {
            unset($this->modelFilter);
        } else {
            $this->modelFilter = $modelFilter;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetModelList
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
