<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderClaimFilter StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderClaimFilter
 * @subpackage Structs
 */
class OrderClaimFilter extends OrderQuestionFilter
{
    /**
     * The OnlyWithMessageFromCdsCustomerService
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $OnlyWithMessageFromCdsCustomerService;
    /**
     * Constructor method for OrderClaimFilter
     * @uses OrderClaimFilter::setOnlyWithMessageFromCdsCustomerService()
     * @param bool $onlyWithMessageFromCdsCustomerService
     */
    public function __construct($onlyWithMessageFromCdsCustomerService = null)
    {
        $this
            ->setOnlyWithMessageFromCdsCustomerService($onlyWithMessageFromCdsCustomerService);
    }
    /**
     * Get OnlyWithMessageFromCdsCustomerService value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getOnlyWithMessageFromCdsCustomerService()
    {
        return isset($this->OnlyWithMessageFromCdsCustomerService) ? $this->OnlyWithMessageFromCdsCustomerService : null;
    }
    /**
     * Set OnlyWithMessageFromCdsCustomerService value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $onlyWithMessageFromCdsCustomerService
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimFilter
     */
    public function setOnlyWithMessageFromCdsCustomerService($onlyWithMessageFromCdsCustomerService = null)
    {
        // validation for constraint: boolean
        if (!is_null($onlyWithMessageFromCdsCustomerService) && !is_bool($onlyWithMessageFromCdsCustomerService)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($onlyWithMessageFromCdsCustomerService, true), gettype($onlyWithMessageFromCdsCustomerService)), __LINE__);
        }
        if (is_null($onlyWithMessageFromCdsCustomerService) || (is_array($onlyWithMessageFromCdsCustomerService) && empty($onlyWithMessageFromCdsCustomerService))) {
            unset($this->OnlyWithMessageFromCdsCustomerService);
        } else {
            $this->OnlyWithMessageFromCdsCustomerService = $onlyWithMessageFromCdsCustomerService;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
