<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductPackageSubmissionResultResponse StructType
 * @subpackage Structs
 */
class GetProductPackageSubmissionResultResponse extends AbstractStructBase
{
    /**
     * The GetProductPackageSubmissionResultResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage
     */
    public $GetProductPackageSubmissionResultResult;
    /**
     * Constructor method for GetProductPackageSubmissionResultResponse
     * @uses GetProductPackageSubmissionResultResponse::setGetProductPackageSubmissionResultResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $getProductPackageSubmissionResultResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $getProductPackageSubmissionResultResult = null)
    {
        $this
            ->setGetProductPackageSubmissionResultResult($getProductPackageSubmissionResultResult);
    }
    /**
     * Get GetProductPackageSubmissionResultResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage|null
     */
    public function getGetProductPackageSubmissionResultResult()
    {
        return isset($this->GetProductPackageSubmissionResultResult) ? $this->GetProductPackageSubmissionResultResult : null;
    }
    /**
     * Set GetProductPackageSubmissionResultResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $getProductPackageSubmissionResultResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageSubmissionResultResponse
     */
    public function setGetProductPackageSubmissionResultResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $getProductPackageSubmissionResultResult = null)
    {
        if (is_null($getProductPackageSubmissionResultResult) || (is_array($getProductPackageSubmissionResultResult) && empty($getProductPackageSubmissionResultResult))) {
            unset($this->GetProductPackageSubmissionResultResult);
        } else {
            $this->GetProductPackageSubmissionResultResult = $getProductPackageSubmissionResultResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageSubmissionResultResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
