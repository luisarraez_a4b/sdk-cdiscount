<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetServiceOrderListResponse StructType
 * @subpackage Structs
 */
class GetServiceOrderListResponse extends AbstractStructBase
{
    /**
     * The GetServiceOrderListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage
     */
    public $GetServiceOrderListResult;
    /**
     * Constructor method for GetServiceOrderListResponse
     * @uses GetServiceOrderListResponse::setGetServiceOrderListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage $getServiceOrderListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage $getServiceOrderListResult = null)
    {
        $this
            ->setGetServiceOrderListResult($getServiceOrderListResult);
    }
    /**
     * Get GetServiceOrderListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage|null
     */
    public function getGetServiceOrderListResult()
    {
        return isset($this->GetServiceOrderListResult) ? $this->GetServiceOrderListResult : null;
    }
    /**
     * Set GetServiceOrderListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage $getServiceOrderListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetServiceOrderListResponse
     */
    public function setGetServiceOrderListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrderListMessage $getServiceOrderListResult = null)
    {
        if (is_null($getServiceOrderListResult) || (is_array($getServiceOrderListResult) && empty($getServiceOrderListResult))) {
            unset($this->GetServiceOrderListResult);
        } else {
            $this->GetServiceOrderListResult = $getServiceOrderListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetServiceOrderListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
