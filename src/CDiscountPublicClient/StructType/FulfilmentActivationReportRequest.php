<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentActivationReportRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentActivationReportRequest
 * @subpackage Structs
 */
class FulfilmentActivationReportRequest extends AbstractStructBase
{
    /**
     * The BeginDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BeginDate;
    /**
     * The DepositIdList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfint
     */
    public $DepositIdList;
    /**
     * The EndDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EndDate;
    /**
     * Constructor method for FulfilmentActivationReportRequest
     * @uses FulfilmentActivationReportRequest::setBeginDate()
     * @uses FulfilmentActivationReportRequest::setDepositIdList()
     * @uses FulfilmentActivationReportRequest::setEndDate()
     * @param string $beginDate
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfint $depositIdList
     * @param string $endDate
     */
    public function __construct($beginDate = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfint $depositIdList = null, $endDate = null)
    {
        $this
            ->setBeginDate($beginDate)
            ->setDepositIdList($depositIdList)
            ->setEndDate($endDate);
    }
    /**
     * Get BeginDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBeginDate()
    {
        return isset($this->BeginDate) ? $this->BeginDate : null;
    }
    /**
     * Set BeginDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $beginDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReportRequest
     */
    public function setBeginDate($beginDate = null)
    {
        // validation for constraint: string
        if (!is_null($beginDate) && !is_string($beginDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($beginDate, true), gettype($beginDate)), __LINE__);
        }
        if (is_null($beginDate) || (is_array($beginDate) && empty($beginDate))) {
            unset($this->BeginDate);
        } else {
            $this->BeginDate = $beginDate;
        }
        return $this;
    }
    /**
     * Get DepositIdList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfint|null
     */
    public function getDepositIdList()
    {
        return isset($this->DepositIdList) ? $this->DepositIdList : null;
    }
    /**
     * Set DepositIdList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfint $depositIdList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReportRequest
     */
    public function setDepositIdList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfint $depositIdList = null)
    {
        if (is_null($depositIdList) || (is_array($depositIdList) && empty($depositIdList))) {
            unset($this->DepositIdList);
        } else {
            $this->DepositIdList = $depositIdList;
        }
        return $this;
    }
    /**
     * Get EndDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndDate()
    {
        return isset($this->EndDate) ? $this->EndDate : null;
    }
    /**
     * Set EndDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReportRequest
     */
    public function setEndDate($endDate = null)
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        if (is_null($endDate) || (is_array($endDate) && empty($endDate))) {
            unset($this->EndDate);
        } else {
            $this->EndDate = $endDate;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReportRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
