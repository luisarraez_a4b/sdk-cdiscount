<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OfferQuestionFilter StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferQuestionFilter
 * @subpackage Structs
 */
class OfferQuestionFilter extends DiscussionFilterBase
{
    /**
     * The ProductEANList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $ProductEANList;
    /**
     * The ProductSellerReferenceList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $ProductSellerReferenceList;
    /**
     * Constructor method for OfferQuestionFilter
     * @uses OfferQuestionFilter::setProductEANList()
     * @uses OfferQuestionFilter::setProductSellerReferenceList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productEANList
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productSellerReferenceList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productEANList = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productSellerReferenceList = null)
    {
        $this
            ->setProductEANList($productEANList)
            ->setProductSellerReferenceList($productSellerReferenceList);
    }
    /**
     * Get ProductEANList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getProductEANList()
    {
        return isset($this->ProductEANList) ? $this->ProductEANList : null;
    }
    /**
     * Set ProductEANList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productEANList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionFilter
     */
    public function setProductEANList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productEANList = null)
    {
        if (is_null($productEANList) || (is_array($productEANList) && empty($productEANList))) {
            unset($this->ProductEANList);
        } else {
            $this->ProductEANList = $productEANList;
        }
        return $this;
    }
    /**
     * Get ProductSellerReferenceList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getProductSellerReferenceList()
    {
        return isset($this->ProductSellerReferenceList) ? $this->ProductSellerReferenceList : null;
    }
    /**
     * Set ProductSellerReferenceList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productSellerReferenceList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionFilter
     */
    public function setProductSellerReferenceList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $productSellerReferenceList = null)
    {
        if (is_null($productSellerReferenceList) || (is_array($productSellerReferenceList) && empty($productSellerReferenceList))) {
            unset($this->ProductSellerReferenceList);
        } else {
            $this->ProductSellerReferenceList = $productSellerReferenceList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
