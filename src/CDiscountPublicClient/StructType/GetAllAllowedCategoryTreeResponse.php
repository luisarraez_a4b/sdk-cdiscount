<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetAllAllowedCategoryTreeResponse StructType
 * @subpackage Structs
 */
class GetAllAllowedCategoryTreeResponse extends AbstractStructBase
{
    /**
     * The GetAllAllowedCategoryTreeResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage
     */
    public $GetAllAllowedCategoryTreeResult;
    /**
     * Constructor method for GetAllAllowedCategoryTreeResponse
     * @uses GetAllAllowedCategoryTreeResponse::setGetAllAllowedCategoryTreeResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllAllowedCategoryTreeResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllAllowedCategoryTreeResult = null)
    {
        $this
            ->setGetAllAllowedCategoryTreeResult($getAllAllowedCategoryTreeResult);
    }
    /**
     * Get GetAllAllowedCategoryTreeResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage|null
     */
    public function getGetAllAllowedCategoryTreeResult()
    {
        return isset($this->GetAllAllowedCategoryTreeResult) ? $this->GetAllAllowedCategoryTreeResult : null;
    }
    /**
     * Set GetAllAllowedCategoryTreeResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllAllowedCategoryTreeResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllAllowedCategoryTreeResponse
     */
    public function setGetAllAllowedCategoryTreeResult(\A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTreeMessage $getAllAllowedCategoryTreeResult = null)
    {
        if (is_null($getAllAllowedCategoryTreeResult) || (is_array($getAllAllowedCategoryTreeResult) && empty($getAllAllowedCategoryTreeResult))) {
            unset($this->GetAllAllowedCategoryTreeResult);
        } else {
            $this->GetAllAllowedCategoryTreeResult = $getAllAllowedCategoryTreeResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllAllowedCategoryTreeResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
