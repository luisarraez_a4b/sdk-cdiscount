<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductPackageProductMatchingFileDataResponse
 * StructType
 * @subpackage Structs
 */
class GetProductPackageProductMatchingFileDataResponse extends AbstractStructBase
{
    /**
     * The GetProductPackageProductMatchingFileDataResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage
     */
    public $GetProductPackageProductMatchingFileDataResult;
    /**
     * Constructor method for GetProductPackageProductMatchingFileDataResponse
     * @uses GetProductPackageProductMatchingFileDataResponse::setGetProductPackageProductMatchingFileDataResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage $getProductPackageProductMatchingFileDataResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage $getProductPackageProductMatchingFileDataResult = null)
    {
        $this
            ->setGetProductPackageProductMatchingFileDataResult($getProductPackageProductMatchingFileDataResult);
    }
    /**
     * Get GetProductPackageProductMatchingFileDataResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage|null
     */
    public function getGetProductPackageProductMatchingFileDataResult()
    {
        return isset($this->GetProductPackageProductMatchingFileDataResult) ? $this->GetProductPackageProductMatchingFileDataResult : null;
    }
    /**
     * Set GetProductPackageProductMatchingFileDataResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage $getProductPackageProductMatchingFileDataResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageProductMatchingFileDataResponse
     */
    public function setGetProductPackageProductMatchingFileDataResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatchingFileMessage $getProductPackageProductMatchingFileDataResult = null)
    {
        if (is_null($getProductPackageProductMatchingFileDataResult) || (is_array($getProductPackageProductMatchingFileDataResult) && empty($getProductPackageProductMatchingFileDataResult))) {
            unset($this->GetProductPackageProductMatchingFileDataResult);
        } else {
            $this->GetProductPackageProductMatchingFileDataResult = $getProductPackageProductMatchingFileDataResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageProductMatchingFileDataResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
