<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DiscountComponent StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscountComponent
 * @subpackage Structs
 */
class DiscountComponent extends AbstractStructBase
{
    /**
     * The DiscountValue
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $DiscountValue;
    /**
     * The EndDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $EndDate;
    /**
     * The Price
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $Price;
    /**
     * The StartDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $StartDate;
    /**
     * The Type
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Type;
    /**
     * Constructor method for DiscountComponent
     * @uses DiscountComponent::setDiscountValue()
     * @uses DiscountComponent::setEndDate()
     * @uses DiscountComponent::setPrice()
     * @uses DiscountComponent::setStartDate()
     * @uses DiscountComponent::setType()
     * @param float $discountValue
     * @param string $endDate
     * @param float $price
     * @param string $startDate
     * @param string $type
     */
    public function __construct($discountValue = null, $endDate = null, $price = null, $startDate = null, $type = null)
    {
        $this
            ->setDiscountValue($discountValue)
            ->setEndDate($endDate)
            ->setPrice($price)
            ->setStartDate($startDate)
            ->setType($type);
    }
    /**
     * Get DiscountValue value
     * @return float|null
     */
    public function getDiscountValue()
    {
        return $this->DiscountValue;
    }
    /**
     * Set DiscountValue value
     * @param float $discountValue
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscountComponent
     */
    public function setDiscountValue($discountValue = null)
    {
        // validation for constraint: float
        if (!is_null($discountValue) && !(is_float($discountValue) || is_numeric($discountValue))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($discountValue, true), gettype($discountValue)), __LINE__);
        }
        $this->DiscountValue = $discountValue;
        return $this;
    }
    /**
     * Get EndDate value
     * @return string|null
     */
    public function getEndDate()
    {
        return $this->EndDate;
    }
    /**
     * Set EndDate value
     * @param string $endDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscountComponent
     */
    public function setEndDate($endDate = null)
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        $this->EndDate = $endDate;
        return $this;
    }
    /**
     * Get Price value
     * @return float|null
     */
    public function getPrice()
    {
        return $this->Price;
    }
    /**
     * Set Price value
     * @param float $price
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscountComponent
     */
    public function setPrice($price = null)
    {
        // validation for constraint: float
        if (!is_null($price) && !(is_float($price) || is_numeric($price))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        $this->Price = $price;
        return $this;
    }
    /**
     * Get StartDate value
     * @return string|null
     */
    public function getStartDate()
    {
        return $this->StartDate;
    }
    /**
     * Set StartDate value
     * @param string $startDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscountComponent
     */
    public function setStartDate($startDate = null)
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->StartDate = $startDate;
        return $this;
    }
    /**
     * Get Type value
     * @return string|null
     */
    public function getType()
    {
        return $this->Type;
    }
    /**
     * Set Type value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscountType::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscountType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $type
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscountComponent
     */
    public function setType($type = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\DiscountType::valueIsValid($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\DiscountType', is_array($type) ? implode(', ', $type) : var_export($type, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscountType::getValidValues())), __LINE__);
        }
        $this->Type = $type;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscountComponent
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
