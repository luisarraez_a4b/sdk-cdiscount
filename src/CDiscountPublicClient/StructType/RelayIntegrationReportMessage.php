<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RelayIntegrationReportMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:RelayIntegrationReportMessage
 * @subpackage Structs
 */
class RelayIntegrationReportMessage extends ServiceBaseAPIMessage
{
    /**
     * The NumberOfErrors
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $NumberOfErrors;
    /**
     * The RelaysFileId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $RelaysFileId;
    /**
     * The RelaysFileIntegrationStatus
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $RelaysFileIntegrationStatus;
    /**
     * The RelaysLogList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRelayIntegrationLog
     */
    public $RelaysLogList;
    /**
     * Constructor method for RelayIntegrationReportMessage
     * @uses RelayIntegrationReportMessage::setNumberOfErrors()
     * @uses RelayIntegrationReportMessage::setRelaysFileId()
     * @uses RelayIntegrationReportMessage::setRelaysFileIntegrationStatus()
     * @uses RelayIntegrationReportMessage::setRelaysLogList()
     * @param int $numberOfErrors
     * @param int $relaysFileId
     * @param string $relaysFileIntegrationStatus
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRelayIntegrationLog $relaysLogList
     */
    public function __construct($numberOfErrors = null, $relaysFileId = null, $relaysFileIntegrationStatus = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRelayIntegrationLog $relaysLogList = null)
    {
        $this
            ->setNumberOfErrors($numberOfErrors)
            ->setRelaysFileId($relaysFileId)
            ->setRelaysFileIntegrationStatus($relaysFileIntegrationStatus)
            ->setRelaysLogList($relaysLogList);
    }
    /**
     * Get NumberOfErrors value
     * @return int|null
     */
    public function getNumberOfErrors()
    {
        return $this->NumberOfErrors;
    }
    /**
     * Set NumberOfErrors value
     * @param int $numberOfErrors
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage
     */
    public function setNumberOfErrors($numberOfErrors = null)
    {
        // validation for constraint: int
        if (!is_null($numberOfErrors) && !(is_int($numberOfErrors) || ctype_digit($numberOfErrors))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($numberOfErrors, true), gettype($numberOfErrors)), __LINE__);
        }
        $this->NumberOfErrors = $numberOfErrors;
        return $this;
    }
    /**
     * Get RelaysFileId value
     * @return int|null
     */
    public function getRelaysFileId()
    {
        return $this->RelaysFileId;
    }
    /**
     * Set RelaysFileId value
     * @param int $relaysFileId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage
     */
    public function setRelaysFileId($relaysFileId = null)
    {
        // validation for constraint: int
        if (!is_null($relaysFileId) && !(is_int($relaysFileId) || ctype_digit($relaysFileId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relaysFileId, true), gettype($relaysFileId)), __LINE__);
        }
        $this->RelaysFileId = $relaysFileId;
        return $this;
    }
    /**
     * Get RelaysFileIntegrationStatus value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRelaysFileIntegrationStatus()
    {
        return isset($this->RelaysFileIntegrationStatus) ? $this->RelaysFileIntegrationStatus : null;
    }
    /**
     * Set RelaysFileIntegrationStatus value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $relaysFileIntegrationStatus
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage
     */
    public function setRelaysFileIntegrationStatus($relaysFileIntegrationStatus = null)
    {
        // validation for constraint: string
        if (!is_null($relaysFileIntegrationStatus) && !is_string($relaysFileIntegrationStatus)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($relaysFileIntegrationStatus, true), gettype($relaysFileIntegrationStatus)), __LINE__);
        }
        if (is_null($relaysFileIntegrationStatus) || (is_array($relaysFileIntegrationStatus) && empty($relaysFileIntegrationStatus))) {
            unset($this->RelaysFileIntegrationStatus);
        } else {
            $this->RelaysFileIntegrationStatus = $relaysFileIntegrationStatus;
        }
        return $this;
    }
    /**
     * Get RelaysLogList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRelayIntegrationLog|null
     */
    public function getRelaysLogList()
    {
        return isset($this->RelaysLogList) ? $this->RelaysLogList : null;
    }
    /**
     * Set RelaysLogList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRelayIntegrationLog $relaysLogList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage
     */
    public function setRelaysLogList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRelayIntegrationLog $relaysLogList = null)
    {
        if (is_null($relaysLogList) || (is_array($relaysLogList) && empty($relaysLogList))) {
            unset($this->RelaysLogList);
        } else {
            $this->RelaysLogList = $relaysLogList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelayIntegrationReportMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
