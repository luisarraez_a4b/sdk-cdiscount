<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOrderQuestionListResponse StructType
 * @subpackage Structs
 */
class GetOrderQuestionListResponse extends AbstractStructBase
{
    /**
     * The GetOrderQuestionListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionListMessage
     */
    public $GetOrderQuestionListResult;
    /**
     * Constructor method for GetOrderQuestionListResponse
     * @uses GetOrderQuestionListResponse::setGetOrderQuestionListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionListMessage $getOrderQuestionListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionListMessage $getOrderQuestionListResult = null)
    {
        $this
            ->setGetOrderQuestionListResult($getOrderQuestionListResult);
    }
    /**
     * Get GetOrderQuestionListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionListMessage|null
     */
    public function getGetOrderQuestionListResult()
    {
        return isset($this->GetOrderQuestionListResult) ? $this->GetOrderQuestionListResult : null;
    }
    /**
     * Set GetOrderQuestionListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionListMessage $getOrderQuestionListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderQuestionListResponse
     */
    public function setGetOrderQuestionListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionListMessage $getOrderQuestionListResult = null)
    {
        if (is_null($getOrderQuestionListResult) || (is_array($getOrderQuestionListResult) && empty($getOrderQuestionListResult))) {
            unset($this->GetOrderQuestionListResult);
        } else {
            $this->GetOrderQuestionListResult = $getOrderQuestionListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderQuestionListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
