<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PackageFilter StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:PackageFilter
 * @subpackage Structs
 */
class PackageFilter extends AbstractStructBase
{
    /**
     * The PackageID
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $PackageID;
    /**
     * Constructor method for PackageFilter
     * @uses PackageFilter::setPackageID()
     * @param int $packageID
     */
    public function __construct($packageID = null)
    {
        $this
            ->setPackageID($packageID);
    }
    /**
     * Get PackageID value
     * @return int|null
     */
    public function getPackageID()
    {
        return $this->PackageID;
    }
    /**
     * Set PackageID value
     * @param int $packageID
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\PackageFilter
     */
    public function setPackageID($packageID = null)
    {
        // validation for constraint: int
        if (!is_null($packageID) && !(is_int($packageID) || ctype_digit($packageID))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($packageID, true), gettype($packageID)), __LINE__);
        }
        $this->PackageID = $packageID;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\PackageFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
