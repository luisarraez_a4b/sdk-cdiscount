<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DiscussionFilterBase StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscussionFilterBase
 * @subpackage Structs
 */
class DiscussionFilterBase extends AbstractStructBase
{
    /**
     * The BeginCreationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BeginCreationDate;
    /**
     * The BeginModificationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BeginModificationDate;
    /**
     * The EndCreationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EndCreationDate;
    /**
     * The EndModificationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EndModificationDate;
    /**
     * The StatusList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter
     */
    public $StatusList;
    /**
     * Constructor method for DiscussionFilterBase
     * @uses DiscussionFilterBase::setBeginCreationDate()
     * @uses DiscussionFilterBase::setBeginModificationDate()
     * @uses DiscussionFilterBase::setEndCreationDate()
     * @uses DiscussionFilterBase::setEndModificationDate()
     * @uses DiscussionFilterBase::setStatusList()
     * @param string $beginCreationDate
     * @param string $beginModificationDate
     * @param string $endCreationDate
     * @param string $endModificationDate
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter $statusList
     */
    public function __construct($beginCreationDate = null, $beginModificationDate = null, $endCreationDate = null, $endModificationDate = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter $statusList = null)
    {
        $this
            ->setBeginCreationDate($beginCreationDate)
            ->setBeginModificationDate($beginModificationDate)
            ->setEndCreationDate($endCreationDate)
            ->setEndModificationDate($endModificationDate)
            ->setStatusList($statusList);
    }
    /**
     * Get BeginCreationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBeginCreationDate()
    {
        return isset($this->BeginCreationDate) ? $this->BeginCreationDate : null;
    }
    /**
     * Set BeginCreationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $beginCreationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionFilterBase
     */
    public function setBeginCreationDate($beginCreationDate = null)
    {
        // validation for constraint: string
        if (!is_null($beginCreationDate) && !is_string($beginCreationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($beginCreationDate, true), gettype($beginCreationDate)), __LINE__);
        }
        if (is_null($beginCreationDate) || (is_array($beginCreationDate) && empty($beginCreationDate))) {
            unset($this->BeginCreationDate);
        } else {
            $this->BeginCreationDate = $beginCreationDate;
        }
        return $this;
    }
    /**
     * Get BeginModificationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBeginModificationDate()
    {
        return isset($this->BeginModificationDate) ? $this->BeginModificationDate : null;
    }
    /**
     * Set BeginModificationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $beginModificationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionFilterBase
     */
    public function setBeginModificationDate($beginModificationDate = null)
    {
        // validation for constraint: string
        if (!is_null($beginModificationDate) && !is_string($beginModificationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($beginModificationDate, true), gettype($beginModificationDate)), __LINE__);
        }
        if (is_null($beginModificationDate) || (is_array($beginModificationDate) && empty($beginModificationDate))) {
            unset($this->BeginModificationDate);
        } else {
            $this->BeginModificationDate = $beginModificationDate;
        }
        return $this;
    }
    /**
     * Get EndCreationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndCreationDate()
    {
        return isset($this->EndCreationDate) ? $this->EndCreationDate : null;
    }
    /**
     * Set EndCreationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endCreationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionFilterBase
     */
    public function setEndCreationDate($endCreationDate = null)
    {
        // validation for constraint: string
        if (!is_null($endCreationDate) && !is_string($endCreationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endCreationDate, true), gettype($endCreationDate)), __LINE__);
        }
        if (is_null($endCreationDate) || (is_array($endCreationDate) && empty($endCreationDate))) {
            unset($this->EndCreationDate);
        } else {
            $this->EndCreationDate = $endCreationDate;
        }
        return $this;
    }
    /**
     * Get EndModificationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndModificationDate()
    {
        return isset($this->EndModificationDate) ? $this->EndModificationDate : null;
    }
    /**
     * Set EndModificationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endModificationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionFilterBase
     */
    public function setEndModificationDate($endModificationDate = null)
    {
        // validation for constraint: string
        if (!is_null($endModificationDate) && !is_string($endModificationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endModificationDate, true), gettype($endModificationDate)), __LINE__);
        }
        if (is_null($endModificationDate) || (is_array($endModificationDate) && empty($endModificationDate))) {
            unset($this->EndModificationDate);
        } else {
            $this->EndModificationDate = $endModificationDate;
        }
        return $this;
    }
    /**
     * Get StatusList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter|null
     */
    public function getStatusList()
    {
        return isset($this->StatusList) ? $this->StatusList : null;
    }
    /**
     * Set StatusList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter $statusList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionFilterBase
     */
    public function setStatusList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter $statusList = null)
    {
        if (is_null($statusList) || (is_array($statusList) && empty($statusList))) {
            unset($this->StatusList);
        } else {
            $this->StatusList = $statusList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionFilterBase
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
