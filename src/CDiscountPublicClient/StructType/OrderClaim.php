<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderClaim StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderClaim
 * @subpackage Structs
 */
class OrderClaim extends OrderQuestion
{
    /**
     * The ClaimType
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ClaimType;
    /**
     * Constructor method for OrderClaim
     * @uses OrderClaim::setClaimType()
     * @param string $claimType
     */
    public function __construct($claimType = null)
    {
        $this
            ->setClaimType($claimType);
    }
    /**
     * Get ClaimType value
     * @return string|null
     */
    public function getClaimType()
    {
        return $this->ClaimType;
    }
    /**
     * Set ClaimType value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ClaimType::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ClaimType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $claimType
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaim
     */
    public function setClaimType($claimType = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ClaimType::valueIsValid($claimType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ClaimType', is_array($claimType) ? implode(', ', $claimType) : var_export($claimType, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ClaimType::getValidValues())), __LINE__);
        }
        $this->ClaimType = $claimType;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaim
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
