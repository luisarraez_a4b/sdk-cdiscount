<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelActionResult StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ParcelActionResult
 * @subpackage Structs
 */
class ParcelActionResult extends ServiceMessage
{
    /**
     * The ActionType
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string[]
     */
    public $ActionType;
    /**
     * The IsActionCreated
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsActionCreated;
    /**
     * The ParcelNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ParcelNumber;
    /**
     * Constructor method for ParcelActionResult
     * @uses ParcelActionResult::setActionType()
     * @uses ParcelActionResult::setIsActionCreated()
     * @uses ParcelActionResult::setParcelNumber()
     * @param string[] $actionType
     * @param bool $isActionCreated
     * @param string $parcelNumber
     */
    public function __construct(array $actionType = array(), $isActionCreated = null, $parcelNumber = null)
    {
        $this
            ->setActionType($actionType)
            ->setIsActionCreated($isActionCreated)
            ->setParcelNumber($parcelNumber);
    }
    /**
     * Get ActionType value
     * @return string[]|null
     */
    public function getActionType()
    {
        return $this->ActionType;
    }
    /**
     * This method is responsible for validating the values passed to the setActionType method
     * This method is willingly generated in order to preserve the one-line inline validation within the setActionType method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateActionTypeForArrayConstraintsFromSetActionType(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $parcelActionResultActionTypeItem) {
            // validation for constraint: enumeration
            if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ParcelActionTypes::valueIsValid($parcelActionResultActionTypeItem)) {
                $invalidValues[] = is_object($parcelActionResultActionTypeItem) ? get_class($parcelActionResultActionTypeItem) : sprintf('%s(%s)', gettype($parcelActionResultActionTypeItem), var_export($parcelActionResultActionTypeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ParcelActionTypes', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ParcelActionTypes::getValidValues()));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ActionType value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ParcelActionTypes::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ParcelActionTypes::getValidValues()
     * @throws \InvalidArgumentException
     * @param string[] $actionType
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelActionResult
     */
    public function setActionType(array $actionType = array())
    {
        // validation for constraint: list
        if ('' !== ($actionTypeArrayErrorMessage = self::validateActionTypeForArrayConstraintsFromSetActionType($actionType))) {
            throw new \InvalidArgumentException($actionTypeArrayErrorMessage, __LINE__);
        }
        $this->ActionType = is_array($actionType) ? implode(' ', $actionType) : null;
        return $this;
    }
    /**
     * Get IsActionCreated value
     * @return bool|null
     */
    public function getIsActionCreated()
    {
        return $this->IsActionCreated;
    }
    /**
     * Set IsActionCreated value
     * @param bool $isActionCreated
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelActionResult
     */
    public function setIsActionCreated($isActionCreated = null)
    {
        // validation for constraint: boolean
        if (!is_null($isActionCreated) && !is_bool($isActionCreated)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isActionCreated, true), gettype($isActionCreated)), __LINE__);
        }
        $this->IsActionCreated = $isActionCreated;
        return $this;
    }
    /**
     * Get ParcelNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelNumber()
    {
        return isset($this->ParcelNumber) ? $this->ParcelNumber : null;
    }
    /**
     * Set ParcelNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelActionResult
     */
    public function setParcelNumber($parcelNumber = null)
    {
        // validation for constraint: string
        if (!is_null($parcelNumber) && !is_string($parcelNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelNumber, true), gettype($parcelNumber)), __LINE__);
        }
        if (is_null($parcelNumber) || (is_array($parcelNumber) && empty($parcelNumber))) {
            unset($this->ParcelNumber);
        } else {
            $this->ParcelNumber = $parcelNumber;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelActionResult
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
