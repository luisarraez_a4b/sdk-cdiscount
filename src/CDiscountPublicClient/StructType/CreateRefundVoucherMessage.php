<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateRefundVoucherMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:CreateRefundVoucherMessage
 * @subpackage Structs
 */
class CreateRefundVoucherMessage extends ServiceBaseAPIMessage
{
    /**
     * The CommercialGestureList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformationMessage
     */
    public $CommercialGestureList;
    /**
     * The OrderNumber
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $OrderNumber;
    /**
     * The SellerRefundList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult
     */
    public $SellerRefundList;
    /**
     * Constructor method for CreateRefundVoucherMessage
     * @uses CreateRefundVoucherMessage::setCommercialGestureList()
     * @uses CreateRefundVoucherMessage::setOrderNumber()
     * @uses CreateRefundVoucherMessage::setSellerRefundList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformationMessage $commercialGestureList
     * @param string $orderNumber
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformationMessage $commercialGestureList = null, $orderNumber = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundList = null)
    {
        $this
            ->setCommercialGestureList($commercialGestureList)
            ->setOrderNumber($orderNumber)
            ->setSellerRefundList($sellerRefundList);
    }
    /**
     * Get CommercialGestureList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformationMessage|null
     */
    public function getCommercialGestureList()
    {
        return $this->CommercialGestureList;
    }
    /**
     * Set CommercialGestureList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformationMessage $commercialGestureList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage
     */
    public function setCommercialGestureList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformationMessage $commercialGestureList = null)
    {
        $this->CommercialGestureList = $commercialGestureList;
        return $this;
    }
    /**
     * Get OrderNumber value
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->OrderNumber;
    }
    /**
     * Set OrderNumber value
     * @param string $orderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage
     */
    public function setOrderNumber($orderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($orderNumber) && !is_string($orderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderNumber, true), gettype($orderNumber)), __LINE__);
        }
        $this->OrderNumber = $orderNumber;
        return $this;
    }
    /**
     * Get SellerRefundList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult|null
     */
    public function getSellerRefundList()
    {
        return $this->SellerRefundList;
    }
    /**
     * Set SellerRefundList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage
     */
    public function setSellerRefundList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundList = null)
    {
        $this->SellerRefundList = $sellerRefundList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
