<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentProductRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentProductRequest
 * @subpackage Structs
 */
class FulfilmentProductRequest extends AbstractStructBase
{
    /**
     * The BarCodeList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $BarCodeList;
    /**
     * The FulfilmentReferencement
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $FulfilmentReferencement;
    /**
     * Constructor method for FulfilmentProductRequest
     * @uses FulfilmentProductRequest::setBarCodeList()
     * @uses FulfilmentProductRequest::setFulfilmentReferencement()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $barCodeList
     * @param string $fulfilmentReferencement
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $barCodeList = null, $fulfilmentReferencement = null)
    {
        $this
            ->setBarCodeList($barCodeList)
            ->setFulfilmentReferencement($fulfilmentReferencement);
    }
    /**
     * Get BarCodeList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getBarCodeList()
    {
        return isset($this->BarCodeList) ? $this->BarCodeList : null;
    }
    /**
     * Set BarCodeList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $barCodeList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductRequest
     */
    public function setBarCodeList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $barCodeList = null)
    {
        if (is_null($barCodeList) || (is_array($barCodeList) && empty($barCodeList))) {
            unset($this->BarCodeList);
        } else {
            $this->BarCodeList = $barCodeList;
        }
        return $this;
    }
    /**
     * Get FulfilmentReferencement value
     * @return string|null
     */
    public function getFulfilmentReferencement()
    {
        return $this->FulfilmentReferencement;
    }
    /**
     * Set FulfilmentReferencement value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\FbcReferencementFilter::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\FbcReferencementFilter::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $fulfilmentReferencement
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductRequest
     */
    public function setFulfilmentReferencement($fulfilmentReferencement = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\FbcReferencementFilter::valueIsValid($fulfilmentReferencement)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\FbcReferencementFilter', is_array($fulfilmentReferencement) ? implode(', ', $fulfilmentReferencement) : var_export($fulfilmentReferencement, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\FbcReferencementFilter::getValidValues())), __LINE__);
        }
        $this->FulfilmentReferencement = $fulfilmentReferencement;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
