<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShippingInformation StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ShippingInformation
 * @subpackage Structs
 */
class ShippingInformation extends AbstractStructBase
{
    /**
     * The AdditionalShippingCharges
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $AdditionalShippingCharges;
    /**
     * The DeliveryMode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation
     */
    public $DeliveryMode;
    /**
     * The MaxLeadTime
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $MaxLeadTime;
    /**
     * The MinLeadTime
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $MinLeadTime;
    /**
     * The ShippingCharges
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $ShippingCharges;
    /**
     * Constructor method for ShippingInformation
     * @uses ShippingInformation::setAdditionalShippingCharges()
     * @uses ShippingInformation::setDeliveryMode()
     * @uses ShippingInformation::setMaxLeadTime()
     * @uses ShippingInformation::setMinLeadTime()
     * @uses ShippingInformation::setShippingCharges()
     * @param float $additionalShippingCharges
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation $deliveryMode
     * @param int $maxLeadTime
     * @param int $minLeadTime
     * @param float $shippingCharges
     */
    public function __construct($additionalShippingCharges = null, \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation $deliveryMode = null, $maxLeadTime = null, $minLeadTime = null, $shippingCharges = null)
    {
        $this
            ->setAdditionalShippingCharges($additionalShippingCharges)
            ->setDeliveryMode($deliveryMode)
            ->setMaxLeadTime($maxLeadTime)
            ->setMinLeadTime($minLeadTime)
            ->setShippingCharges($shippingCharges);
    }
    /**
     * Get AdditionalShippingCharges value
     * @return float|null
     */
    public function getAdditionalShippingCharges()
    {
        return $this->AdditionalShippingCharges;
    }
    /**
     * Set AdditionalShippingCharges value
     * @param float $additionalShippingCharges
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation
     */
    public function setAdditionalShippingCharges($additionalShippingCharges = null)
    {
        // validation for constraint: float
        if (!is_null($additionalShippingCharges) && !(is_float($additionalShippingCharges) || is_numeric($additionalShippingCharges))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($additionalShippingCharges, true), gettype($additionalShippingCharges)), __LINE__);
        }
        $this->AdditionalShippingCharges = $additionalShippingCharges;
        return $this;
    }
    /**
     * Get DeliveryMode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation|null
     */
    public function getDeliveryMode()
    {
        return isset($this->DeliveryMode) ? $this->DeliveryMode : null;
    }
    /**
     * Set DeliveryMode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation $deliveryMode
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation
     */
    public function setDeliveryMode(\A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation $deliveryMode = null)
    {
        if (is_null($deliveryMode) || (is_array($deliveryMode) && empty($deliveryMode))) {
            unset($this->DeliveryMode);
        } else {
            $this->DeliveryMode = $deliveryMode;
        }
        return $this;
    }
    /**
     * Get MaxLeadTime value
     * @return int|null
     */
    public function getMaxLeadTime()
    {
        return $this->MaxLeadTime;
    }
    /**
     * Set MaxLeadTime value
     * @param int $maxLeadTime
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation
     */
    public function setMaxLeadTime($maxLeadTime = null)
    {
        // validation for constraint: int
        if (!is_null($maxLeadTime) && !(is_int($maxLeadTime) || ctype_digit($maxLeadTime))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxLeadTime, true), gettype($maxLeadTime)), __LINE__);
        }
        $this->MaxLeadTime = $maxLeadTime;
        return $this;
    }
    /**
     * Get MinLeadTime value
     * @return int|null
     */
    public function getMinLeadTime()
    {
        return $this->MinLeadTime;
    }
    /**
     * Set MinLeadTime value
     * @param int $minLeadTime
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation
     */
    public function setMinLeadTime($minLeadTime = null)
    {
        // validation for constraint: int
        if (!is_null($minLeadTime) && !(is_int($minLeadTime) || ctype_digit($minLeadTime))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($minLeadTime, true), gettype($minLeadTime)), __LINE__);
        }
        $this->MinLeadTime = $minLeadTime;
        return $this;
    }
    /**
     * Get ShippingCharges value
     * @return float|null
     */
    public function getShippingCharges()
    {
        return $this->ShippingCharges;
    }
    /**
     * Set ShippingCharges value
     * @param float $shippingCharges
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation
     */
    public function setShippingCharges($shippingCharges = null)
    {
        // validation for constraint: float
        if (!is_null($shippingCharges) && !(is_float($shippingCharges) || is_numeric($shippingCharges))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($shippingCharges, true), gettype($shippingCharges)), __LINE__);
        }
        $this->ShippingCharges = $shippingCharges;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
