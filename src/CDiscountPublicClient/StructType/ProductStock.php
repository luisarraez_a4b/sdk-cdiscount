<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductStock StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductStock
 * @subpackage Structs
 */
class ProductStock extends AbstractStructBase
{
    /**
     * The BlockedStock
     * @var int
     */
    public $BlockedStock;
    /**
     * The Ean
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Ean;
    /**
     * The FodStock
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $FodStock;
    /**
     * The FrontStock
     * @var int
     */
    public $FrontStock;
    /**
     * The IsReferenced
     * @var bool
     */
    public $IsReferenced;
    /**
     * The Libelle
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Libelle;
    /**
     * The SellerReference
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $SellerReference;
    /**
     * The Sku
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Sku;
    /**
     * The StockInWarehouse
     * @var int
     */
    public $StockInWarehouse;
    /**
     * The Warehouse
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Warehouse;
    /**
     * Constructor method for ProductStock
     * @uses ProductStock::setBlockedStock()
     * @uses ProductStock::setEan()
     * @uses ProductStock::setFodStock()
     * @uses ProductStock::setFrontStock()
     * @uses ProductStock::setIsReferenced()
     * @uses ProductStock::setLibelle()
     * @uses ProductStock::setSellerReference()
     * @uses ProductStock::setSku()
     * @uses ProductStock::setStockInWarehouse()
     * @uses ProductStock::setWarehouse()
     * @param int $blockedStock
     * @param string $ean
     * @param int $fodStock
     * @param int $frontStock
     * @param bool $isReferenced
     * @param string $libelle
     * @param string $sellerReference
     * @param string $sku
     * @param int $stockInWarehouse
     * @param string $warehouse
     */
    public function __construct($blockedStock = null, $ean = null, $fodStock = null, $frontStock = null, $isReferenced = null, $libelle = null, $sellerReference = null, $sku = null, $stockInWarehouse = null, $warehouse = null)
    {
        $this
            ->setBlockedStock($blockedStock)
            ->setEan($ean)
            ->setFodStock($fodStock)
            ->setFrontStock($frontStock)
            ->setIsReferenced($isReferenced)
            ->setLibelle($libelle)
            ->setSellerReference($sellerReference)
            ->setSku($sku)
            ->setStockInWarehouse($stockInWarehouse)
            ->setWarehouse($warehouse);
    }
    /**
     * Get BlockedStock value
     * @return int|null
     */
    public function getBlockedStock()
    {
        return $this->BlockedStock;
    }
    /**
     * Set BlockedStock value
     * @param int $blockedStock
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setBlockedStock($blockedStock = null)
    {
        // validation for constraint: int
        if (!is_null($blockedStock) && !(is_int($blockedStock) || ctype_digit($blockedStock))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($blockedStock, true), gettype($blockedStock)), __LINE__);
        }
        $this->BlockedStock = $blockedStock;
        return $this;
    }
    /**
     * Get Ean value
     * @return string|null
     */
    public function getEan()
    {
        return $this->Ean;
    }
    /**
     * Set Ean value
     * @param string $ean
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setEan($ean = null)
    {
        // validation for constraint: string
        if (!is_null($ean) && !is_string($ean)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ean, true), gettype($ean)), __LINE__);
        }
        $this->Ean = $ean;
        return $this;
    }
    /**
     * Get FodStock value
     * @return int|null
     */
    public function getFodStock()
    {
        return $this->FodStock;
    }
    /**
     * Set FodStock value
     * @param int $fodStock
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setFodStock($fodStock = null)
    {
        // validation for constraint: int
        if (!is_null($fodStock) && !(is_int($fodStock) || ctype_digit($fodStock))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($fodStock, true), gettype($fodStock)), __LINE__);
        }
        $this->FodStock = $fodStock;
        return $this;
    }
    /**
     * Get FrontStock value
     * @return int|null
     */
    public function getFrontStock()
    {
        return $this->FrontStock;
    }
    /**
     * Set FrontStock value
     * @param int $frontStock
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setFrontStock($frontStock = null)
    {
        // validation for constraint: int
        if (!is_null($frontStock) && !(is_int($frontStock) || ctype_digit($frontStock))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($frontStock, true), gettype($frontStock)), __LINE__);
        }
        $this->FrontStock = $frontStock;
        return $this;
    }
    /**
     * Get IsReferenced value
     * @return bool|null
     */
    public function getIsReferenced()
    {
        return $this->IsReferenced;
    }
    /**
     * Set IsReferenced value
     * @param bool $isReferenced
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setIsReferenced($isReferenced = null)
    {
        // validation for constraint: boolean
        if (!is_null($isReferenced) && !is_bool($isReferenced)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isReferenced, true), gettype($isReferenced)), __LINE__);
        }
        $this->IsReferenced = $isReferenced;
        return $this;
    }
    /**
     * Get Libelle value
     * @return string|null
     */
    public function getLibelle()
    {
        return $this->Libelle;
    }
    /**
     * Set Libelle value
     * @param string $libelle
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setLibelle($libelle = null)
    {
        // validation for constraint: string
        if (!is_null($libelle) && !is_string($libelle)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($libelle, true), gettype($libelle)), __LINE__);
        }
        $this->Libelle = $libelle;
        return $this;
    }
    /**
     * Get SellerReference value
     * @return string|null
     */
    public function getSellerReference()
    {
        return $this->SellerReference;
    }
    /**
     * Set SellerReference value
     * @param string $sellerReference
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setSellerReference($sellerReference = null)
    {
        // validation for constraint: string
        if (!is_null($sellerReference) && !is_string($sellerReference)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerReference, true), gettype($sellerReference)), __LINE__);
        }
        $this->SellerReference = $sellerReference;
        return $this;
    }
    /**
     * Get Sku value
     * @return string|null
     */
    public function getSku()
    {
        return $this->Sku;
    }
    /**
     * Set Sku value
     * @param string $sku
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setSku($sku = null)
    {
        // validation for constraint: string
        if (!is_null($sku) && !is_string($sku)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sku, true), gettype($sku)), __LINE__);
        }
        $this->Sku = $sku;
        return $this;
    }
    /**
     * Get StockInWarehouse value
     * @return int|null
     */
    public function getStockInWarehouse()
    {
        return $this->StockInWarehouse;
    }
    /**
     * Set StockInWarehouse value
     * @param int $stockInWarehouse
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setStockInWarehouse($stockInWarehouse = null)
    {
        // validation for constraint: int
        if (!is_null($stockInWarehouse) && !(is_int($stockInWarehouse) || ctype_digit($stockInWarehouse))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($stockInWarehouse, true), gettype($stockInWarehouse)), __LINE__);
        }
        $this->StockInWarehouse = $stockInWarehouse;
        return $this;
    }
    /**
     * Get Warehouse value
     * @return string|null
     */
    public function getWarehouse()
    {
        return $this->Warehouse;
    }
    /**
     * Set Warehouse value
     * @param string $warehouse
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public function setWarehouse($warehouse = null)
    {
        // validation for constraint: string
        if (!is_null($warehouse) && !is_string($warehouse)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($warehouse, true), gettype($warehouse)), __LINE__);
        }
        $this->Warehouse = $warehouse;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
