<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Carrier StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:Carrier
 * @subpackage Structs
 */
class Carrier extends AbstractStructBase
{
    /**
     * The CarrierId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $CarrierId;
    /**
     * The DefaultURL
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $DefaultURL;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * Constructor method for Carrier
     * @uses Carrier::setCarrierId()
     * @uses Carrier::setDefaultURL()
     * @uses Carrier::setName()
     * @param int $carrierId
     * @param string $defaultURL
     * @param string $name
     */
    public function __construct($carrierId = null, $defaultURL = null, $name = null)
    {
        $this
            ->setCarrierId($carrierId)
            ->setDefaultURL($defaultURL)
            ->setName($name);
    }
    /**
     * Get CarrierId value
     * @return int|null
     */
    public function getCarrierId()
    {
        return $this->CarrierId;
    }
    /**
     * Set CarrierId value
     * @param int $carrierId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Carrier
     */
    public function setCarrierId($carrierId = null)
    {
        // validation for constraint: int
        if (!is_null($carrierId) && !(is_int($carrierId) || ctype_digit($carrierId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($carrierId, true), gettype($carrierId)), __LINE__);
        }
        $this->CarrierId = $carrierId;
        return $this;
    }
    /**
     * Get DefaultURL value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDefaultURL()
    {
        return isset($this->DefaultURL) ? $this->DefaultURL : null;
    }
    /**
     * Set DefaultURL value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $defaultURL
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Carrier
     */
    public function setDefaultURL($defaultURL = null)
    {
        // validation for constraint: string
        if (!is_null($defaultURL) && !is_string($defaultURL)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($defaultURL, true), gettype($defaultURL)), __LINE__);
        }
        if (is_null($defaultURL) || (is_array($defaultURL) && empty($defaultURL))) {
            unset($this->DefaultURL);
        } else {
            $this->DefaultURL = $defaultURL;
        }
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Carrier
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Carrier
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
