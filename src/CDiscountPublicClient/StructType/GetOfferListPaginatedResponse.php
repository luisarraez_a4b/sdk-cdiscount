<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOfferListPaginatedResponse StructType
 * @subpackage Structs
 */
class GetOfferListPaginatedResponse extends AbstractStructBase
{
    /**
     * The GetOfferListPaginatedResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListPaginatedMessage
     */
    public $GetOfferListPaginatedResult;
    /**
     * Constructor method for GetOfferListPaginatedResponse
     * @uses GetOfferListPaginatedResponse::setGetOfferListPaginatedResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListPaginatedMessage $getOfferListPaginatedResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferListPaginatedMessage $getOfferListPaginatedResult = null)
    {
        $this
            ->setGetOfferListPaginatedResult($getOfferListPaginatedResult);
    }
    /**
     * Get GetOfferListPaginatedResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListPaginatedMessage|null
     */
    public function getGetOfferListPaginatedResult()
    {
        return isset($this->GetOfferListPaginatedResult) ? $this->GetOfferListPaginatedResult : null;
    }
    /**
     * Set GetOfferListPaginatedResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferListPaginatedMessage $getOfferListPaginatedResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListPaginatedResponse
     */
    public function setGetOfferListPaginatedResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferListPaginatedMessage $getOfferListPaginatedResult = null)
    {
        if (is_null($getOfferListPaginatedResult) || (is_array($getOfferListPaginatedResult) && empty($getOfferListPaginatedResult))) {
            unset($this->GetOfferListPaginatedResult);
        } else {
            $this->GetOfferListPaginatedResult = $getOfferListPaginatedResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListPaginatedResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
