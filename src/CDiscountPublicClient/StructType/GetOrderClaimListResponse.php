<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOrderClaimListResponse StructType
 * @subpackage Structs
 */
class GetOrderClaimListResponse extends AbstractStructBase
{
    /**
     * The GetOrderClaimListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage
     */
    public $GetOrderClaimListResult;
    /**
     * Constructor method for GetOrderClaimListResponse
     * @uses GetOrderClaimListResponse::setGetOrderClaimListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage $getOrderClaimListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage $getOrderClaimListResult = null)
    {
        $this
            ->setGetOrderClaimListResult($getOrderClaimListResult);
    }
    /**
     * Get GetOrderClaimListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage|null
     */
    public function getGetOrderClaimListResult()
    {
        return isset($this->GetOrderClaimListResult) ? $this->GetOrderClaimListResult : null;
    }
    /**
     * Set GetOrderClaimListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage $getOrderClaimListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderClaimListResponse
     */
    public function setGetOrderClaimListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderClaimListMessage $getOrderClaimListResult = null)
    {
        if (is_null($getOrderClaimListResult) || (is_array($getOrderClaimListResult) && empty($getOrderClaimListResult))) {
            unset($this->GetOrderClaimListResult);
        } else {
            $this->GetOrderClaimListResult = $getOrderClaimListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderClaimListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
