<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderQuestionFilter StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderQuestionFilter
 * @subpackage Structs
 */
class OrderQuestionFilter extends DiscussionFilterBase
{
    /**
     * The OrderNumberList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $OrderNumberList;
    /**
     * Constructor method for OrderQuestionFilter
     * @uses OrderQuestionFilter::setOrderNumberList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderNumberList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderNumberList = null)
    {
        $this
            ->setOrderNumberList($orderNumberList);
    }
    /**
     * Get OrderNumberList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getOrderNumberList()
    {
        return isset($this->OrderNumberList) ? $this->OrderNumberList : null;
    }
    /**
     * Set OrderNumberList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderNumberList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionFilter
     */
    public function setOrderNumberList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderNumberList = null)
    {
        if (is_null($orderNumberList) || (is_array($orderNumberList) && empty($orderNumberList))) {
            unset($this->OrderNumberList);
        } else {
            $this->OrderNumberList = $orderNumberList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderQuestionFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
