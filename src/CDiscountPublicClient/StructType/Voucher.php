<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Voucher StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:Voucher
 * @subpackage Structs
 */
class Voucher extends AbstractStructBase
{
    /**
     * The CreateDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CreateDate;
    /**
     * The RefundInformation
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformation
     */
    public $RefundInformation;
    /**
     * The Source
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Source;
    /**
     * Constructor method for Voucher
     * @uses Voucher::setCreateDate()
     * @uses Voucher::setRefundInformation()
     * @uses Voucher::setSource()
     * @param string $createDate
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformation $refundInformation
     * @param string $source
     */
    public function __construct($createDate = null, \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformation $refundInformation = null, $source = null)
    {
        $this
            ->setCreateDate($createDate)
            ->setRefundInformation($refundInformation)
            ->setSource($source);
    }
    /**
     * Get CreateDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCreateDate()
    {
        return isset($this->CreateDate) ? $this->CreateDate : null;
    }
    /**
     * Set CreateDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $createDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher
     */
    public function setCreateDate($createDate = null)
    {
        // validation for constraint: string
        if (!is_null($createDate) && !is_string($createDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($createDate, true), gettype($createDate)), __LINE__);
        }
        if (is_null($createDate) || (is_array($createDate) && empty($createDate))) {
            unset($this->CreateDate);
        } else {
            $this->CreateDate = $createDate;
        }
        return $this;
    }
    /**
     * Get RefundInformation value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformation|null
     */
    public function getRefundInformation()
    {
        return $this->RefundInformation;
    }
    /**
     * Set RefundInformation value
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformation $refundInformation
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher
     */
    public function setRefundInformation(\A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformation $refundInformation = null)
    {
        $this->RefundInformation = $refundInformation;
        return $this;
    }
    /**
     * Get Source value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSource()
    {
        return isset($this->Source) ? $this->Source : null;
    }
    /**
     * Set Source value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\VoucherSourceActor::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\VoucherSourceActor::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $source
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher
     */
    public function setSource($source = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\VoucherSourceActor::valueIsValid($source)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\VoucherSourceActor', is_array($source) ? implode(', ', $source) : var_export($source, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\VoucherSourceActor::getValidValues())), __LINE__);
        }
        if (is_null($source) || (is_array($source) && empty($source))) {
            unset($this->Source);
        } else {
            $this->Source = $source;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
