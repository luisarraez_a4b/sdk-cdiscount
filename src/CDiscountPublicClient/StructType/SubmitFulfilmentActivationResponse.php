<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitFulfilmentActivationResponse StructType
 * @subpackage Structs
 */
class SubmitFulfilmentActivationResponse extends AbstractStructBase
{
    /**
     * The SubmitFulfilmentActivationResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationMessage
     */
    public $SubmitFulfilmentActivationResult;
    /**
     * Constructor method for SubmitFulfilmentActivationResponse
     * @uses SubmitFulfilmentActivationResponse::setSubmitFulfilmentActivationResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationMessage $submitFulfilmentActivationResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationMessage $submitFulfilmentActivationResult = null)
    {
        $this
            ->setSubmitFulfilmentActivationResult($submitFulfilmentActivationResult);
    }
    /**
     * Get SubmitFulfilmentActivationResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationMessage|null
     */
    public function getSubmitFulfilmentActivationResult()
    {
        return isset($this->SubmitFulfilmentActivationResult) ? $this->SubmitFulfilmentActivationResult : null;
    }
    /**
     * Set SubmitFulfilmentActivationResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationMessage $submitFulfilmentActivationResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentActivationResponse
     */
    public function setSubmitFulfilmentActivationResult(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationMessage $submitFulfilmentActivationResult = null)
    {
        if (is_null($submitFulfilmentActivationResult) || (is_array($submitFulfilmentActivationResult) && empty($submitFulfilmentActivationResult))) {
            unset($this->SubmitFulfilmentActivationResult);
        } else {
            $this->SubmitFulfilmentActivationResult = $submitFulfilmentActivationResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentActivationResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
