<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ManageParcelResultMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ManageParcelResultMessage
 * @subpackage Structs
 */
class ManageParcelResultMessage extends ServiceBaseAPIMessage
{
    /**
     * The ParcelActionResultList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelActionResult
     */
    public $ParcelActionResultList;
    /**
     * Constructor method for ManageParcelResultMessage
     * @uses ManageParcelResultMessage::setParcelActionResultList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelActionResult $parcelActionResultList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelActionResult $parcelActionResultList = null)
    {
        $this
            ->setParcelActionResultList($parcelActionResultList);
    }
    /**
     * Get ParcelActionResultList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelActionResult|null
     */
    public function getParcelActionResultList()
    {
        return isset($this->ParcelActionResultList) ? $this->ParcelActionResultList : null;
    }
    /**
     * Set ParcelActionResultList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelActionResult $parcelActionResultList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcelResultMessage
     */
    public function setParcelActionResultList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelActionResult $parcelActionResultList = null)
    {
        if (is_null($parcelActionResultList) || (is_array($parcelActionResultList) && empty($parcelActionResultList))) {
            unset($this->ParcelActionResultList);
        } else {
            $this->ParcelActionResultList = $parcelActionResultList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcelResultMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
