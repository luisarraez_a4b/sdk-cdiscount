<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RelaysFileIntegrationRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:RelaysFileIntegrationRequest
 * @subpackage Structs
 */
class RelaysFileIntegrationRequest extends AbstractStructBase
{
    /**
     * The RelaysFileURI
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $RelaysFileURI;
    /**
     * Constructor method for RelaysFileIntegrationRequest
     * @uses RelaysFileIntegrationRequest::setRelaysFileURI()
     * @param string $relaysFileURI
     */
    public function __construct($relaysFileURI = null)
    {
        $this
            ->setRelaysFileURI($relaysFileURI);
    }
    /**
     * Get RelaysFileURI value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getRelaysFileURI()
    {
        return isset($this->RelaysFileURI) ? $this->RelaysFileURI : null;
    }
    /**
     * Set RelaysFileURI value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $relaysFileURI
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelaysFileIntegrationRequest
     */
    public function setRelaysFileURI($relaysFileURI = null)
    {
        // validation for constraint: string
        if (!is_null($relaysFileURI) && !is_string($relaysFileURI)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($relaysFileURI, true), gettype($relaysFileURI)), __LINE__);
        }
        if (is_null($relaysFileURI) || (is_array($relaysFileURI) && empty($relaysFileURI))) {
            unset($this->RelaysFileURI);
        } else {
            $this->RelaysFileURI = $relaysFileURI;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelaysFileIntegrationRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
