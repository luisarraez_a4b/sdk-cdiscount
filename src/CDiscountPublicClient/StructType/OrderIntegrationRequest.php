<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderIntegrationRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderIntegrationRequest
 * @subpackage Structs
 */
class OrderIntegrationRequest extends AbstractStructBase
{
    /**
     * The Order
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public $Order;
    /**
     * Constructor method for OrderIntegrationRequest
     * @uses OrderIntegrationRequest::setOrder()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder $order
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder $order = null)
    {
        $this
            ->setOrder($order);
    }
    /**
     * Get Order value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder|null
     */
    public function getOrder()
    {
        return $this->Order;
    }
    /**
     * Set Order value
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder $order
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderIntegrationRequest
     */
    public function setOrder(\A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder $order = null)
    {
        $this->Order = $order;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderIntegrationRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
