<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SupplyOrderReport StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SupplyOrderReport
 * @subpackage Structs
 */
class SupplyOrderReport extends AbstractStructBase
{
    /**
     * The DepositId
     * @var int
     */
    public $DepositId;
    /**
     * The ReportLineList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReportLine
     */
    public $ReportLineList;
    /**
     * Constructor method for SupplyOrderReport
     * @uses SupplyOrderReport::setDepositId()
     * @uses SupplyOrderReport::setReportLineList()
     * @param int $depositId
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReportLine $reportLineList
     */
    public function __construct($depositId = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReportLine $reportLineList = null)
    {
        $this
            ->setDepositId($depositId)
            ->setReportLineList($reportLineList);
    }
    /**
     * Get DepositId value
     * @return int|null
     */
    public function getDepositId()
    {
        return $this->DepositId;
    }
    /**
     * Set DepositId value
     * @param int $depositId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReport
     */
    public function setDepositId($depositId = null)
    {
        // validation for constraint: int
        if (!is_null($depositId) && !(is_int($depositId) || ctype_digit($depositId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($depositId, true), gettype($depositId)), __LINE__);
        }
        $this->DepositId = $depositId;
        return $this;
    }
    /**
     * Get ReportLineList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReportLine|null
     */
    public function getReportLineList()
    {
        return $this->ReportLineList;
    }
    /**
     * Set ReportLineList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReportLine $reportLineList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReport
     */
    public function setReportLineList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReportLine $reportLineList = null)
    {
        $this->ReportLineList = $reportLineList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReport
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
