<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RefundInformationMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:RefundInformationMessage
 * @subpackage Structs
 */
class RefundInformationMessage extends ServiceMessage
{
    /**
     * The Amount
     * @var float
     */
    public $Amount;
    /**
     * The MotiveId
     * @var int
     */
    public $MotiveId;
    /**
     * Constructor method for RefundInformationMessage
     * @uses RefundInformationMessage::setAmount()
     * @uses RefundInformationMessage::setMotiveId()
     * @param float $amount
     * @param int $motiveId
     */
    public function __construct($amount = null, $motiveId = null)
    {
        $this
            ->setAmount($amount)
            ->setMotiveId($motiveId);
    }
    /**
     * Get Amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->Amount;
    }
    /**
     * Set Amount value
     * @param float $amount
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformationMessage
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->Amount = $amount;
        return $this;
    }
    /**
     * Get MotiveId value
     * @return int|null
     */
    public function getMotiveId()
    {
        return $this->MotiveId;
    }
    /**
     * Set MotiveId value
     * @param int $motiveId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformationMessage
     */
    public function setMotiveId($motiveId = null)
    {
        // validation for constraint: int
        if (!is_null($motiveId) && !(is_int($motiveId) || ctype_digit($motiveId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($motiveId, true), gettype($motiveId)), __LINE__);
        }
        $this->MotiveId = $motiveId;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RefundInformationMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
