<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ServiceMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ServiceMessage
 * @subpackage Structs
 */
class ServiceMessage extends AbstractStructBase
{
    /**
     * The ErrorMessage
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ErrorMessage;
    /**
     * The OperationSuccess
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $OperationSuccess;
    /**
     * Constructor method for ServiceMessage
     * @uses ServiceMessage::setErrorMessage()
     * @uses ServiceMessage::setOperationSuccess()
     * @param string $errorMessage
     * @param bool $operationSuccess
     */
    public function __construct($errorMessage = null, $operationSuccess = null)
    {
        $this
            ->setErrorMessage($errorMessage)
            ->setOperationSuccess($operationSuccess);
    }
    /**
     * Get ErrorMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getErrorMessage()
    {
        return isset($this->ErrorMessage) ? $this->ErrorMessage : null;
    }
    /**
     * Set ErrorMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $errorMessage
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceMessage
     */
    public function setErrorMessage($errorMessage = null)
    {
        // validation for constraint: string
        if (!is_null($errorMessage) && !is_string($errorMessage)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorMessage, true), gettype($errorMessage)), __LINE__);
        }
        if (is_null($errorMessage) || (is_array($errorMessage) && empty($errorMessage))) {
            unset($this->ErrorMessage);
        } else {
            $this->ErrorMessage = $errorMessage;
        }
        return $this;
    }
    /**
     * Get OperationSuccess value
     * @return bool|null
     */
    public function getOperationSuccess()
    {
        return $this->OperationSuccess;
    }
    /**
     * Set OperationSuccess value
     * @param bool $operationSuccess
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceMessage
     */
    public function setOperationSuccess($operationSuccess = null)
    {
        // validation for constraint: boolean
        if (!is_null($operationSuccess) && !is_bool($operationSuccess)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($operationSuccess, true), gettype($operationSuccess)), __LINE__);
        }
        $this->OperationSuccess = $operationSuccess;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
