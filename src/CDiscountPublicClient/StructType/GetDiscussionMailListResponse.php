<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetDiscussionMailListResponse StructType
 * @subpackage Structs
 */
class GetDiscussionMailListResponse extends AbstractStructBase
{
    /**
     * The GetDiscussionMailListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage
     */
    public $GetDiscussionMailListResult;
    /**
     * Constructor method for GetDiscussionMailListResponse
     * @uses GetDiscussionMailListResponse::setGetDiscussionMailListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage $getDiscussionMailListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage $getDiscussionMailListResult = null)
    {
        $this
            ->setGetDiscussionMailListResult($getDiscussionMailListResult);
    }
    /**
     * Get GetDiscussionMailListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage|null
     */
    public function getGetDiscussionMailListResult()
    {
        return isset($this->GetDiscussionMailListResult) ? $this->GetDiscussionMailListResult : null;
    }
    /**
     * Set GetDiscussionMailListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage $getDiscussionMailListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetDiscussionMailListResponse
     */
    public function setGetDiscussionMailListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailListMessage $getDiscussionMailListResult = null)
    {
        if (is_null($getDiscussionMailListResult) || (is_array($getDiscussionMailListResult) && empty($getDiscussionMailListResult))) {
            unset($this->GetDiscussionMailListResult);
        } else {
            $this->GetDiscussionMailListResult = $getDiscussionMailListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetDiscussionMailListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
