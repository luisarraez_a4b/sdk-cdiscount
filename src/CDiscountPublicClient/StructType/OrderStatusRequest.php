<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderStatusRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderStatusRequest
 * @subpackage Structs
 */
class OrderStatusRequest extends AbstractStructBase
{
    /**
     * The Corporation
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Corporation;
    /**
     * The CustomerOrderNumber
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $CustomerOrderNumber;
    /**
     * Constructor method for OrderStatusRequest
     * @uses OrderStatusRequest::setCorporation()
     * @uses OrderStatusRequest::setCustomerOrderNumber()
     * @param string $corporation
     * @param string $customerOrderNumber
     */
    public function __construct($corporation = null, $customerOrderNumber = null)
    {
        $this
            ->setCorporation($corporation)
            ->setCustomerOrderNumber($customerOrderNumber);
    }
    /**
     * Get Corporation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporation()
    {
        return isset($this->Corporation) ? $this->Corporation : null;
    }
    /**
     * Set Corporation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporation
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusRequest
     */
    public function setCorporation($corporation = null)
    {
        // validation for constraint: string
        if (!is_null($corporation) && !is_string($corporation)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporation, true), gettype($corporation)), __LINE__);
        }
        if (is_null($corporation) || (is_array($corporation) && empty($corporation))) {
            unset($this->Corporation);
        } else {
            $this->Corporation = $corporation;
        }
        return $this;
    }
    /**
     * Get CustomerOrderNumber value
     * @return string|null
     */
    public function getCustomerOrderNumber()
    {
        return $this->CustomerOrderNumber;
    }
    /**
     * Set CustomerOrderNumber value
     * @param string $customerOrderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusRequest
     */
    public function setCustomerOrderNumber($customerOrderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($customerOrderNumber) && !is_string($customerOrderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerOrderNumber, true), gettype($customerOrderNumber)), __LINE__);
        }
        $this->CustomerOrderNumber = $customerOrderNumber;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
