<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CloseDiscussionListResponse StructType
 * @subpackage Structs
 */
class CloseDiscussionListResponse extends AbstractStructBase
{
    /**
     * The CloseDiscussionListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage
     */
    public $CloseDiscussionListResult;
    /**
     * Constructor method for CloseDiscussionListResponse
     * @uses CloseDiscussionListResponse::setCloseDiscussionListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage $closeDiscussionListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage $closeDiscussionListResult = null)
    {
        $this
            ->setCloseDiscussionListResult($closeDiscussionListResult);
    }
    /**
     * Get CloseDiscussionListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage|null
     */
    public function getCloseDiscussionListResult()
    {
        return isset($this->CloseDiscussionListResult) ? $this->CloseDiscussionListResult : null;
    }
    /**
     * Set CloseDiscussionListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage $closeDiscussionListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionListResponse
     */
    public function setCloseDiscussionListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage $closeDiscussionListResult = null)
    {
        if (is_null($closeDiscussionListResult) || (is_array($closeDiscussionListResult) && empty($closeDiscussionListResult))) {
            unset($this->CloseDiscussionListResult);
        } else {
            $this->CloseDiscussionListResult = $closeDiscussionListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
