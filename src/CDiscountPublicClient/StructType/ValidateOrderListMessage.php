<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidateOrderListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ValidateOrderListMessage
 * @subpackage Structs
 */
class ValidateOrderListMessage extends AbstractStructBase
{
    /**
     * The OrderList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder
     */
    public $OrderList;
    /**
     * Constructor method for ValidateOrderListMessage
     * @uses ValidateOrderListMessage::setOrderList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder $orderList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder $orderList = null)
    {
        $this
            ->setOrderList($orderList);
    }
    /**
     * Get OrderList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder|null
     */
    public function getOrderList()
    {
        return isset($this->OrderList) ? $this->OrderList : null;
    }
    /**
     * Set OrderList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder $orderList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderListMessage
     */
    public function setOrderList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder $orderList = null)
    {
        if (is_null($orderList) || (is_array($orderList) && empty($orderList))) {
            unset($this->OrderList);
        } else {
            $this->OrderList = $orderList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
