<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CloseDiscussionResultMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:CloseDiscussionResultMessage
 * @subpackage Structs
 */
class CloseDiscussionResultMessage extends ServiceBaseAPIMessage
{
    /**
     * The CloseDiscussionResultList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult
     */
    public $CloseDiscussionResultList;
    /**
     * Constructor method for CloseDiscussionResultMessage
     * @uses CloseDiscussionResultMessage::setCloseDiscussionResultList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult $closeDiscussionResultList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult $closeDiscussionResultList = null)
    {
        $this
            ->setCloseDiscussionResultList($closeDiscussionResultList);
    }
    /**
     * Get CloseDiscussionResultList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult|null
     */
    public function getCloseDiscussionResultList()
    {
        return isset($this->CloseDiscussionResultList) ? $this->CloseDiscussionResultList : null;
    }
    /**
     * Set CloseDiscussionResultList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult $closeDiscussionResultList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage
     */
    public function setCloseDiscussionResultList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult $closeDiscussionResultList = null)
    {
        if (is_null($closeDiscussionResultList) || (is_array($closeDiscussionResultList) && empty($closeDiscussionResultList))) {
            unset($this->CloseDiscussionResultList);
        } else {
            $this->CloseDiscussionResultList = $closeDiscussionResultList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResultMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
