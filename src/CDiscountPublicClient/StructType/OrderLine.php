<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderLine StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderLine
 * @subpackage Structs
 */
class OrderLine extends AbstractStructBase
{
    /**
     * The AcceptationState
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $AcceptationState;
    /**
     * The CategoryCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CategoryCode;
    /**
     * The DeliveryDateMax
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $DeliveryDateMax;
    /**
     * The DeliveryDateMin
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $DeliveryDateMin;
    /**
     * The HasClaim
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $HasClaim;
    /**
     * The InitialPrice
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $InitialPrice;
    /**
     * The IsCDAV
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsCDAV;
    /**
     * The IsNegotiated
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $IsNegotiated;
    /**
     * The IsProductEanGenerated
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsProductEanGenerated;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The OrderLineChildList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderLine
     */
    public $OrderLineChildList;
    /**
     * The ProductCondition
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductCondition;
    /**
     * The ProductEan
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductEan;
    /**
     * The ProductId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductId;
    /**
     * The PurchasePrice
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $PurchasePrice;
    /**
     * The Quantity
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Quantity;
    /**
     * The RefundShippingCharges
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $RefundShippingCharges;
    /**
     * The RowId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $RowId;
    /**
     * The SellerProductId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SellerProductId;
    /**
     * The ShippingDateMax
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ShippingDateMax;
    /**
     * The ShippingDateMin
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ShippingDateMin;
    /**
     * The Sku
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Sku;
    /**
     * The SkuParent
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SkuParent;
    /**
     * The UnitAdditionalShippingCharges
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $UnitAdditionalShippingCharges;
    /**
     * The UnitShippingCharges
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $UnitShippingCharges;
    /**
     * Constructor method for OrderLine
     * @uses OrderLine::setAcceptationState()
     * @uses OrderLine::setCategoryCode()
     * @uses OrderLine::setDeliveryDateMax()
     * @uses OrderLine::setDeliveryDateMin()
     * @uses OrderLine::setHasClaim()
     * @uses OrderLine::setInitialPrice()
     * @uses OrderLine::setIsCDAV()
     * @uses OrderLine::setIsNegotiated()
     * @uses OrderLine::setIsProductEanGenerated()
     * @uses OrderLine::setName()
     * @uses OrderLine::setOrderLineChildList()
     * @uses OrderLine::setProductCondition()
     * @uses OrderLine::setProductEan()
     * @uses OrderLine::setProductId()
     * @uses OrderLine::setPurchasePrice()
     * @uses OrderLine::setQuantity()
     * @uses OrderLine::setRefundShippingCharges()
     * @uses OrderLine::setRowId()
     * @uses OrderLine::setSellerProductId()
     * @uses OrderLine::setShippingDateMax()
     * @uses OrderLine::setShippingDateMin()
     * @uses OrderLine::setSku()
     * @uses OrderLine::setSkuParent()
     * @uses OrderLine::setUnitAdditionalShippingCharges()
     * @uses OrderLine::setUnitShippingCharges()
     * @param string $acceptationState
     * @param string $categoryCode
     * @param string $deliveryDateMax
     * @param string $deliveryDateMin
     * @param bool $hasClaim
     * @param float $initialPrice
     * @param bool $isCDAV
     * @param bool $isNegotiated
     * @param bool $isProductEanGenerated
     * @param string $name
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderLine $orderLineChildList
     * @param string $productCondition
     * @param string $productEan
     * @param string $productId
     * @param float $purchasePrice
     * @param int $quantity
     * @param bool $refundShippingCharges
     * @param int $rowId
     * @param string $sellerProductId
     * @param string $shippingDateMax
     * @param string $shippingDateMin
     * @param string $sku
     * @param string $skuParent
     * @param float $unitAdditionalShippingCharges
     * @param float $unitShippingCharges
     */
    public function __construct($acceptationState = null, $categoryCode = null, $deliveryDateMax = null, $deliveryDateMin = null, $hasClaim = null, $initialPrice = null, $isCDAV = null, $isNegotiated = null, $isProductEanGenerated = null, $name = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderLine $orderLineChildList = null, $productCondition = null, $productEan = null, $productId = null, $purchasePrice = null, $quantity = null, $refundShippingCharges = null, $rowId = null, $sellerProductId = null, $shippingDateMax = null, $shippingDateMin = null, $sku = null, $skuParent = null, $unitAdditionalShippingCharges = null, $unitShippingCharges = null)
    {
        $this
            ->setAcceptationState($acceptationState)
            ->setCategoryCode($categoryCode)
            ->setDeliveryDateMax($deliveryDateMax)
            ->setDeliveryDateMin($deliveryDateMin)
            ->setHasClaim($hasClaim)
            ->setInitialPrice($initialPrice)
            ->setIsCDAV($isCDAV)
            ->setIsNegotiated($isNegotiated)
            ->setIsProductEanGenerated($isProductEanGenerated)
            ->setName($name)
            ->setOrderLineChildList($orderLineChildList)
            ->setProductCondition($productCondition)
            ->setProductEan($productEan)
            ->setProductId($productId)
            ->setPurchasePrice($purchasePrice)
            ->setQuantity($quantity)
            ->setRefundShippingCharges($refundShippingCharges)
            ->setRowId($rowId)
            ->setSellerProductId($sellerProductId)
            ->setShippingDateMax($shippingDateMax)
            ->setShippingDateMin($shippingDateMin)
            ->setSku($sku)
            ->setSkuParent($skuParent)
            ->setUnitAdditionalShippingCharges($unitAdditionalShippingCharges)
            ->setUnitShippingCharges($unitShippingCharges);
    }
    /**
     * Get AcceptationState value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getAcceptationState()
    {
        return isset($this->AcceptationState) ? $this->AcceptationState : null;
    }
    /**
     * Set AcceptationState value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $acceptationState
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setAcceptationState($acceptationState = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::valueIsValid($acceptationState)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\AcceptationStateEnum', is_array($acceptationState) ? implode(', ', $acceptationState) : var_export($acceptationState, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\AcceptationStateEnum::getValidValues())), __LINE__);
        }
        if (is_null($acceptationState) || (is_array($acceptationState) && empty($acceptationState))) {
            unset($this->AcceptationState);
        } else {
            $this->AcceptationState = $acceptationState;
        }
        return $this;
    }
    /**
     * Get CategoryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCategoryCode()
    {
        return isset($this->CategoryCode) ? $this->CategoryCode : null;
    }
    /**
     * Set CategoryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $categoryCode
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setCategoryCode($categoryCode = null)
    {
        // validation for constraint: string
        if (!is_null($categoryCode) && !is_string($categoryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($categoryCode, true), gettype($categoryCode)), __LINE__);
        }
        if (is_null($categoryCode) || (is_array($categoryCode) && empty($categoryCode))) {
            unset($this->CategoryCode);
        } else {
            $this->CategoryCode = $categoryCode;
        }
        return $this;
    }
    /**
     * Get DeliveryDateMax value
     * @return string|null
     */
    public function getDeliveryDateMax()
    {
        return $this->DeliveryDateMax;
    }
    /**
     * Set DeliveryDateMax value
     * @param string $deliveryDateMax
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setDeliveryDateMax($deliveryDateMax = null)
    {
        // validation for constraint: string
        if (!is_null($deliveryDateMax) && !is_string($deliveryDateMax)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryDateMax, true), gettype($deliveryDateMax)), __LINE__);
        }
        $this->DeliveryDateMax = $deliveryDateMax;
        return $this;
    }
    /**
     * Get DeliveryDateMin value
     * @return string|null
     */
    public function getDeliveryDateMin()
    {
        return $this->DeliveryDateMin;
    }
    /**
     * Set DeliveryDateMin value
     * @param string $deliveryDateMin
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setDeliveryDateMin($deliveryDateMin = null)
    {
        // validation for constraint: string
        if (!is_null($deliveryDateMin) && !is_string($deliveryDateMin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryDateMin, true), gettype($deliveryDateMin)), __LINE__);
        }
        $this->DeliveryDateMin = $deliveryDateMin;
        return $this;
    }
    /**
     * Get HasClaim value
     * @return bool|null
     */
    public function getHasClaim()
    {
        return $this->HasClaim;
    }
    /**
     * Set HasClaim value
     * @param bool $hasClaim
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setHasClaim($hasClaim = null)
    {
        // validation for constraint: boolean
        if (!is_null($hasClaim) && !is_bool($hasClaim)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($hasClaim, true), gettype($hasClaim)), __LINE__);
        }
        $this->HasClaim = $hasClaim;
        return $this;
    }
    /**
     * Get InitialPrice value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getInitialPrice()
    {
        return isset($this->InitialPrice) ? $this->InitialPrice : null;
    }
    /**
     * Set InitialPrice value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $initialPrice
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setInitialPrice($initialPrice = null)
    {
        // validation for constraint: float
        if (!is_null($initialPrice) && !(is_float($initialPrice) || is_numeric($initialPrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($initialPrice, true), gettype($initialPrice)), __LINE__);
        }
        if (is_null($initialPrice) || (is_array($initialPrice) && empty($initialPrice))) {
            unset($this->InitialPrice);
        } else {
            $this->InitialPrice = $initialPrice;
        }
        return $this;
    }
    /**
     * Get IsCDAV value
     * @return bool|null
     */
    public function getIsCDAV()
    {
        return $this->IsCDAV;
    }
    /**
     * Set IsCDAV value
     * @param bool $isCDAV
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setIsCDAV($isCDAV = null)
    {
        // validation for constraint: boolean
        if (!is_null($isCDAV) && !is_bool($isCDAV)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isCDAV, true), gettype($isCDAV)), __LINE__);
        }
        $this->IsCDAV = $isCDAV;
        return $this;
    }
    /**
     * Get IsNegotiated value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getIsNegotiated()
    {
        return isset($this->IsNegotiated) ? $this->IsNegotiated : null;
    }
    /**
     * Set IsNegotiated value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $isNegotiated
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setIsNegotiated($isNegotiated = null)
    {
        // validation for constraint: boolean
        if (!is_null($isNegotiated) && !is_bool($isNegotiated)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isNegotiated, true), gettype($isNegotiated)), __LINE__);
        }
        if (is_null($isNegotiated) || (is_array($isNegotiated) && empty($isNegotiated))) {
            unset($this->IsNegotiated);
        } else {
            $this->IsNegotiated = $isNegotiated;
        }
        return $this;
    }
    /**
     * Get IsProductEanGenerated value
     * @return bool|null
     */
    public function getIsProductEanGenerated()
    {
        return $this->IsProductEanGenerated;
    }
    /**
     * Set IsProductEanGenerated value
     * @param bool $isProductEanGenerated
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setIsProductEanGenerated($isProductEanGenerated = null)
    {
        // validation for constraint: boolean
        if (!is_null($isProductEanGenerated) && !is_bool($isProductEanGenerated)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isProductEanGenerated, true), gettype($isProductEanGenerated)), __LINE__);
        }
        $this->IsProductEanGenerated = $isProductEanGenerated;
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Get OrderLineChildList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderLine|null
     */
    public function getOrderLineChildList()
    {
        return isset($this->OrderLineChildList) ? $this->OrderLineChildList : null;
    }
    /**
     * Set OrderLineChildList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderLine $orderLineChildList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setOrderLineChildList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderLine $orderLineChildList = null)
    {
        if (is_null($orderLineChildList) || (is_array($orderLineChildList) && empty($orderLineChildList))) {
            unset($this->OrderLineChildList);
        } else {
            $this->OrderLineChildList = $orderLineChildList;
        }
        return $this;
    }
    /**
     * Get ProductCondition value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductCondition()
    {
        return isset($this->ProductCondition) ? $this->ProductCondition : null;
    }
    /**
     * Set ProductCondition value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $productCondition
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setProductCondition($productCondition = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::valueIsValid($productCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ProductConditionEnum', is_array($productCondition) ? implode(', ', $productCondition) : var_export($productCondition, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::getValidValues())), __LINE__);
        }
        if (is_null($productCondition) || (is_array($productCondition) && empty($productCondition))) {
            unset($this->ProductCondition);
        } else {
            $this->ProductCondition = $productCondition;
        }
        return $this;
    }
    /**
     * Get ProductEan value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductEan()
    {
        return isset($this->ProductEan) ? $this->ProductEan : null;
    }
    /**
     * Set ProductEan value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productEan
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setProductEan($productEan = null)
    {
        // validation for constraint: string
        if (!is_null($productEan) && !is_string($productEan)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productEan, true), gettype($productEan)), __LINE__);
        }
        if (is_null($productEan) || (is_array($productEan) && empty($productEan))) {
            unset($this->ProductEan);
        } else {
            $this->ProductEan = $productEan;
        }
        return $this;
    }
    /**
     * Get ProductId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductId()
    {
        return isset($this->ProductId) ? $this->ProductId : null;
    }
    /**
     * Set ProductId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: string
        if (!is_null($productId) && !is_string($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productId, true), gettype($productId)), __LINE__);
        }
        if (is_null($productId) || (is_array($productId) && empty($productId))) {
            unset($this->ProductId);
        } else {
            $this->ProductId = $productId;
        }
        return $this;
    }
    /**
     * Get PurchasePrice value
     * @return float|null
     */
    public function getPurchasePrice()
    {
        return $this->PurchasePrice;
    }
    /**
     * Set PurchasePrice value
     * @param float $purchasePrice
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setPurchasePrice($purchasePrice = null)
    {
        // validation for constraint: float
        if (!is_null($purchasePrice) && !(is_float($purchasePrice) || is_numeric($purchasePrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($purchasePrice, true), gettype($purchasePrice)), __LINE__);
        }
        $this->PurchasePrice = $purchasePrice;
        return $this;
    }
    /**
     * Get Quantity value
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->Quantity;
    }
    /**
     * Set Quantity value
     * @param int $quantity
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setQuantity($quantity = null)
    {
        // validation for constraint: int
        if (!is_null($quantity) && !(is_int($quantity) || ctype_digit($quantity))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->Quantity = $quantity;
        return $this;
    }
    /**
     * Get RefundShippingCharges value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getRefundShippingCharges()
    {
        return isset($this->RefundShippingCharges) ? $this->RefundShippingCharges : null;
    }
    /**
     * Set RefundShippingCharges value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $refundShippingCharges
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setRefundShippingCharges($refundShippingCharges = null)
    {
        // validation for constraint: boolean
        if (!is_null($refundShippingCharges) && !is_bool($refundShippingCharges)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($refundShippingCharges, true), gettype($refundShippingCharges)), __LINE__);
        }
        if (is_null($refundShippingCharges) || (is_array($refundShippingCharges) && empty($refundShippingCharges))) {
            unset($this->RefundShippingCharges);
        } else {
            $this->RefundShippingCharges = $refundShippingCharges;
        }
        return $this;
    }
    /**
     * Get RowId value
     * @return int|null
     */
    public function getRowId()
    {
        return $this->RowId;
    }
    /**
     * Set RowId value
     * @param int $rowId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setRowId($rowId = null)
    {
        // validation for constraint: int
        if (!is_null($rowId) && !(is_int($rowId) || ctype_digit($rowId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($rowId, true), gettype($rowId)), __LINE__);
        }
        $this->RowId = $rowId;
        return $this;
    }
    /**
     * Get SellerProductId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSellerProductId()
    {
        return isset($this->SellerProductId) ? $this->SellerProductId : null;
    }
    /**
     * Set SellerProductId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sellerProductId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setSellerProductId($sellerProductId = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductId) && !is_string($sellerProductId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductId, true), gettype($sellerProductId)), __LINE__);
        }
        if (is_null($sellerProductId) || (is_array($sellerProductId) && empty($sellerProductId))) {
            unset($this->SellerProductId);
        } else {
            $this->SellerProductId = $sellerProductId;
        }
        return $this;
    }
    /**
     * Get ShippingDateMax value
     * @return string|null
     */
    public function getShippingDateMax()
    {
        return $this->ShippingDateMax;
    }
    /**
     * Set ShippingDateMax value
     * @param string $shippingDateMax
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setShippingDateMax($shippingDateMax = null)
    {
        // validation for constraint: string
        if (!is_null($shippingDateMax) && !is_string($shippingDateMax)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shippingDateMax, true), gettype($shippingDateMax)), __LINE__);
        }
        $this->ShippingDateMax = $shippingDateMax;
        return $this;
    }
    /**
     * Get ShippingDateMin value
     * @return string|null
     */
    public function getShippingDateMin()
    {
        return $this->ShippingDateMin;
    }
    /**
     * Set ShippingDateMin value
     * @param string $shippingDateMin
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setShippingDateMin($shippingDateMin = null)
    {
        // validation for constraint: string
        if (!is_null($shippingDateMin) && !is_string($shippingDateMin)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shippingDateMin, true), gettype($shippingDateMin)), __LINE__);
        }
        $this->ShippingDateMin = $shippingDateMin;
        return $this;
    }
    /**
     * Get Sku value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSku()
    {
        return isset($this->Sku) ? $this->Sku : null;
    }
    /**
     * Set Sku value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sku
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setSku($sku = null)
    {
        // validation for constraint: string
        if (!is_null($sku) && !is_string($sku)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sku, true), gettype($sku)), __LINE__);
        }
        if (is_null($sku) || (is_array($sku) && empty($sku))) {
            unset($this->Sku);
        } else {
            $this->Sku = $sku;
        }
        return $this;
    }
    /**
     * Get SkuParent value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSkuParent()
    {
        return isset($this->SkuParent) ? $this->SkuParent : null;
    }
    /**
     * Set SkuParent value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $skuParent
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setSkuParent($skuParent = null)
    {
        // validation for constraint: string
        if (!is_null($skuParent) && !is_string($skuParent)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($skuParent, true), gettype($skuParent)), __LINE__);
        }
        if (is_null($skuParent) || (is_array($skuParent) && empty($skuParent))) {
            unset($this->SkuParent);
        } else {
            $this->SkuParent = $skuParent;
        }
        return $this;
    }
    /**
     * Get UnitAdditionalShippingCharges value
     * @return float|null
     */
    public function getUnitAdditionalShippingCharges()
    {
        return $this->UnitAdditionalShippingCharges;
    }
    /**
     * Set UnitAdditionalShippingCharges value
     * @param float $unitAdditionalShippingCharges
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setUnitAdditionalShippingCharges($unitAdditionalShippingCharges = null)
    {
        // validation for constraint: float
        if (!is_null($unitAdditionalShippingCharges) && !(is_float($unitAdditionalShippingCharges) || is_numeric($unitAdditionalShippingCharges))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($unitAdditionalShippingCharges, true), gettype($unitAdditionalShippingCharges)), __LINE__);
        }
        $this->UnitAdditionalShippingCharges = $unitAdditionalShippingCharges;
        return $this;
    }
    /**
     * Get UnitShippingCharges value
     * @return float|null
     */
    public function getUnitShippingCharges()
    {
        return $this->UnitShippingCharges;
    }
    /**
     * Set UnitShippingCharges value
     * @param float $unitShippingCharges
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public function setUnitShippingCharges($unitShippingCharges = null)
    {
        // validation for constraint: float
        if (!is_null($unitShippingCharges) && !(is_float($unitShippingCharges) || is_numeric($unitShippingCharges))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($unitShippingCharges, true), gettype($unitShippingCharges)), __LINE__);
        }
        $this->UnitShippingCharges = $unitShippingCharges;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderLine
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
