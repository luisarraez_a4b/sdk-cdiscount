<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GlobalConfigurationMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:GlobalConfigurationMessage
 * @subpackage Structs
 */
class GlobalConfigurationMessage extends ServiceBaseAPIMessage
{
    /**
     * The CarrierList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCarrier
     */
    public $CarrierList;
    /**
     * Constructor method for GlobalConfigurationMessage
     * @uses GlobalConfigurationMessage::setCarrierList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCarrier $carrierList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCarrier $carrierList = null)
    {
        $this
            ->setCarrierList($carrierList);
    }
    /**
     * Get CarrierList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCarrier|null
     */
    public function getCarrierList()
    {
        return isset($this->CarrierList) ? $this->CarrierList : null;
    }
    /**
     * Set CarrierList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCarrier $carrierList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage
     */
    public function setCarrierList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCarrier $carrierList = null)
    {
        if (is_null($carrierList) || (is_array($carrierList) && empty($carrierList))) {
            unset($this->CarrierList);
        } else {
            $this->CarrierList = $carrierList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GlobalConfigurationMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
