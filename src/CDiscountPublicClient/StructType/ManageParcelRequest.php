<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ManageParcelRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ManageParcelRequest
 * @subpackage Structs
 */
class ManageParcelRequest extends AbstractStructBase
{
    /**
     * The ParcelActionsList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelInfos
     */
    public $ParcelActionsList;
    /**
     * The ScopusId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ScopusId;
    /**
     * Constructor method for ManageParcelRequest
     * @uses ManageParcelRequest::setParcelActionsList()
     * @uses ManageParcelRequest::setScopusId()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelInfos $parcelActionsList
     * @param string $scopusId
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelInfos $parcelActionsList = null, $scopusId = null)
    {
        $this
            ->setParcelActionsList($parcelActionsList)
            ->setScopusId($scopusId);
    }
    /**
     * Get ParcelActionsList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelInfos|null
     */
    public function getParcelActionsList()
    {
        return isset($this->ParcelActionsList) ? $this->ParcelActionsList : null;
    }
    /**
     * Set ParcelActionsList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelInfos $parcelActionsList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcelRequest
     */
    public function setParcelActionsList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelInfos $parcelActionsList = null)
    {
        if (is_null($parcelActionsList) || (is_array($parcelActionsList) && empty($parcelActionsList))) {
            unset($this->ParcelActionsList);
        } else {
            $this->ParcelActionsList = $parcelActionsList;
        }
        return $this;
    }
    /**
     * Get ScopusId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getScopusId()
    {
        return isset($this->ScopusId) ? $this->ScopusId : null;
    }
    /**
     * Set ScopusId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $scopusId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcelRequest
     */
    public function setScopusId($scopusId = null)
    {
        // validation for constraint: string
        if (!is_null($scopusId) && !is_string($scopusId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($scopusId, true), gettype($scopusId)), __LINE__);
        }
        if (is_null($scopusId) || (is_array($scopusId) && empty($scopusId))) {
            unset($this->ScopusId);
        } else {
            $this->ScopusId = $scopusId;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcelRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
