<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductListByIdentifierMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductListByIdentifierMessage
 * @subpackage Structs
 */
class ProductListByIdentifierMessage extends ServiceBaseAPIMessage
{
    /**
     * The ProductListByIdentifier
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductByIdentifier
     */
    public $ProductListByIdentifier;
    /**
     * Constructor method for ProductListByIdentifierMessage
     * @uses ProductListByIdentifierMessage::setProductListByIdentifier()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductByIdentifier $productListByIdentifier
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductByIdentifier $productListByIdentifier = null)
    {
        $this
            ->setProductListByIdentifier($productListByIdentifier);
    }
    /**
     * Get ProductListByIdentifier value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductByIdentifier|null
     */
    public function getProductListByIdentifier()
    {
        return isset($this->ProductListByIdentifier) ? $this->ProductListByIdentifier : null;
    }
    /**
     * Set ProductListByIdentifier value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductByIdentifier $productListByIdentifier
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage
     */
    public function setProductListByIdentifier(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductByIdentifier $productListByIdentifier = null)
    {
        if (is_null($productListByIdentifier) || (is_array($productListByIdentifier) && empty($productListByIdentifier))) {
            unset($this->ProductListByIdentifier);
        } else {
            $this->ProductListByIdentifier = $productListByIdentifier;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListByIdentifierMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
