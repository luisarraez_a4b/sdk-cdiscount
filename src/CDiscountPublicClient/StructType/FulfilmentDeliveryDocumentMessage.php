<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentDeliveryDocumentMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentDeliveryDocumentMessage
 * @subpackage Structs
 */
class FulfilmentDeliveryDocumentMessage extends ServiceBaseAPIMessage
{
    /**
     * The PdfDocument
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PdfDocument;
    /**
     * Constructor method for FulfilmentDeliveryDocumentMessage
     * @uses FulfilmentDeliveryDocumentMessage::setPdfDocument()
     * @param string $pdfDocument
     */
    public function __construct($pdfDocument = null)
    {
        $this
            ->setPdfDocument($pdfDocument);
    }
    /**
     * Get PdfDocument value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPdfDocument()
    {
        return isset($this->PdfDocument) ? $this->PdfDocument : null;
    }
    /**
     * Set PdfDocument value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $pdfDocument
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentDeliveryDocumentMessage
     */
    public function setPdfDocument($pdfDocument = null)
    {
        // validation for constraint: string
        if (!is_null($pdfDocument) && !is_string($pdfDocument)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pdfDocument, true), gettype($pdfDocument)), __LINE__);
        }
        if (is_null($pdfDocument) || (is_array($pdfDocument) && empty($pdfDocument))) {
            unset($this->PdfDocument);
        } else {
            $this->PdfDocument = $pdfDocument;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentDeliveryDocumentMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
