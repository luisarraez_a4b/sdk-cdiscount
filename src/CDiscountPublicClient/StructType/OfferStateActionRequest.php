<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OfferStateActionRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferStateActionRequest
 * @subpackage Structs
 */
class OfferStateActionRequest extends AbstractStructBase
{
    /**
     * The Action
     * @var string
     */
    public $Action;
    /**
     * The SellerProductId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SellerProductId;
    /**
     * Constructor method for OfferStateActionRequest
     * @uses OfferStateActionRequest::setAction()
     * @uses OfferStateActionRequest::setSellerProductId()
     * @param string $action
     * @param string $sellerProductId
     */
    public function __construct($action = null, $sellerProductId = null)
    {
        $this
            ->setAction($action)
            ->setSellerProductId($sellerProductId);
    }
    /**
     * Get Action value
     * @return string|null
     */
    public function getAction()
    {
        return $this->Action;
    }
    /**
     * Set Action value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateActionType::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateActionType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $action
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateActionRequest
     */
    public function setAction($action = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateActionType::valueIsValid($action)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\OfferStateActionType', is_array($action) ? implode(', ', $action) : var_export($action, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateActionType::getValidValues())), __LINE__);
        }
        $this->Action = $action;
        return $this;
    }
    /**
     * Get SellerProductId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSellerProductId()
    {
        return isset($this->SellerProductId) ? $this->SellerProductId : null;
    }
    /**
     * Set SellerProductId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sellerProductId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateActionRequest
     */
    public function setSellerProductId($sellerProductId = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductId) && !is_string($sellerProductId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductId, true), gettype($sellerProductId)), __LINE__);
        }
        if (is_null($sellerProductId) || (is_array($sellerProductId) && empty($sellerProductId))) {
            unset($this->SellerProductId);
        } else {
            $this->SellerProductId = $sellerProductId;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferStateActionRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
