<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductListResponse StructType
 * @subpackage Structs
 */
class GetProductListResponse extends AbstractStructBase
{
    /**
     * The GetProductListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage
     */
    public $GetProductListResult;
    /**
     * Constructor method for GetProductListResponse
     * @uses GetProductListResponse::setGetProductListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage $getProductListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage $getProductListResult = null)
    {
        $this
            ->setGetProductListResult($getProductListResult);
    }
    /**
     * Get GetProductListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage|null
     */
    public function getGetProductListResult()
    {
        return isset($this->GetProductListResult) ? $this->GetProductListResult : null;
    }
    /**
     * Set GetProductListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage $getProductListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListResponse
     */
    public function setGetProductListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage $getProductListResult = null)
    {
        if (is_null($getProductListResult) || (is_array($getProductListResult) && empty($getProductListResult))) {
            unset($this->GetProductListResult);
        } else {
            $this->GetProductListResult = $getProductListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
