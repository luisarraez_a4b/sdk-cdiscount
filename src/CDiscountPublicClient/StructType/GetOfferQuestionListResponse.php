<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOfferQuestionListResponse StructType
 * @subpackage Structs
 */
class GetOfferQuestionListResponse extends AbstractStructBase
{
    /**
     * The GetOfferQuestionListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionListMessage
     */
    public $GetOfferQuestionListResult;
    /**
     * Constructor method for GetOfferQuestionListResponse
     * @uses GetOfferQuestionListResponse::setGetOfferQuestionListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionListMessage $getOfferQuestionListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionListMessage $getOfferQuestionListResult = null)
    {
        $this
            ->setGetOfferQuestionListResult($getOfferQuestionListResult);
    }
    /**
     * Get GetOfferQuestionListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionListMessage|null
     */
    public function getGetOfferQuestionListResult()
    {
        return isset($this->GetOfferQuestionListResult) ? $this->GetOfferQuestionListResult : null;
    }
    /**
     * Set GetOfferQuestionListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionListMessage $getOfferQuestionListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferQuestionListResponse
     */
    public function setGetOfferQuestionListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferQuestionListMessage $getOfferQuestionListResult = null)
    {
        if (is_null($getOfferQuestionListResult) || (is_array($getOfferQuestionListResult) && empty($getOfferQuestionListResult))) {
            unset($this->GetOfferQuestionListResult);
        } else {
            $this->GetOfferQuestionListResult = $getOfferQuestionListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferQuestionListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
