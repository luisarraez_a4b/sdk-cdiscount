<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentActivationRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentActivationRequest
 * @subpackage Structs
 */
class FulfilmentActivationRequest extends AbstractStructBase
{
    /**
     * The ProductList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductActivationData
     */
    public $ProductList;
    /**
     * Constructor method for FulfilmentActivationRequest
     * @uses FulfilmentActivationRequest::setProductList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductActivationData $productList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductActivationData $productList = null)
    {
        $this
            ->setProductList($productList);
    }
    /**
     * Get ProductList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductActivationData|null
     */
    public function getProductList()
    {
        return isset($this->ProductList) ? $this->ProductList : null;
    }
    /**
     * Set ProductList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductActivationData $productList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationRequest
     */
    public function setProductList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductActivationData $productList = null)
    {
        if (is_null($productList) || (is_array($productList) && empty($productList))) {
            unset($this->ProductList);
        } else {
            $this->ProductList = $productList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
