<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidateOrder StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ValidateOrder
 * @subpackage Structs
 */
class ValidateOrder extends AbstractStructBase
{
    /**
     * The CarrierName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CarrierName;
    /**
     * The OrderLineList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLine
     */
    public $OrderLineList;
    /**
     * The OrderNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrderNumber;
    /**
     * The OrderState
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $OrderState;
    /**
     * The TrackingNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $TrackingNumber;
    /**
     * The TrackingUrl
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $TrackingUrl;
    /**
     * Constructor method for ValidateOrder
     * @uses ValidateOrder::setCarrierName()
     * @uses ValidateOrder::setOrderLineList()
     * @uses ValidateOrder::setOrderNumber()
     * @uses ValidateOrder::setOrderState()
     * @uses ValidateOrder::setTrackingNumber()
     * @uses ValidateOrder::setTrackingUrl()
     * @param string $carrierName
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLine $orderLineList
     * @param string $orderNumber
     * @param string $orderState
     * @param string $trackingNumber
     * @param string $trackingUrl
     */
    public function __construct($carrierName = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLine $orderLineList = null, $orderNumber = null, $orderState = null, $trackingNumber = null, $trackingUrl = null)
    {
        $this
            ->setCarrierName($carrierName)
            ->setOrderLineList($orderLineList)
            ->setOrderNumber($orderNumber)
            ->setOrderState($orderState)
            ->setTrackingNumber($trackingNumber)
            ->setTrackingUrl($trackingUrl);
    }
    /**
     * Get CarrierName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCarrierName()
    {
        return isset($this->CarrierName) ? $this->CarrierName : null;
    }
    /**
     * Set CarrierName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $carrierName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder
     */
    public function setCarrierName($carrierName = null)
    {
        // validation for constraint: string
        if (!is_null($carrierName) && !is_string($carrierName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($carrierName, true), gettype($carrierName)), __LINE__);
        }
        if (is_null($carrierName) || (is_array($carrierName) && empty($carrierName))) {
            unset($this->CarrierName);
        } else {
            $this->CarrierName = $carrierName;
        }
        return $this;
    }
    /**
     * Get OrderLineList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLine|null
     */
    public function getOrderLineList()
    {
        return isset($this->OrderLineList) ? $this->OrderLineList : null;
    }
    /**
     * Set OrderLineList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLine $orderLineList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder
     */
    public function setOrderLineList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLine $orderLineList = null)
    {
        if (is_null($orderLineList) || (is_array($orderLineList) && empty($orderLineList))) {
            unset($this->OrderLineList);
        } else {
            $this->OrderLineList = $orderLineList;
        }
        return $this;
    }
    /**
     * Get OrderNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrderNumber()
    {
        return isset($this->OrderNumber) ? $this->OrderNumber : null;
    }
    /**
     * Set OrderNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder
     */
    public function setOrderNumber($orderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($orderNumber) && !is_string($orderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderNumber, true), gettype($orderNumber)), __LINE__);
        }
        if (is_null($orderNumber) || (is_array($orderNumber) && empty($orderNumber))) {
            unset($this->OrderNumber);
        } else {
            $this->OrderNumber = $orderNumber;
        }
        return $this;
    }
    /**
     * Get OrderState value
     * @return string|null
     */
    public function getOrderState()
    {
        return $this->OrderState;
    }
    /**
     * Set OrderState value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OrderStateEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OrderStateEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $orderState
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder
     */
    public function setOrderState($orderState = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\OrderStateEnum::valueIsValid($orderState)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\OrderStateEnum', is_array($orderState) ? implode(', ', $orderState) : var_export($orderState, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\OrderStateEnum::getValidValues())), __LINE__);
        }
        $this->OrderState = $orderState;
        return $this;
    }
    /**
     * Get TrackingNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTrackingNumber()
    {
        return isset($this->TrackingNumber) ? $this->TrackingNumber : null;
    }
    /**
     * Set TrackingNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $trackingNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder
     */
    public function setTrackingNumber($trackingNumber = null)
    {
        // validation for constraint: string
        if (!is_null($trackingNumber) && !is_string($trackingNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($trackingNumber, true), gettype($trackingNumber)), __LINE__);
        }
        if (is_null($trackingNumber) || (is_array($trackingNumber) && empty($trackingNumber))) {
            unset($this->TrackingNumber);
        } else {
            $this->TrackingNumber = $trackingNumber;
        }
        return $this;
    }
    /**
     * Get TrackingUrl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getTrackingUrl()
    {
        return isset($this->TrackingUrl) ? $this->TrackingUrl : null;
    }
    /**
     * Set TrackingUrl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $trackingUrl
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder
     */
    public function setTrackingUrl($trackingUrl = null)
    {
        // validation for constraint: string
        if (!is_null($trackingUrl) && !is_string($trackingUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($trackingUrl, true), gettype($trackingUrl)), __LINE__);
        }
        if (is_null($trackingUrl) || (is_array($trackingUrl) && empty($trackingUrl))) {
            unset($this->TrackingUrl);
        } else {
            $this->TrackingUrl = $trackingUrl;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
