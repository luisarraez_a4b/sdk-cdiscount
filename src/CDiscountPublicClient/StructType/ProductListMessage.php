<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductListMessage
 * @subpackage Structs
 */
class ProductListMessage extends ServiceBaseAPIMessage
{
    /**
     * The ProductList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProduct
     */
    public $ProductList;
    /**
     * Constructor method for ProductListMessage
     * @uses ProductListMessage::setProductList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProduct $productList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProduct $productList = null)
    {
        $this
            ->setProductList($productList);
    }
    /**
     * Get ProductList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProduct|null
     */
    public function getProductList()
    {
        return isset($this->ProductList) ? $this->ProductList : null;
    }
    /**
     * Set ProductList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProduct $productList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage
     */
    public function setProductList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProduct $productList = null)
    {
        if (is_null($productList) || (is_array($productList) && empty($productList))) {
            unset($this->ProductList);
        } else {
            $this->ProductList = $productList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
