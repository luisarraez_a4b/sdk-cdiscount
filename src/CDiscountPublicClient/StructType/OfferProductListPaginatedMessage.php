<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OfferProductListPaginatedMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferProductListPaginatedMessage
 * @subpackage Structs
 */
class OfferProductListPaginatedMessage extends ServiceBaseAPIMessage
{
    /**
     * The CurrentPageNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $CurrentPageNumber;
    /**
     * The NumberOfPages
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $NumberOfPages;
    /**
     * The OfferProductList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct
     */
    public $OfferProductList;
    /**
     * The OfferTotalCount
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $OfferTotalCount;
    /**
     * Constructor method for OfferProductListPaginatedMessage
     * @uses OfferProductListPaginatedMessage::setCurrentPageNumber()
     * @uses OfferProductListPaginatedMessage::setNumberOfPages()
     * @uses OfferProductListPaginatedMessage::setOfferProductList()
     * @uses OfferProductListPaginatedMessage::setOfferTotalCount()
     * @param int $currentPageNumber
     * @param int $numberOfPages
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct $offerProductList
     * @param int $offerTotalCount
     */
    public function __construct($currentPageNumber = null, $numberOfPages = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct $offerProductList = null, $offerTotalCount = null)
    {
        $this
            ->setCurrentPageNumber($currentPageNumber)
            ->setNumberOfPages($numberOfPages)
            ->setOfferProductList($offerProductList)
            ->setOfferTotalCount($offerTotalCount);
    }
    /**
     * Get CurrentPageNumber value
     * @return int|null
     */
    public function getCurrentPageNumber()
    {
        return $this->CurrentPageNumber;
    }
    /**
     * Set CurrentPageNumber value
     * @param int $currentPageNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductListPaginatedMessage
     */
    public function setCurrentPageNumber($currentPageNumber = null)
    {
        // validation for constraint: int
        if (!is_null($currentPageNumber) && !(is_int($currentPageNumber) || ctype_digit($currentPageNumber))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($currentPageNumber, true), gettype($currentPageNumber)), __LINE__);
        }
        $this->CurrentPageNumber = $currentPageNumber;
        return $this;
    }
    /**
     * Get NumberOfPages value
     * @return int|null
     */
    public function getNumberOfPages()
    {
        return $this->NumberOfPages;
    }
    /**
     * Set NumberOfPages value
     * @param int $numberOfPages
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductListPaginatedMessage
     */
    public function setNumberOfPages($numberOfPages = null)
    {
        // validation for constraint: int
        if (!is_null($numberOfPages) && !(is_int($numberOfPages) || ctype_digit($numberOfPages))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($numberOfPages, true), gettype($numberOfPages)), __LINE__);
        }
        $this->NumberOfPages = $numberOfPages;
        return $this;
    }
    /**
     * Get OfferProductList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct|null
     */
    public function getOfferProductList()
    {
        return isset($this->OfferProductList) ? $this->OfferProductList : null;
    }
    /**
     * Set OfferProductList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct $offerProductList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductListPaginatedMessage
     */
    public function setOfferProductList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct $offerProductList = null)
    {
        if (is_null($offerProductList) || (is_array($offerProductList) && empty($offerProductList))) {
            unset($this->OfferProductList);
        } else {
            $this->OfferProductList = $offerProductList;
        }
        return $this;
    }
    /**
     * Get OfferTotalCount value
     * @return int|null
     */
    public function getOfferTotalCount()
    {
        return $this->OfferTotalCount;
    }
    /**
     * Set OfferTotalCount value
     * @param int $offerTotalCount
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductListPaginatedMessage
     */
    public function setOfferTotalCount($offerTotalCount = null)
    {
        // validation for constraint: int
        if (!is_null($offerTotalCount) && !(is_int($offerTotalCount) || ctype_digit($offerTotalCount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($offerTotalCount, true), gettype($offerTotalCount)), __LINE__);
        }
        $this->OfferTotalCount = $offerTotalCount;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductListPaginatedMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
