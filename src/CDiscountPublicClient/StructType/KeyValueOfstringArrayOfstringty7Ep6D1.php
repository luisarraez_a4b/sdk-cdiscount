<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for KeyValueOfstringArrayOfstringty7Ep6D1 StructType
 * @subpackage Structs
 */
class KeyValueOfstringArrayOfstringty7Ep6D1 extends AbstractStructBase
{
    /**
     * The Key
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Key;
    /**
     * The Value
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $Value;
    /**
     * Constructor method for KeyValueOfstringArrayOfstringty7Ep6D1
     * @uses KeyValueOfstringArrayOfstringty7Ep6D1::setKey()
     * @uses KeyValueOfstringArrayOfstringty7Ep6D1::setValue()
     * @param string $key
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $value
     */
    public function __construct($key = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $value = null)
    {
        $this
            ->setKey($key)
            ->setValue($value);
    }
    /**
     * Get Key value
     * @return string|null
     */
    public function getKey()
    {
        return $this->Key;
    }
    /**
     * Set Key value
     * @param string $key
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\KeyValueOfstringArrayOfstringty7Ep6D1
     */
    public function setKey($key = null)
    {
        // validation for constraint: string
        if (!is_null($key) && !is_string($key)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        $this->Key = $key;
        return $this;
    }
    /**
     * Get Value value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getValue()
    {
        return $this->Value;
    }
    /**
     * Set Value value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\KeyValueOfstringArrayOfstringty7Ep6D1
     */
    public function setValue(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $value = null)
    {
        $this->Value = $value;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\KeyValueOfstringArrayOfstringty7Ep6D1
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
