<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GenerateDiscussionMailGuidResponse StructType
 * @subpackage Structs
 */
class GenerateDiscussionMailGuidResponse extends AbstractStructBase
{
    /**
     * The GenerateDiscussionMailGuidResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage
     */
    public $GenerateDiscussionMailGuidResult;
    /**
     * Constructor method for GenerateDiscussionMailGuidResponse
     * @uses GenerateDiscussionMailGuidResponse::setGenerateDiscussionMailGuidResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage $generateDiscussionMailGuidResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage $generateDiscussionMailGuidResult = null)
    {
        $this
            ->setGenerateDiscussionMailGuidResult($generateDiscussionMailGuidResult);
    }
    /**
     * Get GenerateDiscussionMailGuidResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage|null
     */
    public function getGenerateDiscussionMailGuidResult()
    {
        return isset($this->GenerateDiscussionMailGuidResult) ? $this->GenerateDiscussionMailGuidResult : null;
    }
    /**
     * Set GenerateDiscussionMailGuidResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage $generateDiscussionMailGuidResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GenerateDiscussionMailGuidResponse
     */
    public function setGenerateDiscussionMailGuidResult(\A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage $generateDiscussionMailGuidResult = null)
    {
        if (is_null($generateDiscussionMailGuidResult) || (is_array($generateDiscussionMailGuidResult) && empty($generateDiscussionMailGuidResult))) {
            unset($this->GenerateDiscussionMailGuidResult);
        } else {
            $this->GenerateDiscussionMailGuidResult = $generateDiscussionMailGuidResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GenerateDiscussionMailGuidResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
