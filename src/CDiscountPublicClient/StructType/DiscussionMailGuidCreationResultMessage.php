<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DiscussionMailGuidCreationResultMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscussionMailGuidCreationResultMessage
 * @subpackage Structs
 */
class DiscussionMailGuidCreationResultMessage extends ServiceBaseAPIMessage
{
    /**
     * The MailGuid
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $MailGuid;
    /**
     * Constructor method for DiscussionMailGuidCreationResultMessage
     * @uses DiscussionMailGuidCreationResultMessage::setMailGuid()
     * @param string $mailGuid
     */
    public function __construct($mailGuid = null)
    {
        $this
            ->setMailGuid($mailGuid);
    }
    /**
     * Get MailGuid value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMailGuid()
    {
        return isset($this->MailGuid) ? $this->MailGuid : null;
    }
    /**
     * Set MailGuid value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mailGuid
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage
     */
    public function setMailGuid($mailGuid = null)
    {
        // validation for constraint: string
        if (!is_null($mailGuid) && !is_string($mailGuid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mailGuid, true), gettype($mailGuid)), __LINE__);
        }
        if (is_null($mailGuid) || (is_array($mailGuid) && empty($mailGuid))) {
            unset($this->MailGuid);
        } else {
            $this->MailGuid = $mailGuid;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMailGuidCreationResultMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
