<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetOfferProductListPaginatedRest StructType
 * @subpackage Structs
 */
class GetOfferProductListPaginatedRest extends AbstractStructBase
{
    /**
     * The headerMessage
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage
     */
    public $headerMessage;
    /**
     * The offerProductfilter
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public $offerProductfilter;
    /**
     * Constructor method for GetOfferProductListPaginatedRest
     * @uses GetOfferProductListPaginatedRest::setHeaderMessage()
     * @uses GetOfferProductListPaginatedRest::setOfferProductfilter()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated $offerProductfilter
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage = null, \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated $offerProductfilter = null)
    {
        $this
            ->setHeaderMessage($headerMessage)
            ->setOfferProductfilter($offerProductfilter);
    }
    /**
     * Get headerMessage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage|null
     */
    public function getHeaderMessage()
    {
        return isset($this->headerMessage) ? $this->headerMessage : null;
    }
    /**
     * Set headerMessage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferProductListPaginatedRest
     */
    public function setHeaderMessage(\A4BGroup\Client\CDiscountPublicClient\StructType\HeaderMessage $headerMessage = null)
    {
        if (is_null($headerMessage) || (is_array($headerMessage) && empty($headerMessage))) {
            unset($this->headerMessage);
        } else {
            $this->headerMessage = $headerMessage;
        }
        return $this;
    }
    /**
     * Get offerProductfilter value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated|null
     */
    public function getOfferProductfilter()
    {
        return isset($this->offerProductfilter) ? $this->offerProductfilter : null;
    }
    /**
     * Set offerProductfilter value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated $offerProductfilter
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferProductListPaginatedRest
     */
    public function setOfferProductfilter(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated $offerProductfilter = null)
    {
        if (is_null($offerProductfilter) || (is_array($offerProductfilter) && empty($offerProductfilter))) {
            unset($this->offerProductfilter);
        } else {
            $this->offerProductfilter = $offerProductfilter;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferProductListPaginatedRest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
