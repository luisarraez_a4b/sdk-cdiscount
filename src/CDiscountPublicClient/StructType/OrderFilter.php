<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OrderFilter StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderFilter
 * @subpackage Structs
 */
class OrderFilter extends AbstractStructBase
{
    /**
     * The BeginCreationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BeginCreationDate;
    /**
     * The BeginModificationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BeginModificationDate;
    /**
     * The CorporationCode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CorporationCode;
    /**
     * The EndCreationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EndCreationDate;
    /**
     * The EndModificationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EndModificationDate;
    /**
     * The FetchOrderLines
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $FetchOrderLines;
    /**
     * The FetchParcels
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $FetchParcels;
    /**
     * The IncludeExternalFbcSiteId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IncludeExternalFbcSiteId;
    /**
     * The OrderReferenceList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $OrderReferenceList;
    /**
     * The OrderType
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrderType;
    /**
     * The PartnerOrderRef
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PartnerOrderRef;
    /**
     * The States
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderStateEnum
     */
    public $States;
    /**
     * Constructor method for OrderFilter
     * @uses OrderFilter::setBeginCreationDate()
     * @uses OrderFilter::setBeginModificationDate()
     * @uses OrderFilter::setCorporationCode()
     * @uses OrderFilter::setEndCreationDate()
     * @uses OrderFilter::setEndModificationDate()
     * @uses OrderFilter::setFetchOrderLines()
     * @uses OrderFilter::setFetchParcels()
     * @uses OrderFilter::setIncludeExternalFbcSiteId()
     * @uses OrderFilter::setOrderReferenceList()
     * @uses OrderFilter::setOrderType()
     * @uses OrderFilter::setPartnerOrderRef()
     * @uses OrderFilter::setStates()
     * @param string $beginCreationDate
     * @param string $beginModificationDate
     * @param string $corporationCode
     * @param string $endCreationDate
     * @param string $endModificationDate
     * @param bool $fetchOrderLines
     * @param bool $fetchParcels
     * @param bool $includeExternalFbcSiteId
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderReferenceList
     * @param string $orderType
     * @param string $partnerOrderRef
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderStateEnum $states
     */
    public function __construct($beginCreationDate = null, $beginModificationDate = null, $corporationCode = null, $endCreationDate = null, $endModificationDate = null, $fetchOrderLines = null, $fetchParcels = null, $includeExternalFbcSiteId = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderReferenceList = null, $orderType = null, $partnerOrderRef = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderStateEnum $states = null)
    {
        $this
            ->setBeginCreationDate($beginCreationDate)
            ->setBeginModificationDate($beginModificationDate)
            ->setCorporationCode($corporationCode)
            ->setEndCreationDate($endCreationDate)
            ->setEndModificationDate($endModificationDate)
            ->setFetchOrderLines($fetchOrderLines)
            ->setFetchParcels($fetchParcels)
            ->setIncludeExternalFbcSiteId($includeExternalFbcSiteId)
            ->setOrderReferenceList($orderReferenceList)
            ->setOrderType($orderType)
            ->setPartnerOrderRef($partnerOrderRef)
            ->setStates($states);
    }
    /**
     * Get BeginCreationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBeginCreationDate()
    {
        return isset($this->BeginCreationDate) ? $this->BeginCreationDate : null;
    }
    /**
     * Set BeginCreationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $beginCreationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setBeginCreationDate($beginCreationDate = null)
    {
        // validation for constraint: string
        if (!is_null($beginCreationDate) && !is_string($beginCreationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($beginCreationDate, true), gettype($beginCreationDate)), __LINE__);
        }
        if (is_null($beginCreationDate) || (is_array($beginCreationDate) && empty($beginCreationDate))) {
            unset($this->BeginCreationDate);
        } else {
            $this->BeginCreationDate = $beginCreationDate;
        }
        return $this;
    }
    /**
     * Get BeginModificationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBeginModificationDate()
    {
        return isset($this->BeginModificationDate) ? $this->BeginModificationDate : null;
    }
    /**
     * Set BeginModificationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $beginModificationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setBeginModificationDate($beginModificationDate = null)
    {
        // validation for constraint: string
        if (!is_null($beginModificationDate) && !is_string($beginModificationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($beginModificationDate, true), gettype($beginModificationDate)), __LINE__);
        }
        if (is_null($beginModificationDate) || (is_array($beginModificationDate) && empty($beginModificationDate))) {
            unset($this->BeginModificationDate);
        } else {
            $this->BeginModificationDate = $beginModificationDate;
        }
        return $this;
    }
    /**
     * Get CorporationCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCorporationCode()
    {
        return isset($this->CorporationCode) ? $this->CorporationCode : null;
    }
    /**
     * Set CorporationCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $corporationCode
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setCorporationCode($corporationCode = null)
    {
        // validation for constraint: string
        if (!is_null($corporationCode) && !is_string($corporationCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporationCode, true), gettype($corporationCode)), __LINE__);
        }
        if (is_null($corporationCode) || (is_array($corporationCode) && empty($corporationCode))) {
            unset($this->CorporationCode);
        } else {
            $this->CorporationCode = $corporationCode;
        }
        return $this;
    }
    /**
     * Get EndCreationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndCreationDate()
    {
        return isset($this->EndCreationDate) ? $this->EndCreationDate : null;
    }
    /**
     * Set EndCreationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endCreationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setEndCreationDate($endCreationDate = null)
    {
        // validation for constraint: string
        if (!is_null($endCreationDate) && !is_string($endCreationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endCreationDate, true), gettype($endCreationDate)), __LINE__);
        }
        if (is_null($endCreationDate) || (is_array($endCreationDate) && empty($endCreationDate))) {
            unset($this->EndCreationDate);
        } else {
            $this->EndCreationDate = $endCreationDate;
        }
        return $this;
    }
    /**
     * Get EndModificationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndModificationDate()
    {
        return isset($this->EndModificationDate) ? $this->EndModificationDate : null;
    }
    /**
     * Set EndModificationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endModificationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setEndModificationDate($endModificationDate = null)
    {
        // validation for constraint: string
        if (!is_null($endModificationDate) && !is_string($endModificationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endModificationDate, true), gettype($endModificationDate)), __LINE__);
        }
        if (is_null($endModificationDate) || (is_array($endModificationDate) && empty($endModificationDate))) {
            unset($this->EndModificationDate);
        } else {
            $this->EndModificationDate = $endModificationDate;
        }
        return $this;
    }
    /**
     * Get FetchOrderLines value
     * @return bool|null
     */
    public function getFetchOrderLines()
    {
        return $this->FetchOrderLines;
    }
    /**
     * Set FetchOrderLines value
     * @param bool $fetchOrderLines
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setFetchOrderLines($fetchOrderLines = null)
    {
        // validation for constraint: boolean
        if (!is_null($fetchOrderLines) && !is_bool($fetchOrderLines)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($fetchOrderLines, true), gettype($fetchOrderLines)), __LINE__);
        }
        $this->FetchOrderLines = $fetchOrderLines;
        return $this;
    }
    /**
     * Get FetchParcels value
     * @return bool|null
     */
    public function getFetchParcels()
    {
        return $this->FetchParcels;
    }
    /**
     * Set FetchParcels value
     * @param bool $fetchParcels
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setFetchParcels($fetchParcels = null)
    {
        // validation for constraint: boolean
        if (!is_null($fetchParcels) && !is_bool($fetchParcels)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($fetchParcels, true), gettype($fetchParcels)), __LINE__);
        }
        $this->FetchParcels = $fetchParcels;
        return $this;
    }
    /**
     * Get IncludeExternalFbcSiteId value
     * @return bool|null
     */
    public function getIncludeExternalFbcSiteId()
    {
        return $this->IncludeExternalFbcSiteId;
    }
    /**
     * Set IncludeExternalFbcSiteId value
     * @param bool $includeExternalFbcSiteId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setIncludeExternalFbcSiteId($includeExternalFbcSiteId = null)
    {
        // validation for constraint: boolean
        if (!is_null($includeExternalFbcSiteId) && !is_bool($includeExternalFbcSiteId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($includeExternalFbcSiteId, true), gettype($includeExternalFbcSiteId)), __LINE__);
        }
        $this->IncludeExternalFbcSiteId = $includeExternalFbcSiteId;
        return $this;
    }
    /**
     * Get OrderReferenceList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getOrderReferenceList()
    {
        return isset($this->OrderReferenceList) ? $this->OrderReferenceList : null;
    }
    /**
     * Set OrderReferenceList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderReferenceList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setOrderReferenceList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $orderReferenceList = null)
    {
        if (is_null($orderReferenceList) || (is_array($orderReferenceList) && empty($orderReferenceList))) {
            unset($this->OrderReferenceList);
        } else {
            $this->OrderReferenceList = $orderReferenceList;
        }
        return $this;
    }
    /**
     * Get OrderType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrderType()
    {
        return isset($this->OrderType) ? $this->OrderType : null;
    }
    /**
     * Set OrderType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OrderTypeEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OrderTypeEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $orderType
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setOrderType($orderType = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\OrderTypeEnum::valueIsValid($orderType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\OrderTypeEnum', is_array($orderType) ? implode(', ', $orderType) : var_export($orderType, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\OrderTypeEnum::getValidValues())), __LINE__);
        }
        if (is_null($orderType) || (is_array($orderType) && empty($orderType))) {
            unset($this->OrderType);
        } else {
            $this->OrderType = $orderType;
        }
        return $this;
    }
    /**
     * Get PartnerOrderRef value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPartnerOrderRef()
    {
        return isset($this->PartnerOrderRef) ? $this->PartnerOrderRef : null;
    }
    /**
     * Set PartnerOrderRef value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $partnerOrderRef
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setPartnerOrderRef($partnerOrderRef = null)
    {
        // validation for constraint: string
        if (!is_null($partnerOrderRef) && !is_string($partnerOrderRef)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($partnerOrderRef, true), gettype($partnerOrderRef)), __LINE__);
        }
        if (is_null($partnerOrderRef) || (is_array($partnerOrderRef) && empty($partnerOrderRef))) {
            unset($this->PartnerOrderRef);
        } else {
            $this->PartnerOrderRef = $partnerOrderRef;
        }
        return $this;
    }
    /**
     * Get States value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderStateEnum|null
     */
    public function getStates()
    {
        return isset($this->States) ? $this->States : null;
    }
    /**
     * Set States value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderStateEnum $states
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public function setStates(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOrderStateEnum $states = null)
    {
        if (is_null($states) || (is_array($states) && empty($states))) {
            unset($this->States);
        } else {
            $this->States = $states;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
