<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductStockListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductStockListMessage
 * @subpackage Structs
 */
class ProductStockListMessage extends ServiceBaseAPIMessage
{
    /**
     * The ProductStockList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock
     */
    public $ProductStockList;
    /**
     * The Status
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Status;
    /**
     * The TotalProductCount
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $TotalProductCount;
    /**
     * Constructor method for ProductStockListMessage
     * @uses ProductStockListMessage::setProductStockList()
     * @uses ProductStockListMessage::setStatus()
     * @uses ProductStockListMessage::setTotalProductCount()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock $productStockList
     * @param string $status
     * @param int $totalProductCount
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock $productStockList = null, $status = null, $totalProductCount = null)
    {
        $this
            ->setProductStockList($productStockList)
            ->setStatus($status)
            ->setTotalProductCount($totalProductCount);
    }
    /**
     * Get ProductStockList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock|null
     */
    public function getProductStockList()
    {
        return isset($this->ProductStockList) ? $this->ProductStockList : null;
    }
    /**
     * Set ProductStockList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock $productStockList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage
     */
    public function setProductStockList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock $productStockList = null)
    {
        if (is_null($productStockList) || (is_array($productStockList) && empty($productStockList))) {
            unset($this->ProductStockList);
        } else {
            $this->ProductStockList = $productStockList;
        }
        return $this;
    }
    /**
     * Get Status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->Status;
    }
    /**
     * Set Status value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\FulfilmentProductListStatus::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\FulfilmentProductListStatus::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $status
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage
     */
    public function setStatus($status = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\FulfilmentProductListStatus::valueIsValid($status)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\FulfilmentProductListStatus', is_array($status) ? implode(', ', $status) : var_export($status, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\FulfilmentProductListStatus::getValidValues())), __LINE__);
        }
        $this->Status = $status;
        return $this;
    }
    /**
     * Get TotalProductCount value
     * @return int|null
     */
    public function getTotalProductCount()
    {
        return $this->TotalProductCount;
    }
    /**
     * Set TotalProductCount value
     * @param int $totalProductCount
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage
     */
    public function setTotalProductCount($totalProductCount = null)
    {
        // validation for constraint: int
        if (!is_null($totalProductCount) && !(is_int($totalProductCount) || ctype_digit($totalProductCount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalProductCount, true), gettype($totalProductCount)), __LINE__);
        }
        $this->TotalProductCount = $totalProductCount;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
