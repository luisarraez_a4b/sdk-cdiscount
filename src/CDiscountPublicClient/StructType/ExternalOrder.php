<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExternalOrder StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ExternalOrder
 * @subpackage Structs
 */
class ExternalOrder extends AbstractStructBase
{
    /**
     * The Comments
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Comments;
    /**
     * The Corporation
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Corporation;
    /**
     * The Customer
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalCustomer
     */
    public $Customer;
    /**
     * The CustomerOrderNumber
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $CustomerOrderNumber;
    /**
     * The OrderDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrderDate;
    /**
     * The OrderLineList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfExternalOrderLine
     */
    public $OrderLineList;
    /**
     * The ShippingMode
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ShippingMode;
    /**
     * Constructor method for ExternalOrder
     * @uses ExternalOrder::setComments()
     * @uses ExternalOrder::setCorporation()
     * @uses ExternalOrder::setCustomer()
     * @uses ExternalOrder::setCustomerOrderNumber()
     * @uses ExternalOrder::setOrderDate()
     * @uses ExternalOrder::setOrderLineList()
     * @uses ExternalOrder::setShippingMode()
     * @param string $comments
     * @param string $corporation
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalCustomer $customer
     * @param string $customerOrderNumber
     * @param string $orderDate
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfExternalOrderLine $orderLineList
     * @param string $shippingMode
     */
    public function __construct($comments = null, $corporation = null, \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalCustomer $customer = null, $customerOrderNumber = null, $orderDate = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfExternalOrderLine $orderLineList = null, $shippingMode = null)
    {
        $this
            ->setComments($comments)
            ->setCorporation($corporation)
            ->setCustomer($customer)
            ->setCustomerOrderNumber($customerOrderNumber)
            ->setOrderDate($orderDate)
            ->setOrderLineList($orderLineList)
            ->setShippingMode($shippingMode);
    }
    /**
     * Get Comments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getComments()
    {
        return isset($this->Comments) ? $this->Comments : null;
    }
    /**
     * Set Comments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $comments
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public function setComments($comments = null)
    {
        // validation for constraint: string
        if (!is_null($comments) && !is_string($comments)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comments, true), gettype($comments)), __LINE__);
        }
        if (is_null($comments) || (is_array($comments) && empty($comments))) {
            unset($this->Comments);
        } else {
            $this->Comments = $comments;
        }
        return $this;
    }
    /**
     * Get Corporation value
     * @return string|null
     */
    public function getCorporation()
    {
        return $this->Corporation;
    }
    /**
     * Set Corporation value
     * @param string $corporation
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public function setCorporation($corporation = null)
    {
        // validation for constraint: string
        if (!is_null($corporation) && !is_string($corporation)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($corporation, true), gettype($corporation)), __LINE__);
        }
        $this->Corporation = $corporation;
        return $this;
    }
    /**
     * Get Customer value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalCustomer|null
     */
    public function getCustomer()
    {
        return $this->Customer;
    }
    /**
     * Set Customer value
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalCustomer $customer
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public function setCustomer(\A4BGroup\Client\CDiscountPublicClient\StructType\ExternalCustomer $customer = null)
    {
        $this->Customer = $customer;
        return $this;
    }
    /**
     * Get CustomerOrderNumber value
     * @return string|null
     */
    public function getCustomerOrderNumber()
    {
        return $this->CustomerOrderNumber;
    }
    /**
     * Set CustomerOrderNumber value
     * @param string $customerOrderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public function setCustomerOrderNumber($customerOrderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($customerOrderNumber) && !is_string($customerOrderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerOrderNumber, true), gettype($customerOrderNumber)), __LINE__);
        }
        $this->CustomerOrderNumber = $customerOrderNumber;
        return $this;
    }
    /**
     * Get OrderDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrderDate()
    {
        return isset($this->OrderDate) ? $this->OrderDate : null;
    }
    /**
     * Set OrderDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orderDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public function setOrderDate($orderDate = null)
    {
        // validation for constraint: string
        if (!is_null($orderDate) && !is_string($orderDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderDate, true), gettype($orderDate)), __LINE__);
        }
        if (is_null($orderDate) || (is_array($orderDate) && empty($orderDate))) {
            unset($this->OrderDate);
        } else {
            $this->OrderDate = $orderDate;
        }
        return $this;
    }
    /**
     * Get OrderLineList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfExternalOrderLine|null
     */
    public function getOrderLineList()
    {
        return $this->OrderLineList;
    }
    /**
     * Set OrderLineList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfExternalOrderLine $orderLineList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public function setOrderLineList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfExternalOrderLine $orderLineList = null)
    {
        $this->OrderLineList = $orderLineList;
        return $this;
    }
    /**
     * Get ShippingMode value
     * @return string|null
     */
    public function getShippingMode()
    {
        return $this->ShippingMode;
    }
    /**
     * Set ShippingMode value
     * @param string $shippingMode
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public function setShippingMode($shippingMode = null)
    {
        // validation for constraint: string
        if (!is_null($shippingMode) && !is_string($shippingMode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shippingMode, true), gettype($shippingMode)), __LINE__);
        }
        $this->ShippingMode = $shippingMode;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrder
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
