<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CloseDiscussionResult StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:CloseDiscussionResult
 * @subpackage Structs
 */
class CloseDiscussionResult extends AbstractStructBase
{
    /**
     * The DiscussionId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $DiscussionId;
    /**
     * The OperationStatus
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $OperationStatus;
    /**
     * Constructor method for CloseDiscussionResult
     * @uses CloseDiscussionResult::setDiscussionId()
     * @uses CloseDiscussionResult::setOperationStatus()
     * @param int $discussionId
     * @param string $operationStatus
     */
    public function __construct($discussionId = null, $operationStatus = null)
    {
        $this
            ->setDiscussionId($discussionId)
            ->setOperationStatus($operationStatus);
    }
    /**
     * Get DiscussionId value
     * @return int|null
     */
    public function getDiscussionId()
    {
        return $this->DiscussionId;
    }
    /**
     * Set DiscussionId value
     * @param int $discussionId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult
     */
    public function setDiscussionId($discussionId = null)
    {
        // validation for constraint: int
        if (!is_null($discussionId) && !(is_int($discussionId) || ctype_digit($discussionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($discussionId, true), gettype($discussionId)), __LINE__);
        }
        $this->DiscussionId = $discussionId;
        return $this;
    }
    /**
     * Get OperationStatus value
     * @return string|null
     */
    public function getOperationStatus()
    {
        return $this->OperationStatus;
    }
    /**
     * Set OperationStatus value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\CloseDiscussionStatus::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\CloseDiscussionStatus::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $operationStatus
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult
     */
    public function setOperationStatus($operationStatus = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\CloseDiscussionStatus::valueIsValid($operationStatus)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\CloseDiscussionStatus', is_array($operationStatus) ? implode(', ', $operationStatus) : var_export($operationStatus, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\CloseDiscussionStatus::getValidValues())), __LINE__);
        }
        $this->OperationStatus = $operationStatus;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
