<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GeoCoordinate StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:GeoCoordinate
 * @subpackage Structs
 */
class GeoCoordinate extends AbstractStructBase
{
    /**
     * The Altitude
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $Altitude;
    /**
     * The Course
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $Course;
    /**
     * The HorizontalAccuracy
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $HorizontalAccuracy;
    /**
     * The Latitude
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $Latitude;
    /**
     * The Longitude
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $Longitude;
    /**
     * The Speed
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $Speed;
    /**
     * The VerticalAccuracy
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $VerticalAccuracy;
    /**
     * Constructor method for GeoCoordinate
     * @uses GeoCoordinate::setAltitude()
     * @uses GeoCoordinate::setCourse()
     * @uses GeoCoordinate::setHorizontalAccuracy()
     * @uses GeoCoordinate::setLatitude()
     * @uses GeoCoordinate::setLongitude()
     * @uses GeoCoordinate::setSpeed()
     * @uses GeoCoordinate::setVerticalAccuracy()
     * @param float $altitude
     * @param float $course
     * @param float $horizontalAccuracy
     * @param float $latitude
     * @param float $longitude
     * @param float $speed
     * @param float $verticalAccuracy
     */
    public function __construct($altitude = null, $course = null, $horizontalAccuracy = null, $latitude = null, $longitude = null, $speed = null, $verticalAccuracy = null)
    {
        $this
            ->setAltitude($altitude)
            ->setCourse($course)
            ->setHorizontalAccuracy($horizontalAccuracy)
            ->setLatitude($latitude)
            ->setLongitude($longitude)
            ->setSpeed($speed)
            ->setVerticalAccuracy($verticalAccuracy);
    }
    /**
     * Get Altitude value
     * @return float|null
     */
    public function getAltitude()
    {
        return $this->Altitude;
    }
    /**
     * Set Altitude value
     * @param float $altitude
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public function setAltitude($altitude = null)
    {
        // validation for constraint: float
        if (!is_null($altitude) && !(is_float($altitude) || is_numeric($altitude))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($altitude, true), gettype($altitude)), __LINE__);
        }
        $this->Altitude = $altitude;
        return $this;
    }
    /**
     * Get Course value
     * @return float|null
     */
    public function getCourse()
    {
        return $this->Course;
    }
    /**
     * Set Course value
     * @param float $course
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public function setCourse($course = null)
    {
        // validation for constraint: float
        if (!is_null($course) && !(is_float($course) || is_numeric($course))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($course, true), gettype($course)), __LINE__);
        }
        $this->Course = $course;
        return $this;
    }
    /**
     * Get HorizontalAccuracy value
     * @return float|null
     */
    public function getHorizontalAccuracy()
    {
        return $this->HorizontalAccuracy;
    }
    /**
     * Set HorizontalAccuracy value
     * @param float $horizontalAccuracy
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public function setHorizontalAccuracy($horizontalAccuracy = null)
    {
        // validation for constraint: float
        if (!is_null($horizontalAccuracy) && !(is_float($horizontalAccuracy) || is_numeric($horizontalAccuracy))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($horizontalAccuracy, true), gettype($horizontalAccuracy)), __LINE__);
        }
        $this->HorizontalAccuracy = $horizontalAccuracy;
        return $this;
    }
    /**
     * Get Latitude value
     * @return float|null
     */
    public function getLatitude()
    {
        return $this->Latitude;
    }
    /**
     * Set Latitude value
     * @param float $latitude
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public function setLatitude($latitude = null)
    {
        // validation for constraint: float
        if (!is_null($latitude) && !(is_float($latitude) || is_numeric($latitude))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($latitude, true), gettype($latitude)), __LINE__);
        }
        $this->Latitude = $latitude;
        return $this;
    }
    /**
     * Get Longitude value
     * @return float|null
     */
    public function getLongitude()
    {
        return $this->Longitude;
    }
    /**
     * Set Longitude value
     * @param float $longitude
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public function setLongitude($longitude = null)
    {
        // validation for constraint: float
        if (!is_null($longitude) && !(is_float($longitude) || is_numeric($longitude))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($longitude, true), gettype($longitude)), __LINE__);
        }
        $this->Longitude = $longitude;
        return $this;
    }
    /**
     * Get Speed value
     * @return float|null
     */
    public function getSpeed()
    {
        return $this->Speed;
    }
    /**
     * Set Speed value
     * @param float $speed
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public function setSpeed($speed = null)
    {
        // validation for constraint: float
        if (!is_null($speed) && !(is_float($speed) || is_numeric($speed))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($speed, true), gettype($speed)), __LINE__);
        }
        $this->Speed = $speed;
        return $this;
    }
    /**
     * Get VerticalAccuracy value
     * @return float|null
     */
    public function getVerticalAccuracy()
    {
        return $this->VerticalAccuracy;
    }
    /**
     * Set VerticalAccuracy value
     * @param float $verticalAccuracy
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public function setVerticalAccuracy($verticalAccuracy = null)
    {
        // validation for constraint: float
        if (!is_null($verticalAccuracy) && !(is_float($verticalAccuracy) || is_numeric($verticalAccuracy))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($verticalAccuracy, true), gettype($verticalAccuracy)), __LINE__);
        }
        $this->VerticalAccuracy = $verticalAccuracy;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GeoCoordinate
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
