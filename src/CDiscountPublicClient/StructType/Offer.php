<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Offer StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:Offer
 * @subpackage Structs
 */
class Offer extends AbstractStructBase
{
    /**
     * The BestShippingCharges
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $BestShippingCharges;
    /**
     * The Comments
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Comments;
    /**
     * The CreationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $CreationDate;
    /**
     * The DeaTax
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $DeaTax;
    /**
     * The DiscountList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscountComponent
     */
    public $DiscountList;
    /**
     * The EcoTax
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $EcoTax;
    /**
     * The IntegrationPrice
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $IntegrationPrice;
    /**
     * The IsCDAV
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsCDAV;
    /**
     * The LastUpdateDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $LastUpdateDate;
    /**
     * The LogisticMode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $LogisticMode;
    /**
     * The MinimumPriceForPriceAlignment
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $MinimumPriceForPriceAlignment;
    /**
     * The OfferBenchMark
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferPriceBenchMark
     */
    public $OfferBenchMark;
    /**
     * The OfferPoolList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool
     */
    public $OfferPoolList;
    /**
     * The OfferState
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $OfferState;
    /**
     * The ParentProductId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ParentProductId;
    /**
     * The Price
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $Price;
    /**
     * The PriceMustBeAligned
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $PriceMustBeAligned;
    /**
     * The ProductCondition
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ProductCondition;
    /**
     * The ProductEan
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductEan;
    /**
     * The ProductId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductId;
    /**
     * The ProductPackagingUnit
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ProductPackagingUnit;
    /**
     * The ProductPackagingUnitPrice
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $ProductPackagingUnitPrice;
    /**
     * The ProductPackagingValue
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $ProductPackagingValue;
    /**
     * The SellerProductId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SellerProductId;
    /**
     * The ShippingInformationList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation
     */
    public $ShippingInformationList;
    /**
     * The Stock
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $Stock;
    /**
     * The StrikedPrice
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $StrikedPrice;
    /**
     * The VatRate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $VatRate;
    /**
     * Constructor method for Offer
     * @uses Offer::setBestShippingCharges()
     * @uses Offer::setComments()
     * @uses Offer::setCreationDate()
     * @uses Offer::setDeaTax()
     * @uses Offer::setDiscountList()
     * @uses Offer::setEcoTax()
     * @uses Offer::setIntegrationPrice()
     * @uses Offer::setIsCDAV()
     * @uses Offer::setLastUpdateDate()
     * @uses Offer::setLogisticMode()
     * @uses Offer::setMinimumPriceForPriceAlignment()
     * @uses Offer::setOfferBenchMark()
     * @uses Offer::setOfferPoolList()
     * @uses Offer::setOfferState()
     * @uses Offer::setParentProductId()
     * @uses Offer::setPrice()
     * @uses Offer::setPriceMustBeAligned()
     * @uses Offer::setProductCondition()
     * @uses Offer::setProductEan()
     * @uses Offer::setProductId()
     * @uses Offer::setProductPackagingUnit()
     * @uses Offer::setProductPackagingUnitPrice()
     * @uses Offer::setProductPackagingValue()
     * @uses Offer::setSellerProductId()
     * @uses Offer::setShippingInformationList()
     * @uses Offer::setStock()
     * @uses Offer::setStrikedPrice()
     * @uses Offer::setVatRate()
     * @param float $bestShippingCharges
     * @param string $comments
     * @param string $creationDate
     * @param float $deaTax
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscountComponent $discountList
     * @param float $ecoTax
     * @param float $integrationPrice
     * @param bool $isCDAV
     * @param string $lastUpdateDate
     * @param string $logisticMode
     * @param float $minimumPriceForPriceAlignment
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferPriceBenchMark $offerBenchMark
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList
     * @param string $offerState
     * @param string $parentProductId
     * @param float $price
     * @param string $priceMustBeAligned
     * @param string $productCondition
     * @param string $productEan
     * @param string $productId
     * @param string $productPackagingUnit
     * @param float $productPackagingUnitPrice
     * @param float $productPackagingValue
     * @param string $sellerProductId
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation $shippingInformationList
     * @param int $stock
     * @param float $strikedPrice
     * @param float $vatRate
     */
    public function __construct($bestShippingCharges = null, $comments = null, $creationDate = null, $deaTax = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscountComponent $discountList = null, $ecoTax = null, $integrationPrice = null, $isCDAV = null, $lastUpdateDate = null, $logisticMode = null, $minimumPriceForPriceAlignment = null, \A4BGroup\Client\CDiscountPublicClient\StructType\OfferPriceBenchMark $offerBenchMark = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList = null, $offerState = null, $parentProductId = null, $price = null, $priceMustBeAligned = null, $productCondition = null, $productEan = null, $productId = null, $productPackagingUnit = null, $productPackagingUnitPrice = null, $productPackagingValue = null, $sellerProductId = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation $shippingInformationList = null, $stock = null, $strikedPrice = null, $vatRate = null)
    {
        $this
            ->setBestShippingCharges($bestShippingCharges)
            ->setComments($comments)
            ->setCreationDate($creationDate)
            ->setDeaTax($deaTax)
            ->setDiscountList($discountList)
            ->setEcoTax($ecoTax)
            ->setIntegrationPrice($integrationPrice)
            ->setIsCDAV($isCDAV)
            ->setLastUpdateDate($lastUpdateDate)
            ->setLogisticMode($logisticMode)
            ->setMinimumPriceForPriceAlignment($minimumPriceForPriceAlignment)
            ->setOfferBenchMark($offerBenchMark)
            ->setOfferPoolList($offerPoolList)
            ->setOfferState($offerState)
            ->setParentProductId($parentProductId)
            ->setPrice($price)
            ->setPriceMustBeAligned($priceMustBeAligned)
            ->setProductCondition($productCondition)
            ->setProductEan($productEan)
            ->setProductId($productId)
            ->setProductPackagingUnit($productPackagingUnit)
            ->setProductPackagingUnitPrice($productPackagingUnitPrice)
            ->setProductPackagingValue($productPackagingValue)
            ->setSellerProductId($sellerProductId)
            ->setShippingInformationList($shippingInformationList)
            ->setStock($stock)
            ->setStrikedPrice($strikedPrice)
            ->setVatRate($vatRate);
    }
    /**
     * Get BestShippingCharges value
     * @return float|null
     */
    public function getBestShippingCharges()
    {
        return $this->BestShippingCharges;
    }
    /**
     * Set BestShippingCharges value
     * @param float $bestShippingCharges
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setBestShippingCharges($bestShippingCharges = null)
    {
        // validation for constraint: float
        if (!is_null($bestShippingCharges) && !(is_float($bestShippingCharges) || is_numeric($bestShippingCharges))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($bestShippingCharges, true), gettype($bestShippingCharges)), __LINE__);
        }
        $this->BestShippingCharges = $bestShippingCharges;
        return $this;
    }
    /**
     * Get Comments value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getComments()
    {
        return isset($this->Comments) ? $this->Comments : null;
    }
    /**
     * Set Comments value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $comments
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setComments($comments = null)
    {
        // validation for constraint: string
        if (!is_null($comments) && !is_string($comments)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comments, true), gettype($comments)), __LINE__);
        }
        if (is_null($comments) || (is_array($comments) && empty($comments))) {
            unset($this->Comments);
        } else {
            $this->Comments = $comments;
        }
        return $this;
    }
    /**
     * Get CreationDate value
     * @return string|null
     */
    public function getCreationDate()
    {
        return $this->CreationDate;
    }
    /**
     * Set CreationDate value
     * @param string $creationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setCreationDate($creationDate = null)
    {
        // validation for constraint: string
        if (!is_null($creationDate) && !is_string($creationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($creationDate, true), gettype($creationDate)), __LINE__);
        }
        $this->CreationDate = $creationDate;
        return $this;
    }
    /**
     * Get DeaTax value
     * @return float|null
     */
    public function getDeaTax()
    {
        return $this->DeaTax;
    }
    /**
     * Set DeaTax value
     * @param float $deaTax
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setDeaTax($deaTax = null)
    {
        // validation for constraint: float
        if (!is_null($deaTax) && !(is_float($deaTax) || is_numeric($deaTax))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($deaTax, true), gettype($deaTax)), __LINE__);
        }
        $this->DeaTax = $deaTax;
        return $this;
    }
    /**
     * Get DiscountList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscountComponent|null
     */
    public function getDiscountList()
    {
        return isset($this->DiscountList) ? $this->DiscountList : null;
    }
    /**
     * Set DiscountList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscountComponent $discountList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setDiscountList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscountComponent $discountList = null)
    {
        if (is_null($discountList) || (is_array($discountList) && empty($discountList))) {
            unset($this->DiscountList);
        } else {
            $this->DiscountList = $discountList;
        }
        return $this;
    }
    /**
     * Get EcoTax value
     * @return float|null
     */
    public function getEcoTax()
    {
        return $this->EcoTax;
    }
    /**
     * Set EcoTax value
     * @param float $ecoTax
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setEcoTax($ecoTax = null)
    {
        // validation for constraint: float
        if (!is_null($ecoTax) && !(is_float($ecoTax) || is_numeric($ecoTax))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($ecoTax, true), gettype($ecoTax)), __LINE__);
        }
        $this->EcoTax = $ecoTax;
        return $this;
    }
    /**
     * Get IntegrationPrice value
     * @return float|null
     */
    public function getIntegrationPrice()
    {
        return $this->IntegrationPrice;
    }
    /**
     * Set IntegrationPrice value
     * @param float $integrationPrice
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setIntegrationPrice($integrationPrice = null)
    {
        // validation for constraint: float
        if (!is_null($integrationPrice) && !(is_float($integrationPrice) || is_numeric($integrationPrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($integrationPrice, true), gettype($integrationPrice)), __LINE__);
        }
        $this->IntegrationPrice = $integrationPrice;
        return $this;
    }
    /**
     * Get IsCDAV value
     * @return bool|null
     */
    public function getIsCDAV()
    {
        return $this->IsCDAV;
    }
    /**
     * Set IsCDAV value
     * @param bool $isCDAV
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setIsCDAV($isCDAV = null)
    {
        // validation for constraint: boolean
        if (!is_null($isCDAV) && !is_bool($isCDAV)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isCDAV, true), gettype($isCDAV)), __LINE__);
        }
        $this->IsCDAV = $isCDAV;
        return $this;
    }
    /**
     * Get LastUpdateDate value
     * @return string|null
     */
    public function getLastUpdateDate()
    {
        return $this->LastUpdateDate;
    }
    /**
     * Set LastUpdateDate value
     * @param string $lastUpdateDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setLastUpdateDate($lastUpdateDate = null)
    {
        // validation for constraint: string
        if (!is_null($lastUpdateDate) && !is_string($lastUpdateDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastUpdateDate, true), gettype($lastUpdateDate)), __LINE__);
        }
        $this->LastUpdateDate = $lastUpdateDate;
        return $this;
    }
    /**
     * Get LogisticMode value
     * @return string|null
     */
    public function getLogisticMode()
    {
        return $this->LogisticMode;
    }
    /**
     * Set LogisticMode value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\LogisticMode::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\LogisticMode::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $logisticMode
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setLogisticMode($logisticMode = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\LogisticMode::valueIsValid($logisticMode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\LogisticMode', is_array($logisticMode) ? implode(', ', $logisticMode) : var_export($logisticMode, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\LogisticMode::getValidValues())), __LINE__);
        }
        $this->LogisticMode = $logisticMode;
        return $this;
    }
    /**
     * Get MinimumPriceForPriceAlignment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getMinimumPriceForPriceAlignment()
    {
        return isset($this->MinimumPriceForPriceAlignment) ? $this->MinimumPriceForPriceAlignment : null;
    }
    /**
     * Set MinimumPriceForPriceAlignment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $minimumPriceForPriceAlignment
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setMinimumPriceForPriceAlignment($minimumPriceForPriceAlignment = null)
    {
        // validation for constraint: float
        if (!is_null($minimumPriceForPriceAlignment) && !(is_float($minimumPriceForPriceAlignment) || is_numeric($minimumPriceForPriceAlignment))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($minimumPriceForPriceAlignment, true), gettype($minimumPriceForPriceAlignment)), __LINE__);
        }
        if (is_null($minimumPriceForPriceAlignment) || (is_array($minimumPriceForPriceAlignment) && empty($minimumPriceForPriceAlignment))) {
            unset($this->MinimumPriceForPriceAlignment);
        } else {
            $this->MinimumPriceForPriceAlignment = $minimumPriceForPriceAlignment;
        }
        return $this;
    }
    /**
     * Get OfferBenchMark value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferPriceBenchMark|null
     */
    public function getOfferBenchMark()
    {
        return isset($this->OfferBenchMark) ? $this->OfferBenchMark : null;
    }
    /**
     * Set OfferBenchMark value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferPriceBenchMark $offerBenchMark
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setOfferBenchMark(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferPriceBenchMark $offerBenchMark = null)
    {
        if (is_null($offerBenchMark) || (is_array($offerBenchMark) && empty($offerBenchMark))) {
            unset($this->OfferBenchMark);
        } else {
            $this->OfferBenchMark = $offerBenchMark;
        }
        return $this;
    }
    /**
     * Get OfferPoolList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool|null
     */
    public function getOfferPoolList()
    {
        return isset($this->OfferPoolList) ? $this->OfferPoolList : null;
    }
    /**
     * Set OfferPoolList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setOfferPoolList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList = null)
    {
        if (is_null($offerPoolList) || (is_array($offerPoolList) && empty($offerPoolList))) {
            unset($this->OfferPoolList);
        } else {
            $this->OfferPoolList = $offerPoolList;
        }
        return $this;
    }
    /**
     * Get OfferState value
     * @return string|null
     */
    public function getOfferState()
    {
        return $this->OfferState;
    }
    /**
     * Set OfferState value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $offerState
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setOfferState($offerState = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateEnum::valueIsValid($offerState)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\OfferStateEnum', is_array($offerState) ? implode(', ', $offerState) : var_export($offerState, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateEnum::getValidValues())), __LINE__);
        }
        $this->OfferState = $offerState;
        return $this;
    }
    /**
     * Get ParentProductId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParentProductId()
    {
        return isset($this->ParentProductId) ? $this->ParentProductId : null;
    }
    /**
     * Set ParentProductId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parentProductId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setParentProductId($parentProductId = null)
    {
        // validation for constraint: string
        if (!is_null($parentProductId) && !is_string($parentProductId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parentProductId, true), gettype($parentProductId)), __LINE__);
        }
        if (is_null($parentProductId) || (is_array($parentProductId) && empty($parentProductId))) {
            unset($this->ParentProductId);
        } else {
            $this->ParentProductId = $parentProductId;
        }
        return $this;
    }
    /**
     * Get Price value
     * @return float|null
     */
    public function getPrice()
    {
        return $this->Price;
    }
    /**
     * Set Price value
     * @param float $price
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setPrice($price = null)
    {
        // validation for constraint: float
        if (!is_null($price) && !(is_float($price) || is_numeric($price))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        $this->Price = $price;
        return $this;
    }
    /**
     * Get PriceMustBeAligned value
     * @return string|null
     */
    public function getPriceMustBeAligned()
    {
        return $this->PriceMustBeAligned;
    }
    /**
     * Set PriceMustBeAligned value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\PriceAlignmentAction::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\PriceAlignmentAction::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $priceMustBeAligned
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setPriceMustBeAligned($priceMustBeAligned = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\PriceAlignmentAction::valueIsValid($priceMustBeAligned)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\PriceAlignmentAction', is_array($priceMustBeAligned) ? implode(', ', $priceMustBeAligned) : var_export($priceMustBeAligned, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\PriceAlignmentAction::getValidValues())), __LINE__);
        }
        $this->PriceMustBeAligned = $priceMustBeAligned;
        return $this;
    }
    /**
     * Get ProductCondition value
     * @return string|null
     */
    public function getProductCondition()
    {
        return $this->ProductCondition;
    }
    /**
     * Set ProductCondition value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $productCondition
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setProductCondition($productCondition = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::valueIsValid($productCondition)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ProductConditionEnum', is_array($productCondition) ? implode(', ', $productCondition) : var_export($productCondition, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductConditionEnum::getValidValues())), __LINE__);
        }
        $this->ProductCondition = $productCondition;
        return $this;
    }
    /**
     * Get ProductEan value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductEan()
    {
        return isset($this->ProductEan) ? $this->ProductEan : null;
    }
    /**
     * Set ProductEan value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productEan
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setProductEan($productEan = null)
    {
        // validation for constraint: string
        if (!is_null($productEan) && !is_string($productEan)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productEan, true), gettype($productEan)), __LINE__);
        }
        if (is_null($productEan) || (is_array($productEan) && empty($productEan))) {
            unset($this->ProductEan);
        } else {
            $this->ProductEan = $productEan;
        }
        return $this;
    }
    /**
     * Get ProductId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductId()
    {
        return isset($this->ProductId) ? $this->ProductId : null;
    }
    /**
     * Set ProductId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setProductId($productId = null)
    {
        // validation for constraint: string
        if (!is_null($productId) && !is_string($productId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productId, true), gettype($productId)), __LINE__);
        }
        if (is_null($productId) || (is_array($productId) && empty($productId))) {
            unset($this->ProductId);
        } else {
            $this->ProductId = $productId;
        }
        return $this;
    }
    /**
     * Get ProductPackagingUnit value
     * @return string|null
     */
    public function getProductPackagingUnit()
    {
        return $this->ProductPackagingUnit;
    }
    /**
     * Set ProductPackagingUnit value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductPackagingUnit::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductPackagingUnit::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $productPackagingUnit
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setProductPackagingUnit($productPackagingUnit = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ProductPackagingUnit::valueIsValid($productPackagingUnit)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ProductPackagingUnit', is_array($productPackagingUnit) ? implode(', ', $productPackagingUnit) : var_export($productPackagingUnit, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductPackagingUnit::getValidValues())), __LINE__);
        }
        $this->ProductPackagingUnit = $productPackagingUnit;
        return $this;
    }
    /**
     * Get ProductPackagingUnitPrice value
     * @return float|null
     */
    public function getProductPackagingUnitPrice()
    {
        return $this->ProductPackagingUnitPrice;
    }
    /**
     * Set ProductPackagingUnitPrice value
     * @param float $productPackagingUnitPrice
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setProductPackagingUnitPrice($productPackagingUnitPrice = null)
    {
        // validation for constraint: float
        if (!is_null($productPackagingUnitPrice) && !(is_float($productPackagingUnitPrice) || is_numeric($productPackagingUnitPrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($productPackagingUnitPrice, true), gettype($productPackagingUnitPrice)), __LINE__);
        }
        $this->ProductPackagingUnitPrice = $productPackagingUnitPrice;
        return $this;
    }
    /**
     * Get ProductPackagingValue value
     * @return float|null
     */
    public function getProductPackagingValue()
    {
        return $this->ProductPackagingValue;
    }
    /**
     * Set ProductPackagingValue value
     * @param float $productPackagingValue
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setProductPackagingValue($productPackagingValue = null)
    {
        // validation for constraint: float
        if (!is_null($productPackagingValue) && !(is_float($productPackagingValue) || is_numeric($productPackagingValue))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($productPackagingValue, true), gettype($productPackagingValue)), __LINE__);
        }
        $this->ProductPackagingValue = $productPackagingValue;
        return $this;
    }
    /**
     * Get SellerProductId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSellerProductId()
    {
        return isset($this->SellerProductId) ? $this->SellerProductId : null;
    }
    /**
     * Set SellerProductId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sellerProductId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setSellerProductId($sellerProductId = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductId) && !is_string($sellerProductId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductId, true), gettype($sellerProductId)), __LINE__);
        }
        if (is_null($sellerProductId) || (is_array($sellerProductId) && empty($sellerProductId))) {
            unset($this->SellerProductId);
        } else {
            $this->SellerProductId = $sellerProductId;
        }
        return $this;
    }
    /**
     * Get ShippingInformationList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation|null
     */
    public function getShippingInformationList()
    {
        return isset($this->ShippingInformationList) ? $this->ShippingInformationList : null;
    }
    /**
     * Set ShippingInformationList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation $shippingInformationList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setShippingInformationList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation $shippingInformationList = null)
    {
        if (is_null($shippingInformationList) || (is_array($shippingInformationList) && empty($shippingInformationList))) {
            unset($this->ShippingInformationList);
        } else {
            $this->ShippingInformationList = $shippingInformationList;
        }
        return $this;
    }
    /**
     * Get Stock value
     * @return int|null
     */
    public function getStock()
    {
        return $this->Stock;
    }
    /**
     * Set Stock value
     * @param int $stock
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setStock($stock = null)
    {
        // validation for constraint: int
        if (!is_null($stock) && !(is_int($stock) || ctype_digit($stock))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($stock, true), gettype($stock)), __LINE__);
        }
        $this->Stock = $stock;
        return $this;
    }
    /**
     * Get StrikedPrice value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getStrikedPrice()
    {
        return isset($this->StrikedPrice) ? $this->StrikedPrice : null;
    }
    /**
     * Set StrikedPrice value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $strikedPrice
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setStrikedPrice($strikedPrice = null)
    {
        // validation for constraint: float
        if (!is_null($strikedPrice) && !(is_float($strikedPrice) || is_numeric($strikedPrice))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($strikedPrice, true), gettype($strikedPrice)), __LINE__);
        }
        if (is_null($strikedPrice) || (is_array($strikedPrice) && empty($strikedPrice))) {
            unset($this->StrikedPrice);
        } else {
            $this->StrikedPrice = $strikedPrice;
        }
        return $this;
    }
    /**
     * Get VatRate value
     * @return float|null
     */
    public function getVatRate()
    {
        return $this->VatRate;
    }
    /**
     * Set VatRate value
     * @param float $vatRate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public function setVatRate($vatRate = null)
    {
        // validation for constraint: float
        if (!is_null($vatRate) && !(is_float($vatRate) || is_numeric($vatRate))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($vatRate, true), gettype($vatRate)), __LINE__);
        }
        $this->VatRate = $vatRate;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
