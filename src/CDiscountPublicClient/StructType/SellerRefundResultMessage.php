<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SellerRefundResultMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerRefundResultMessage
 * @subpackage Structs
 */
class SellerRefundResultMessage extends ServiceMessage
{
    /**
     * The OrderNumber
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $OrderNumber;
    /**
     * The SellerRefundResultList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult
     */
    public $SellerRefundResultList;
    /**
     * Constructor method for SellerRefundResultMessage
     * @uses SellerRefundResultMessage::setOrderNumber()
     * @uses SellerRefundResultMessage::setSellerRefundResultList()
     * @param string $orderNumber
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundResultList
     */
    public function __construct($orderNumber = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundResultList = null)
    {
        $this
            ->setOrderNumber($orderNumber)
            ->setSellerRefundResultList($sellerRefundResultList);
    }
    /**
     * Get OrderNumber value
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->OrderNumber;
    }
    /**
     * Set OrderNumber value
     * @param string $orderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage
     */
    public function setOrderNumber($orderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($orderNumber) && !is_string($orderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderNumber, true), gettype($orderNumber)), __LINE__);
        }
        $this->OrderNumber = $orderNumber;
        return $this;
    }
    /**
     * Get SellerRefundResultList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult|null
     */
    public function getSellerRefundResultList()
    {
        return $this->SellerRefundResultList;
    }
    /**
     * Set SellerRefundResultList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundResultList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage
     */
    public function setSellerRefundResultList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundResult $sellerRefundResultList = null)
    {
        $this->SellerRefundResultList = $sellerRefundResultList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
