<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SellerIndicator StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerIndicator
 * @subpackage Structs
 */
class SellerIndicator extends AbstractStructBase
{
    /**
     * The ComputationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ComputationDate;
    /**
     * The Description
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Description;
    /**
     * The Threshold
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $Threshold;
    /**
     * The ThresholdType
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ThresholdType;
    /**
     * The ValueD30
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $ValueD30;
    /**
     * The ValueD60
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var float
     */
    public $ValueD60;
    /**
     * Constructor method for SellerIndicator
     * @uses SellerIndicator::setComputationDate()
     * @uses SellerIndicator::setDescription()
     * @uses SellerIndicator::setThreshold()
     * @uses SellerIndicator::setThresholdType()
     * @uses SellerIndicator::setValueD30()
     * @uses SellerIndicator::setValueD60()
     * @param string $computationDate
     * @param string $description
     * @param float $threshold
     * @param string $thresholdType
     * @param float $valueD30
     * @param float $valueD60
     */
    public function __construct($computationDate = null, $description = null, $threshold = null, $thresholdType = null, $valueD30 = null, $valueD60 = null)
    {
        $this
            ->setComputationDate($computationDate)
            ->setDescription($description)
            ->setThreshold($threshold)
            ->setThresholdType($thresholdType)
            ->setValueD30($valueD30)
            ->setValueD60($valueD60);
    }
    /**
     * Get ComputationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getComputationDate()
    {
        return isset($this->ComputationDate) ? $this->ComputationDate : null;
    }
    /**
     * Set ComputationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $computationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerIndicator
     */
    public function setComputationDate($computationDate = null)
    {
        // validation for constraint: string
        if (!is_null($computationDate) && !is_string($computationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($computationDate, true), gettype($computationDate)), __LINE__);
        }
        if (is_null($computationDate) || (is_array($computationDate) && empty($computationDate))) {
            unset($this->ComputationDate);
        } else {
            $this->ComputationDate = $computationDate;
        }
        return $this;
    }
    /**
     * Get Description value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDescription()
    {
        return isset($this->Description) ? $this->Description : null;
    }
    /**
     * Set Description value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $description
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerIndicator
     */
    public function setDescription($description = null)
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        if (is_null($description) || (is_array($description) && empty($description))) {
            unset($this->Description);
        } else {
            $this->Description = $description;
        }
        return $this;
    }
    /**
     * Get Threshold value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getThreshold()
    {
        return isset($this->Threshold) ? $this->Threshold : null;
    }
    /**
     * Set Threshold value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $threshold
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerIndicator
     */
    public function setThreshold($threshold = null)
    {
        // validation for constraint: float
        if (!is_null($threshold) && !(is_float($threshold) || is_numeric($threshold))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($threshold, true), gettype($threshold)), __LINE__);
        }
        if (is_null($threshold) || (is_array($threshold) && empty($threshold))) {
            unset($this->Threshold);
        } else {
            $this->Threshold = $threshold;
        }
        return $this;
    }
    /**
     * Get ThresholdType value
     * @return string|null
     */
    public function getThresholdType()
    {
        return $this->ThresholdType;
    }
    /**
     * Set ThresholdType value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\TresholdType::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\TresholdType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $thresholdType
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerIndicator
     */
    public function setThresholdType($thresholdType = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\TresholdType::valueIsValid($thresholdType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\TresholdType', is_array($thresholdType) ? implode(', ', $thresholdType) : var_export($thresholdType, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\TresholdType::getValidValues())), __LINE__);
        }
        $this->ThresholdType = $thresholdType;
        return $this;
    }
    /**
     * Get ValueD30 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getValueD30()
    {
        return isset($this->ValueD30) ? $this->ValueD30 : null;
    }
    /**
     * Set ValueD30 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $valueD30
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerIndicator
     */
    public function setValueD30($valueD30 = null)
    {
        // validation for constraint: float
        if (!is_null($valueD30) && !(is_float($valueD30) || is_numeric($valueD30))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($valueD30, true), gettype($valueD30)), __LINE__);
        }
        if (is_null($valueD30) || (is_array($valueD30) && empty($valueD30))) {
            unset($this->ValueD30);
        } else {
            $this->ValueD30 = $valueD30;
        }
        return $this;
    }
    /**
     * Get ValueD60 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return float|null
     */
    public function getValueD60()
    {
        return isset($this->ValueD60) ? $this->ValueD60 : null;
    }
    /**
     * Set ValueD60 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param float $valueD60
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerIndicator
     */
    public function setValueD60($valueD60 = null)
    {
        // validation for constraint: float
        if (!is_null($valueD60) && !(is_float($valueD60) || is_numeric($valueD60))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($valueD60, true), gettype($valueD60)), __LINE__);
        }
        if (is_null($valueD60) || (is_array($valueD60) && empty($valueD60))) {
            unset($this->ValueD60);
        } else {
            $this->ValueD60 = $valueD60;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerIndicator
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
