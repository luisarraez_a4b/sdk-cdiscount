<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DomainRights StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DomainRights
 * @subpackage Structs
 */
class DomainRights extends AbstractStructBase
{
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The SecurityDescriptorList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor
     */
    public $SecurityDescriptorList;
    /**
     * Constructor method for DomainRights
     * @uses DomainRights::setName()
     * @uses DomainRights::setSecurityDescriptorList()
     * @param string $name
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor $securityDescriptorList
     */
    public function __construct($name = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor $securityDescriptorList = null)
    {
        $this
            ->setName($name)
            ->setSecurityDescriptorList($securityDescriptorList);
    }
    /**
     * Get Name value
     * @return string|null
     */
    public function getName()
    {
        return $this->Name;
    }
    /**
     * Set Name value
     * @param string $name
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DomainRights
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->Name = $name;
        return $this;
    }
    /**
     * Get SecurityDescriptorList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor|null
     */
    public function getSecurityDescriptorList()
    {
        return $this->SecurityDescriptorList;
    }
    /**
     * Set SecurityDescriptorList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor $securityDescriptorList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DomainRights
     */
    public function setSecurityDescriptorList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor $securityDescriptorList = null)
    {
        $this->SecurityDescriptorList = $securityDescriptorList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DomainRights
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
