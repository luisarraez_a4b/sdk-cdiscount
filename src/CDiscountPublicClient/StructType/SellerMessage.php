<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SellerMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerMessage
 * @subpackage Structs
 */
class SellerMessage extends ServiceBaseAPIMessage
{
    /**
     * The DeliveryModes
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation
     */
    public $DeliveryModes;
    /**
     * The OfferPoolList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool
     */
    public $OfferPoolList;
    /**
     * The Seller
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public $Seller;
    /**
     * Constructor method for SellerMessage
     * @uses SellerMessage::setDeliveryModes()
     * @uses SellerMessage::setOfferPoolList()
     * @uses SellerMessage::setSeller()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation $deliveryModes
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Seller $seller
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation $deliveryModes = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList = null, \A4BGroup\Client\CDiscountPublicClient\StructType\Seller $seller = null)
    {
        $this
            ->setDeliveryModes($deliveryModes)
            ->setOfferPoolList($offerPoolList)
            ->setSeller($seller);
    }
    /**
     * Get DeliveryModes value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation|null
     */
    public function getDeliveryModes()
    {
        return isset($this->DeliveryModes) ? $this->DeliveryModes : null;
    }
    /**
     * Set DeliveryModes value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation $deliveryModes
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage
     */
    public function setDeliveryModes(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation $deliveryModes = null)
    {
        if (is_null($deliveryModes) || (is_array($deliveryModes) && empty($deliveryModes))) {
            unset($this->DeliveryModes);
        } else {
            $this->DeliveryModes = $deliveryModes;
        }
        return $this;
    }
    /**
     * Get OfferPoolList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool|null
     */
    public function getOfferPoolList()
    {
        return isset($this->OfferPoolList) ? $this->OfferPoolList : null;
    }
    /**
     * Set OfferPoolList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage
     */
    public function setOfferPoolList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferPool $offerPoolList = null)
    {
        if (is_null($offerPoolList) || (is_array($offerPoolList) && empty($offerPoolList))) {
            unset($this->OfferPoolList);
        } else {
            $this->OfferPoolList = $offerPoolList;
        }
        return $this;
    }
    /**
     * Get Seller value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller|null
     */
    public function getSeller()
    {
        return isset($this->Seller) ? $this->Seller : null;
    }
    /**
     * Set Seller value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Seller $seller
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage
     */
    public function setSeller(\A4BGroup\Client\CDiscountPublicClient\StructType\Seller $seller = null)
    {
        if (is_null($seller) || (is_array($seller) && empty($seller))) {
            unset($this->Seller);
        } else {
            $this->Seller = $seller;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
