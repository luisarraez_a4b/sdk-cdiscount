<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateRefundVoucherAfterShipmentResponse StructType
 * @subpackage Structs
 */
class CreateRefundVoucherAfterShipmentResponse extends AbstractStructBase
{
    /**
     * The CreateRefundVoucherAfterShipmentResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage
     */
    public $CreateRefundVoucherAfterShipmentResult;
    /**
     * Constructor method for CreateRefundVoucherAfterShipmentResponse
     * @uses CreateRefundVoucherAfterShipmentResponse::setCreateRefundVoucherAfterShipmentResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage $createRefundVoucherAfterShipmentResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage $createRefundVoucherAfterShipmentResult = null)
    {
        $this
            ->setCreateRefundVoucherAfterShipmentResult($createRefundVoucherAfterShipmentResult);
    }
    /**
     * Get CreateRefundVoucherAfterShipmentResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage|null
     */
    public function getCreateRefundVoucherAfterShipmentResult()
    {
        return isset($this->CreateRefundVoucherAfterShipmentResult) ? $this->CreateRefundVoucherAfterShipmentResult : null;
    }
    /**
     * Set CreateRefundVoucherAfterShipmentResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage $createRefundVoucherAfterShipmentResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherAfterShipmentResponse
     */
    public function setCreateRefundVoucherAfterShipmentResult(\A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResultMessage $createRefundVoucherAfterShipmentResult = null)
    {
        if (is_null($createRefundVoucherAfterShipmentResult) || (is_array($createRefundVoucherAfterShipmentResult) && empty($createRefundVoucherAfterShipmentResult))) {
            unset($this->CreateRefundVoucherAfterShipmentResult);
        } else {
            $this->CreateRefundVoucherAfterShipmentResult = $createRefundVoucherAfterShipmentResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherAfterShipmentResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
