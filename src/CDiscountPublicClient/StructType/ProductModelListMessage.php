<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductModelListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductModelListMessage
 * @subpackage Structs
 */
class ProductModelListMessage extends ServiceBaseAPIMessage
{
    /**
     * The ModelList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductModel
     */
    public $ModelList;
    /**
     * Constructor method for ProductModelListMessage
     * @uses ProductModelListMessage::setModelList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductModel $modelList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductModel $modelList = null)
    {
        $this
            ->setModelList($modelList);
    }
    /**
     * Get ModelList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductModel|null
     */
    public function getModelList()
    {
        return isset($this->ModelList) ? $this->ModelList : null;
    }
    /**
     * Set ModelList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductModel $modelList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage
     */
    public function setModelList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductModel $modelList = null)
    {
        if (is_null($modelList) || (is_array($modelList) && empty($modelList))) {
            unset($this->ModelList);
        } else {
            $this->ModelList = $modelList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
