<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DiscussionMail StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscussionMail
 * @subpackage Structs
 */
class DiscussionMail extends AbstractStructBase
{
    /**
     * The DiscussionId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $DiscussionId;
    /**
     * The MailAddress
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $MailAddress;
    /**
     * The OperationStatus
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $OperationStatus;
    /**
     * Constructor method for DiscussionMail
     * @uses DiscussionMail::setDiscussionId()
     * @uses DiscussionMail::setMailAddress()
     * @uses DiscussionMail::setOperationStatus()
     * @param int $discussionId
     * @param string $mailAddress
     * @param string $operationStatus
     */
    public function __construct($discussionId = null, $mailAddress = null, $operationStatus = null)
    {
        $this
            ->setDiscussionId($discussionId)
            ->setMailAddress($mailAddress)
            ->setOperationStatus($operationStatus);
    }
    /**
     * Get DiscussionId value
     * @return int|null
     */
    public function getDiscussionId()
    {
        return $this->DiscussionId;
    }
    /**
     * Set DiscussionId value
     * @param int $discussionId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMail
     */
    public function setDiscussionId($discussionId = null)
    {
        // validation for constraint: int
        if (!is_null($discussionId) && !(is_int($discussionId) || ctype_digit($discussionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($discussionId, true), gettype($discussionId)), __LINE__);
        }
        $this->DiscussionId = $discussionId;
        return $this;
    }
    /**
     * Get MailAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMailAddress()
    {
        return isset($this->MailAddress) ? $this->MailAddress : null;
    }
    /**
     * Set MailAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mailAddress
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMail
     */
    public function setMailAddress($mailAddress = null)
    {
        // validation for constraint: string
        if (!is_null($mailAddress) && !is_string($mailAddress)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mailAddress, true), gettype($mailAddress)), __LINE__);
        }
        if (is_null($mailAddress) || (is_array($mailAddress) && empty($mailAddress))) {
            unset($this->MailAddress);
        } else {
            $this->MailAddress = $mailAddress;
        }
        return $this;
    }
    /**
     * Get OperationStatus value
     * @return string|null
     */
    public function getOperationStatus()
    {
        return $this->OperationStatus;
    }
    /**
     * Set OperationStatus value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionMailStatusEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionMailStatusEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $operationStatus
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMail
     */
    public function setOperationStatus($operationStatus = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionMailStatusEnum::valueIsValid($operationStatus)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\DiscussionMailStatusEnum', is_array($operationStatus) ? implode(', ', $operationStatus) : var_export($operationStatus, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionMailStatusEnum::getValidValues())), __LINE__);
        }
        $this->OperationStatus = $operationStatus;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DiscussionMail
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
