<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SellerRefundResult StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerRefundResult
 * @subpackage Structs
 */
class SellerRefundResult extends ServiceMessage
{
    /**
     * The Ean
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Ean;
    /**
     * The Motive
     * @var string
     */
    public $Motive;
    /**
     * The SellerProductId
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $SellerProductId;
    /**
     * The Value
     * @var float
     */
    public $Value;
    /**
     * Constructor method for SellerRefundResult
     * @uses SellerRefundResult::setEan()
     * @uses SellerRefundResult::setMotive()
     * @uses SellerRefundResult::setSellerProductId()
     * @uses SellerRefundResult::setValue()
     * @param string $ean
     * @param string $motive
     * @param string $sellerProductId
     * @param float $value
     */
    public function __construct($ean = null, $motive = null, $sellerProductId = null, $value = null)
    {
        $this
            ->setEan($ean)
            ->setMotive($motive)
            ->setSellerProductId($sellerProductId)
            ->setValue($value);
    }
    /**
     * Get Ean value
     * @return string|null
     */
    public function getEan()
    {
        return $this->Ean;
    }
    /**
     * Set Ean value
     * @param string $ean
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResult
     */
    public function setEan($ean = null)
    {
        // validation for constraint: string
        if (!is_null($ean) && !is_string($ean)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ean, true), gettype($ean)), __LINE__);
        }
        $this->Ean = $ean;
        return $this;
    }
    /**
     * Get Motive value
     * @return string|null
     */
    public function getMotive()
    {
        return $this->Motive;
    }
    /**
     * Set Motive value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $motive
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResult
     */
    public function setMotive($motive = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::valueIsValid($motive)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\RefundMotive', is_array($motive) ? implode(', ', $motive) : var_export($motive, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::getValidValues())), __LINE__);
        }
        $this->Motive = $motive;
        return $this;
    }
    /**
     * Get SellerProductId value
     * @return string|null
     */
    public function getSellerProductId()
    {
        return $this->SellerProductId;
    }
    /**
     * Set SellerProductId value
     * @param string $sellerProductId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResult
     */
    public function setSellerProductId($sellerProductId = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductId) && !is_string($sellerProductId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductId, true), gettype($sellerProductId)), __LINE__);
        }
        $this->SellerProductId = $sellerProductId;
        return $this;
    }
    /**
     * Get Value value
     * @return float|null
     */
    public function getValue()
    {
        return $this->Value;
    }
    /**
     * Set Value value
     * @param float $value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResult
     */
    public function setValue($value = null)
    {
        // validation for constraint: float
        if (!is_null($value) && !(is_float($value) || is_numeric($value))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->Value = $value;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundResult
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
