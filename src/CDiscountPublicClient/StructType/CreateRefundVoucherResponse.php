<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateRefundVoucherResponse StructType
 * @subpackage Structs
 */
class CreateRefundVoucherResponse extends AbstractStructBase
{
    /**
     * The CreateRefundVoucherResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage
     */
    public $CreateRefundVoucherResult;
    /**
     * Constructor method for CreateRefundVoucherResponse
     * @uses CreateRefundVoucherResponse::setCreateRefundVoucherResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage $createRefundVoucherResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage $createRefundVoucherResult = null)
    {
        $this
            ->setCreateRefundVoucherResult($createRefundVoucherResult);
    }
    /**
     * Get CreateRefundVoucherResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage|null
     */
    public function getCreateRefundVoucherResult()
    {
        return isset($this->CreateRefundVoucherResult) ? $this->CreateRefundVoucherResult : null;
    }
    /**
     * Set CreateRefundVoucherResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage $createRefundVoucherResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherResponse
     */
    public function setCreateRefundVoucherResult(\A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherMessage $createRefundVoucherResult = null)
    {
        if (is_null($createRefundVoucherResult) || (is_array($createRefundVoucherResult) && empty($createRefundVoucherResult))) {
            unset($this->CreateRefundVoucherResult);
        } else {
            $this->CreateRefundVoucherResult = $createRefundVoucherResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
