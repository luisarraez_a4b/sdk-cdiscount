<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetExternalOrderStatusResponse StructType
 * @subpackage Structs
 */
class GetExternalOrderStatusResponse extends AbstractStructBase
{
    /**
     * The GetExternalOrderStatusResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage
     */
    public $GetExternalOrderStatusResult;
    /**
     * Constructor method for GetExternalOrderStatusResponse
     * @uses GetExternalOrderStatusResponse::setGetExternalOrderStatusResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage $getExternalOrderStatusResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage $getExternalOrderStatusResult = null)
    {
        $this
            ->setGetExternalOrderStatusResult($getExternalOrderStatusResult);
    }
    /**
     * Get GetExternalOrderStatusResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage|null
     */
    public function getGetExternalOrderStatusResult()
    {
        return isset($this->GetExternalOrderStatusResult) ? $this->GetExternalOrderStatusResult : null;
    }
    /**
     * Set GetExternalOrderStatusResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage $getExternalOrderStatusResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetExternalOrderStatusResponse
     */
    public function setGetExternalOrderStatusResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OrderStatusMessage $getExternalOrderStatusResult = null)
    {
        if (is_null($getExternalOrderStatusResult) || (is_array($getExternalOrderStatusResult) && empty($getExternalOrderStatusResult))) {
            unset($this->GetExternalOrderStatusResult);
        } else {
            $this->GetExternalOrderStatusResult = $getExternalOrderStatusResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetExternalOrderStatusResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
