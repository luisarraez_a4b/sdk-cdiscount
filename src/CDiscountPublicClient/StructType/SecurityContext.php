<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SecurityContext StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SecurityContext
 * @subpackage Structs
 */
class SecurityContext extends AbstractStructBase
{
    /**
     * The DomainRightsList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDomainRights
     */
    public $DomainRightsList;
    /**
     * The IssuerID
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $IssuerID;
    /**
     * The SessionID
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $SessionID;
    /**
     * The SubjectLocality
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality
     */
    public $SubjectLocality;
    /**
     * The TokenId
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $TokenId;
    /**
     * The UserName
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $UserName;
    /**
     * Constructor method for SecurityContext
     * @uses SecurityContext::setDomainRightsList()
     * @uses SecurityContext::setIssuerID()
     * @uses SecurityContext::setSessionID()
     * @uses SecurityContext::setSubjectLocality()
     * @uses SecurityContext::setTokenId()
     * @uses SecurityContext::setUserName()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDomainRights $domainRightsList
     * @param string $issuerID
     * @param string $sessionID
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality $subjectLocality
     * @param string $tokenId
     * @param string $userName
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDomainRights $domainRightsList = null, $issuerID = null, $sessionID = null, \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality $subjectLocality = null, $tokenId = null, $userName = null)
    {
        $this
            ->setDomainRightsList($domainRightsList)
            ->setIssuerID($issuerID)
            ->setSessionID($sessionID)
            ->setSubjectLocality($subjectLocality)
            ->setTokenId($tokenId)
            ->setUserName($userName);
    }
    /**
     * Get DomainRightsList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDomainRights|null
     */
    public function getDomainRightsList()
    {
        return $this->DomainRightsList;
    }
    /**
     * Set DomainRightsList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDomainRights $domainRightsList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public function setDomainRightsList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDomainRights $domainRightsList = null)
    {
        $this->DomainRightsList = $domainRightsList;
        return $this;
    }
    /**
     * Get IssuerID value
     * @return string|null
     */
    public function getIssuerID()
    {
        return $this->IssuerID;
    }
    /**
     * Set IssuerID value
     * @param string $issuerID
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public function setIssuerID($issuerID = null)
    {
        // validation for constraint: string
        if (!is_null($issuerID) && !is_string($issuerID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($issuerID, true), gettype($issuerID)), __LINE__);
        }
        $this->IssuerID = $issuerID;
        return $this;
    }
    /**
     * Get SessionID value
     * @return string|null
     */
    public function getSessionID()
    {
        return $this->SessionID;
    }
    /**
     * Set SessionID value
     * @param string $sessionID
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public function setSessionID($sessionID = null)
    {
        // validation for constraint: string
        if (!is_null($sessionID) && !is_string($sessionID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sessionID, true), gettype($sessionID)), __LINE__);
        }
        $this->SessionID = $sessionID;
        return $this;
    }
    /**
     * Get SubjectLocality value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality|null
     */
    public function getSubjectLocality()
    {
        return isset($this->SubjectLocality) ? $this->SubjectLocality : null;
    }
    /**
     * Set SubjectLocality value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality $subjectLocality
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public function setSubjectLocality(\A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality $subjectLocality = null)
    {
        if (is_null($subjectLocality) || (is_array($subjectLocality) && empty($subjectLocality))) {
            unset($this->SubjectLocality);
        } else {
            $this->SubjectLocality = $subjectLocality;
        }
        return $this;
    }
    /**
     * Get TokenId value
     * @return string|null
     */
    public function getTokenId()
    {
        return $this->TokenId;
    }
    /**
     * Set TokenId value
     * @param string $tokenId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public function setTokenId($tokenId = null)
    {
        // validation for constraint: string
        if (!is_null($tokenId) && !is_string($tokenId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tokenId, true), gettype($tokenId)), __LINE__);
        }
        $this->TokenId = $tokenId;
        return $this;
    }
    /**
     * Get UserName value
     * @return string|null
     */
    public function getUserName()
    {
        return $this->UserName;
    }
    /**
     * Set UserName value
     * @param string $userName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public function setUserName($userName = null)
    {
        // validation for constraint: string
        if (!is_null($userName) && !is_string($userName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($userName, true), gettype($userName)), __LINE__);
        }
        $this->UserName = $userName;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityContext
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
