<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentActivationReportMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentActivationReportMessage
 * @subpackage Structs
 */
class FulfilmentActivationReportMessage extends ServiceBaseAPIMessage
{
    /**
     * The FulfilmentActivationReportList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport
     */
    public $FulfilmentActivationReportList;
    /**
     * Constructor method for FulfilmentActivationReportMessage
     * @uses FulfilmentActivationReportMessage::setFulfilmentActivationReportList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport $fulfilmentActivationReportList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport $fulfilmentActivationReportList = null)
    {
        $this
            ->setFulfilmentActivationReportList($fulfilmentActivationReportList);
    }
    /**
     * Get FulfilmentActivationReportList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport|null
     */
    public function getFulfilmentActivationReportList()
    {
        return $this->FulfilmentActivationReportList;
    }
    /**
     * Set FulfilmentActivationReportList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport $fulfilmentActivationReportList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReportMessage
     */
    public function setFulfilmentActivationReportList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport $fulfilmentActivationReportList = null)
    {
        $this->FulfilmentActivationReportList = $fulfilmentActivationReportList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReportMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
