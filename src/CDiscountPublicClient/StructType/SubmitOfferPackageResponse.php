<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitOfferPackageResponse StructType
 * @subpackage Structs
 */
class SubmitOfferPackageResponse extends AbstractStructBase
{
    /**
     * The SubmitOfferPackageResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferIntegrationReportMessage
     */
    public $SubmitOfferPackageResult;
    /**
     * Constructor method for SubmitOfferPackageResponse
     * @uses SubmitOfferPackageResponse::setSubmitOfferPackageResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferIntegrationReportMessage $submitOfferPackageResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferIntegrationReportMessage $submitOfferPackageResult = null)
    {
        $this
            ->setSubmitOfferPackageResult($submitOfferPackageResult);
    }
    /**
     * Get SubmitOfferPackageResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferIntegrationReportMessage|null
     */
    public function getSubmitOfferPackageResult()
    {
        return isset($this->SubmitOfferPackageResult) ? $this->SubmitOfferPackageResult : null;
    }
    /**
     * Set SubmitOfferPackageResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferIntegrationReportMessage $submitOfferPackageResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferPackageResponse
     */
    public function setSubmitOfferPackageResult(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferIntegrationReportMessage $submitOfferPackageResult = null)
    {
        if (is_null($submitOfferPackageResult) || (is_array($submitOfferPackageResult) && empty($submitOfferPackageResult))) {
            unset($this->SubmitOfferPackageResult);
        } else {
            $this->SubmitOfferPackageResult = $submitOfferPackageResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferPackageResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
