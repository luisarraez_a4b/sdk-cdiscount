<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidationResultMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ValidationResultMessage
 * @subpackage Structs
 */
class ValidationResultMessage extends ServiceBaseAPIMessage
{
    /**
     * The ValidateOrderResults
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderResult
     */
    public $ValidateOrderResults;
    /**
     * Constructor method for ValidationResultMessage
     * @uses ValidationResultMessage::setValidateOrderResults()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderResult $validateOrderResults
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderResult $validateOrderResults = null)
    {
        $this
            ->setValidateOrderResults($validateOrderResults);
    }
    /**
     * Get ValidateOrderResults value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderResult|null
     */
    public function getValidateOrderResults()
    {
        return isset($this->ValidateOrderResults) ? $this->ValidateOrderResults : null;
    }
    /**
     * Set ValidateOrderResults value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderResult $validateOrderResults
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidationResultMessage
     */
    public function setValidateOrderResults(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderResult $validateOrderResults = null)
    {
        if (is_null($validateOrderResults) || (is_array($validateOrderResults) && empty($validateOrderResults))) {
            unset($this->ValidateOrderResults);
        } else {
            $this->ValidateOrderResults = $validateOrderResults;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidationResultMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
