<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExternalOrderLine StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ExternalOrderLine
 * @subpackage Structs
 */
class ExternalOrderLine extends AbstractStructBase
{
    /**
     * The ProductEan
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ProductEan;
    /**
     * The ProductReference
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ProductReference;
    /**
     * The Quantity
     * @var int
     */
    public $Quantity;
    /**
     * Constructor method for ExternalOrderLine
     * @uses ExternalOrderLine::setProductEan()
     * @uses ExternalOrderLine::setProductReference()
     * @uses ExternalOrderLine::setQuantity()
     * @param string $productEan
     * @param string $productReference
     * @param int $quantity
     */
    public function __construct($productEan = null, $productReference = null, $quantity = null)
    {
        $this
            ->setProductEan($productEan)
            ->setProductReference($productReference)
            ->setQuantity($quantity);
    }
    /**
     * Get ProductEan value
     * @return string|null
     */
    public function getProductEan()
    {
        return $this->ProductEan;
    }
    /**
     * Set ProductEan value
     * @param string $productEan
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrderLine
     */
    public function setProductEan($productEan = null)
    {
        // validation for constraint: string
        if (!is_null($productEan) && !is_string($productEan)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productEan, true), gettype($productEan)), __LINE__);
        }
        $this->ProductEan = $productEan;
        return $this;
    }
    /**
     * Get ProductReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getProductReference()
    {
        return isset($this->ProductReference) ? $this->ProductReference : null;
    }
    /**
     * Set ProductReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $productReference
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrderLine
     */
    public function setProductReference($productReference = null)
    {
        // validation for constraint: string
        if (!is_null($productReference) && !is_string($productReference)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productReference, true), gettype($productReference)), __LINE__);
        }
        if (is_null($productReference) || (is_array($productReference) && empty($productReference))) {
            unset($this->ProductReference);
        } else {
            $this->ProductReference = $productReference;
        }
        return $this;
    }
    /**
     * Get Quantity value
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->Quantity;
    }
    /**
     * Set Quantity value
     * @param int $quantity
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrderLine
     */
    public function setQuantity($quantity = null)
    {
        // validation for constraint: int
        if (!is_null($quantity) && !(is_int($quantity) || ctype_digit($quantity))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->Quantity = $quantity;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ExternalOrderLine
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
