<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Customer StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:Customer
 * @subpackage Structs
 */
class Customer extends AbstractStructBase
{
    /**
     * The Civility
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Civility;
    /**
     * The CustomerId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $CustomerId;
    /**
     * The Email
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Email;
    /**
     * The EncryptedEmail
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EncryptedEmail;
    /**
     * The FirstName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $FirstName;
    /**
     * The LastName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $LastName;
    /**
     * The MobilePhone
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $MobilePhone;
    /**
     * The Phone
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Phone;
    /**
     * The ShippingFirstName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ShippingFirstName;
    /**
     * The ShippingLastName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ShippingLastName;
    /**
     * Constructor method for Customer
     * @uses Customer::setCivility()
     * @uses Customer::setCustomerId()
     * @uses Customer::setEmail()
     * @uses Customer::setEncryptedEmail()
     * @uses Customer::setFirstName()
     * @uses Customer::setLastName()
     * @uses Customer::setMobilePhone()
     * @uses Customer::setPhone()
     * @uses Customer::setShippingFirstName()
     * @uses Customer::setShippingLastName()
     * @param string $civility
     * @param string $customerId
     * @param string $email
     * @param string $encryptedEmail
     * @param string $firstName
     * @param string $lastName
     * @param string $mobilePhone
     * @param string $phone
     * @param string $shippingFirstName
     * @param string $shippingLastName
     */
    public function __construct($civility = null, $customerId = null, $email = null, $encryptedEmail = null, $firstName = null, $lastName = null, $mobilePhone = null, $phone = null, $shippingFirstName = null, $shippingLastName = null)
    {
        $this
            ->setCivility($civility)
            ->setCustomerId($customerId)
            ->setEmail($email)
            ->setEncryptedEmail($encryptedEmail)
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setMobilePhone($mobilePhone)
            ->setPhone($phone)
            ->setShippingFirstName($shippingFirstName)
            ->setShippingLastName($shippingLastName);
    }
    /**
     * Get Civility value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCivility()
    {
        return isset($this->Civility) ? $this->Civility : null;
    }
    /**
     * Set Civility value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Civility::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Civility::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $civility
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setCivility($civility = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\Civility::valueIsValid($civility)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\Civility', is_array($civility) ? implode(', ', $civility) : var_export($civility, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\Civility::getValidValues())), __LINE__);
        }
        if (is_null($civility) || (is_array($civility) && empty($civility))) {
            unset($this->Civility);
        } else {
            $this->Civility = $civility;
        }
        return $this;
    }
    /**
     * Get CustomerId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCustomerId()
    {
        return isset($this->CustomerId) ? $this->CustomerId : null;
    }
    /**
     * Set CustomerId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $customerId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setCustomerId($customerId = null)
    {
        // validation for constraint: string
        if (!is_null($customerId) && !is_string($customerId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($customerId, true), gettype($customerId)), __LINE__);
        }
        if (is_null($customerId) || (is_array($customerId) && empty($customerId))) {
            unset($this->CustomerId);
        } else {
            $this->CustomerId = $customerId;
        }
        return $this;
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail()
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        return $this;
    }
    /**
     * Get EncryptedEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEncryptedEmail()
    {
        return isset($this->EncryptedEmail) ? $this->EncryptedEmail : null;
    }
    /**
     * Set EncryptedEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $encryptedEmail
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setEncryptedEmail($encryptedEmail = null)
    {
        // validation for constraint: string
        if (!is_null($encryptedEmail) && !is_string($encryptedEmail)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($encryptedEmail, true), gettype($encryptedEmail)), __LINE__);
        }
        if (is_null($encryptedEmail) || (is_array($encryptedEmail) && empty($encryptedEmail))) {
            unset($this->EncryptedEmail);
        } else {
            $this->EncryptedEmail = $encryptedEmail;
        }
        return $this;
    }
    /**
     * Get FirstName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getFirstName()
    {
        return isset($this->FirstName) ? $this->FirstName : null;
    }
    /**
     * Set FirstName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $firstName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setFirstName($firstName = null)
    {
        // validation for constraint: string
        if (!is_null($firstName) && !is_string($firstName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($firstName, true), gettype($firstName)), __LINE__);
        }
        if (is_null($firstName) || (is_array($firstName) && empty($firstName))) {
            unset($this->FirstName);
        } else {
            $this->FirstName = $firstName;
        }
        return $this;
    }
    /**
     * Get LastName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLastName()
    {
        return isset($this->LastName) ? $this->LastName : null;
    }
    /**
     * Set LastName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $lastName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setLastName($lastName = null)
    {
        // validation for constraint: string
        if (!is_null($lastName) && !is_string($lastName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastName, true), gettype($lastName)), __LINE__);
        }
        if (is_null($lastName) || (is_array($lastName) && empty($lastName))) {
            unset($this->LastName);
        } else {
            $this->LastName = $lastName;
        }
        return $this;
    }
    /**
     * Get MobilePhone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMobilePhone()
    {
        return isset($this->MobilePhone) ? $this->MobilePhone : null;
    }
    /**
     * Set MobilePhone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mobilePhone
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setMobilePhone($mobilePhone = null)
    {
        // validation for constraint: string
        if (!is_null($mobilePhone) && !is_string($mobilePhone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mobilePhone, true), gettype($mobilePhone)), __LINE__);
        }
        if (is_null($mobilePhone) || (is_array($mobilePhone) && empty($mobilePhone))) {
            unset($this->MobilePhone);
        } else {
            $this->MobilePhone = $mobilePhone;
        }
        return $this;
    }
    /**
     * Get Phone value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPhone()
    {
        return isset($this->Phone) ? $this->Phone : null;
    }
    /**
     * Set Phone value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $phone
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setPhone($phone = null)
    {
        // validation for constraint: string
        if (!is_null($phone) && !is_string($phone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phone, true), gettype($phone)), __LINE__);
        }
        if (is_null($phone) || (is_array($phone) && empty($phone))) {
            unset($this->Phone);
        } else {
            $this->Phone = $phone;
        }
        return $this;
    }
    /**
     * Get ShippingFirstName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShippingFirstName()
    {
        return isset($this->ShippingFirstName) ? $this->ShippingFirstName : null;
    }
    /**
     * Set ShippingFirstName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shippingFirstName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setShippingFirstName($shippingFirstName = null)
    {
        // validation for constraint: string
        if (!is_null($shippingFirstName) && !is_string($shippingFirstName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shippingFirstName, true), gettype($shippingFirstName)), __LINE__);
        }
        if (is_null($shippingFirstName) || (is_array($shippingFirstName) && empty($shippingFirstName))) {
            unset($this->ShippingFirstName);
        } else {
            $this->ShippingFirstName = $shippingFirstName;
        }
        return $this;
    }
    /**
     * Get ShippingLastName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShippingLastName()
    {
        return isset($this->ShippingLastName) ? $this->ShippingLastName : null;
    }
    /**
     * Set ShippingLastName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shippingLastName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public function setShippingLastName($shippingLastName = null)
    {
        // validation for constraint: string
        if (!is_null($shippingLastName) && !is_string($shippingLastName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shippingLastName, true), gettype($shippingLastName)), __LINE__);
        }
        if (is_null($shippingLastName) || (is_array($shippingLastName) && empty($shippingLastName))) {
            unset($this->ShippingLastName);
        } else {
            $this->ShippingLastName = $shippingLastName;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Customer
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
