<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SupplyOrderReportListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SupplyOrderReportListMessage
 * @subpackage Structs
 */
class SupplyOrderReportListMessage extends ServiceBaseAPIMessage
{
    /**
     * The CurrentPageNumber
     * @var int
     */
    public $CurrentPageNumber;
    /**
     * The NumberOfPages
     * @var int
     */
    public $NumberOfPages;
    /**
     * The ReportList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReport
     */
    public $ReportList;
    /**
     * Constructor method for SupplyOrderReportListMessage
     * @uses SupplyOrderReportListMessage::setCurrentPageNumber()
     * @uses SupplyOrderReportListMessage::setNumberOfPages()
     * @uses SupplyOrderReportListMessage::setReportList()
     * @param int $currentPageNumber
     * @param int $numberOfPages
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReport $reportList
     */
    public function __construct($currentPageNumber = null, $numberOfPages = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReport $reportList = null)
    {
        $this
            ->setCurrentPageNumber($currentPageNumber)
            ->setNumberOfPages($numberOfPages)
            ->setReportList($reportList);
    }
    /**
     * Get CurrentPageNumber value
     * @return int|null
     */
    public function getCurrentPageNumber()
    {
        return $this->CurrentPageNumber;
    }
    /**
     * Set CurrentPageNumber value
     * @param int $currentPageNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage
     */
    public function setCurrentPageNumber($currentPageNumber = null)
    {
        // validation for constraint: int
        if (!is_null($currentPageNumber) && !(is_int($currentPageNumber) || ctype_digit($currentPageNumber))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($currentPageNumber, true), gettype($currentPageNumber)), __LINE__);
        }
        $this->CurrentPageNumber = $currentPageNumber;
        return $this;
    }
    /**
     * Get NumberOfPages value
     * @return int|null
     */
    public function getNumberOfPages()
    {
        return $this->NumberOfPages;
    }
    /**
     * Set NumberOfPages value
     * @param int $numberOfPages
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage
     */
    public function setNumberOfPages($numberOfPages = null)
    {
        // validation for constraint: int
        if (!is_null($numberOfPages) && !(is_int($numberOfPages) || ctype_digit($numberOfPages))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($numberOfPages, true), gettype($numberOfPages)), __LINE__);
        }
        $this->NumberOfPages = $numberOfPages;
        return $this;
    }
    /**
     * Get ReportList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReport|null
     */
    public function getReportList()
    {
        return $this->ReportList;
    }
    /**
     * Set ReportList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReport $reportList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage
     */
    public function setReportList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderReport $reportList = null)
    {
        $this->ReportList = $reportList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
