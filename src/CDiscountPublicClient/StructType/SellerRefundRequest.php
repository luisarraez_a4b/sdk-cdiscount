<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SellerRefundRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerRefundRequest
 * @subpackage Structs
 */
class SellerRefundRequest extends AbstractStructBase
{
    /**
     * The Mode
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Mode;
    /**
     * The Motive
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $Motive;
    /**
     * The RefundOrderLine
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundOrderLine
     */
    public $RefundOrderLine;
    /**
     * Constructor method for SellerRefundRequest
     * @uses SellerRefundRequest::setMode()
     * @uses SellerRefundRequest::setMotive()
     * @uses SellerRefundRequest::setRefundOrderLine()
     * @param string $mode
     * @param string $motive
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundOrderLine $refundOrderLine
     */
    public function __construct($mode = null, $motive = null, \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundOrderLine $refundOrderLine = null)
    {
        $this
            ->setMode($mode)
            ->setMotive($motive)
            ->setRefundOrderLine($refundOrderLine);
    }
    /**
     * Get Mode value
     * @return string|null
     */
    public function getMode()
    {
        return $this->Mode;
    }
    /**
     * Set Mode value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerRefundRequestMode::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerRefundRequestMode::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $mode
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest
     */
    public function setMode($mode = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\SellerRefundRequestMode::valueIsValid($mode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\SellerRefundRequestMode', is_array($mode) ? implode(', ', $mode) : var_export($mode, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerRefundRequestMode::getValidValues())), __LINE__);
        }
        $this->Mode = $mode;
        return $this;
    }
    /**
     * Get Motive value
     * @return string|null
     */
    public function getMotive()
    {
        return $this->Motive;
    }
    /**
     * Set Motive value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $motive
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest
     */
    public function setMotive($motive = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::valueIsValid($motive)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\RefundMotive', is_array($motive) ? implode(', ', $motive) : var_export($motive, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\RefundMotive::getValidValues())), __LINE__);
        }
        $this->Motive = $motive;
        return $this;
    }
    /**
     * Get RefundOrderLine value
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundOrderLine|null
     */
    public function getRefundOrderLine()
    {
        return $this->RefundOrderLine;
    }
    /**
     * Set RefundOrderLine value
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundOrderLine $refundOrderLine
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest
     */
    public function setRefundOrderLine(\A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundOrderLine $refundOrderLine = null)
    {
        $this->RefundOrderLine = $refundOrderLine;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
