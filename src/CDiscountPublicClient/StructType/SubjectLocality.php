<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubjectLocality StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SubjectLocality
 * @subpackage Structs
 */
class SubjectLocality extends AbstractStructBase
{
    /**
     * The Address
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Address;
    /**
     * The DnsName
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $DnsName;
    /**
     * Constructor method for SubjectLocality
     * @uses SubjectLocality::setAddress()
     * @uses SubjectLocality::setDnsName()
     * @param string $address
     * @param string $dnsName
     */
    public function __construct($address = null, $dnsName = null)
    {
        $this
            ->setAddress($address)
            ->setDnsName($dnsName);
    }
    /**
     * Get Address value
     * @return string|null
     */
    public function getAddress()
    {
        return $this->Address;
    }
    /**
     * Set Address value
     * @param string $address
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality
     */
    public function setAddress($address = null)
    {
        // validation for constraint: string
        if (!is_null($address) && !is_string($address)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($address, true), gettype($address)), __LINE__);
        }
        $this->Address = $address;
        return $this;
    }
    /**
     * Get DnsName value
     * @return string|null
     */
    public function getDnsName()
    {
        return $this->DnsName;
    }
    /**
     * Set DnsName value
     * @param string $dnsName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality
     */
    public function setDnsName($dnsName = null)
    {
        // validation for constraint: string
        if (!is_null($dnsName) && !is_string($dnsName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dnsName, true), gettype($dnsName)), __LINE__);
        }
        $this->DnsName = $dnsName;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubjectLocality
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
