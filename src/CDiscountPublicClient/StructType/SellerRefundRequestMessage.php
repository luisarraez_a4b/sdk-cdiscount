<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SellerRefundRequestMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerRefundRequestMessage
 * @subpackage Structs
 */
class SellerRefundRequestMessage extends AbstractStructBase
{
    /**
     * The OrderNumber
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $OrderNumber;
    /**
     * The SellerRefundRequestList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest
     */
    public $SellerRefundRequestList;
    /**
     * Constructor method for SellerRefundRequestMessage
     * @uses SellerRefundRequestMessage::setOrderNumber()
     * @uses SellerRefundRequestMessage::setSellerRefundRequestList()
     * @param string $orderNumber
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundRequestList
     */
    public function __construct($orderNumber = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundRequestList = null)
    {
        $this
            ->setOrderNumber($orderNumber)
            ->setSellerRefundRequestList($sellerRefundRequestList);
    }
    /**
     * Get OrderNumber value
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->OrderNumber;
    }
    /**
     * Set OrderNumber value
     * @param string $orderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequestMessage
     */
    public function setOrderNumber($orderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($orderNumber) && !is_string($orderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderNumber, true), gettype($orderNumber)), __LINE__);
        }
        $this->OrderNumber = $orderNumber;
        return $this;
    }
    /**
     * Get SellerRefundRequestList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest|null
     */
    public function getSellerRefundRequestList()
    {
        return $this->SellerRefundRequestList;
    }
    /**
     * Set SellerRefundRequestList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundRequestList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequestMessage
     */
    public function setSellerRefundRequestList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundRequestList = null)
    {
        $this->SellerRefundRequestList = $sellerRefundRequestList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequestMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
