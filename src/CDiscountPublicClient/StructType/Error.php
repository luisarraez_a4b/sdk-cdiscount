<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Error StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:Error
 * @subpackage Structs
 */
class Error extends AbstractStructBase
{
    /**
     * The ErrorType
     * @var string
     */
    public $ErrorType;
    /**
     * The Message
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Message;
    /**
     * Constructor method for Error
     * @uses Error::setErrorType()
     * @uses Error::setMessage()
     * @param string $errorType
     * @param string $message
     */
    public function __construct($errorType = null, $message = null)
    {
        $this
            ->setErrorType($errorType)
            ->setMessage($message);
    }
    /**
     * Get ErrorType value
     * @return string|null
     */
    public function getErrorType()
    {
        return $this->ErrorType;
    }
    /**
     * Set ErrorType value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ErrorTypeEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ErrorTypeEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $errorType
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Error
     */
    public function setErrorType($errorType = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ErrorTypeEnum::valueIsValid($errorType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ErrorTypeEnum', is_array($errorType) ? implode(', ', $errorType) : var_export($errorType, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ErrorTypeEnum::getValidValues())), __LINE__);
        }
        $this->ErrorType = $errorType;
        return $this;
    }
    /**
     * Get Message value
     * @return string|null
     */
    public function getMessage()
    {
        return $this->Message;
    }
    /**
     * Set Message value
     * @param string $message
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Error
     */
    public function setMessage($message = null)
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        $this->Message = $message;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Error
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
