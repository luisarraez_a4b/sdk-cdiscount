<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SupplyOrderRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SupplyOrderRequest
 * @subpackage Structs
 */
class SupplyOrderRequest extends AbstractStructBase
{
    /**
     * The BeginModificationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $BeginModificationDate;
    /**
     * The EndModificationDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $EndModificationDate;
    /**
     * The PageNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $PageNumber;
    /**
     * The PageSize
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $PageSize;
    /**
     * The SupplyOrderNumberList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $SupplyOrderNumberList;
    /**
     * Constructor method for SupplyOrderRequest
     * @uses SupplyOrderRequest::setBeginModificationDate()
     * @uses SupplyOrderRequest::setEndModificationDate()
     * @uses SupplyOrderRequest::setPageNumber()
     * @uses SupplyOrderRequest::setPageSize()
     * @uses SupplyOrderRequest::setSupplyOrderNumberList()
     * @param string $beginModificationDate
     * @param string $endModificationDate
     * @param int $pageNumber
     * @param int $pageSize
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $supplyOrderNumberList
     */
    public function __construct($beginModificationDate = null, $endModificationDate = null, $pageNumber = null, $pageSize = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $supplyOrderNumberList = null)
    {
        $this
            ->setBeginModificationDate($beginModificationDate)
            ->setEndModificationDate($endModificationDate)
            ->setPageNumber($pageNumber)
            ->setPageSize($pageSize)
            ->setSupplyOrderNumberList($supplyOrderNumberList);
    }
    /**
     * Get BeginModificationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getBeginModificationDate()
    {
        return isset($this->BeginModificationDate) ? $this->BeginModificationDate : null;
    }
    /**
     * Set BeginModificationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $beginModificationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderRequest
     */
    public function setBeginModificationDate($beginModificationDate = null)
    {
        // validation for constraint: string
        if (!is_null($beginModificationDate) && !is_string($beginModificationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($beginModificationDate, true), gettype($beginModificationDate)), __LINE__);
        }
        if (is_null($beginModificationDate) || (is_array($beginModificationDate) && empty($beginModificationDate))) {
            unset($this->BeginModificationDate);
        } else {
            $this->BeginModificationDate = $beginModificationDate;
        }
        return $this;
    }
    /**
     * Get EndModificationDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEndModificationDate()
    {
        return isset($this->EndModificationDate) ? $this->EndModificationDate : null;
    }
    /**
     * Set EndModificationDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $endModificationDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderRequest
     */
    public function setEndModificationDate($endModificationDate = null)
    {
        // validation for constraint: string
        if (!is_null($endModificationDate) && !is_string($endModificationDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endModificationDate, true), gettype($endModificationDate)), __LINE__);
        }
        if (is_null($endModificationDate) || (is_array($endModificationDate) && empty($endModificationDate))) {
            unset($this->EndModificationDate);
        } else {
            $this->EndModificationDate = $endModificationDate;
        }
        return $this;
    }
    /**
     * Get PageNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPageNumber()
    {
        return isset($this->PageNumber) ? $this->PageNumber : null;
    }
    /**
     * Set PageNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $pageNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderRequest
     */
    public function setPageNumber($pageNumber = null)
    {
        // validation for constraint: int
        if (!is_null($pageNumber) && !(is_int($pageNumber) || ctype_digit($pageNumber))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageNumber, true), gettype($pageNumber)), __LINE__);
        }
        if (is_null($pageNumber) || (is_array($pageNumber) && empty($pageNumber))) {
            unset($this->PageNumber);
        } else {
            $this->PageNumber = $pageNumber;
        }
        return $this;
    }
    /**
     * Get PageSize value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPageSize()
    {
        return isset($this->PageSize) ? $this->PageSize : null;
    }
    /**
     * Set PageSize value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $pageSize
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderRequest
     */
    public function setPageSize($pageSize = null)
    {
        // validation for constraint: int
        if (!is_null($pageSize) && !(is_int($pageSize) || ctype_digit($pageSize))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageSize, true), gettype($pageSize)), __LINE__);
        }
        if (is_null($pageSize) || (is_array($pageSize) && empty($pageSize))) {
            unset($this->PageSize);
        } else {
            $this->PageSize = $pageSize;
        }
        return $this;
    }
    /**
     * Get SupplyOrderNumberList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getSupplyOrderNumberList()
    {
        return isset($this->SupplyOrderNumberList) ? $this->SupplyOrderNumberList : null;
    }
    /**
     * Set SupplyOrderNumberList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $supplyOrderNumberList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderRequest
     */
    public function setSupplyOrderNumberList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $supplyOrderNumberList = null)
    {
        if (is_null($supplyOrderNumberList) || (is_array($supplyOrderNumberList) && empty($supplyOrderNumberList))) {
            unset($this->SupplyOrderNumberList);
        } else {
            $this->SupplyOrderNumberList = $supplyOrderNumberList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
