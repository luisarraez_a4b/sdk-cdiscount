<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CreateRefundVoucherRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:CreateRefundVoucherRequest
 * @subpackage Structs
 */
class CreateRefundVoucherRequest extends AbstractStructBase
{
    /**
     * The CommercialGestureList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformation
     */
    public $CommercialGestureList;
    /**
     * The OrderNumber
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $OrderNumber;
    /**
     * The SellerRefundList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest
     */
    public $SellerRefundList;
    /**
     * Constructor method for CreateRefundVoucherRequest
     * @uses CreateRefundVoucherRequest::setCommercialGestureList()
     * @uses CreateRefundVoucherRequest::setOrderNumber()
     * @uses CreateRefundVoucherRequest::setSellerRefundList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformation $commercialGestureList
     * @param string $orderNumber
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformation $commercialGestureList = null, $orderNumber = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundList = null)
    {
        $this
            ->setCommercialGestureList($commercialGestureList)
            ->setOrderNumber($orderNumber)
            ->setSellerRefundList($sellerRefundList);
    }
    /**
     * Get CommercialGestureList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformation|null
     */
    public function getCommercialGestureList()
    {
        return isset($this->CommercialGestureList) ? $this->CommercialGestureList : null;
    }
    /**
     * Set CommercialGestureList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformation $commercialGestureList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherRequest
     */
    public function setCommercialGestureList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfRefundInformation $commercialGestureList = null)
    {
        if (is_null($commercialGestureList) || (is_array($commercialGestureList) && empty($commercialGestureList))) {
            unset($this->CommercialGestureList);
        } else {
            $this->CommercialGestureList = $commercialGestureList;
        }
        return $this;
    }
    /**
     * Get OrderNumber value
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->OrderNumber;
    }
    /**
     * Set OrderNumber value
     * @param string $orderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherRequest
     */
    public function setOrderNumber($orderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($orderNumber) && !is_string($orderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderNumber, true), gettype($orderNumber)), __LINE__);
        }
        $this->OrderNumber = $orderNumber;
        return $this;
    }
    /**
     * Get SellerRefundList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest|null
     */
    public function getSellerRefundList()
    {
        return isset($this->SellerRefundList) ? $this->SellerRefundList : null;
    }
    /**
     * Set SellerRefundList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherRequest
     */
    public function setSellerRefundList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest $sellerRefundList = null)
    {
        if (is_null($sellerRefundList) || (is_array($sellerRefundList) && empty($sellerRefundList))) {
            unset($this->SellerRefundList);
        } else {
            $this->SellerRefundList = $sellerRefundList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
