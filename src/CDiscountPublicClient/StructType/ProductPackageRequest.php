<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductPackageRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductPackageRequest
 * @subpackage Structs
 */
class ProductPackageRequest extends AbstractStructBase
{
    /**
     * The ZipFileFullPath
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ZipFileFullPath;
    /**
     * Constructor method for ProductPackageRequest
     * @uses ProductPackageRequest::setZipFileFullPath()
     * @param string $zipFileFullPath
     */
    public function __construct($zipFileFullPath = null)
    {
        $this
            ->setZipFileFullPath($zipFileFullPath);
    }
    /**
     * Get ZipFileFullPath value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipFileFullPath()
    {
        return isset($this->ZipFileFullPath) ? $this->ZipFileFullPath : null;
    }
    /**
     * Set ZipFileFullPath value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipFileFullPath
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductPackageRequest
     */
    public function setZipFileFullPath($zipFileFullPath = null)
    {
        // validation for constraint: string
        if (!is_null($zipFileFullPath) && !is_string($zipFileFullPath)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipFileFullPath, true), gettype($zipFileFullPath)), __LINE__);
        }
        if (is_null($zipFileFullPath) || (is_array($zipFileFullPath) && empty($zipFileFullPath))) {
            unset($this->ZipFileFullPath);
        } else {
            $this->ZipFileFullPath = $zipFileFullPath;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductPackageRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
