<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CategoryTree StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:CategoryTree
 * @subpackage Structs
 */
class CategoryTree extends AbstractStructBase
{
    /**
     * The AllowOfferIntegration
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $AllowOfferIntegration;
    /**
     * The AllowProductIntegration
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $AllowProductIntegration;
    /**
     * The ChildrenCategoryList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree
     */
    public $ChildrenCategoryList;
    /**
     * The Code
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Code;
    /**
     * The IsEANOptional
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $IsEANOptional;
    /**
     * The IsStandardProductKindEligible
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsStandardProductKindEligible;
    /**
     * The IsVariantProductKindEligible
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $IsVariantProductKindEligible;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * Constructor method for CategoryTree
     * @uses CategoryTree::setAllowOfferIntegration()
     * @uses CategoryTree::setAllowProductIntegration()
     * @uses CategoryTree::setChildrenCategoryList()
     * @uses CategoryTree::setCode()
     * @uses CategoryTree::setIsEANOptional()
     * @uses CategoryTree::setIsStandardProductKindEligible()
     * @uses CategoryTree::setIsVariantProductKindEligible()
     * @uses CategoryTree::setName()
     * @param bool $allowOfferIntegration
     * @param bool $allowProductIntegration
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree $childrenCategoryList
     * @param string $code
     * @param bool $isEANOptional
     * @param bool $isStandardProductKindEligible
     * @param bool $isVariantProductKindEligible
     * @param string $name
     */
    public function __construct($allowOfferIntegration = null, $allowProductIntegration = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree $childrenCategoryList = null, $code = null, $isEANOptional = null, $isStandardProductKindEligible = null, $isVariantProductKindEligible = null, $name = null)
    {
        $this
            ->setAllowOfferIntegration($allowOfferIntegration)
            ->setAllowProductIntegration($allowProductIntegration)
            ->setChildrenCategoryList($childrenCategoryList)
            ->setCode($code)
            ->setIsEANOptional($isEANOptional)
            ->setIsStandardProductKindEligible($isStandardProductKindEligible)
            ->setIsVariantProductKindEligible($isVariantProductKindEligible)
            ->setName($name);
    }
    /**
     * Get AllowOfferIntegration value
     * @return bool|null
     */
    public function getAllowOfferIntegration()
    {
        return $this->AllowOfferIntegration;
    }
    /**
     * Set AllowOfferIntegration value
     * @param bool $allowOfferIntegration
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setAllowOfferIntegration($allowOfferIntegration = null)
    {
        // validation for constraint: boolean
        if (!is_null($allowOfferIntegration) && !is_bool($allowOfferIntegration)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowOfferIntegration, true), gettype($allowOfferIntegration)), __LINE__);
        }
        $this->AllowOfferIntegration = $allowOfferIntegration;
        return $this;
    }
    /**
     * Get AllowProductIntegration value
     * @return bool|null
     */
    public function getAllowProductIntegration()
    {
        return $this->AllowProductIntegration;
    }
    /**
     * Set AllowProductIntegration value
     * @param bool $allowProductIntegration
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setAllowProductIntegration($allowProductIntegration = null)
    {
        // validation for constraint: boolean
        if (!is_null($allowProductIntegration) && !is_bool($allowProductIntegration)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($allowProductIntegration, true), gettype($allowProductIntegration)), __LINE__);
        }
        $this->AllowProductIntegration = $allowProductIntegration;
        return $this;
    }
    /**
     * Get ChildrenCategoryList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree|null
     */
    public function getChildrenCategoryList()
    {
        return isset($this->ChildrenCategoryList) ? $this->ChildrenCategoryList : null;
    }
    /**
     * Set ChildrenCategoryList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree $childrenCategoryList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setChildrenCategoryList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree $childrenCategoryList = null)
    {
        if (is_null($childrenCategoryList) || (is_array($childrenCategoryList) && empty($childrenCategoryList))) {
            unset($this->ChildrenCategoryList);
        } else {
            $this->ChildrenCategoryList = $childrenCategoryList;
        }
        return $this;
    }
    /**
     * Get Code value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCode()
    {
        return isset($this->Code) ? $this->Code : null;
    }
    /**
     * Set Code value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $code
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setCode($code = null)
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        if (is_null($code) || (is_array($code) && empty($code))) {
            unset($this->Code);
        } else {
            $this->Code = $code;
        }
        return $this;
    }
    /**
     * Get IsEANOptional value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getIsEANOptional()
    {
        return isset($this->IsEANOptional) ? $this->IsEANOptional : null;
    }
    /**
     * Set IsEANOptional value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $isEANOptional
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setIsEANOptional($isEANOptional = null)
    {
        // validation for constraint: boolean
        if (!is_null($isEANOptional) && !is_bool($isEANOptional)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isEANOptional, true), gettype($isEANOptional)), __LINE__);
        }
        if (is_null($isEANOptional) || (is_array($isEANOptional) && empty($isEANOptional))) {
            unset($this->IsEANOptional);
        } else {
            $this->IsEANOptional = $isEANOptional;
        }
        return $this;
    }
    /**
     * Get IsStandardProductKindEligible value
     * @return bool|null
     */
    public function getIsStandardProductKindEligible()
    {
        return $this->IsStandardProductKindEligible;
    }
    /**
     * Set IsStandardProductKindEligible value
     * @param bool $isStandardProductKindEligible
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setIsStandardProductKindEligible($isStandardProductKindEligible = null)
    {
        // validation for constraint: boolean
        if (!is_null($isStandardProductKindEligible) && !is_bool($isStandardProductKindEligible)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isStandardProductKindEligible, true), gettype($isStandardProductKindEligible)), __LINE__);
        }
        $this->IsStandardProductKindEligible = $isStandardProductKindEligible;
        return $this;
    }
    /**
     * Get IsVariantProductKindEligible value
     * @return bool|null
     */
    public function getIsVariantProductKindEligible()
    {
        return $this->IsVariantProductKindEligible;
    }
    /**
     * Set IsVariantProductKindEligible value
     * @param bool $isVariantProductKindEligible
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setIsVariantProductKindEligible($isVariantProductKindEligible = null)
    {
        // validation for constraint: boolean
        if (!is_null($isVariantProductKindEligible) && !is_bool($isVariantProductKindEligible)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($isVariantProductKindEligible, true), gettype($isVariantProductKindEligible)), __LINE__);
        }
        $this->IsVariantProductKindEligible = $isVariantProductKindEligible;
        return $this;
    }
    /**
     * Get Name value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getName()
    {
        return isset($this->Name) ? $this->Name : null;
    }
    /**
     * Set Name value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $name
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        if (is_null($name) || (is_array($name) && empty($name))) {
            unset($this->Name);
        } else {
            $this->Name = $name;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
