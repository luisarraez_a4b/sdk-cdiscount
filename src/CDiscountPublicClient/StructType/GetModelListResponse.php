<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetModelListResponse StructType
 * @subpackage Structs
 */
class GetModelListResponse extends AbstractStructBase
{
    /**
     * The GetModelListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage
     */
    public $GetModelListResult;
    /**
     * Constructor method for GetModelListResponse
     * @uses GetModelListResponse::setGetModelListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage $getModelListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage $getModelListResult = null)
    {
        $this
            ->setGetModelListResult($getModelListResult);
    }
    /**
     * Get GetModelListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage|null
     */
    public function getGetModelListResult()
    {
        return isset($this->GetModelListResult) ? $this->GetModelListResult : null;
    }
    /**
     * Set GetModelListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage $getModelListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetModelListResponse
     */
    public function setGetModelListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductModelListMessage $getModelListResult = null)
    {
        if (is_null($getModelListResult) || (is_array($getModelListResult) && empty($getModelListResult))) {
            unset($this->GetModelListResult);
        } else {
            $this->GetModelListResult = $getModelListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetModelListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
