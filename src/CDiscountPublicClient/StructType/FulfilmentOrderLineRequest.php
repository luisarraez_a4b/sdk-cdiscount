<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentOrderLineRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentOrderLineRequest
 * @subpackage Structs
 */
class FulfilmentOrderLineRequest extends AbstractStructBase
{
    /**
     * The OrderReference
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $OrderReference;
    /**
     * The ProductEan
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ProductEan;
    /**
     * Constructor method for FulfilmentOrderLineRequest
     * @uses FulfilmentOrderLineRequest::setOrderReference()
     * @uses FulfilmentOrderLineRequest::setProductEan()
     * @param string $orderReference
     * @param string $productEan
     */
    public function __construct($orderReference = null, $productEan = null)
    {
        $this
            ->setOrderReference($orderReference)
            ->setProductEan($productEan);
    }
    /**
     * Get OrderReference value
     * @return string|null
     */
    public function getOrderReference()
    {
        return $this->OrderReference;
    }
    /**
     * Set OrderReference value
     * @param string $orderReference
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest
     */
    public function setOrderReference($orderReference = null)
    {
        // validation for constraint: string
        if (!is_null($orderReference) && !is_string($orderReference)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderReference, true), gettype($orderReference)), __LINE__);
        }
        $this->OrderReference = $orderReference;
        return $this;
    }
    /**
     * Get ProductEan value
     * @return string|null
     */
    public function getProductEan()
    {
        return $this->ProductEan;
    }
    /**
     * Set ProductEan value
     * @param string $productEan
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest
     */
    public function setProductEan($productEan = null)
    {
        // validation for constraint: string
        if (!is_null($productEan) && !is_string($productEan)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productEan, true), gettype($productEan)), __LINE__);
        }
        $this->ProductEan = $productEan;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
