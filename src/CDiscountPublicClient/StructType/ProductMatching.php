<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ProductMatching StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductMatching
 * @subpackage Structs
 */
class ProductMatching extends AbstractStructBase
{
    /**
     * The Color
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Color;
    /**
     * The Comment
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Comment;
    /**
     * The Ean
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Ean;
    /**
     * The MatchingStatus
     * @var string
     */
    public $MatchingStatus;
    /**
     * The Name
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Name;
    /**
     * The SellerProductId
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $SellerProductId;
    /**
     * The Size
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Size;
    /**
     * The Sku
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Sku;
    /**
     * Constructor method for ProductMatching
     * @uses ProductMatching::setColor()
     * @uses ProductMatching::setComment()
     * @uses ProductMatching::setEan()
     * @uses ProductMatching::setMatchingStatus()
     * @uses ProductMatching::setName()
     * @uses ProductMatching::setSellerProductId()
     * @uses ProductMatching::setSize()
     * @uses ProductMatching::setSku()
     * @param string $color
     * @param string $comment
     * @param string $ean
     * @param string $matchingStatus
     * @param string $name
     * @param string $sellerProductId
     * @param string $size
     * @param string $sku
     */
    public function __construct($color = null, $comment = null, $ean = null, $matchingStatus = null, $name = null, $sellerProductId = null, $size = null, $sku = null)
    {
        $this
            ->setColor($color)
            ->setComment($comment)
            ->setEan($ean)
            ->setMatchingStatus($matchingStatus)
            ->setName($name)
            ->setSellerProductId($sellerProductId)
            ->setSize($size)
            ->setSku($sku);
    }
    /**
     * Get Color value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getColor()
    {
        return isset($this->Color) ? $this->Color : null;
    }
    /**
     * Set Color value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $color
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setColor($color = null)
    {
        // validation for constraint: string
        if (!is_null($color) && !is_string($color)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($color, true), gettype($color)), __LINE__);
        }
        if (is_null($color) || (is_array($color) && empty($color))) {
            unset($this->Color);
        } else {
            $this->Color = $color;
        }
        return $this;
    }
    /**
     * Get Comment value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getComment()
    {
        return isset($this->Comment) ? $this->Comment : null;
    }
    /**
     * Set Comment value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $comment
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setComment($comment = null)
    {
        // validation for constraint: string
        if (!is_null($comment) && !is_string($comment)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($comment, true), gettype($comment)), __LINE__);
        }
        if (is_null($comment) || (is_array($comment) && empty($comment))) {
            unset($this->Comment);
        } else {
            $this->Comment = $comment;
        }
        return $this;
    }
    /**
     * Get Ean value
     * @return string|null
     */
    public function getEan()
    {
        return $this->Ean;
    }
    /**
     * Set Ean value
     * @param string $ean
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setEan($ean = null)
    {
        // validation for constraint: string
        if (!is_null($ean) && !is_string($ean)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ean, true), gettype($ean)), __LINE__);
        }
        $this->Ean = $ean;
        return $this;
    }
    /**
     * Get MatchingStatus value
     * @return string|null
     */
    public function getMatchingStatus()
    {
        return $this->MatchingStatus;
    }
    /**
     * Set MatchingStatus value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductMatchingStatusEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductMatchingStatusEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $matchingStatus
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setMatchingStatus($matchingStatus = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\ProductMatchingStatusEnum::valueIsValid($matchingStatus)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\ProductMatchingStatusEnum', is_array($matchingStatus) ? implode(', ', $matchingStatus) : var_export($matchingStatus, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\ProductMatchingStatusEnum::getValidValues())), __LINE__);
        }
        $this->MatchingStatus = $matchingStatus;
        return $this;
    }
    /**
     * Get Name value
     * @return string|null
     */
    public function getName()
    {
        return $this->Name;
    }
    /**
     * Set Name value
     * @param string $name
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->Name = $name;
        return $this;
    }
    /**
     * Get SellerProductId value
     * @return string|null
     */
    public function getSellerProductId()
    {
        return $this->SellerProductId;
    }
    /**
     * Set SellerProductId value
     * @param string $sellerProductId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setSellerProductId($sellerProductId = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductId) && !is_string($sellerProductId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductId, true), gettype($sellerProductId)), __LINE__);
        }
        $this->SellerProductId = $sellerProductId;
        return $this;
    }
    /**
     * Get Size value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSize()
    {
        return isset($this->Size) ? $this->Size : null;
    }
    /**
     * Set Size value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $size
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setSize($size = null)
    {
        // validation for constraint: string
        if (!is_null($size) && !is_string($size)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($size, true), gettype($size)), __LINE__);
        }
        if (is_null($size) || (is_array($size) && empty($size))) {
            unset($this->Size);
        } else {
            $this->Size = $size;
        }
        return $this;
    }
    /**
     * Get Sku value
     * @return string|null
     */
    public function getSku()
    {
        return $this->Sku;
    }
    /**
     * Set Sku value
     * @param string $sku
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public function setSku($sku = null)
    {
        // validation for constraint: string
        if (!is_null($sku) && !is_string($sku)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sku, true), gettype($sku)), __LINE__);
        }
        $this->Sku = $sku;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductMatching
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
