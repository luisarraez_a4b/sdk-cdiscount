<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitFulfilmentSupplyOrderResponse StructType
 * @subpackage Structs
 */
class SubmitFulfilmentSupplyOrderResponse extends AbstractStructBase
{
    /**
     * The SubmitFulfilmentSupplyOrderResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage
     */
    public $SubmitFulfilmentSupplyOrderResult;
    /**
     * Constructor method for SubmitFulfilmentSupplyOrderResponse
     * @uses SubmitFulfilmentSupplyOrderResponse::setSubmitFulfilmentSupplyOrderResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentSupplyOrderResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentSupplyOrderResult = null)
    {
        $this
            ->setSubmitFulfilmentSupplyOrderResult($submitFulfilmentSupplyOrderResult);
    }
    /**
     * Get SubmitFulfilmentSupplyOrderResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage|null
     */
    public function getSubmitFulfilmentSupplyOrderResult()
    {
        return isset($this->SubmitFulfilmentSupplyOrderResult) ? $this->SubmitFulfilmentSupplyOrderResult : null;
    }
    /**
     * Set SubmitFulfilmentSupplyOrderResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentSupplyOrderResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentSupplyOrderResponse
     */
    public function setSubmitFulfilmentSupplyOrderResult(\A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportMessage $submitFulfilmentSupplyOrderResult = null)
    {
        if (is_null($submitFulfilmentSupplyOrderResult) || (is_array($submitFulfilmentSupplyOrderResult) && empty($submitFulfilmentSupplyOrderResult))) {
            unset($this->SubmitFulfilmentSupplyOrderResult);
        } else {
            $this->SubmitFulfilmentSupplyOrderResult = $submitFulfilmentSupplyOrderResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentSupplyOrderResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
