<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentSupplyOrderRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentSupplyOrderRequest
 * @subpackage Structs
 */
class FulfilmentSupplyOrderRequest extends AbstractStructBase
{
    /**
     * The ProductList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription
     */
    public $ProductList;
    /**
     * Constructor method for FulfilmentSupplyOrderRequest
     * @uses FulfilmentSupplyOrderRequest::setProductList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription $productList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription $productList = null)
    {
        $this
            ->setProductList($productList);
    }
    /**
     * Get ProductList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription|null
     */
    public function getProductList()
    {
        return $this->ProductList;
    }
    /**
     * Set ProductList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription $productList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentSupplyOrderRequest
     */
    public function setProductList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription $productList = null)
    {
        $this->ProductList = $productList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentSupplyOrderRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
