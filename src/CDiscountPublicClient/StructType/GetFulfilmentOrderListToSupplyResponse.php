<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetFulfilmentOrderListToSupplyResponse StructType
 * @subpackage Structs
 */
class GetFulfilmentOrderListToSupplyResponse extends AbstractStructBase
{
    /**
     * The GetFulfilmentOrderListToSupplyResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineListToSupplyMessage
     */
    public $GetFulfilmentOrderListToSupplyResult;
    /**
     * Constructor method for GetFulfilmentOrderListToSupplyResponse
     * @uses GetFulfilmentOrderListToSupplyResponse::setGetFulfilmentOrderListToSupplyResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineListToSupplyMessage $getFulfilmentOrderListToSupplyResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineListToSupplyMessage $getFulfilmentOrderListToSupplyResult = null)
    {
        $this
            ->setGetFulfilmentOrderListToSupplyResult($getFulfilmentOrderListToSupplyResult);
    }
    /**
     * Get GetFulfilmentOrderListToSupplyResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineListToSupplyMessage|null
     */
    public function getGetFulfilmentOrderListToSupplyResult()
    {
        return isset($this->GetFulfilmentOrderListToSupplyResult) ? $this->GetFulfilmentOrderListToSupplyResult : null;
    }
    /**
     * Set GetFulfilmentOrderListToSupplyResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineListToSupplyMessage $getFulfilmentOrderListToSupplyResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentOrderListToSupplyResponse
     */
    public function setGetFulfilmentOrderListToSupplyResult(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineListToSupplyMessage $getFulfilmentOrderListToSupplyResult = null)
    {
        if (is_null($getFulfilmentOrderListToSupplyResult) || (is_array($getFulfilmentOrderListToSupplyResult) && empty($getFulfilmentOrderListToSupplyResult))) {
            unset($this->GetFulfilmentOrderListToSupplyResult);
        } else {
            $this->GetFulfilmentOrderListToSupplyResult = $getFulfilmentOrderListToSupplyResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentOrderListToSupplyResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
