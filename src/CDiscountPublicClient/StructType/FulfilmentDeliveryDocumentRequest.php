<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentDeliveryDocumentRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentDeliveryDocumentRequest
 * @subpackage Structs
 */
class FulfilmentDeliveryDocumentRequest extends AbstractStructBase
{
    /**
     * The DepositId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $DepositId;
    /**
     * Constructor method for FulfilmentDeliveryDocumentRequest
     * @uses FulfilmentDeliveryDocumentRequest::setDepositId()
     * @param int $depositId
     */
    public function __construct($depositId = null)
    {
        $this
            ->setDepositId($depositId);
    }
    /**
     * Get DepositId value
     * @return int|null
     */
    public function getDepositId()
    {
        return $this->DepositId;
    }
    /**
     * Set DepositId value
     * @param int $depositId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentDeliveryDocumentRequest
     */
    public function setDepositId($depositId = null)
    {
        // validation for constraint: int
        if (!is_null($depositId) && !(is_int($depositId) || ctype_digit($depositId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($depositId, true), gettype($depositId)), __LINE__);
        }
        $this->DepositId = $depositId;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentDeliveryDocumentRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
