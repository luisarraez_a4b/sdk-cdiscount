<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SecurityDescriptor StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SecurityDescriptor
 * @subpackage Structs
 */
class SecurityDescriptor extends AbstractStructBase
{
    /**
     * The Authorization
     * @var string
     */
    public $Authorization;
    /**
     * The FunctionIdentifier
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $FunctionIdentifier;
    /**
     * The Version
     * @var int
     */
    public $Version;
    /**
     * Constructor method for SecurityDescriptor
     * @uses SecurityDescriptor::setAuthorization()
     * @uses SecurityDescriptor::setFunctionIdentifier()
     * @uses SecurityDescriptor::setVersion()
     * @param string $authorization
     * @param string $functionIdentifier
     * @param int $version
     */
    public function __construct($authorization = null, $functionIdentifier = null, $version = null)
    {
        $this
            ->setAuthorization($authorization)
            ->setFunctionIdentifier($functionIdentifier)
            ->setVersion($version);
    }
    /**
     * Get Authorization value
     * @return string|null
     */
    public function getAuthorization()
    {
        return $this->Authorization;
    }
    /**
     * Set Authorization value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Authorization::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\Authorization::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $authorization
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor
     */
    public function setAuthorization($authorization = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\Authorization::valueIsValid($authorization)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\Authorization', is_array($authorization) ? implode(', ', $authorization) : var_export($authorization, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\Authorization::getValidValues())), __LINE__);
        }
        $this->Authorization = $authorization;
        return $this;
    }
    /**
     * Get FunctionIdentifier value
     * @return string|null
     */
    public function getFunctionIdentifier()
    {
        return $this->FunctionIdentifier;
    }
    /**
     * Set FunctionIdentifier value
     * @param string $functionIdentifier
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor
     */
    public function setFunctionIdentifier($functionIdentifier = null)
    {
        // validation for constraint: string
        if (!is_null($functionIdentifier) && !is_string($functionIdentifier)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($functionIdentifier, true), gettype($functionIdentifier)), __LINE__);
        }
        $this->FunctionIdentifier = $functionIdentifier;
        return $this;
    }
    /**
     * Get Version value
     * @return int|null
     */
    public function getVersion()
    {
        return $this->Version;
    }
    /**
     * Set Version value
     * @param int $version
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor
     */
    public function setVersion($version = null)
    {
        // validation for constraint: int
        if (!is_null($version) && !(is_int($version) || ctype_digit($version))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($version, true), gettype($version)), __LINE__);
        }
        $this->Version = $version;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
