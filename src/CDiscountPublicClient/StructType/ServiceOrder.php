<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ServiceOrder StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ServiceOrder
 * @subpackage Structs
 */
class ServiceOrder extends Order
{
    /**
     * The AssociatedOrderInfoList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo
     */
    public $AssociatedOrderInfoList;
    /**
     * Constructor method for ServiceOrder
     * @uses ServiceOrder::setAssociatedOrderInfoList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo $associatedOrderInfoList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo $associatedOrderInfoList = null)
    {
        $this
            ->setAssociatedOrderInfoList($associatedOrderInfoList);
    }
    /**
     * Get AssociatedOrderInfoList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo|null
     */
    public function getAssociatedOrderInfoList()
    {
        return isset($this->AssociatedOrderInfoList) ? $this->AssociatedOrderInfoList : null;
    }
    /**
     * Set AssociatedOrderInfoList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo $associatedOrderInfoList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder
     */
    public function setAssociatedOrderInfoList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo $associatedOrderInfoList = null)
    {
        if (is_null($associatedOrderInfoList) || (is_array($associatedOrderInfoList) && empty($associatedOrderInfoList))) {
            unset($this->AssociatedOrderInfoList);
        } else {
            $this->AssociatedOrderInfoList = $associatedOrderInfoList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
