<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OfferProductFilterPaginated StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferProductFilterPaginated
 * @subpackage Structs
 */
class OfferProductFilterPaginated extends AbstractStructBase
{
    /**
     * The OfferPerPage
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $OfferPerPage;
    /**
     * The OfferPoolId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $OfferPoolId;
    /**
     * The OfferSortOrder
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OfferSortOrder;
    /**
     * The OfferStateFilter
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OfferStateFilter;
    /**
     * The OfferSupplyModeFilter
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OfferSupplyModeFilter;
    /**
     * The PageNumber
     * @var int
     */
    public $PageNumber;
    /**
     * The SellerProductIdList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring
     */
    public $SellerProductIdList;
    /**
     * The TotalItems
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $TotalItems;
    /**
     * Constructor method for OfferProductFilterPaginated
     * @uses OfferProductFilterPaginated::setOfferPerPage()
     * @uses OfferProductFilterPaginated::setOfferPoolId()
     * @uses OfferProductFilterPaginated::setOfferSortOrder()
     * @uses OfferProductFilterPaginated::setOfferStateFilter()
     * @uses OfferProductFilterPaginated::setOfferSupplyModeFilter()
     * @uses OfferProductFilterPaginated::setPageNumber()
     * @uses OfferProductFilterPaginated::setSellerProductIdList()
     * @uses OfferProductFilterPaginated::setTotalItems()
     * @param int $offerPerPage
     * @param int $offerPoolId
     * @param string $offerSortOrder
     * @param string $offerStateFilter
     * @param string $offerSupplyModeFilter
     * @param int $pageNumber
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList
     * @param int $totalItems
     */
    public function __construct($offerPerPage = null, $offerPoolId = null, $offerSortOrder = null, $offerStateFilter = null, $offerSupplyModeFilter = null, $pageNumber = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList = null, $totalItems = null)
    {
        $this
            ->setOfferPerPage($offerPerPage)
            ->setOfferPoolId($offerPoolId)
            ->setOfferSortOrder($offerSortOrder)
            ->setOfferStateFilter($offerStateFilter)
            ->setOfferSupplyModeFilter($offerSupplyModeFilter)
            ->setPageNumber($pageNumber)
            ->setSellerProductIdList($sellerProductIdList)
            ->setTotalItems($totalItems);
    }
    /**
     * Get OfferPerPage value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getOfferPerPage()
    {
        return isset($this->OfferPerPage) ? $this->OfferPerPage : null;
    }
    /**
     * Set OfferPerPage value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $offerPerPage
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setOfferPerPage($offerPerPage = null)
    {
        // validation for constraint: int
        if (!is_null($offerPerPage) && !(is_int($offerPerPage) || ctype_digit($offerPerPage))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($offerPerPage, true), gettype($offerPerPage)), __LINE__);
        }
        if (is_null($offerPerPage) || (is_array($offerPerPage) && empty($offerPerPage))) {
            unset($this->OfferPerPage);
        } else {
            $this->OfferPerPage = $offerPerPage;
        }
        return $this;
    }
    /**
     * Get OfferPoolId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getOfferPoolId()
    {
        return isset($this->OfferPoolId) ? $this->OfferPoolId : null;
    }
    /**
     * Set OfferPoolId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $offerPoolId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setOfferPoolId($offerPoolId = null)
    {
        // validation for constraint: int
        if (!is_null($offerPoolId) && !(is_int($offerPoolId) || ctype_digit($offerPoolId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($offerPoolId, true), gettype($offerPoolId)), __LINE__);
        }
        if (is_null($offerPoolId) || (is_array($offerPoolId) && empty($offerPoolId))) {
            unset($this->OfferPoolId);
        } else {
            $this->OfferPoolId = $offerPoolId;
        }
        return $this;
    }
    /**
     * Get OfferSortOrder value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOfferSortOrder()
    {
        return isset($this->OfferSortOrder) ? $this->OfferSortOrder : null;
    }
    /**
     * Set OfferSortOrder value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferAdvancedSortOrder::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferAdvancedSortOrder::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $offerSortOrder
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setOfferSortOrder($offerSortOrder = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\OfferAdvancedSortOrder::valueIsValid($offerSortOrder)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\OfferAdvancedSortOrder', is_array($offerSortOrder) ? implode(', ', $offerSortOrder) : var_export($offerSortOrder, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferAdvancedSortOrder::getValidValues())), __LINE__);
        }
        if (is_null($offerSortOrder) || (is_array($offerSortOrder) && empty($offerSortOrder))) {
            unset($this->OfferSortOrder);
        } else {
            $this->OfferSortOrder = $offerSortOrder;
        }
        return $this;
    }
    /**
     * Get OfferStateFilter value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOfferStateFilter()
    {
        return isset($this->OfferStateFilter) ? $this->OfferStateFilter : null;
    }
    /**
     * Set OfferStateFilter value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateFilter::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateFilter::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $offerStateFilter
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setOfferStateFilter($offerStateFilter = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateFilter::valueIsValid($offerStateFilter)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\OfferStateFilter', is_array($offerStateFilter) ? implode(', ', $offerStateFilter) : var_export($offerStateFilter, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferStateFilter::getValidValues())), __LINE__);
        }
        if (is_null($offerStateFilter) || (is_array($offerStateFilter) && empty($offerStateFilter))) {
            unset($this->OfferStateFilter);
        } else {
            $this->OfferStateFilter = $offerStateFilter;
        }
        return $this;
    }
    /**
     * Get OfferSupplyModeFilter value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOfferSupplyModeFilter()
    {
        return isset($this->OfferSupplyModeFilter) ? $this->OfferSupplyModeFilter : null;
    }
    /**
     * Set OfferSupplyModeFilter value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferSupplyModeFilter::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferSupplyModeFilter::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $offerSupplyModeFilter
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setOfferSupplyModeFilter($offerSupplyModeFilter = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\OfferSupplyModeFilter::valueIsValid($offerSupplyModeFilter)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\OfferSupplyModeFilter', is_array($offerSupplyModeFilter) ? implode(', ', $offerSupplyModeFilter) : var_export($offerSupplyModeFilter, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\OfferSupplyModeFilter::getValidValues())), __LINE__);
        }
        if (is_null($offerSupplyModeFilter) || (is_array($offerSupplyModeFilter) && empty($offerSupplyModeFilter))) {
            unset($this->OfferSupplyModeFilter);
        } else {
            $this->OfferSupplyModeFilter = $offerSupplyModeFilter;
        }
        return $this;
    }
    /**
     * Get PageNumber value
     * @return int|null
     */
    public function getPageNumber()
    {
        return $this->PageNumber;
    }
    /**
     * Set PageNumber value
     * @param int $pageNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setPageNumber($pageNumber = null)
    {
        // validation for constraint: int
        if (!is_null($pageNumber) && !(is_int($pageNumber) || ctype_digit($pageNumber))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($pageNumber, true), gettype($pageNumber)), __LINE__);
        }
        $this->PageNumber = $pageNumber;
        return $this;
    }
    /**
     * Get SellerProductIdList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring|null
     */
    public function getSellerProductIdList()
    {
        return isset($this->SellerProductIdList) ? $this->SellerProductIdList : null;
    }
    /**
     * Set SellerProductIdList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setSellerProductIdList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfstring $sellerProductIdList = null)
    {
        if (is_null($sellerProductIdList) || (is_array($sellerProductIdList) && empty($sellerProductIdList))) {
            unset($this->SellerProductIdList);
        } else {
            $this->SellerProductIdList = $sellerProductIdList;
        }
        return $this;
    }
    /**
     * Get TotalItems value
     * @return int|null
     */
    public function getTotalItems()
    {
        return $this->TotalItems;
    }
    /**
     * Set TotalItems value
     * @param int $totalItems
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public function setTotalItems($totalItems = null)
    {
        // validation for constraint: int
        if (!is_null($totalItems) && !(is_int($totalItems) || ctype_digit($totalItems))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($totalItems, true), gettype($totalItems)), __LINE__);
        }
        $this->TotalItems = $totalItems;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProductFilterPaginated
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
