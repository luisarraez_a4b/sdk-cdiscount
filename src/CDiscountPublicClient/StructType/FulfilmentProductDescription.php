<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentProductDescription StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentProductDescription
 * @subpackage Structs
 */
class FulfilmentProductDescription extends AbstractStructBase
{
    /**
     * The ExternalSupplyOrderId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ExternalSupplyOrderId;
    /**
     * The ProductEan
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ProductEan;
    /**
     * The Quantity
     * @var int
     */
    public $Quantity;
    /**
     * The SellerProductReference
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SellerProductReference;
    /**
     * The Warehouse
     * @var string
     */
    public $Warehouse;
    /**
     * The WarehouseReceptionMinDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $WarehouseReceptionMinDate;
    /**
     * Constructor method for FulfilmentProductDescription
     * @uses FulfilmentProductDescription::setExternalSupplyOrderId()
     * @uses FulfilmentProductDescription::setProductEan()
     * @uses FulfilmentProductDescription::setQuantity()
     * @uses FulfilmentProductDescription::setSellerProductReference()
     * @uses FulfilmentProductDescription::setWarehouse()
     * @uses FulfilmentProductDescription::setWarehouseReceptionMinDate()
     * @param string $externalSupplyOrderId
     * @param string $productEan
     * @param int $quantity
     * @param string $sellerProductReference
     * @param string $warehouse
     * @param string $warehouseReceptionMinDate
     */
    public function __construct($externalSupplyOrderId = null, $productEan = null, $quantity = null, $sellerProductReference = null, $warehouse = null, $warehouseReceptionMinDate = null)
    {
        $this
            ->setExternalSupplyOrderId($externalSupplyOrderId)
            ->setProductEan($productEan)
            ->setQuantity($quantity)
            ->setSellerProductReference($sellerProductReference)
            ->setWarehouse($warehouse)
            ->setWarehouseReceptionMinDate($warehouseReceptionMinDate);
    }
    /**
     * Get ExternalSupplyOrderId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExternalSupplyOrderId()
    {
        return isset($this->ExternalSupplyOrderId) ? $this->ExternalSupplyOrderId : null;
    }
    /**
     * Set ExternalSupplyOrderId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $externalSupplyOrderId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription
     */
    public function setExternalSupplyOrderId($externalSupplyOrderId = null)
    {
        // validation for constraint: string
        if (!is_null($externalSupplyOrderId) && !is_string($externalSupplyOrderId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($externalSupplyOrderId, true), gettype($externalSupplyOrderId)), __LINE__);
        }
        if (is_null($externalSupplyOrderId) || (is_array($externalSupplyOrderId) && empty($externalSupplyOrderId))) {
            unset($this->ExternalSupplyOrderId);
        } else {
            $this->ExternalSupplyOrderId = $externalSupplyOrderId;
        }
        return $this;
    }
    /**
     * Get ProductEan value
     * @return string|null
     */
    public function getProductEan()
    {
        return $this->ProductEan;
    }
    /**
     * Set ProductEan value
     * @param string $productEan
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription
     */
    public function setProductEan($productEan = null)
    {
        // validation for constraint: string
        if (!is_null($productEan) && !is_string($productEan)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productEan, true), gettype($productEan)), __LINE__);
        }
        $this->ProductEan = $productEan;
        return $this;
    }
    /**
     * Get Quantity value
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->Quantity;
    }
    /**
     * Set Quantity value
     * @param int $quantity
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription
     */
    public function setQuantity($quantity = null)
    {
        // validation for constraint: int
        if (!is_null($quantity) && !(is_int($quantity) || ctype_digit($quantity))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($quantity, true), gettype($quantity)), __LINE__);
        }
        $this->Quantity = $quantity;
        return $this;
    }
    /**
     * Get SellerProductReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSellerProductReference()
    {
        return isset($this->SellerProductReference) ? $this->SellerProductReference : null;
    }
    /**
     * Set SellerProductReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sellerProductReference
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription
     */
    public function setSellerProductReference($sellerProductReference = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductReference) && !is_string($sellerProductReference)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductReference, true), gettype($sellerProductReference)), __LINE__);
        }
        if (is_null($sellerProductReference) || (is_array($sellerProductReference) && empty($sellerProductReference))) {
            unset($this->SellerProductReference);
        } else {
            $this->SellerProductReference = $sellerProductReference;
        }
        return $this;
    }
    /**
     * Get Warehouse value
     * @return string|null
     */
    public function getWarehouse()
    {
        return $this->Warehouse;
    }
    /**
     * Set Warehouse value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $warehouse
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription
     */
    public function setWarehouse($warehouse = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::valueIsValid($warehouse)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\WarehouseType', is_array($warehouse) ? implode(', ', $warehouse) : var_export($warehouse, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::getValidValues())), __LINE__);
        }
        $this->Warehouse = $warehouse;
        return $this;
    }
    /**
     * Get WarehouseReceptionMinDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getWarehouseReceptionMinDate()
    {
        return isset($this->WarehouseReceptionMinDate) ? $this->WarehouseReceptionMinDate : null;
    }
    /**
     * Set WarehouseReceptionMinDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $warehouseReceptionMinDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription
     */
    public function setWarehouseReceptionMinDate($warehouseReceptionMinDate = null)
    {
        // validation for constraint: string
        if (!is_null($warehouseReceptionMinDate) && !is_string($warehouseReceptionMinDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($warehouseReceptionMinDate, true), gettype($warehouseReceptionMinDate)), __LINE__);
        }
        if (is_null($warehouseReceptionMinDate) || (is_array($warehouseReceptionMinDate) && empty($warehouseReceptionMinDate))) {
            unset($this->WarehouseReceptionMinDate);
        } else {
            $this->WarehouseReceptionMinDate = $warehouseReceptionMinDate;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
