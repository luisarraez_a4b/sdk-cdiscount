<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SupplyOrderMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SupplyOrderMessage
 * @subpackage Structs
 */
class SupplyOrderMessage extends ServiceBaseAPIMessage
{
    /**
     * The CurrentPageNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $CurrentPageNumber;
    /**
     * The NumberOfPages
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $NumberOfPages;
    /**
     * The SupplyOrderLineList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine
     */
    public $SupplyOrderLineList;
    /**
     * Constructor method for SupplyOrderMessage
     * @uses SupplyOrderMessage::setCurrentPageNumber()
     * @uses SupplyOrderMessage::setNumberOfPages()
     * @uses SupplyOrderMessage::setSupplyOrderLineList()
     * @param int $currentPageNumber
     * @param int $numberOfPages
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine $supplyOrderLineList
     */
    public function __construct($currentPageNumber = null, $numberOfPages = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine $supplyOrderLineList = null)
    {
        $this
            ->setCurrentPageNumber($currentPageNumber)
            ->setNumberOfPages($numberOfPages)
            ->setSupplyOrderLineList($supplyOrderLineList);
    }
    /**
     * Get CurrentPageNumber value
     * @return int|null
     */
    public function getCurrentPageNumber()
    {
        return $this->CurrentPageNumber;
    }
    /**
     * Set CurrentPageNumber value
     * @param int $currentPageNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderMessage
     */
    public function setCurrentPageNumber($currentPageNumber = null)
    {
        // validation for constraint: int
        if (!is_null($currentPageNumber) && !(is_int($currentPageNumber) || ctype_digit($currentPageNumber))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($currentPageNumber, true), gettype($currentPageNumber)), __LINE__);
        }
        $this->CurrentPageNumber = $currentPageNumber;
        return $this;
    }
    /**
     * Get NumberOfPages value
     * @return int|null
     */
    public function getNumberOfPages()
    {
        return $this->NumberOfPages;
    }
    /**
     * Set NumberOfPages value
     * @param int $numberOfPages
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderMessage
     */
    public function setNumberOfPages($numberOfPages = null)
    {
        // validation for constraint: int
        if (!is_null($numberOfPages) && !(is_int($numberOfPages) || ctype_digit($numberOfPages))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($numberOfPages, true), gettype($numberOfPages)), __LINE__);
        }
        $this->NumberOfPages = $numberOfPages;
        return $this;
    }
    /**
     * Get SupplyOrderLineList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine|null
     */
    public function getSupplyOrderLineList()
    {
        return isset($this->SupplyOrderLineList) ? $this->SupplyOrderLineList : null;
    }
    /**
     * Set SupplyOrderLineList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine $supplyOrderLineList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderMessage
     */
    public function setSupplyOrderLineList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine $supplyOrderLineList = null)
    {
        if (is_null($supplyOrderLineList) || (is_array($supplyOrderLineList) && empty($supplyOrderLineList))) {
            unset($this->SupplyOrderLineList);
        } else {
            $this->SupplyOrderLineList = $supplyOrderLineList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
