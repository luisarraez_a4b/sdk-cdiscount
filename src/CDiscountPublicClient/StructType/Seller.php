<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Seller StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:Seller
 * @subpackage Structs
 */
class Seller extends AbstractStructBase
{
    /**
     * The Email
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Email;
    /**
     * The IsAvailable
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $IsAvailable;
    /**
     * The Login
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $Login;
    /**
     * The MobileNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $MobileNumber;
    /**
     * The PhoneNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $PhoneNumber;
    /**
     * The SellerAddress
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\Address
     */
    public $SellerAddress;
    /**
     * The ShopName
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ShopName;
    /**
     * The ShopUrl
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $ShopUrl;
    /**
     * The SiretNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SiretNumber;
    /**
     * The State
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $State;
    /**
     * Constructor method for Seller
     * @uses Seller::setEmail()
     * @uses Seller::setIsAvailable()
     * @uses Seller::setLogin()
     * @uses Seller::setMobileNumber()
     * @uses Seller::setPhoneNumber()
     * @uses Seller::setSellerAddress()
     * @uses Seller::setShopName()
     * @uses Seller::setShopUrl()
     * @uses Seller::setSiretNumber()
     * @uses Seller::setState()
     * @param string $email
     * @param string $isAvailable
     * @param string $login
     * @param string $mobileNumber
     * @param string $phoneNumber
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Address $sellerAddress
     * @param string $shopName
     * @param string $shopUrl
     * @param string $siretNumber
     * @param string $state
     */
    public function __construct($email = null, $isAvailable = null, $login = null, $mobileNumber = null, $phoneNumber = null, \A4BGroup\Client\CDiscountPublicClient\StructType\Address $sellerAddress = null, $shopName = null, $shopUrl = null, $siretNumber = null, $state = null)
    {
        $this
            ->setEmail($email)
            ->setIsAvailable($isAvailable)
            ->setLogin($login)
            ->setMobileNumber($mobileNumber)
            ->setPhoneNumber($phoneNumber)
            ->setSellerAddress($sellerAddress)
            ->setShopName($shopName)
            ->setShopUrl($shopUrl)
            ->setSiretNumber($siretNumber)
            ->setState($state);
    }
    /**
     * Get Email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail()
    {
        return isset($this->Email) ? $this->Email : null;
    }
    /**
     * Set Email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->Email);
        } else {
            $this->Email = $email;
        }
        return $this;
    }
    /**
     * Get IsAvailable value
     * @return string|null
     */
    public function getIsAvailable()
    {
        return $this->IsAvailable;
    }
    /**
     * Set IsAvailable value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerSubStateEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerSubStateEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $isAvailable
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setIsAvailable($isAvailable = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\SellerSubStateEnum::valueIsValid($isAvailable)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\SellerSubStateEnum', is_array($isAvailable) ? implode(', ', $isAvailable) : var_export($isAvailable, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerSubStateEnum::getValidValues())), __LINE__);
        }
        $this->IsAvailable = $isAvailable;
        return $this;
    }
    /**
     * Get Login value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getLogin()
    {
        return isset($this->Login) ? $this->Login : null;
    }
    /**
     * Set Login value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $login
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setLogin($login = null)
    {
        // validation for constraint: string
        if (!is_null($login) && !is_string($login)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($login, true), gettype($login)), __LINE__);
        }
        if (is_null($login) || (is_array($login) && empty($login))) {
            unset($this->Login);
        } else {
            $this->Login = $login;
        }
        return $this;
    }
    /**
     * Get MobileNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getMobileNumber()
    {
        return isset($this->MobileNumber) ? $this->MobileNumber : null;
    }
    /**
     * Set MobileNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $mobileNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setMobileNumber($mobileNumber = null)
    {
        // validation for constraint: string
        if (!is_null($mobileNumber) && !is_string($mobileNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mobileNumber, true), gettype($mobileNumber)), __LINE__);
        }
        if (is_null($mobileNumber) || (is_array($mobileNumber) && empty($mobileNumber))) {
            unset($this->MobileNumber);
        } else {
            $this->MobileNumber = $mobileNumber;
        }
        return $this;
    }
    /**
     * Get PhoneNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPhoneNumber()
    {
        return isset($this->PhoneNumber) ? $this->PhoneNumber : null;
    }
    /**
     * Set PhoneNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $phoneNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setPhoneNumber($phoneNumber = null)
    {
        // validation for constraint: string
        if (!is_null($phoneNumber) && !is_string($phoneNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phoneNumber, true), gettype($phoneNumber)), __LINE__);
        }
        if (is_null($phoneNumber) || (is_array($phoneNumber) && empty($phoneNumber))) {
            unset($this->PhoneNumber);
        } else {
            $this->PhoneNumber = $phoneNumber;
        }
        return $this;
    }
    /**
     * Get SellerAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Address|null
     */
    public function getSellerAddress()
    {
        return isset($this->SellerAddress) ? $this->SellerAddress : null;
    }
    /**
     * Set SellerAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Address $sellerAddress
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setSellerAddress(\A4BGroup\Client\CDiscountPublicClient\StructType\Address $sellerAddress = null)
    {
        if (is_null($sellerAddress) || (is_array($sellerAddress) && empty($sellerAddress))) {
            unset($this->SellerAddress);
        } else {
            $this->SellerAddress = $sellerAddress;
        }
        return $this;
    }
    /**
     * Get ShopName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShopName()
    {
        return isset($this->ShopName) ? $this->ShopName : null;
    }
    /**
     * Set ShopName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shopName
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setShopName($shopName = null)
    {
        // validation for constraint: string
        if (!is_null($shopName) && !is_string($shopName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shopName, true), gettype($shopName)), __LINE__);
        }
        if (is_null($shopName) || (is_array($shopName) && empty($shopName))) {
            unset($this->ShopName);
        } else {
            $this->ShopName = $shopName;
        }
        return $this;
    }
    /**
     * Get ShopUrl value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShopUrl()
    {
        return isset($this->ShopUrl) ? $this->ShopUrl : null;
    }
    /**
     * Set ShopUrl value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shopUrl
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setShopUrl($shopUrl = null)
    {
        // validation for constraint: string
        if (!is_null($shopUrl) && !is_string($shopUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shopUrl, true), gettype($shopUrl)), __LINE__);
        }
        if (is_null($shopUrl) || (is_array($shopUrl) && empty($shopUrl))) {
            unset($this->ShopUrl);
        } else {
            $this->ShopUrl = $shopUrl;
        }
        return $this;
    }
    /**
     * Get SiretNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSiretNumber()
    {
        return isset($this->SiretNumber) ? $this->SiretNumber : null;
    }
    /**
     * Set SiretNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $siretNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setSiretNumber($siretNumber = null)
    {
        // validation for constraint: string
        if (!is_null($siretNumber) && !is_string($siretNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($siretNumber, true), gettype($siretNumber)), __LINE__);
        }
        if (is_null($siretNumber) || (is_array($siretNumber) && empty($siretNumber))) {
            unset($this->SiretNumber);
        } else {
            $this->SiretNumber = $siretNumber;
        }
        return $this;
    }
    /**
     * Get State value
     * @return string|null
     */
    public function getState()
    {
        return $this->State;
    }
    /**
     * Set State value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerStateEnum::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerStateEnum::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $state
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public function setState($state = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\SellerStateEnum::valueIsValid($state)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\SellerStateEnum', is_array($state) ? implode(', ', $state) : var_export($state, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\SellerStateEnum::getValidValues())), __LINE__);
        }
        $this->State = $state;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Seller
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
