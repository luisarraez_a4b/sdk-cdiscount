<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SubmitProductPackageResponse StructType
 * @subpackage Structs
 */
class SubmitProductPackageResponse extends AbstractStructBase
{
    /**
     * The SubmitProductPackageResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage
     */
    public $SubmitProductPackageResult;
    /**
     * Constructor method for SubmitProductPackageResponse
     * @uses SubmitProductPackageResponse::setSubmitProductPackageResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $submitProductPackageResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $submitProductPackageResult = null)
    {
        $this
            ->setSubmitProductPackageResult($submitProductPackageResult);
    }
    /**
     * Get SubmitProductPackageResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage|null
     */
    public function getSubmitProductPackageResult()
    {
        return isset($this->SubmitProductPackageResult) ? $this->SubmitProductPackageResult : null;
    }
    /**
     * Set SubmitProductPackageResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $submitProductPackageResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitProductPackageResponse
     */
    public function setSubmitProductPackageResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductIntegrationReportMessage $submitProductPackageResult = null)
    {
        if (is_null($submitProductPackageResult) || (is_array($submitProductPackageResult) && empty($submitProductPackageResult))) {
            unset($this->SubmitProductPackageResult);
        } else {
            $this->SubmitProductPackageResult = $submitProductPackageResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitProductPackageResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
