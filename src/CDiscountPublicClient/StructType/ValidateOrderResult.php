<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ValidateOrderResult StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ValidateOrderResult
 * @subpackage Structs
 */
class ValidateOrderResult extends AbstractStructBase
{
    /**
     * The Errors
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError
     */
    public $Errors;
    /**
     * The OrderNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $OrderNumber;
    /**
     * The ValidateOrderLineResults
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLineResult
     */
    public $ValidateOrderLineResults;
    /**
     * The Validated
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $Validated;
    /**
     * The Warnings
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError
     */
    public $Warnings;
    /**
     * Constructor method for ValidateOrderResult
     * @uses ValidateOrderResult::setErrors()
     * @uses ValidateOrderResult::setOrderNumber()
     * @uses ValidateOrderResult::setValidateOrderLineResults()
     * @uses ValidateOrderResult::setValidated()
     * @uses ValidateOrderResult::setWarnings()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $errors
     * @param string $orderNumber
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLineResult $validateOrderLineResults
     * @param bool $validated
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $warnings
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $errors = null, $orderNumber = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLineResult $validateOrderLineResults = null, $validated = null, \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $warnings = null)
    {
        $this
            ->setErrors($errors)
            ->setOrderNumber($orderNumber)
            ->setValidateOrderLineResults($validateOrderLineResults)
            ->setValidated($validated)
            ->setWarnings($warnings);
    }
    /**
     * Get Errors value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError|null
     */
    public function getErrors()
    {
        return isset($this->Errors) ? $this->Errors : null;
    }
    /**
     * Set Errors value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $errors
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderResult
     */
    public function setErrors(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $errors = null)
    {
        if (is_null($errors) || (is_array($errors) && empty($errors))) {
            unset($this->Errors);
        } else {
            $this->Errors = $errors;
        }
        return $this;
    }
    /**
     * Get OrderNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getOrderNumber()
    {
        return isset($this->OrderNumber) ? $this->OrderNumber : null;
    }
    /**
     * Set OrderNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $orderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderResult
     */
    public function setOrderNumber($orderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($orderNumber) && !is_string($orderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderNumber, true), gettype($orderNumber)), __LINE__);
        }
        if (is_null($orderNumber) || (is_array($orderNumber) && empty($orderNumber))) {
            unset($this->OrderNumber);
        } else {
            $this->OrderNumber = $orderNumber;
        }
        return $this;
    }
    /**
     * Get ValidateOrderLineResults value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLineResult|null
     */
    public function getValidateOrderLineResults()
    {
        return isset($this->ValidateOrderLineResults) ? $this->ValidateOrderLineResults : null;
    }
    /**
     * Set ValidateOrderLineResults value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLineResult $validateOrderLineResults
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderResult
     */
    public function setValidateOrderLineResults(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrderLineResult $validateOrderLineResults = null)
    {
        if (is_null($validateOrderLineResults) || (is_array($validateOrderLineResults) && empty($validateOrderLineResults))) {
            unset($this->ValidateOrderLineResults);
        } else {
            $this->ValidateOrderLineResults = $validateOrderLineResults;
        }
        return $this;
    }
    /**
     * Get Validated value
     * @return bool|null
     */
    public function getValidated()
    {
        return $this->Validated;
    }
    /**
     * Set Validated value
     * @param bool $validated
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderResult
     */
    public function setValidated($validated = null)
    {
        // validation for constraint: boolean
        if (!is_null($validated) && !is_bool($validated)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($validated, true), gettype($validated)), __LINE__);
        }
        $this->Validated = $validated;
        return $this;
    }
    /**
     * Get Warnings value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError|null
     */
    public function getWarnings()
    {
        return isset($this->Warnings) ? $this->Warnings : null;
    }
    /**
     * Set Warnings value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $warnings
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderResult
     */
    public function setWarnings(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfError $warnings = null)
    {
        if (is_null($warnings) || (is_array($warnings) && empty($warnings))) {
            unset($this->Warnings);
        } else {
            $this->Warnings = $warnings;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrderResult
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
