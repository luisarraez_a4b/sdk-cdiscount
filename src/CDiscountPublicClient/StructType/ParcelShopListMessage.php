<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopListMessage StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ParcelShopListMessage
 * @subpackage Structs
 */
class ParcelShopListMessage extends ServiceBaseAPIMessage
{
    /**
     * The ParcelShopList
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop
     */
    public $ParcelShopList;
    /**
     * Constructor method for ParcelShopListMessage
     * @uses ParcelShopListMessage::setParcelShopList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop $parcelShopList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop $parcelShopList = null)
    {
        $this
            ->setParcelShopList($parcelShopList);
    }
    /**
     * Get ParcelShopList value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop|null
     */
    public function getParcelShopList()
    {
        return isset($this->ParcelShopList) ? $this->ParcelShopList : null;
    }
    /**
     * Set ParcelShopList value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop $parcelShopList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage
     */
    public function setParcelShopList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop $parcelShopList = null)
    {
        if (is_null($parcelShopList) || (is_array($parcelShopList) && empty($parcelShopList))) {
            unset($this->ParcelShopList);
        } else {
            $this->ParcelShopList = $parcelShopList;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
