<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for FulfilmentOnDemandSupplyOrderRequest StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentOnDemandSupplyOrderRequest
 * @subpackage Structs
 */
class FulfilmentOnDemandSupplyOrderRequest extends AbstractStructBase
{
    /**
     * The OrderLineList
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest
     */
    public $OrderLineList;
    /**
     * Constructor method for FulfilmentOnDemandSupplyOrderRequest
     * @uses FulfilmentOnDemandSupplyOrderRequest::setOrderLineList()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest $orderLineList
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest $orderLineList = null)
    {
        $this
            ->setOrderLineList($orderLineList);
    }
    /**
     * Get OrderLineList value
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest|null
     */
    public function getOrderLineList()
    {
        return $this->OrderLineList;
    }
    /**
     * Set OrderLineList value
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest $orderLineList
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOnDemandSupplyOrderRequest
     */
    public function setOrderLineList(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest $orderLineList = null)
    {
        $this->OrderLineList = $orderLineList;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOnDemandSupplyOrderRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
