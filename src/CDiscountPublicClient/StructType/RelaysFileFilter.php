<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for RelaysFileFilter StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:RelaysFileFilter
 * @subpackage Structs
 */
class RelaysFileFilter extends AbstractStructBase
{
    /**
     * The RelaysFileId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $RelaysFileId;
    /**
     * Constructor method for RelaysFileFilter
     * @uses RelaysFileFilter::setRelaysFileId()
     * @param int $relaysFileId
     */
    public function __construct($relaysFileId = null)
    {
        $this
            ->setRelaysFileId($relaysFileId);
    }
    /**
     * Get RelaysFileId value
     * @return int|null
     */
    public function getRelaysFileId()
    {
        return $this->RelaysFileId;
    }
    /**
     * Set RelaysFileId value
     * @param int $relaysFileId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelaysFileFilter
     */
    public function setRelaysFileId($relaysFileId = null)
    {
        // validation for constraint: int
        if (!is_null($relaysFileId) && !(is_int($relaysFileId) || ctype_digit($relaysFileId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($relaysFileId, true), gettype($relaysFileId)), __LINE__);
        }
        $this->RelaysFileId = $relaysFileId;
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\RelaysFileFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
