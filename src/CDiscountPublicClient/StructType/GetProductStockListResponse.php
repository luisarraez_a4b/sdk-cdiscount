<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetProductStockListResponse StructType
 * @subpackage Structs
 */
class GetProductStockListResponse extends AbstractStructBase
{
    /**
     * The GetProductStockListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage
     */
    public $GetProductStockListResult;
    /**
     * Constructor method for GetProductStockListResponse
     * @uses GetProductStockListResponse::setGetProductStockListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage $getProductStockListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage $getProductStockListResult = null)
    {
        $this
            ->setGetProductStockListResult($getProductStockListResult);
    }
    /**
     * Get GetProductStockListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage|null
     */
    public function getGetProductStockListResult()
    {
        return isset($this->GetProductStockListResult) ? $this->GetProductStockListResult : null;
    }
    /**
     * Set GetProductStockListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage $getProductStockListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductStockListResponse
     */
    public function setGetProductStockListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductStockListMessage $getProductStockListResult = null)
    {
        if (is_null($getProductStockListResult) || (is_array($getProductStockListResult) && empty($getProductStockListResult))) {
            unset($this->GetProductStockListResult);
        } else {
            $this->GetProductStockListResult = $getProductStockListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductStockListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
