<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for GetParcelShopListResponse StructType
 * @subpackage Structs
 */
class GetParcelShopListResponse extends AbstractStructBase
{
    /**
     * The GetParcelShopListResult
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage
     */
    public $GetParcelShopListResult;
    /**
     * Constructor method for GetParcelShopListResponse
     * @uses GetParcelShopListResponse::setGetParcelShopListResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage $getParcelShopListResult
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage $getParcelShopListResult = null)
    {
        $this
            ->setGetParcelShopListResult($getParcelShopListResult);
    }
    /**
     * Get GetParcelShopListResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage|null
     */
    public function getGetParcelShopListResult()
    {
        return isset($this->GetParcelShopListResult) ? $this->GetParcelShopListResult : null;
    }
    /**
     * Set GetParcelShopListResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage $getParcelShopListResult
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetParcelShopListResponse
     */
    public function setGetParcelShopListResult(\A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShopListMessage $getParcelShopListResult = null)
    {
        if (is_null($getParcelShopListResult) || (is_array($getParcelShopListResult) && empty($getParcelShopListResult))) {
            unset($this->GetParcelShopListResult);
        } else {
            $this->GetParcelShopListResult = $getParcelShopListResult;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetParcelShopListResponse
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
