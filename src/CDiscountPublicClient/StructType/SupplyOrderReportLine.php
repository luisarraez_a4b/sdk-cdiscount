<?php

namespace A4BGroup\Client\CDiscountPublicClient\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for SupplyOrderReportLine StructType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SupplyOrderReportLine
 * @subpackage Structs
 */
class SupplyOrderReportLine extends AbstractStructBase
{
    /**
     * The ErrorDetails
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfintstring
     */
    public $ErrorDetails;
    /**
     * The OrderedQuantity
     * @var int
     */
    public $OrderedQuantity;
    /**
     * The ProductEan
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ProductEan;
    /**
     * The SellerId
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $SellerId;
    /**
     * The SellerProductReference
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SellerProductReference;
    /**
     * The SellerSupplyOrderNumber
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $SellerSupplyOrderNumber;
    /**
     * The SupplyOrderNumber
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $SupplyOrderNumber;
    /**
     * The Warehouse
     * Meta informations extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $Warehouse;
    /**
     * The WarehouseReceptionMinDate
     * Meta informations extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $WarehouseReceptionMinDate;
    /**
     * Constructor method for SupplyOrderReportLine
     * @uses SupplyOrderReportLine::setErrorDetails()
     * @uses SupplyOrderReportLine::setOrderedQuantity()
     * @uses SupplyOrderReportLine::setProductEan()
     * @uses SupplyOrderReportLine::setSellerId()
     * @uses SupplyOrderReportLine::setSellerProductReference()
     * @uses SupplyOrderReportLine::setSellerSupplyOrderNumber()
     * @uses SupplyOrderReportLine::setSupplyOrderNumber()
     * @uses SupplyOrderReportLine::setWarehouse()
     * @uses SupplyOrderReportLine::setWarehouseReceptionMinDate()
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfintstring $errorDetails
     * @param int $orderedQuantity
     * @param string $productEan
     * @param int $sellerId
     * @param string $sellerProductReference
     * @param string $sellerSupplyOrderNumber
     * @param string $supplyOrderNumber
     * @param string $warehouse
     * @param string $warehouseReceptionMinDate
     */
    public function __construct(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfintstring $errorDetails = null, $orderedQuantity = null, $productEan = null, $sellerId = null, $sellerProductReference = null, $sellerSupplyOrderNumber = null, $supplyOrderNumber = null, $warehouse = null, $warehouseReceptionMinDate = null)
    {
        $this
            ->setErrorDetails($errorDetails)
            ->setOrderedQuantity($orderedQuantity)
            ->setProductEan($productEan)
            ->setSellerId($sellerId)
            ->setSellerProductReference($sellerProductReference)
            ->setSellerSupplyOrderNumber($sellerSupplyOrderNumber)
            ->setSupplyOrderNumber($supplyOrderNumber)
            ->setWarehouse($warehouse)
            ->setWarehouseReceptionMinDate($warehouseReceptionMinDate);
    }
    /**
     * Get ErrorDetails value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfintstring|null
     */
    public function getErrorDetails()
    {
        return isset($this->ErrorDetails) ? $this->ErrorDetails : null;
    }
    /**
     * Set ErrorDetails value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfintstring $errorDetails
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setErrorDetails(\A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfKeyValueOfintstring $errorDetails = null)
    {
        if (is_null($errorDetails) || (is_array($errorDetails) && empty($errorDetails))) {
            unset($this->ErrorDetails);
        } else {
            $this->ErrorDetails = $errorDetails;
        }
        return $this;
    }
    /**
     * Get OrderedQuantity value
     * @return int|null
     */
    public function getOrderedQuantity()
    {
        return $this->OrderedQuantity;
    }
    /**
     * Set OrderedQuantity value
     * @param int $orderedQuantity
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setOrderedQuantity($orderedQuantity = null)
    {
        // validation for constraint: int
        if (!is_null($orderedQuantity) && !(is_int($orderedQuantity) || ctype_digit($orderedQuantity))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($orderedQuantity, true), gettype($orderedQuantity)), __LINE__);
        }
        $this->OrderedQuantity = $orderedQuantity;
        return $this;
    }
    /**
     * Get ProductEan value
     * @return string|null
     */
    public function getProductEan()
    {
        return $this->ProductEan;
    }
    /**
     * Set ProductEan value
     * @param string $productEan
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setProductEan($productEan = null)
    {
        // validation for constraint: string
        if (!is_null($productEan) && !is_string($productEan)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($productEan, true), gettype($productEan)), __LINE__);
        }
        $this->ProductEan = $productEan;
        return $this;
    }
    /**
     * Get SellerId value
     * @return int|null
     */
    public function getSellerId()
    {
        return $this->SellerId;
    }
    /**
     * Set SellerId value
     * @param int $sellerId
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setSellerId($sellerId = null)
    {
        // validation for constraint: int
        if (!is_null($sellerId) && !(is_int($sellerId) || ctype_digit($sellerId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($sellerId, true), gettype($sellerId)), __LINE__);
        }
        $this->SellerId = $sellerId;
        return $this;
    }
    /**
     * Get SellerProductReference value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSellerProductReference()
    {
        return isset($this->SellerProductReference) ? $this->SellerProductReference : null;
    }
    /**
     * Set SellerProductReference value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sellerProductReference
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setSellerProductReference($sellerProductReference = null)
    {
        // validation for constraint: string
        if (!is_null($sellerProductReference) && !is_string($sellerProductReference)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerProductReference, true), gettype($sellerProductReference)), __LINE__);
        }
        if (is_null($sellerProductReference) || (is_array($sellerProductReference) && empty($sellerProductReference))) {
            unset($this->SellerProductReference);
        } else {
            $this->SellerProductReference = $sellerProductReference;
        }
        return $this;
    }
    /**
     * Get SellerSupplyOrderNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getSellerSupplyOrderNumber()
    {
        return isset($this->SellerSupplyOrderNumber) ? $this->SellerSupplyOrderNumber : null;
    }
    /**
     * Set SellerSupplyOrderNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $sellerSupplyOrderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setSellerSupplyOrderNumber($sellerSupplyOrderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($sellerSupplyOrderNumber) && !is_string($sellerSupplyOrderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sellerSupplyOrderNumber, true), gettype($sellerSupplyOrderNumber)), __LINE__);
        }
        if (is_null($sellerSupplyOrderNumber) || (is_array($sellerSupplyOrderNumber) && empty($sellerSupplyOrderNumber))) {
            unset($this->SellerSupplyOrderNumber);
        } else {
            $this->SellerSupplyOrderNumber = $sellerSupplyOrderNumber;
        }
        return $this;
    }
    /**
     * Get SupplyOrderNumber value
     * @return string|null
     */
    public function getSupplyOrderNumber()
    {
        return $this->SupplyOrderNumber;
    }
    /**
     * Set SupplyOrderNumber value
     * @param string $supplyOrderNumber
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setSupplyOrderNumber($supplyOrderNumber = null)
    {
        // validation for constraint: string
        if (!is_null($supplyOrderNumber) && !is_string($supplyOrderNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($supplyOrderNumber, true), gettype($supplyOrderNumber)), __LINE__);
        }
        $this->SupplyOrderNumber = $supplyOrderNumber;
        return $this;
    }
    /**
     * Get Warehouse value
     * @return string|null
     */
    public function getWarehouse()
    {
        return $this->Warehouse;
    }
    /**
     * Set Warehouse value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $warehouse
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setWarehouse($warehouse = null)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::valueIsValid($warehouse)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\WarehouseType', is_array($warehouse) ? implode(', ', $warehouse) : var_export($warehouse, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\WarehouseType::getValidValues())), __LINE__);
        }
        $this->Warehouse = $warehouse;
        return $this;
    }
    /**
     * Get WarehouseReceptionMinDate value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getWarehouseReceptionMinDate()
    {
        return isset($this->WarehouseReceptionMinDate) ? $this->WarehouseReceptionMinDate : null;
    }
    /**
     * Set WarehouseReceptionMinDate value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $warehouseReceptionMinDate
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public function setWarehouseReceptionMinDate($warehouseReceptionMinDate = null)
    {
        // validation for constraint: string
        if (!is_null($warehouseReceptionMinDate) && !is_string($warehouseReceptionMinDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($warehouseReceptionMinDate, true), gettype($warehouseReceptionMinDate)), __LINE__);
        }
        if (is_null($warehouseReceptionMinDate) || (is_array($warehouseReceptionMinDate) && empty($warehouseReceptionMinDate))) {
            unset($this->WarehouseReceptionMinDate);
        } else {
            $this->WarehouseReceptionMinDate = $warehouseReceptionMinDate;
        }
        return $this;
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructBase::__set_state()
     * @uses AbstractStructBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderReportLine
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
