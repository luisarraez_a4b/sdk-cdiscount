<?php

namespace A4BGroup\Client\CDiscountPublicClient\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Submit ServiceType
 * @subpackage Services
 */
class Submit extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named SubmitProductPackage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitProductPackage $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitProductPackageResponse|bool
     */
    public function SubmitProductPackage(\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitProductPackage $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitProductPackage($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubmitOfferPackage
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferPackage $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferPackageResponse|bool
     */
    public function SubmitOfferPackage(\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferPackage $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitOfferPackage($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubmitRelaysFile
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitRelaysFile $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitRelaysFileResponse|bool
     */
    public function SubmitRelaysFile(\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitRelaysFile $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitRelaysFile($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubmitFulfilmentSupplyOrder
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentSupplyOrder $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentSupplyOrderResponse|bool
     */
    public function SubmitFulfilmentSupplyOrder(\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentSupplyOrder $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitFulfilmentSupplyOrder($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * SubmitFulfilmentOnDemandSupplyOrder
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentOnDemandSupplyOrder $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentOnDemandSupplyOrderResponse|bool
     */
    public function SubmitFulfilmentOnDemandSupplyOrder(\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentOnDemandSupplyOrder $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitFulfilmentOnDemandSupplyOrder($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubmitOfferStateAction
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferStateAction $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferStateActionResponse|bool
     */
    public function SubmitOfferStateAction(\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferStateAction $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitOfferStateAction($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named SubmitFulfilmentActivation
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentActivation $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentActivationResponse|bool
     */
    public function SubmitFulfilmentActivation(\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentActivation $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->SubmitFulfilmentActivation($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentActivationResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentOnDemandSupplyOrderResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitFulfilmentSupplyOrderResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferPackageResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitOfferStateActionResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitProductPackageResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\SubmitRelaysFileResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
