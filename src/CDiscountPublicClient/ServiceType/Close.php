<?php

namespace A4BGroup\Client\CDiscountPublicClient\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Close ServiceType
 * @subpackage Services
 */
class Close extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CloseDiscussionList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionListResponse|bool
     */
    public function CloseDiscussionList(\A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->CloseDiscussionList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionListResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
