<?php

namespace A4BGroup\Client\CDiscountPublicClient\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Create ServiceType
 * @subpackage Services
 */
class Create extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named CreateRefundVoucherAfterShipment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherAfterShipment $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherAfterShipmentResponse|bool
     */
    public function CreateRefundVoucherAfterShipment(\A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherAfterShipment $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->CreateRefundVoucherAfterShipment($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateRefundVoucher
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucher $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherResponse|bool
     */
    public function CreateRefundVoucher(\A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucher $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->CreateRefundVoucher($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named CreateExternalOrder
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CreateExternalOrder $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateExternalOrderResponse|bool
     */
    public function CreateExternalOrder(\A4BGroup\Client\CDiscountPublicClient\StructType\CreateExternalOrder $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->CreateExternalOrder($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CreateExternalOrderResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherAfterShipmentResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\CreateRefundVoucherResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
