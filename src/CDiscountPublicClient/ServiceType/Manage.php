<?php

namespace A4BGroup\Client\CDiscountPublicClient\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Manage ServiceType
 * @subpackage Services
 */
class Manage extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named ManageParcel
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcel $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcelResponse|bool
     */
    public function ManageParcel(\A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcel $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->ManageParcel($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ManageParcelResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
