<?php

namespace A4BGroup\Client\CDiscountPublicClient\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Generate ServiceType
 * @subpackage Services
 */
class Generate extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named GenerateDiscussionMailGuid
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GenerateDiscussionMailGuid $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GenerateDiscussionMailGuidResponse|bool
     */
    public function GenerateDiscussionMailGuid(\A4BGroup\Client\CDiscountPublicClient\StructType\GenerateDiscussionMailGuid $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GenerateDiscussionMailGuid($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GenerateDiscussionMailGuidResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
