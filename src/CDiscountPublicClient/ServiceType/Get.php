<?php

namespace A4BGroup\Client\CDiscountPublicClient\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named GetProductList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListResponse|bool
     */
    public function GetProductList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetProductList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProductPackageSubmissionResult
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageSubmissionResult $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageSubmissionResultResponse|bool
     */
    public function GetProductPackageSubmissionResult(\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageSubmissionResult $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetProductPackageSubmissionResult($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named
     * GetProductPackageProductMatchingFileData
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageProductMatchingFileData $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageProductMatchingFileDataResponse|bool
     */
    public function GetProductPackageProductMatchingFileData(\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageProductMatchingFileData $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetProductPackageProductMatchingFileData($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAllowedCategoryTree
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllowedCategoryTree $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllowedCategoryTreeResponse|bool
     */
    public function GetAllowedCategoryTree(\A4BGroup\Client\CDiscountPublicClient\StructType\GetAllowedCategoryTree $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetAllowedCategoryTree($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAllAllowedCategoryTree
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllAllowedCategoryTree $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllAllowedCategoryTreeResponse|bool
     */
    public function GetAllAllowedCategoryTree(\A4BGroup\Client\CDiscountPublicClient\StructType\GetAllAllowedCategoryTree $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetAllAllowedCategoryTree($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetModelList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetModelList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetModelListResponse|bool
     */
    public function GetModelList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetModelList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetModelList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetAllModelList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllModelList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllModelListResponse|bool
     */
    public function GetAllModelList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetAllModelList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetAllModelList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetBrandList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetBrandList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetBrandListResponse|bool
     */
    public function GetBrandList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetBrandList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetBrandList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProductListByIdentifier
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListByIdentifier $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListByIdentifierResponse|bool
     */
    public function GetProductListByIdentifier(\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListByIdentifier $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetProductListByIdentifier($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOfferPackageSubmissionResult
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferPackageSubmissionResult $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferPackageSubmissionResultResponse|bool
     */
    public function GetOfferPackageSubmissionResult(\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferPackageSubmissionResult $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetOfferPackageSubmissionResult($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOfferList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListResponse|bool
     */
    public function GetOfferList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetOfferList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOfferListPaginated
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListPaginated $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListPaginatedResponse|bool
     */
    public function GetOfferListPaginated(\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListPaginated $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetOfferListPaginated($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSellerInformation
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerInformation $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerInformationResponse|bool
     */
    public function GetSellerInformation(\A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerInformation $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetSellerInformation($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetSellerIndicators
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerIndicators $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerIndicatorsResponse|bool
     */
    public function GetSellerIndicators(\A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerIndicators $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetSellerIndicators($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOrderList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderListResponse|bool
     */
    public function GetOrderList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetOrderList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetServiceOrderList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetServiceOrderList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetServiceOrderListResponse|bool
     */
    public function GetServiceOrderList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetServiceOrderList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetServiceOrderList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetGlobalConfiguration
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetGlobalConfiguration $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetGlobalConfigurationResponse|bool
     */
    public function GetGlobalConfiguration(\A4BGroup\Client\CDiscountPublicClient\StructType\GetGlobalConfiguration $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetGlobalConfiguration($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetRelaysFileSubmissionResult
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetRelaysFileSubmissionResult $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetRelaysFileSubmissionResultResponse|bool
     */
    public function GetRelaysFileSubmissionResult(\A4BGroup\Client\CDiscountPublicClient\StructType\GetRelaysFileSubmissionResult $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetRelaysFileSubmissionResult($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetParcelShopList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetParcelShopList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetParcelShopListResponse|bool
     */
    public function GetParcelShopList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetParcelShopList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetParcelShopList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOrderClaimList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderClaimList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderClaimListResponse|bool
     */
    public function GetOrderClaimList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderClaimList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetOrderClaimList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOrderQuestionList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderQuestionList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderQuestionListResponse|bool
     */
    public function GetOrderQuestionList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderQuestionList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetOrderQuestionList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetOfferQuestionList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferQuestionList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferQuestionListResponse|bool
     */
    public function GetOfferQuestionList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferQuestionList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetOfferQuestionList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetDiscussionMailList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetDiscussionMailList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetDiscussionMailListResponse|bool
     */
    public function GetDiscussionMailList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetDiscussionMailList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetDiscussionMailList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetExternalOrderStatus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetExternalOrderStatus $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetExternalOrderStatusResponse|bool
     */
    public function GetExternalOrderStatus(\A4BGroup\Client\CDiscountPublicClient\StructType\GetExternalOrderStatus $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetExternalOrderStatus($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetProductStockList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductStockList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetProductStockListResponse|bool
     */
    public function GetProductStockList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductStockList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetProductStockList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFulfilmentActivationReportList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentActivationReportList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentActivationReportListResponse|bool
     */
    public function GetFulfilmentActivationReportList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentActivationReportList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetFulfilmentActivationReportList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFulfilmentOrderListToSupply
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentOrderListToSupply $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentOrderListToSupplyResponse|bool
     */
    public function GetFulfilmentOrderListToSupply(\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentOrderListToSupply $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetFulfilmentOrderListToSupply($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFulfilmentDeliveryDocument
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentDeliveryDocument $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentDeliveryDocumentResponse|bool
     */
    public function GetFulfilmentDeliveryDocument(\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentDeliveryDocument $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetFulfilmentDeliveryDocument($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFulfilmentSupplyOrder
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrder $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderResponse|bool
     */
    public function GetFulfilmentSupplyOrder(\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrder $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetFulfilmentSupplyOrder($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named GetFulfilmentSupplyOrderReportList
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderReportList $parameters
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderReportListResponse|bool
     */
    public function GetFulfilmentSupplyOrderReportList(\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderReportList $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->GetFulfilmentSupplyOrderReportList($parameters));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\GetAllAllowedCategoryTreeResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetAllModelListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetAllowedCategoryTreeResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetBrandListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetDiscussionMailListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetExternalOrderStatusResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentActivationReportListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentDeliveryDocumentResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentOrderListToSupplyResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderReportListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetFulfilmentSupplyOrderResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetGlobalConfigurationResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetModelListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListPaginatedResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferPackageSubmissionResultResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetOfferQuestionListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderClaimListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetOrderQuestionListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetParcelShopListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListByIdentifierResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageProductMatchingFileDataResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductPackageSubmissionResultResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetProductStockListResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetRelaysFileSubmissionResultResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerIndicatorsResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetSellerInformationResponse|\A4BGroup\Client\CDiscountPublicClient\StructType\GetServiceOrderListResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
