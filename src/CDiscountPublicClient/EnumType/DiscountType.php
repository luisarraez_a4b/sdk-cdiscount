<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for DiscountType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscountType
 * @subpackage Enumerations
 */
class DiscountType
{
    /**
     * Constant for value 'StrikedPrice'
     * @return string 'StrikedPrice'
     */
    const VALUE_STRIKED_PRICE = 'StrikedPrice';
    /**
     * Constant for value 'Flash'
     * @return string 'Flash'
     */
    const VALUE_FLASH = 'Flash';
    /**
     * Constant for value 'Sales'
     * @return string 'Sales'
     */
    const VALUE_SALES = 'Sales';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_STRIKED_PRICE
     * @uses self::VALUE_FLASH
     * @uses self::VALUE_SALES
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_STRIKED_PRICE,
            self::VALUE_FLASH,
            self::VALUE_SALES,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
