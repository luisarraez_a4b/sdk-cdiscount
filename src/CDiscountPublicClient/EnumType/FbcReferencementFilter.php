<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for FbcReferencementFilter EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FbcReferencementFilter
 * @subpackage Enumerations
 */
class FbcReferencementFilter
{
    /**
     * Constant for value 'All'
     * @return string 'All'
     */
    const VALUE_ALL = 'All';
    /**
     * Constant for value 'OnlyReferenced'
     * @return string 'OnlyReferenced'
     */
    const VALUE_ONLY_REFERENCED = 'OnlyReferenced';
    /**
     * Constant for value 'OnlyNotReferenced'
     * @return string 'OnlyNotReferenced'
     */
    const VALUE_ONLY_NOT_REFERENCED = 'OnlyNotReferenced';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_ALL
     * @uses self::VALUE_ONLY_REFERENCED
     * @uses self::VALUE_ONLY_NOT_REFERENCED
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_ALL,
            self::VALUE_ONLY_REFERENCED,
            self::VALUE_ONLY_NOT_REFERENCED,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
