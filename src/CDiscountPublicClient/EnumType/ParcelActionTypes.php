<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for ParcelActionTypes EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ParcelActionTypes
 * @subpackage Enumerations
 */
class ParcelActionTypes
{
    /**
     * Constant for value 'AskingForInvestigation'
     * @return string 'AskingForInvestigation'
     */
    const VALUE_ASKING_FOR_INVESTIGATION = 'AskingForInvestigation';
    /**
     * Constant for value 'AskingForDeliveryCertification'
     * @return string 'AskingForDeliveryCertification'
     */
    const VALUE_ASKING_FOR_DELIVERY_CERTIFICATION = 'AskingForDeliveryCertification';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_ASKING_FOR_INVESTIGATION
     * @uses self::VALUE_ASKING_FOR_DELIVERY_CERTIFICATION
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_ASKING_FOR_INVESTIGATION,
            self::VALUE_ASKING_FOR_DELIVERY_CERTIFICATION,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
