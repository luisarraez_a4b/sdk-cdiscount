<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for WarehouseType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:WarehouseType
 * @subpackage Enumerations
 */
class WarehouseType
{
    /**
     * Constant for value 'CEM'
     * @return string 'CEM'
     */
    const VALUE_CEM = 'CEM';
    /**
     * Constant for value 'ANZ'
     * @return string 'ANZ'
     */
    const VALUE_ANZ = 'ANZ';
    /**
     * Constant for value 'SMD'
     * @return string 'SMD'
     */
    const VALUE_SMD = 'SMD';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_CEM
     * @uses self::VALUE_ANZ
     * @uses self::VALUE_SMD
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_CEM,
            self::VALUE_ANZ,
            self::VALUE_SMD,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
