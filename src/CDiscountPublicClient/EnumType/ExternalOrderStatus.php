<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for ExternalOrderStatus EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ExternalOrderStatus
 * @subpackage Enumerations
 */
class ExternalOrderStatus
{
    /**
     * Constant for value 'Ok'
     * @return string 'Ok'
     */
    const VALUE_OK = 'Ok';
    /**
     * Constant for value 'Pending'
     * @return string 'Pending'
     */
    const VALUE_PENDING = 'Pending';
    /**
     * Constant for value 'Ko'
     * @return string 'Ko'
     */
    const VALUE_KO = 'Ko';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_OK
     * @uses self::VALUE_PENDING
     * @uses self::VALUE_KO
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_OK,
            self::VALUE_PENDING,
            self::VALUE_KO,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
