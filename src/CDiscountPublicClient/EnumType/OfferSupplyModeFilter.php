<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for OfferSupplyModeFilter EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferSupplyModeFilter
 * @subpackage Enumerations
 */
class OfferSupplyModeFilter
{
    /**
     * Constant for value 'Marketplace'
     * @return string 'Marketplace'
     */
    const VALUE_MARKETPLACE = 'Marketplace';
    /**
     * Constant for value 'CLogistique'
     * @return string 'CLogistique'
     */
    const VALUE_CLOGISTIQUE = 'CLogistique';
    /**
     * Constant for value 'FOD'
     * @return string 'FOD'
     */
    const VALUE_FOD = 'FOD';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_MARKETPLACE
     * @uses self::VALUE_CLOGISTIQUE
     * @uses self::VALUE_FOD
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_MARKETPLACE,
            self::VALUE_CLOGISTIQUE,
            self::VALUE_FOD,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
