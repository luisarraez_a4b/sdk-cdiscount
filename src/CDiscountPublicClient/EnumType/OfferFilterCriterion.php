<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for OfferFilterCriterion EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferFilterCriterion
 * @subpackage Enumerations
 */
class OfferFilterCriterion
{
    /**
     * Constant for value 'NewOffersOnly'
     * @return string 'NewOffersOnly'
     */
    const VALUE_NEW_OFFERS_ONLY = 'NewOffersOnly';
    /**
     * Constant for value 'UsedOffersOnly'
     * @return string 'UsedOffersOnly'
     */
    const VALUE_USED_OFFERS_ONLY = 'UsedOffersOnly';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_NEW_OFFERS_ONLY
     * @uses self::VALUE_USED_OFFERS_ONLY
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NEW_OFFERS_ONLY,
            self::VALUE_USED_OFFERS_ONLY,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
