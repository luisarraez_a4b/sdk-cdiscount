<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for VoucherSourceActor EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:VoucherSourceActor
 * @subpackage Enumerations
 */
class VoucherSourceActor
{
    /**
     * Constant for value 'Customer'
     * @return string 'Customer'
     */
    const VALUE_CUSTOMER = 'Customer';
    /**
     * Constant for value 'CDiscountPublicClient'
     * @return string 'CDiscountPublicClient'
     */
    const VALUE_CDISCOUNT = 'CDiscountPublicClient';
    /**
     * Constant for value 'Seller'
     * @return string 'Seller'
     */
    const VALUE_SELLER = 'Seller';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_CUSTOMER
     * @uses self::VALUE_CDISCOUNT
     * @uses self::VALUE_SELLER
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_CUSTOMER,
            self::VALUE_CDISCOUNT,
            self::VALUE_SELLER,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
