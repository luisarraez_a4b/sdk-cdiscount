<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for Civility EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:Civility
 * @subpackage Enumerations
 */
class Civility
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'MR'
     * @return string 'MR'
     */
    const VALUE_MR = 'MR';
    /**
     * Constant for value 'MISS'
     * @return string 'MISS'
     */
    const VALUE_MISS = 'MISS';
    /**
     * Constant for value 'MRS'
     * @return string 'MRS'
     */
    const VALUE_MRS = 'MRS';
    /**
     * Constant for value 'DR'
     * @return string 'DR'
     */
    const VALUE_DR = 'DR';
    /**
     * Constant for value 'PHD'
     * @return string 'PHD'
     */
    const VALUE_PHD = 'PHD';
    /**
     * Constant for value 'PR'
     * @return string 'PR'
     */
    const VALUE_PR = 'PR';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_MR
     * @uses self::VALUE_MISS
     * @uses self::VALUE_MRS
     * @uses self::VALUE_DR
     * @uses self::VALUE_PHD
     * @uses self::VALUE_PR
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NONE,
            self::VALUE_MR,
            self::VALUE_MISS,
            self::VALUE_MRS,
            self::VALUE_DR,
            self::VALUE_PHD,
            self::VALUE_PR,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
