<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for CloseDiscussionStatus EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:CloseDiscussionStatus
 * @subpackage Enumerations
 */
class CloseDiscussionStatus
{
    /**
     * Constant for value 'DiscussionClosed'
     * @return string 'DiscussionClosed'
     */
    const VALUE_DISCUSSION_CLOSED = 'DiscussionClosed';
    /**
     * Constant for value 'DiscussionNotFound'
     * @return string 'DiscussionNotFound'
     */
    const VALUE_DISCUSSION_NOT_FOUND = 'DiscussionNotFound';
    /**
     * Constant for value 'UnauthorizedCancelPending'
     * @return string 'UnauthorizedCancelPending'
     */
    const VALUE_UNAUTHORIZED_CANCEL_PENDING = 'UnauthorizedCancelPending';
    /**
     * Constant for value 'UnauthorizedLastAnswer'
     * @return string 'UnauthorizedLastAnswer'
     */
    const VALUE_UNAUTHORIZED_LAST_ANSWER = 'UnauthorizedLastAnswer';
    /**
     * Constant for value 'AlreadyClosed'
     * @return string 'AlreadyClosed'
     */
    const VALUE_ALREADY_CLOSED = 'AlreadyClosed';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_DISCUSSION_CLOSED
     * @uses self::VALUE_DISCUSSION_NOT_FOUND
     * @uses self::VALUE_UNAUTHORIZED_CANCEL_PENDING
     * @uses self::VALUE_UNAUTHORIZED_LAST_ANSWER
     * @uses self::VALUE_ALREADY_CLOSED
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_DISCUSSION_CLOSED,
            self::VALUE_DISCUSSION_NOT_FOUND,
            self::VALUE_UNAUTHORIZED_CANCEL_PENDING,
            self::VALUE_UNAUTHORIZED_LAST_ANSWER,
            self::VALUE_ALREADY_CLOSED,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
