<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for ProductPackagingUnit EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductPackagingUnit
 * @subpackage Enumerations
 */
class ProductPackagingUnit
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'Liter'
     * @return string 'Liter'
     */
    const VALUE_LITER = 'Liter';
    /**
     * Constant for value 'Kilogram'
     * @return string 'Kilogram'
     */
    const VALUE_KILOGRAM = 'Kilogram';
    /**
     * Constant for value 'SquareMeter'
     * @return string 'SquareMeter'
     */
    const VALUE_SQUARE_METER = 'SquareMeter';
    /**
     * Constant for value 'CubicMeter'
     * @return string 'CubicMeter'
     */
    const VALUE_CUBIC_METER = 'CubicMeter';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_LITER
     * @uses self::VALUE_KILOGRAM
     * @uses self::VALUE_SQUARE_METER
     * @uses self::VALUE_CUBIC_METER
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NONE,
            self::VALUE_LITER,
            self::VALUE_KILOGRAM,
            self::VALUE_SQUARE_METER,
            self::VALUE_CUBIC_METER,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
