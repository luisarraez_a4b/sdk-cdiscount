<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for SupplyOrderStatusType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SupplyOrderStatusType
 * @subpackage Enumerations
 */
class SupplyOrderStatusType
{
    /**
     * Constant for value 'Registered'
     * @return string 'Registered'
     */
    const VALUE_REGISTERED = 'Registered';
    /**
     * Constant for value 'AppointmentScheduled'
     * @return string 'AppointmentScheduled'
     */
    const VALUE_APPOINTMENT_SCHEDULED = 'AppointmentScheduled';
    /**
     * Constant for value 'SupplyingInProgress'
     * @return string 'SupplyingInProgress'
     */
    const VALUE_SUPPLYING_IN_PROGRESS = 'SupplyingInProgress';
    /**
     * Constant for value 'SupplyingDone'
     * @return string 'SupplyingDone'
     */
    const VALUE_SUPPLYING_DONE = 'SupplyingDone';
    /**
     * Constant for value 'Rejected'
     * @return string 'Rejected'
     */
    const VALUE_REJECTED = 'Rejected';
    /**
     * Constant for value 'Validated'
     * @return string 'Validated'
     */
    const VALUE_VALIDATED = 'Validated';
    /**
     * Constant for value 'Cancelled'
     * @return string 'Cancelled'
     */
    const VALUE_CANCELLED = 'Cancelled';
    /**
     * Constant for value 'PartiallyReceived'
     * @return string 'PartiallyReceived'
     */
    const VALUE_PARTIALLY_RECEIVED = 'PartiallyReceived';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_REGISTERED
     * @uses self::VALUE_APPOINTMENT_SCHEDULED
     * @uses self::VALUE_SUPPLYING_IN_PROGRESS
     * @uses self::VALUE_SUPPLYING_DONE
     * @uses self::VALUE_REJECTED
     * @uses self::VALUE_VALIDATED
     * @uses self::VALUE_CANCELLED
     * @uses self::VALUE_PARTIALLY_RECEIVED
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_REGISTERED,
            self::VALUE_APPOINTMENT_SCHEDULED,
            self::VALUE_SUPPLYING_IN_PROGRESS,
            self::VALUE_SUPPLYING_DONE,
            self::VALUE_REJECTED,
            self::VALUE_VALIDATED,
            self::VALUE_CANCELLED,
            self::VALUE_PARTIALLY_RECEIVED,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
