<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for OfferSortOrder EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferSortOrder
 * @subpackage Enumerations
 */
class OfferSortOrder
{
    /**
     * Constant for value 'BySoldQuantityDescending'
     * @return string 'BySoldQuantityDescending'
     */
    const VALUE_BY_SOLD_QUANTITY_DESCENDING = 'BySoldQuantityDescending';
    /**
     * Constant for value 'ByPriceAscending'
     * @return string 'ByPriceAscending'
     */
    const VALUE_BY_PRICE_ASCENDING = 'ByPriceAscending';
    /**
     * Constant for value 'ByPriceDescending'
     * @return string 'ByPriceDescending'
     */
    const VALUE_BY_PRICE_DESCENDING = 'ByPriceDescending';
    /**
     * Constant for value 'ByCreationDateDescending'
     * @return string 'ByCreationDateDescending'
     */
    const VALUE_BY_CREATION_DATE_DESCENDING = 'ByCreationDateDescending';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_BY_SOLD_QUANTITY_DESCENDING
     * @uses self::VALUE_BY_PRICE_ASCENDING
     * @uses self::VALUE_BY_PRICE_DESCENDING
     * @uses self::VALUE_BY_CREATION_DATE_DESCENDING
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_BY_SOLD_QUANTITY_DESCENDING,
            self::VALUE_BY_PRICE_ASCENDING,
            self::VALUE_BY_PRICE_DESCENDING,
            self::VALUE_BY_CREATION_DATE_DESCENDING,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
