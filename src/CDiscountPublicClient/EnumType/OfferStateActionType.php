<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for OfferStateActionType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferStateActionType
 * @subpackage Enumerations
 */
class OfferStateActionType
{
    /**
     * Constant for value 'Publish'
     * @return string 'Publish'
     */
    const VALUE_PUBLISH = 'Publish';
    /**
     * Constant for value 'Unpublish'
     * @return string 'Unpublish'
     */
    const VALUE_UNPUBLISH = 'Unpublish';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_PUBLISH
     * @uses self::VALUE_UNPUBLISH
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_PUBLISH,
            self::VALUE_UNPUBLISH,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
