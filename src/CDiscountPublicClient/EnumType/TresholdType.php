<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for TresholdType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:TresholdType
 * @subpackage Enumerations
 */
class TresholdType
{
    /**
     * Constant for value 'ThresholdMin'
     * @return string 'ThresholdMin'
     */
    const VALUE_THRESHOLD_MIN = 'ThresholdMin';
    /**
     * Constant for value 'ThresholdMax'
     * @return string 'ThresholdMax'
     */
    const VALUE_THRESHOLD_MAX = 'ThresholdMax';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_THRESHOLD_MIN
     * @uses self::VALUE_THRESHOLD_MAX
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_THRESHOLD_MIN,
            self::VALUE_THRESHOLD_MAX,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
