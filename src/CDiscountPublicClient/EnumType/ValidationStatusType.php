<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for ValidationStatusType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ValidationStatusType
 * @subpackage Enumerations
 */
class ValidationStatusType
{
    /**
     * Constant for value 'ValidatingFianet'
     * @return string 'ValidatingFianet'
     */
    const VALUE_VALIDATING_FIANET = 'ValidatingFianet';
    /**
     * Constant for value 'PreAccepted'
     * @return string 'PreAccepted'
     */
    const VALUE_PRE_ACCEPTED = 'PreAccepted';
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_VALIDATING_FIANET
     * @uses self::VALUE_PRE_ACCEPTED
     * @uses self::VALUE_NONE
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_VALIDATING_FIANET,
            self::VALUE_PRE_ACCEPTED,
            self::VALUE_NONE,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
