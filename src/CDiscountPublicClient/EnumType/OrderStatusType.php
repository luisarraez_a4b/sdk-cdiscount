<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for OrderStatusType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderStatusType
 * @subpackage Enumerations
 */
class OrderStatusType
{
    /**
     * Constant for value 'NonValidated'
     * @return string 'NonValidated'
     */
    const VALUE_NON_VALIDATED = 'NonValidated';
    /**
     * Constant for value 'NoPaymentAttempt'
     * @return string 'NoPaymentAttempt'
     */
    const VALUE_NO_PAYMENT_ATTEMPT = 'NoPaymentAttempt';
    /**
     * Constant for value 'Cancelled'
     * @return string 'Cancelled'
     */
    const VALUE_CANCELLED = 'Cancelled';
    /**
     * Constant for value 'Validated'
     * @return string 'Validated'
     */
    const VALUE_VALIDATED = 'Validated';
    /**
     * Constant for value 'Waiting'
     * @return string 'Waiting'
     */
    const VALUE_WAITING = 'Waiting';
    /**
     * Constant for value 'Completed'
     * @return string 'Completed'
     */
    const VALUE_COMPLETED = 'Completed';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_NON_VALIDATED
     * @uses self::VALUE_NO_PAYMENT_ATTEMPT
     * @uses self::VALUE_CANCELLED
     * @uses self::VALUE_VALIDATED
     * @uses self::VALUE_WAITING
     * @uses self::VALUE_COMPLETED
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NON_VALIDATED,
            self::VALUE_NO_PAYMENT_ATTEMPT,
            self::VALUE_CANCELLED,
            self::VALUE_VALIDATED,
            self::VALUE_WAITING,
            self::VALUE_COMPLETED,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
