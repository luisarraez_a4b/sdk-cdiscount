<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for SupplyModeType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SupplyModeType
 * @subpackage Enumerations
 */
class SupplyModeType
{
    /**
     * Constant for value 'Mkp'
     * @return string 'Mkp'
     */
    const VALUE_MKP = 'Mkp';
    /**
     * Constant for value 'Fbc'
     * @return string 'Fbc'
     */
    const VALUE_FBC = 'Fbc';
    /**
     * Constant for value 'Ttd'
     * @return string 'Ttd'
     */
    const VALUE_TTD = 'Ttd';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_MKP
     * @uses self::VALUE_FBC
     * @uses self::VALUE_TTD
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_MKP,
            self::VALUE_FBC,
            self::VALUE_TTD,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
