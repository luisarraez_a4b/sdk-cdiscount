<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for SellerStateEnum EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerStateEnum
 * @subpackage Enumerations
 */
class SellerStateEnum
{
    /**
     * Constant for value 'Activated'
     * @return string 'Activated'
     */
    const VALUE_ACTIVATED = 'Activated';
    /**
     * Constant for value 'Deactivated'
     * @return string 'Deactivated'
     */
    const VALUE_DEACTIVATED = 'Deactivated';
    /**
     * Constant for value 'PutOnNotice'
     * @return string 'PutOnNotice'
     */
    const VALUE_PUT_ON_NOTICE = 'PutOnNotice';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_ACTIVATED
     * @uses self::VALUE_DEACTIVATED
     * @uses self::VALUE_PUT_ON_NOTICE
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_ACTIVATED,
            self::VALUE_DEACTIVATED,
            self::VALUE_PUT_ON_NOTICE,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
