<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for FulfilmentProductActionType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentProductActionType
 * @subpackage Enumerations
 */
class FulfilmentProductActionType
{
    /**
     * Constant for value 'Activation'
     * @return string 'Activation'
     */
    const VALUE_ACTIVATION = 'Activation';
    /**
     * Constant for value 'Deactivation'
     * @return string 'Deactivation'
     */
    const VALUE_DEACTIVATION = 'Deactivation';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_ACTIVATION
     * @uses self::VALUE_DEACTIVATION
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_ACTIVATION,
            self::VALUE_DEACTIVATION,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
