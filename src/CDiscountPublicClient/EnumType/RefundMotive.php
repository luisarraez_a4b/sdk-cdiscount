<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for RefundMotive EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:RefundMotive
 * @subpackage Enumerations
 */
class RefundMotive
{
    /**
     * Constant for value 'VendorRejection'
     * @return string 'VendorRejection'
     */
    const VALUE_VENDOR_REJECTION = 'VendorRejection';
    /**
     * Constant for value 'ClientCancellation'
     * @return string 'ClientCancellation'
     */
    const VALUE_CLIENT_CANCELLATION = 'ClientCancellation';
    /**
     * Constant for value 'VendorRejectionAndClientCancellation'
     * @return string 'VendorRejectionAndClientCancellation'
     */
    const VALUE_VENDOR_REJECTION_AND_CLIENT_CANCELLATION = 'VendorRejectionAndClientCancellation';
    /**
     * Constant for value 'ClientClaim'
     * @return string 'ClientClaim'
     */
    const VALUE_CLIENT_CLAIM = 'ClientClaim';
    /**
     * Constant for value 'VendorInitiatedRefund'
     * @return string 'VendorInitiatedRefund'
     */
    const VALUE_VENDOR_INITIATED_REFUND = 'VendorInitiatedRefund';
    /**
     * Constant for value 'ClientRetraction'
     * @return string 'ClientRetraction'
     */
    const VALUE_CLIENT_RETRACTION = 'ClientRetraction';
    /**
     * Constant for value 'NoClientWithDrawal'
     * @return string 'NoClientWithDrawal'
     */
    const VALUE_NO_CLIENT_WITH_DRAWAL = 'NoClientWithDrawal';
    /**
     * Constant for value 'ProductStockUnavailable'
     * @return string 'ProductStockUnavailable'
     */
    const VALUE_PRODUCT_STOCK_UNAVAILABLE = 'ProductStockUnavailable';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_VENDOR_REJECTION
     * @uses self::VALUE_CLIENT_CANCELLATION
     * @uses self::VALUE_VENDOR_REJECTION_AND_CLIENT_CANCELLATION
     * @uses self::VALUE_CLIENT_CLAIM
     * @uses self::VALUE_VENDOR_INITIATED_REFUND
     * @uses self::VALUE_CLIENT_RETRACTION
     * @uses self::VALUE_NO_CLIENT_WITH_DRAWAL
     * @uses self::VALUE_PRODUCT_STOCK_UNAVAILABLE
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_VENDOR_REJECTION,
            self::VALUE_CLIENT_CANCELLATION,
            self::VALUE_VENDOR_REJECTION_AND_CLIENT_CANCELLATION,
            self::VALUE_CLIENT_CLAIM,
            self::VALUE_VENDOR_INITIATED_REFUND,
            self::VALUE_CLIENT_RETRACTION,
            self::VALUE_NO_CLIENT_WITH_DRAWAL,
            self::VALUE_PRODUCT_STOCK_UNAVAILABLE,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
