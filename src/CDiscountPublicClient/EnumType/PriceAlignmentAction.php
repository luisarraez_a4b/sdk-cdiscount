<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for PriceAlignmentAction EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:PriceAlignmentAction
 * @subpackage Enumerations
 */
class PriceAlignmentAction
{
    /**
     * Constant for value 'Empty'
     * @return string 'Empty'
     */
    const VALUE_EMPTY = 'Empty';
    /**
     * Constant for value 'Unknown'
     * @return string 'Unknown'
     */
    const VALUE_UNKNOWN = 'Unknown';
    /**
     * Constant for value 'Align'
     * @return string 'Align'
     */
    const VALUE_ALIGN = 'Align';
    /**
     * Constant for value 'DontAlign'
     * @return string 'DontAlign'
     */
    const VALUE_DONT_ALIGN = 'DontAlign';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_EMPTY
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_ALIGN
     * @uses self::VALUE_DONT_ALIGN
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_EMPTY,
            self::VALUE_UNKNOWN,
            self::VALUE_ALIGN,
            self::VALUE_DONT_ALIGN,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
