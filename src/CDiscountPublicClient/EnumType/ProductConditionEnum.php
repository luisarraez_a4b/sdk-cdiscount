<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for ProductConditionEnum EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ProductConditionEnum
 * @subpackage Enumerations
 */
class ProductConditionEnum
{
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Constant for value 'LikeNew'
     * @return string 'LikeNew'
     */
    const VALUE_LIKE_NEW = 'LikeNew';
    /**
     * Constant for value 'VeryGoodState'
     * @return string 'VeryGoodState'
     */
    const VALUE_VERY_GOOD_STATE = 'VeryGoodState';
    /**
     * Constant for value 'GoodState'
     * @return string 'GoodState'
     */
    const VALUE_GOOD_STATE = 'GoodState';
    /**
     * Constant for value 'AverageState'
     * @return string 'AverageState'
     */
    const VALUE_AVERAGE_STATE = 'AverageState';
    /**
     * Constant for value 'Refurbished'
     * @return string 'Refurbished'
     */
    const VALUE_REFURBISHED = 'Refurbished';
    /**
     * Constant for value 'New'
     * @return string 'New'
     */
    const VALUE_NEW = 'New';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_NONE
     * @uses self::VALUE_LIKE_NEW
     * @uses self::VALUE_VERY_GOOD_STATE
     * @uses self::VALUE_GOOD_STATE
     * @uses self::VALUE_AVERAGE_STATE
     * @uses self::VALUE_REFURBISHED
     * @uses self::VALUE_NEW
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_NONE,
            self::VALUE_LIKE_NEW,
            self::VALUE_VERY_GOOD_STATE,
            self::VALUE_GOOD_STATE,
            self::VALUE_AVERAGE_STATE,
            self::VALUE_REFURBISHED,
            self::VALUE_NEW,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
