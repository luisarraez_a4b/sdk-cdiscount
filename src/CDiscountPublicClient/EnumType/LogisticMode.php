<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for LogisticMode EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:LogisticMode
 * @subpackage Enumerations
 */
class LogisticMode
{
    /**
     * Constant for value 'Marketplace'
     * @return string 'Marketplace'
     */
    const VALUE_MARKETPLACE = 'Marketplace';
    /**
     * Constant for value 'Fulfilment'
     * @return string 'Fulfilment'
     */
    const VALUE_FULFILMENT = 'Fulfilment';
    /**
     * Constant for value 'Fulfilment on demand'
     * @return string 'Fulfilment on demand'
     */
    const VALUE_FULFILMENT_ON_DEMAND = 'Fulfilment on demand';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_MARKETPLACE
     * @uses self::VALUE_FULFILMENT
     * @uses self::VALUE_FULFILMENT_ON_DEMAND
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_MARKETPLACE,
            self::VALUE_FULFILMENT,
            self::VALUE_FULFILMENT_ON_DEMAND,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
