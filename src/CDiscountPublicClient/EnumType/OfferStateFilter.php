<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for OfferStateFilter EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OfferStateFilter
 * @subpackage Enumerations
 */
class OfferStateFilter
{
    /**
     * Constant for value 'AllOffersOnly'
     * @return string 'AllOffersOnly'
     */
    const VALUE_ALL_OFFERS_ONLY = 'AllOffersOnly';
    /**
     * Constant for value 'OnlineOffersOnly'
     * @return string 'OnlineOffersOnly'
     */
    const VALUE_ONLINE_OFFERS_ONLY = 'OnlineOffersOnly';
    /**
     * Constant for value 'PublishedOffersOnly'
     * @return string 'PublishedOffersOnly'
     */
    const VALUE_PUBLISHED_OFFERS_ONLY = 'PublishedOffersOnly';
    /**
     * Constant for value 'OfflineOffersOnly'
     * @return string 'OfflineOffersOnly'
     */
    const VALUE_OFFLINE_OFFERS_ONLY = 'OfflineOffersOnly';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_ALL_OFFERS_ONLY
     * @uses self::VALUE_ONLINE_OFFERS_ONLY
     * @uses self::VALUE_PUBLISHED_OFFERS_ONLY
     * @uses self::VALUE_OFFLINE_OFFERS_ONLY
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_ALL_OFFERS_ONLY,
            self::VALUE_ONLINE_OFFERS_ONLY,
            self::VALUE_PUBLISHED_OFFERS_ONLY,
            self::VALUE_OFFLINE_OFFERS_ONLY,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
