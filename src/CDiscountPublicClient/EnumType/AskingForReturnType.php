<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for AskingForReturnType EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:AskingForReturnType
 * @subpackage Enumerations
 */
class AskingForReturnType
{
    /**
     * Constant for value 'AskingForReturn'
     * @return string 'AskingForReturn'
     */
    const VALUE_ASKING_FOR_RETURN = 'AskingForReturn';
    /**
     * Constant for value 'AskingForReturnAndRemoval'
     * @return string 'AskingForReturnAndRemoval'
     */
    const VALUE_ASKING_FOR_RETURN_AND_REMOVAL = 'AskingForReturnAndRemoval';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_ASKING_FOR_RETURN
     * @uses self::VALUE_ASKING_FOR_RETURN_AND_REMOVAL
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_ASKING_FOR_RETURN,
            self::VALUE_ASKING_FOR_RETURN_AND_REMOVAL,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
