<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for FulfilmentProductListStatus EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:FulfilmentProductListStatus
 * @subpackage Enumerations
 */
class FulfilmentProductListStatus
{
    /**
     * Constant for value 'Ok'
     * @return string 'Ok'
     */
    const VALUE_OK = 'Ok';
    /**
     * Constant for value 'NoData'
     * @return string 'NoData'
     */
    const VALUE_NO_DATA = 'NoData';
    /**
     * Constant for value 'KO'
     * @return string 'KO'
     */
    const VALUE_KO = 'KO';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_OK
     * @uses self::VALUE_NO_DATA
     * @uses self::VALUE_KO
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_OK,
            self::VALUE_NO_DATA,
            self::VALUE_KO,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
