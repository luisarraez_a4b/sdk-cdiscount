<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for SellerRefundRequestMode EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerRefundRequestMode
 * @subpackage Enumerations
 */
class SellerRefundRequestMode
{
    /**
     * Constant for value 'Claim'
     * @return string 'Claim'
     */
    const VALUE_CLAIM = 'Claim';
    /**
     * Constant for value 'Retraction'
     * @return string 'Retraction'
     */
    const VALUE_RETRACTION = 'Retraction';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_CLAIM
     * @uses self::VALUE_RETRACTION
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_CLAIM,
            self::VALUE_RETRACTION,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
