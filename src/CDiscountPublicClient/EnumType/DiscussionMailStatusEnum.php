<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for DiscussionMailStatusEnum EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:DiscussionMailStatusEnum
 * @subpackage Enumerations
 */
class DiscussionMailStatusEnum
{
    /**
     * Constant for value 'DiscussionNotFound'
     * @return string 'DiscussionNotFound'
     */
    const VALUE_DISCUSSION_NOT_FOUND = 'DiscussionNotFound';
    /**
     * Constant for value 'UnknownError'
     * @return string 'UnknownError'
     */
    const VALUE_UNKNOWN_ERROR = 'UnknownError';
    /**
     * Constant for value 'Success'
     * @return string 'Success'
     */
    const VALUE_SUCCESS = 'Success';
    /**
     * Constant for value 'DiscussionClosed'
     * @return string 'DiscussionClosed'
     */
    const VALUE_DISCUSSION_CLOSED = 'DiscussionClosed';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_DISCUSSION_NOT_FOUND
     * @uses self::VALUE_UNKNOWN_ERROR
     * @uses self::VALUE_SUCCESS
     * @uses self::VALUE_DISCUSSION_CLOSED
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_DISCUSSION_NOT_FOUND,
            self::VALUE_UNKNOWN_ERROR,
            self::VALUE_SUCCESS,
            self::VALUE_DISCUSSION_CLOSED,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
