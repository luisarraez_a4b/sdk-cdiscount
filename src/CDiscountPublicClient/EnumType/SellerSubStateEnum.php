<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for SellerSubStateEnum EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:SellerSubStateEnum
 * @subpackage Enumerations
 */
class SellerSubStateEnum
{
    /**
     * Constant for value 'Holidays'
     * @return string 'Holidays'
     */
    const VALUE_HOLIDAYS = 'Holidays';
    /**
     * Constant for value 'ActiveSeller'
     * @return string 'ActiveSeller'
     */
    const VALUE_ACTIVE_SELLER = 'ActiveSeller';
    /**
     * Constant for value 'BannedSeller'
     * @return string 'BannedSeller'
     */
    const VALUE_BANNED_SELLER = 'BannedSeller';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_HOLIDAYS
     * @uses self::VALUE_ACTIVE_SELLER
     * @uses self::VALUE_BANNED_SELLER
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_HOLIDAYS,
            self::VALUE_ACTIVE_SELLER,
            self::VALUE_BANNED_SELLER,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
