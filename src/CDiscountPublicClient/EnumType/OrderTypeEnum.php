<?php

namespace A4BGroup\Client\CDiscountPublicClient\EnumType;

/**
 * This class stands for OrderTypeEnum EnumType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:OrderTypeEnum
 * @subpackage Enumerations
 */
class OrderTypeEnum
{
    /**
     * Constant for value 'IsFulfillment'
     * @return string 'IsFulfillment'
     */
    const VALUE_IS_FULFILLMENT = 'IsFulfillment';
    /**
     * Constant for value 'MKPFBC'
     * @return string 'MKPFBC'
     */
    const VALUE_MKPFBC = 'MKPFBC';
    /**
     * Constant for value 'EXTFBC'
     * @return string 'EXTFBC'
     */
    const VALUE_EXTFBC = 'EXTFBC';
    /**
     * Constant for value 'None'
     * @return string 'None'
     */
    const VALUE_NONE = 'None';
    /**
     * Return true if value is allowed
     * @uses self::getValidValues()
     * @param mixed $value value
     * @return bool true|false
     */
    public static function valueIsValid($value)
    {
        return ($value === null) || in_array($value, self::getValidValues(), true);
    }
    /**
     * Return allowed values
     * @uses self::VALUE_IS_FULFILLMENT
     * @uses self::VALUE_MKPFBC
     * @uses self::VALUE_EXTFBC
     * @uses self::VALUE_NONE
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_IS_FULFILLMENT,
            self::VALUE_MKPFBC,
            self::VALUE_EXTFBC,
            self::VALUE_NONE,
        );
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
