<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfValidateOrder ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfValidateOrder
 * @subpackage Arrays
 */
class ArrayOfValidateOrder extends AbstractStructArrayBase
{
    /**
     * The ValidateOrder
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder[]
     */
    public $ValidateOrder;
    /**
     * Constructor method for ArrayOfValidateOrder
     * @uses ArrayOfValidateOrder::setValidateOrder()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder[] $validateOrder
     */
    public function __construct(array $validateOrder = array())
    {
        $this
            ->setValidateOrder($validateOrder);
    }
    /**
     * Get ValidateOrder value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder[]|null
     */
    public function getValidateOrder()
    {
        return isset($this->ValidateOrder) ? $this->ValidateOrder : null;
    }
    /**
     * This method is responsible for validating the values passed to the setValidateOrder method
     * This method is willingly generated in order to preserve the one-line inline validation within the setValidateOrder method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateValidateOrderForArrayConstraintsFromSetValidateOrder(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfValidateOrderValidateOrderItem) {
            // validation for constraint: itemType
            if (!$arrayOfValidateOrderValidateOrderItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder) {
                $invalidValues[] = is_object($arrayOfValidateOrderValidateOrderItem) ? get_class($arrayOfValidateOrderValidateOrderItem) : sprintf('%s(%s)', gettype($arrayOfValidateOrderValidateOrderItem), var_export($arrayOfValidateOrderValidateOrderItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ValidateOrder property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ValidateOrder, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ValidateOrder value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder[] $validateOrder
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder
     */
    public function setValidateOrder(array $validateOrder = array())
    {
        // validation for constraint: array
        if ('' !== ($validateOrderArrayErrorMessage = self::validateValidateOrderForArrayConstraintsFromSetValidateOrder($validateOrder))) {
            throw new \InvalidArgumentException($validateOrderArrayErrorMessage, __LINE__);
        }
        if (is_null($validateOrder) || (is_array($validateOrder) && empty($validateOrder))) {
            unset($this->ValidateOrder);
        } else {
            $this->ValidateOrder = $validateOrder;
        }
        return $this;
    }
    /**
     * Add item to ValidateOrder value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder
     */
    public function addToValidateOrder(\A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder) {
            throw new \InvalidArgumentException(sprintf('The ValidateOrder property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ValidateOrder, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ValidateOrder[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ValidateOrder|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ValidateOrder
     */
    public function getAttributeName()
    {
        return 'ValidateOrder';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfValidateOrder
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
