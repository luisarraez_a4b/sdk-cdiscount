<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfDeliveryModeInformation ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfDeliveryModeInformation
 * @subpackage Arrays
 */
class ArrayOfDeliveryModeInformation extends AbstractStructArrayBase
{
    /**
     * The DeliveryModeInformation
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation[]
     */
    public $DeliveryModeInformation;
    /**
     * Constructor method for ArrayOfDeliveryModeInformation
     * @uses ArrayOfDeliveryModeInformation::setDeliveryModeInformation()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation[] $deliveryModeInformation
     */
    public function __construct(array $deliveryModeInformation = array())
    {
        $this
            ->setDeliveryModeInformation($deliveryModeInformation);
    }
    /**
     * Get DeliveryModeInformation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation[]|null
     */
    public function getDeliveryModeInformation()
    {
        return isset($this->DeliveryModeInformation) ? $this->DeliveryModeInformation : null;
    }
    /**
     * This method is responsible for validating the values passed to the setDeliveryModeInformation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDeliveryModeInformation method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDeliveryModeInformationForArrayConstraintsFromSetDeliveryModeInformation(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfDeliveryModeInformationDeliveryModeInformationItem) {
            // validation for constraint: itemType
            if (!$arrayOfDeliveryModeInformationDeliveryModeInformationItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation) {
                $invalidValues[] = is_object($arrayOfDeliveryModeInformationDeliveryModeInformationItem) ? get_class($arrayOfDeliveryModeInformationDeliveryModeInformationItem) : sprintf('%s(%s)', gettype($arrayOfDeliveryModeInformationDeliveryModeInformationItem), var_export($arrayOfDeliveryModeInformationDeliveryModeInformationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The DeliveryModeInformation property can only contain items of type \A4BGroup\Client\CDiscount\StructType\DeliveryModeInformation, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set DeliveryModeInformation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation[] $deliveryModeInformation
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation
     */
    public function setDeliveryModeInformation(array $deliveryModeInformation = array())
    {
        // validation for constraint: array
        if ('' !== ($deliveryModeInformationArrayErrorMessage = self::validateDeliveryModeInformationForArrayConstraintsFromSetDeliveryModeInformation($deliveryModeInformation))) {
            throw new \InvalidArgumentException($deliveryModeInformationArrayErrorMessage, __LINE__);
        }
        if (is_null($deliveryModeInformation) || (is_array($deliveryModeInformation) && empty($deliveryModeInformation))) {
            unset($this->DeliveryModeInformation);
        } else {
            $this->DeliveryModeInformation = $deliveryModeInformation;
        }
        return $this;
    }
    /**
     * Add item to DeliveryModeInformation value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation
     */
    public function addToDeliveryModeInformation(\A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation) {
            throw new \InvalidArgumentException(sprintf('The DeliveryModeInformation property can only contain items of type \A4BGroup\Client\CDiscount\StructType\DeliveryModeInformation, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->DeliveryModeInformation[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\DeliveryModeInformation|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string DeliveryModeInformation
     */
    public function getAttributeName()
    {
        return 'DeliveryModeInformation';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDeliveryModeInformation
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
