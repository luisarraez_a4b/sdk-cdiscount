<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfFulfilmentActivationReport ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfFulfilmentActivationReport
 * @subpackage Arrays
 */
class ArrayOfFulfilmentActivationReport extends AbstractStructArrayBase
{
    /**
     * The FulfilmentActivationReport
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport[]
     */
    public $FulfilmentActivationReport;
    /**
     * Constructor method for ArrayOfFulfilmentActivationReport
     * @uses ArrayOfFulfilmentActivationReport::setFulfilmentActivationReport()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport[] $fulfilmentActivationReport
     */
    public function __construct(array $fulfilmentActivationReport = array())
    {
        $this
            ->setFulfilmentActivationReport($fulfilmentActivationReport);
    }
    /**
     * Get FulfilmentActivationReport value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport[]|null
     */
    public function getFulfilmentActivationReport()
    {
        return isset($this->FulfilmentActivationReport) ? $this->FulfilmentActivationReport : null;
    }
    /**
     * This method is responsible for validating the values passed to the setFulfilmentActivationReport method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFulfilmentActivationReport method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFulfilmentActivationReportForArrayConstraintsFromSetFulfilmentActivationReport(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfFulfilmentActivationReportFulfilmentActivationReportItem) {
            // validation for constraint: itemType
            if (!$arrayOfFulfilmentActivationReportFulfilmentActivationReportItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport) {
                $invalidValues[] = is_object($arrayOfFulfilmentActivationReportFulfilmentActivationReportItem) ? get_class($arrayOfFulfilmentActivationReportFulfilmentActivationReportItem) : sprintf('%s(%s)', gettype($arrayOfFulfilmentActivationReportFulfilmentActivationReportItem), var_export($arrayOfFulfilmentActivationReportFulfilmentActivationReportItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The FulfilmentActivationReport property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentActivationReport, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set FulfilmentActivationReport value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport[] $fulfilmentActivationReport
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport
     */
    public function setFulfilmentActivationReport(array $fulfilmentActivationReport = array())
    {
        // validation for constraint: array
        if ('' !== ($fulfilmentActivationReportArrayErrorMessage = self::validateFulfilmentActivationReportForArrayConstraintsFromSetFulfilmentActivationReport($fulfilmentActivationReport))) {
            throw new \InvalidArgumentException($fulfilmentActivationReportArrayErrorMessage, __LINE__);
        }
        if (is_null($fulfilmentActivationReport) || (is_array($fulfilmentActivationReport) && empty($fulfilmentActivationReport))) {
            unset($this->FulfilmentActivationReport);
        } else {
            $this->FulfilmentActivationReport = $fulfilmentActivationReport;
        }
        return $this;
    }
    /**
     * Add item to FulfilmentActivationReport value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport
     */
    public function addToFulfilmentActivationReport(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport) {
            throw new \InvalidArgumentException(sprintf('The FulfilmentActivationReport property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentActivationReport, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->FulfilmentActivationReport[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentActivationReport|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string FulfilmentActivationReport
     */
    public function getAttributeName()
    {
        return 'FulfilmentActivationReport';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentActivationReport
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
