<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfProductStock ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfProductStock
 * @subpackage Arrays
 */
class ArrayOfProductStock extends AbstractStructArrayBase
{
    /**
     * The ProductStock
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock[]
     */
    public $ProductStock;
    /**
     * Constructor method for ArrayOfProductStock
     * @uses ArrayOfProductStock::setProductStock()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock[] $productStock
     */
    public function __construct(array $productStock = array())
    {
        $this
            ->setProductStock($productStock);
    }
    /**
     * Get ProductStock value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock[]|null
     */
    public function getProductStock()
    {
        return isset($this->ProductStock) ? $this->ProductStock : null;
    }
    /**
     * This method is responsible for validating the values passed to the setProductStock method
     * This method is willingly generated in order to preserve the one-line inline validation within the setProductStock method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateProductStockForArrayConstraintsFromSetProductStock(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfProductStockProductStockItem) {
            // validation for constraint: itemType
            if (!$arrayOfProductStockProductStockItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock) {
                $invalidValues[] = is_object($arrayOfProductStockProductStockItem) ? get_class($arrayOfProductStockProductStockItem) : sprintf('%s(%s)', gettype($arrayOfProductStockProductStockItem), var_export($arrayOfProductStockProductStockItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ProductStock property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ProductStock, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ProductStock value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock[] $productStock
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock
     */
    public function setProductStock(array $productStock = array())
    {
        // validation for constraint: array
        if ('' !== ($productStockArrayErrorMessage = self::validateProductStockForArrayConstraintsFromSetProductStock($productStock))) {
            throw new \InvalidArgumentException($productStockArrayErrorMessage, __LINE__);
        }
        if (is_null($productStock) || (is_array($productStock) && empty($productStock))) {
            unset($this->ProductStock);
        } else {
            $this->ProductStock = $productStock;
        }
        return $this;
    }
    /**
     * Add item to ProductStock value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock
     */
    public function addToProductStock(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock) {
            throw new \InvalidArgumentException(sprintf('The ProductStock property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ProductStock, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ProductStock[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductStock|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ProductStock
     */
    public function getAttributeName()
    {
        return 'ProductStock';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductStock
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
