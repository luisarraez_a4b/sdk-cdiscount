<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfVariationDescription ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfVariationDescription
 * @subpackage Arrays
 */
class ArrayOfVariationDescription extends AbstractStructArrayBase
{
    /**
     * The VariationDescription
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription[]
     */
    public $VariationDescription;
    /**
     * Constructor method for ArrayOfVariationDescription
     * @uses ArrayOfVariationDescription::setVariationDescription()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription[] $variationDescription
     */
    public function __construct(array $variationDescription = array())
    {
        $this
            ->setVariationDescription($variationDescription);
    }
    /**
     * Get VariationDescription value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription[]|null
     */
    public function getVariationDescription()
    {
        return isset($this->VariationDescription) ? $this->VariationDescription : null;
    }
    /**
     * This method is responsible for validating the values passed to the setVariationDescription method
     * This method is willingly generated in order to preserve the one-line inline validation within the setVariationDescription method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateVariationDescriptionForArrayConstraintsFromSetVariationDescription(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfVariationDescriptionVariationDescriptionItem) {
            // validation for constraint: itemType
            if (!$arrayOfVariationDescriptionVariationDescriptionItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription) {
                $invalidValues[] = is_object($arrayOfVariationDescriptionVariationDescriptionItem) ? get_class($arrayOfVariationDescriptionVariationDescriptionItem) : sprintf('%s(%s)', gettype($arrayOfVariationDescriptionVariationDescriptionItem), var_export($arrayOfVariationDescriptionVariationDescriptionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The VariationDescription property can only contain items of type \A4BGroup\Client\CDiscount\StructType\VariationDescription, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set VariationDescription value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription[] $variationDescription
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription
     */
    public function setVariationDescription(array $variationDescription = array())
    {
        // validation for constraint: array
        if ('' !== ($variationDescriptionArrayErrorMessage = self::validateVariationDescriptionForArrayConstraintsFromSetVariationDescription($variationDescription))) {
            throw new \InvalidArgumentException($variationDescriptionArrayErrorMessage, __LINE__);
        }
        if (is_null($variationDescription) || (is_array($variationDescription) && empty($variationDescription))) {
            unset($this->VariationDescription);
        } else {
            $this->VariationDescription = $variationDescription;
        }
        return $this;
    }
    /**
     * Add item to VariationDescription value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription
     */
    public function addToVariationDescription(\A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription) {
            throw new \InvalidArgumentException(sprintf('The VariationDescription property can only contain items of type \A4BGroup\Client\CDiscount\StructType\VariationDescription, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->VariationDescription[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\VariationDescription|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string VariationDescription
     */
    public function getAttributeName()
    {
        return 'VariationDescription';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVariationDescription
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
