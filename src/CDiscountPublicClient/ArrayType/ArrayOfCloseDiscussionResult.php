<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfCloseDiscussionResult ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfCloseDiscussionResult
 * @subpackage Arrays
 */
class ArrayOfCloseDiscussionResult extends AbstractStructArrayBase
{
    /**
     * The CloseDiscussionResult
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult[]
     */
    public $CloseDiscussionResult;
    /**
     * Constructor method for ArrayOfCloseDiscussionResult
     * @uses ArrayOfCloseDiscussionResult::setCloseDiscussionResult()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult[] $closeDiscussionResult
     */
    public function __construct(array $closeDiscussionResult = array())
    {
        $this
            ->setCloseDiscussionResult($closeDiscussionResult);
    }
    /**
     * Get CloseDiscussionResult value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult[]|null
     */
    public function getCloseDiscussionResult()
    {
        return isset($this->CloseDiscussionResult) ? $this->CloseDiscussionResult : null;
    }
    /**
     * This method is responsible for validating the values passed to the setCloseDiscussionResult method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCloseDiscussionResult method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCloseDiscussionResultForArrayConstraintsFromSetCloseDiscussionResult(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfCloseDiscussionResultCloseDiscussionResultItem) {
            // validation for constraint: itemType
            if (!$arrayOfCloseDiscussionResultCloseDiscussionResultItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult) {
                $invalidValues[] = is_object($arrayOfCloseDiscussionResultCloseDiscussionResultItem) ? get_class($arrayOfCloseDiscussionResultCloseDiscussionResultItem) : sprintf('%s(%s)', gettype($arrayOfCloseDiscussionResultCloseDiscussionResultItem), var_export($arrayOfCloseDiscussionResultCloseDiscussionResultItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The CloseDiscussionResult property can only contain items of type \A4BGroup\Client\CDiscount\StructType\CloseDiscussionResult, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set CloseDiscussionResult value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult[] $closeDiscussionResult
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult
     */
    public function setCloseDiscussionResult(array $closeDiscussionResult = array())
    {
        // validation for constraint: array
        if ('' !== ($closeDiscussionResultArrayErrorMessage = self::validateCloseDiscussionResultForArrayConstraintsFromSetCloseDiscussionResult($closeDiscussionResult))) {
            throw new \InvalidArgumentException($closeDiscussionResultArrayErrorMessage, __LINE__);
        }
        if (is_null($closeDiscussionResult) || (is_array($closeDiscussionResult) && empty($closeDiscussionResult))) {
            unset($this->CloseDiscussionResult);
        } else {
            $this->CloseDiscussionResult = $closeDiscussionResult;
        }
        return $this;
    }
    /**
     * Add item to CloseDiscussionResult value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult
     */
    public function addToCloseDiscussionResult(\A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult) {
            throw new \InvalidArgumentException(sprintf('The CloseDiscussionResult property can only contain items of type \A4BGroup\Client\CDiscount\StructType\CloseDiscussionResult, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->CloseDiscussionResult[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CloseDiscussionResult|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string CloseDiscussionResult
     */
    public function getAttributeName()
    {
        return 'CloseDiscussionResult';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCloseDiscussionResult
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
