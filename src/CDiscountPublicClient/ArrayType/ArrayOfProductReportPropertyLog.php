<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfProductReportPropertyLog ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfProductReportPropertyLog
 * @subpackage Arrays
 */
class ArrayOfProductReportPropertyLog extends AbstractStructArrayBase
{
    /**
     * The ProductReportPropertyLog
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog[]
     */
    public $ProductReportPropertyLog;
    /**
     * Constructor method for ArrayOfProductReportPropertyLog
     * @uses ArrayOfProductReportPropertyLog::setProductReportPropertyLog()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog[] $productReportPropertyLog
     */
    public function __construct(array $productReportPropertyLog = array())
    {
        $this
            ->setProductReportPropertyLog($productReportPropertyLog);
    }
    /**
     * Get ProductReportPropertyLog value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog[]|null
     */
    public function getProductReportPropertyLog()
    {
        return isset($this->ProductReportPropertyLog) ? $this->ProductReportPropertyLog : null;
    }
    /**
     * This method is responsible for validating the values passed to the setProductReportPropertyLog method
     * This method is willingly generated in order to preserve the one-line inline validation within the setProductReportPropertyLog method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateProductReportPropertyLogForArrayConstraintsFromSetProductReportPropertyLog(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfProductReportPropertyLogProductReportPropertyLogItem) {
            // validation for constraint: itemType
            if (!$arrayOfProductReportPropertyLogProductReportPropertyLogItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog) {
                $invalidValues[] = is_object($arrayOfProductReportPropertyLogProductReportPropertyLogItem) ? get_class($arrayOfProductReportPropertyLogProductReportPropertyLogItem) : sprintf('%s(%s)', gettype($arrayOfProductReportPropertyLogProductReportPropertyLogItem), var_export($arrayOfProductReportPropertyLogProductReportPropertyLogItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ProductReportPropertyLog property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ProductReportPropertyLog, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ProductReportPropertyLog value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog[] $productReportPropertyLog
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductReportPropertyLog
     */
    public function setProductReportPropertyLog(array $productReportPropertyLog = array())
    {
        // validation for constraint: array
        if ('' !== ($productReportPropertyLogArrayErrorMessage = self::validateProductReportPropertyLogForArrayConstraintsFromSetProductReportPropertyLog($productReportPropertyLog))) {
            throw new \InvalidArgumentException($productReportPropertyLogArrayErrorMessage, __LINE__);
        }
        if (is_null($productReportPropertyLog) || (is_array($productReportPropertyLog) && empty($productReportPropertyLog))) {
            unset($this->ProductReportPropertyLog);
        } else {
            $this->ProductReportPropertyLog = $productReportPropertyLog;
        }
        return $this;
    }
    /**
     * Add item to ProductReportPropertyLog value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductReportPropertyLog
     */
    public function addToProductReportPropertyLog(\A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog) {
            throw new \InvalidArgumentException(sprintf('The ProductReportPropertyLog property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ProductReportPropertyLog, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ProductReportPropertyLog[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ProductReportPropertyLog|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ProductReportPropertyLog
     */
    public function getAttributeName()
    {
        return 'ProductReportPropertyLog';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfProductReportPropertyLog
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
