<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfServiceOrder ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfServiceOrder
 * @subpackage Arrays
 */
class ArrayOfServiceOrder extends AbstractStructArrayBase
{
    /**
     * The ServiceOrder
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder[]
     */
    public $ServiceOrder;
    /**
     * Constructor method for ArrayOfServiceOrder
     * @uses ArrayOfServiceOrder::setServiceOrder()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder[] $serviceOrder
     */
    public function __construct(array $serviceOrder = array())
    {
        $this
            ->setServiceOrder($serviceOrder);
    }
    /**
     * Get ServiceOrder value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder[]|null
     */
    public function getServiceOrder()
    {
        return isset($this->ServiceOrder) ? $this->ServiceOrder : null;
    }
    /**
     * This method is responsible for validating the values passed to the setServiceOrder method
     * This method is willingly generated in order to preserve the one-line inline validation within the setServiceOrder method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateServiceOrderForArrayConstraintsFromSetServiceOrder(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfServiceOrderServiceOrderItem) {
            // validation for constraint: itemType
            if (!$arrayOfServiceOrderServiceOrderItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder) {
                $invalidValues[] = is_object($arrayOfServiceOrderServiceOrderItem) ? get_class($arrayOfServiceOrderServiceOrderItem) : sprintf('%s(%s)', gettype($arrayOfServiceOrderServiceOrderItem), var_export($arrayOfServiceOrderServiceOrderItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ServiceOrder property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ServiceOrder, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ServiceOrder value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder[] $serviceOrder
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder
     */
    public function setServiceOrder(array $serviceOrder = array())
    {
        // validation for constraint: array
        if ('' !== ($serviceOrderArrayErrorMessage = self::validateServiceOrderForArrayConstraintsFromSetServiceOrder($serviceOrder))) {
            throw new \InvalidArgumentException($serviceOrderArrayErrorMessage, __LINE__);
        }
        if (is_null($serviceOrder) || (is_array($serviceOrder) && empty($serviceOrder))) {
            unset($this->ServiceOrder);
        } else {
            $this->ServiceOrder = $serviceOrder;
        }
        return $this;
    }
    /**
     * Add item to ServiceOrder value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder
     */
    public function addToServiceOrder(\A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder) {
            throw new \InvalidArgumentException(sprintf('The ServiceOrder property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ServiceOrder, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ServiceOrder[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ServiceOrder|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ServiceOrder
     */
    public function getAttributeName()
    {
        return 'ServiceOrder';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfServiceOrder
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
