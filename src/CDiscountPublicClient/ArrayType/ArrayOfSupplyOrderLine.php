<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfSupplyOrderLine ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfSupplyOrderLine
 * @subpackage Arrays
 */
class ArrayOfSupplyOrderLine extends AbstractStructArrayBase
{
    /**
     * The SupplyOrderLine
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine[]
     */
    public $SupplyOrderLine;
    /**
     * Constructor method for ArrayOfSupplyOrderLine
     * @uses ArrayOfSupplyOrderLine::setSupplyOrderLine()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine[] $supplyOrderLine
     */
    public function __construct(array $supplyOrderLine = array())
    {
        $this
            ->setSupplyOrderLine($supplyOrderLine);
    }
    /**
     * Get SupplyOrderLine value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine[]|null
     */
    public function getSupplyOrderLine()
    {
        return isset($this->SupplyOrderLine) ? $this->SupplyOrderLine : null;
    }
    /**
     * This method is responsible for validating the values passed to the setSupplyOrderLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSupplyOrderLine method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSupplyOrderLineForArrayConstraintsFromSetSupplyOrderLine(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfSupplyOrderLineSupplyOrderLineItem) {
            // validation for constraint: itemType
            if (!$arrayOfSupplyOrderLineSupplyOrderLineItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine) {
                $invalidValues[] = is_object($arrayOfSupplyOrderLineSupplyOrderLineItem) ? get_class($arrayOfSupplyOrderLineSupplyOrderLineItem) : sprintf('%s(%s)', gettype($arrayOfSupplyOrderLineSupplyOrderLineItem), var_export($arrayOfSupplyOrderLineSupplyOrderLineItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The SupplyOrderLine property can only contain items of type \A4BGroup\Client\CDiscount\StructType\SupplyOrderLine, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set SupplyOrderLine value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine[] $supplyOrderLine
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine
     */
    public function setSupplyOrderLine(array $supplyOrderLine = array())
    {
        // validation for constraint: array
        if ('' !== ($supplyOrderLineArrayErrorMessage = self::validateSupplyOrderLineForArrayConstraintsFromSetSupplyOrderLine($supplyOrderLine))) {
            throw new \InvalidArgumentException($supplyOrderLineArrayErrorMessage, __LINE__);
        }
        if (is_null($supplyOrderLine) || (is_array($supplyOrderLine) && empty($supplyOrderLine))) {
            unset($this->SupplyOrderLine);
        } else {
            $this->SupplyOrderLine = $supplyOrderLine;
        }
        return $this;
    }
    /**
     * Add item to SupplyOrderLine value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine
     */
    public function addToSupplyOrderLine(\A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine) {
            throw new \InvalidArgumentException(sprintf('The SupplyOrderLine property can only contain items of type \A4BGroup\Client\CDiscount\StructType\SupplyOrderLine, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->SupplyOrderLine[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SupplyOrderLine|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string SupplyOrderLine
     */
    public function getAttributeName()
    {
        return 'SupplyOrderLine';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSupplyOrderLine
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
