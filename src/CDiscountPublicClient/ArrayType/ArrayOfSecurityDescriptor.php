<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfSecurityDescriptor ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfSecurityDescriptor
 * @subpackage Arrays
 */
class ArrayOfSecurityDescriptor extends AbstractStructArrayBase
{
    /**
     * The SecurityDescriptor
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor[]
     */
    public $SecurityDescriptor;
    /**
     * Constructor method for ArrayOfSecurityDescriptor
     * @uses ArrayOfSecurityDescriptor::setSecurityDescriptor()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor[] $securityDescriptor
     */
    public function __construct(array $securityDescriptor = array())
    {
        $this
            ->setSecurityDescriptor($securityDescriptor);
    }
    /**
     * Get SecurityDescriptor value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor[]|null
     */
    public function getSecurityDescriptor()
    {
        return isset($this->SecurityDescriptor) ? $this->SecurityDescriptor : null;
    }
    /**
     * This method is responsible for validating the values passed to the setSecurityDescriptor method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSecurityDescriptor method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSecurityDescriptorForArrayConstraintsFromSetSecurityDescriptor(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfSecurityDescriptorSecurityDescriptorItem) {
            // validation for constraint: itemType
            if (!$arrayOfSecurityDescriptorSecurityDescriptorItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor) {
                $invalidValues[] = is_object($arrayOfSecurityDescriptorSecurityDescriptorItem) ? get_class($arrayOfSecurityDescriptorSecurityDescriptorItem) : sprintf('%s(%s)', gettype($arrayOfSecurityDescriptorSecurityDescriptorItem), var_export($arrayOfSecurityDescriptorSecurityDescriptorItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The SecurityDescriptor property can only contain items of type \A4BGroup\Client\CDiscount\StructType\SecurityDescriptor, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set SecurityDescriptor value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor[] $securityDescriptor
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor
     */
    public function setSecurityDescriptor(array $securityDescriptor = array())
    {
        // validation for constraint: array
        if ('' !== ($securityDescriptorArrayErrorMessage = self::validateSecurityDescriptorForArrayConstraintsFromSetSecurityDescriptor($securityDescriptor))) {
            throw new \InvalidArgumentException($securityDescriptorArrayErrorMessage, __LINE__);
        }
        if (is_null($securityDescriptor) || (is_array($securityDescriptor) && empty($securityDescriptor))) {
            unset($this->SecurityDescriptor);
        } else {
            $this->SecurityDescriptor = $securityDescriptor;
        }
        return $this;
    }
    /**
     * Add item to SecurityDescriptor value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor
     */
    public function addToSecurityDescriptor(\A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor) {
            throw new \InvalidArgumentException(sprintf('The SecurityDescriptor property can only contain items of type \A4BGroup\Client\CDiscount\StructType\SecurityDescriptor, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->SecurityDescriptor[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SecurityDescriptor|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string SecurityDescriptor
     */
    public function getAttributeName()
    {
        return 'SecurityDescriptor';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSecurityDescriptor
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
