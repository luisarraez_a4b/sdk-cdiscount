<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfFulfilmentProductDescription ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfFulfilmentProductDescription
 * @subpackage Arrays
 */
class ArrayOfFulfilmentProductDescription extends AbstractStructArrayBase
{
    /**
     * The FulfilmentProductDescription
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription[]
     */
    public $FulfilmentProductDescription;
    /**
     * Constructor method for ArrayOfFulfilmentProductDescription
     * @uses ArrayOfFulfilmentProductDescription::setFulfilmentProductDescription()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription[] $fulfilmentProductDescription
     */
    public function __construct(array $fulfilmentProductDescription = array())
    {
        $this
            ->setFulfilmentProductDescription($fulfilmentProductDescription);
    }
    /**
     * Get FulfilmentProductDescription value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription[]|null
     */
    public function getFulfilmentProductDescription()
    {
        return isset($this->FulfilmentProductDescription) ? $this->FulfilmentProductDescription : null;
    }
    /**
     * This method is responsible for validating the values passed to the setFulfilmentProductDescription method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFulfilmentProductDescription method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFulfilmentProductDescriptionForArrayConstraintsFromSetFulfilmentProductDescription(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfFulfilmentProductDescriptionFulfilmentProductDescriptionItem) {
            // validation for constraint: itemType
            if (!$arrayOfFulfilmentProductDescriptionFulfilmentProductDescriptionItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription) {
                $invalidValues[] = is_object($arrayOfFulfilmentProductDescriptionFulfilmentProductDescriptionItem) ? get_class($arrayOfFulfilmentProductDescriptionFulfilmentProductDescriptionItem) : sprintf('%s(%s)', gettype($arrayOfFulfilmentProductDescriptionFulfilmentProductDescriptionItem), var_export($arrayOfFulfilmentProductDescriptionFulfilmentProductDescriptionItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The FulfilmentProductDescription property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentProductDescription, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set FulfilmentProductDescription value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription[] $fulfilmentProductDescription
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription
     */
    public function setFulfilmentProductDescription(array $fulfilmentProductDescription = array())
    {
        // validation for constraint: array
        if ('' !== ($fulfilmentProductDescriptionArrayErrorMessage = self::validateFulfilmentProductDescriptionForArrayConstraintsFromSetFulfilmentProductDescription($fulfilmentProductDescription))) {
            throw new \InvalidArgumentException($fulfilmentProductDescriptionArrayErrorMessage, __LINE__);
        }
        if (is_null($fulfilmentProductDescription) || (is_array($fulfilmentProductDescription) && empty($fulfilmentProductDescription))) {
            unset($this->FulfilmentProductDescription);
        } else {
            $this->FulfilmentProductDescription = $fulfilmentProductDescription;
        }
        return $this;
    }
    /**
     * Add item to FulfilmentProductDescription value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription
     */
    public function addToFulfilmentProductDescription(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription) {
            throw new \InvalidArgumentException(sprintf('The FulfilmentProductDescription property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentProductDescription, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->FulfilmentProductDescription[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentProductDescription|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string FulfilmentProductDescription
     */
    public function getAttributeName()
    {
        return 'FulfilmentProductDescription';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentProductDescription
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
