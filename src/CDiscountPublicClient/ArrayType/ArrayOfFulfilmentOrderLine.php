<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfFulfilmentOrderLine ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfFulfilmentOrderLine
 * @subpackage Arrays
 */
class ArrayOfFulfilmentOrderLine extends AbstractStructArrayBase
{
    /**
     * The FulfilmentOrderLine
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine[]
     */
    public $FulfilmentOrderLine;
    /**
     * Constructor method for ArrayOfFulfilmentOrderLine
     * @uses ArrayOfFulfilmentOrderLine::setFulfilmentOrderLine()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine[] $fulfilmentOrderLine
     */
    public function __construct(array $fulfilmentOrderLine = array())
    {
        $this
            ->setFulfilmentOrderLine($fulfilmentOrderLine);
    }
    /**
     * Get FulfilmentOrderLine value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine[]|null
     */
    public function getFulfilmentOrderLine()
    {
        return isset($this->FulfilmentOrderLine) ? $this->FulfilmentOrderLine : null;
    }
    /**
     * This method is responsible for validating the values passed to the setFulfilmentOrderLine method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFulfilmentOrderLine method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFulfilmentOrderLineForArrayConstraintsFromSetFulfilmentOrderLine(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfFulfilmentOrderLineFulfilmentOrderLineItem) {
            // validation for constraint: itemType
            if (!$arrayOfFulfilmentOrderLineFulfilmentOrderLineItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine) {
                $invalidValues[] = is_object($arrayOfFulfilmentOrderLineFulfilmentOrderLineItem) ? get_class($arrayOfFulfilmentOrderLineFulfilmentOrderLineItem) : sprintf('%s(%s)', gettype($arrayOfFulfilmentOrderLineFulfilmentOrderLineItem), var_export($arrayOfFulfilmentOrderLineFulfilmentOrderLineItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The FulfilmentOrderLine property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentOrderLine, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set FulfilmentOrderLine value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine[] $fulfilmentOrderLine
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLine
     */
    public function setFulfilmentOrderLine(array $fulfilmentOrderLine = array())
    {
        // validation for constraint: array
        if ('' !== ($fulfilmentOrderLineArrayErrorMessage = self::validateFulfilmentOrderLineForArrayConstraintsFromSetFulfilmentOrderLine($fulfilmentOrderLine))) {
            throw new \InvalidArgumentException($fulfilmentOrderLineArrayErrorMessage, __LINE__);
        }
        if (is_null($fulfilmentOrderLine) || (is_array($fulfilmentOrderLine) && empty($fulfilmentOrderLine))) {
            unset($this->FulfilmentOrderLine);
        } else {
            $this->FulfilmentOrderLine = $fulfilmentOrderLine;
        }
        return $this;
    }
    /**
     * Add item to FulfilmentOrderLine value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLine
     */
    public function addToFulfilmentOrderLine(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine) {
            throw new \InvalidArgumentException(sprintf('The FulfilmentOrderLine property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentOrderLine, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->FulfilmentOrderLine[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLine|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string FulfilmentOrderLine
     */
    public function getAttributeName()
    {
        return 'FulfilmentOrderLine';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLine
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
