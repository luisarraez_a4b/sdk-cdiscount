<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfCategoryTree ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfCategoryTree
 * @subpackage Arrays
 */
class ArrayOfCategoryTree extends AbstractStructArrayBase
{
    /**
     * The CategoryTree
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree[]
     */
    public $CategoryTree;
    /**
     * Constructor method for ArrayOfCategoryTree
     * @uses ArrayOfCategoryTree::setCategoryTree()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree[] $categoryTree
     */
    public function __construct(array $categoryTree = array())
    {
        $this
            ->setCategoryTree($categoryTree);
    }
    /**
     * Get CategoryTree value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree[]|null
     */
    public function getCategoryTree()
    {
        return isset($this->CategoryTree) ? $this->CategoryTree : null;
    }
    /**
     * This method is responsible for validating the values passed to the setCategoryTree method
     * This method is willingly generated in order to preserve the one-line inline validation within the setCategoryTree method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateCategoryTreeForArrayConstraintsFromSetCategoryTree(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfCategoryTreeCategoryTreeItem) {
            // validation for constraint: itemType
            if (!$arrayOfCategoryTreeCategoryTreeItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree) {
                $invalidValues[] = is_object($arrayOfCategoryTreeCategoryTreeItem) ? get_class($arrayOfCategoryTreeCategoryTreeItem) : sprintf('%s(%s)', gettype($arrayOfCategoryTreeCategoryTreeItem), var_export($arrayOfCategoryTreeCategoryTreeItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The CategoryTree property can only contain items of type \A4BGroup\Client\CDiscount\StructType\CategoryTree, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set CategoryTree value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree[] $categoryTree
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree
     */
    public function setCategoryTree(array $categoryTree = array())
    {
        // validation for constraint: array
        if ('' !== ($categoryTreeArrayErrorMessage = self::validateCategoryTreeForArrayConstraintsFromSetCategoryTree($categoryTree))) {
            throw new \InvalidArgumentException($categoryTreeArrayErrorMessage, __LINE__);
        }
        if (is_null($categoryTree) || (is_array($categoryTree) && empty($categoryTree))) {
            unset($this->CategoryTree);
        } else {
            $this->CategoryTree = $categoryTree;
        }
        return $this;
    }
    /**
     * Add item to CategoryTree value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree
     */
    public function addToCategoryTree(\A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree) {
            throw new \InvalidArgumentException(sprintf('The CategoryTree property can only contain items of type \A4BGroup\Client\CDiscount\StructType\CategoryTree, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->CategoryTree[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\CategoryTree|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string CategoryTree
     */
    public function getAttributeName()
    {
        return 'CategoryTree';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfCategoryTree
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
