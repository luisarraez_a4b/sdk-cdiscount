<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfShippingInformation ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfShippingInformation
 * @subpackage Arrays
 */
class ArrayOfShippingInformation extends AbstractStructArrayBase
{
    /**
     * The ShippingInformation
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation[]
     */
    public $ShippingInformation;
    /**
     * Constructor method for ArrayOfShippingInformation
     * @uses ArrayOfShippingInformation::setShippingInformation()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation[] $shippingInformation
     */
    public function __construct(array $shippingInformation = array())
    {
        $this
            ->setShippingInformation($shippingInformation);
    }
    /**
     * Get ShippingInformation value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation[]|null
     */
    public function getShippingInformation()
    {
        return isset($this->ShippingInformation) ? $this->ShippingInformation : null;
    }
    /**
     * This method is responsible for validating the values passed to the setShippingInformation method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShippingInformation method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShippingInformationForArrayConstraintsFromSetShippingInformation(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfShippingInformationShippingInformationItem) {
            // validation for constraint: itemType
            if (!$arrayOfShippingInformationShippingInformationItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation) {
                $invalidValues[] = is_object($arrayOfShippingInformationShippingInformationItem) ? get_class($arrayOfShippingInformationShippingInformationItem) : sprintf('%s(%s)', gettype($arrayOfShippingInformationShippingInformationItem), var_export($arrayOfShippingInformationShippingInformationItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ShippingInformation property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ShippingInformation, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ShippingInformation value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation[] $shippingInformation
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation
     */
    public function setShippingInformation(array $shippingInformation = array())
    {
        // validation for constraint: array
        if ('' !== ($shippingInformationArrayErrorMessage = self::validateShippingInformationForArrayConstraintsFromSetShippingInformation($shippingInformation))) {
            throw new \InvalidArgumentException($shippingInformationArrayErrorMessage, __LINE__);
        }
        if (is_null($shippingInformation) || (is_array($shippingInformation) && empty($shippingInformation))) {
            unset($this->ShippingInformation);
        } else {
            $this->ShippingInformation = $shippingInformation;
        }
        return $this;
    }
    /**
     * Add item to ShippingInformation value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation
     */
    public function addToShippingInformation(\A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation) {
            throw new \InvalidArgumentException(sprintf('The ShippingInformation property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ShippingInformation, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ShippingInformation[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ShippingInformation|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ShippingInformation
     */
    public function getAttributeName()
    {
        return 'ShippingInformation';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfShippingInformation
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
