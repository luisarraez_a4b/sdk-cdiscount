<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfDiscussionStateFilter ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfDiscussionStateFilter
 * @subpackage Arrays
 */
class ArrayOfDiscussionStateFilter extends AbstractStructArrayBase
{
    /**
     * The DiscussionStateFilter
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $DiscussionStateFilter;
    /**
     * Constructor method for ArrayOfDiscussionStateFilter
     * @uses ArrayOfDiscussionStateFilter::setDiscussionStateFilter()
     * @param string[] $discussionStateFilter
     */
    public function __construct(array $discussionStateFilter = array())
    {
        $this
            ->setDiscussionStateFilter($discussionStateFilter);
    }
    /**
     * Get DiscussionStateFilter value
     * @return string[]|null
     */
    public function getDiscussionStateFilter()
    {
        return $this->DiscussionStateFilter;
    }
    /**
     * This method is responsible for validating the values passed to the setDiscussionStateFilter method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDiscussionStateFilter method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDiscussionStateFilterForArrayConstraintsFromSetDiscussionStateFilter(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfDiscussionStateFilterDiscussionStateFilterItem) {
            // validation for constraint: enumeration
            if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::valueIsValid($arrayOfDiscussionStateFilterDiscussionStateFilterItem)) {
                $invalidValues[] = is_object($arrayOfDiscussionStateFilterDiscussionStateFilterItem) ? get_class($arrayOfDiscussionStateFilterDiscussionStateFilterItem) : sprintf('%s(%s)', gettype($arrayOfDiscussionStateFilterDiscussionStateFilterItem), var_export($arrayOfDiscussionStateFilterDiscussionStateFilterItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\DiscussionStateFilter', is_array($invalidValues) ? implode(', ', $invalidValues) : var_export($invalidValues, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::getValidValues()));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set DiscussionStateFilter value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::getValidValues()
     * @throws \InvalidArgumentException
     * @param string[] $discussionStateFilter
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter
     */
    public function setDiscussionStateFilter(array $discussionStateFilter = array())
    {
        // validation for constraint: array
        if ('' !== ($discussionStateFilterArrayErrorMessage = self::validateDiscussionStateFilterForArrayConstraintsFromSetDiscussionStateFilter($discussionStateFilter))) {
            throw new \InvalidArgumentException($discussionStateFilterArrayErrorMessage, __LINE__);
        }
        $this->DiscussionStateFilter = $discussionStateFilter;
        return $this;
    }
    /**
     * Add item to DiscussionStateFilter value
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::valueIsValid()
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter
     */
    public function addToDiscussionStateFilter($item)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::valueIsValid($item)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\DiscussionStateFilter', is_array($item) ? implode(', ', $item) : var_export($item, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::getValidValues())), __LINE__);
        }
        $this->DiscussionStateFilter[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return string|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return string|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return string|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return string|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return string|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Add element to array
     * @see AbstractStructArrayBase::add()
     * @throws \InvalidArgumentException
     * @uses \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::valueIsValid()
     * @param string $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter
     */
    public function add($item)
    {
        // validation for constraint: enumeration
        if (!\A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::valueIsValid($item)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \A4BGroup\Client\CDiscount\EnumType\DiscussionStateFilter', is_array($item) ? implode(', ', $item) : var_export($item, true), implode(', ', \A4BGroup\Client\CDiscountPublicClient\EnumType\DiscussionStateFilter::getValidValues())), __LINE__);
        }
        return parent::add($item);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string DiscussionStateFilter
     */
    public function getAttributeName()
    {
        return 'DiscussionStateFilter';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfDiscussionStateFilter
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
