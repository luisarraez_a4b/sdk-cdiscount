<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfFulfilmentOrderLineRequest ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfFulfilmentOrderLineRequest
 * @subpackage Arrays
 */
class ArrayOfFulfilmentOrderLineRequest extends AbstractStructArrayBase
{
    /**
     * The FulfilmentOrderLineRequest
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest[]
     */
    public $FulfilmentOrderLineRequest;
    /**
     * Constructor method for ArrayOfFulfilmentOrderLineRequest
     * @uses ArrayOfFulfilmentOrderLineRequest::setFulfilmentOrderLineRequest()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest[] $fulfilmentOrderLineRequest
     */
    public function __construct(array $fulfilmentOrderLineRequest = array())
    {
        $this
            ->setFulfilmentOrderLineRequest($fulfilmentOrderLineRequest);
    }
    /**
     * Get FulfilmentOrderLineRequest value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest[]|null
     */
    public function getFulfilmentOrderLineRequest()
    {
        return isset($this->FulfilmentOrderLineRequest) ? $this->FulfilmentOrderLineRequest : null;
    }
    /**
     * This method is responsible for validating the values passed to the setFulfilmentOrderLineRequest method
     * This method is willingly generated in order to preserve the one-line inline validation within the setFulfilmentOrderLineRequest method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateFulfilmentOrderLineRequestForArrayConstraintsFromSetFulfilmentOrderLineRequest(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfFulfilmentOrderLineRequestFulfilmentOrderLineRequestItem) {
            // validation for constraint: itemType
            if (!$arrayOfFulfilmentOrderLineRequestFulfilmentOrderLineRequestItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest) {
                $invalidValues[] = is_object($arrayOfFulfilmentOrderLineRequestFulfilmentOrderLineRequestItem) ? get_class($arrayOfFulfilmentOrderLineRequestFulfilmentOrderLineRequestItem) : sprintf('%s(%s)', gettype($arrayOfFulfilmentOrderLineRequestFulfilmentOrderLineRequestItem), var_export($arrayOfFulfilmentOrderLineRequestFulfilmentOrderLineRequestItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The FulfilmentOrderLineRequest property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentOrderLineRequest, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set FulfilmentOrderLineRequest value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest[] $fulfilmentOrderLineRequest
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest
     */
    public function setFulfilmentOrderLineRequest(array $fulfilmentOrderLineRequest = array())
    {
        // validation for constraint: array
        if ('' !== ($fulfilmentOrderLineRequestArrayErrorMessage = self::validateFulfilmentOrderLineRequestForArrayConstraintsFromSetFulfilmentOrderLineRequest($fulfilmentOrderLineRequest))) {
            throw new \InvalidArgumentException($fulfilmentOrderLineRequestArrayErrorMessage, __LINE__);
        }
        if (is_null($fulfilmentOrderLineRequest) || (is_array($fulfilmentOrderLineRequest) && empty($fulfilmentOrderLineRequest))) {
            unset($this->FulfilmentOrderLineRequest);
        } else {
            $this->FulfilmentOrderLineRequest = $fulfilmentOrderLineRequest;
        }
        return $this;
    }
    /**
     * Add item to FulfilmentOrderLineRequest value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest
     */
    public function addToFulfilmentOrderLineRequest(\A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest) {
            throw new \InvalidArgumentException(sprintf('The FulfilmentOrderLineRequest property can only contain items of type \A4BGroup\Client\CDiscount\StructType\FulfilmentOrderLineRequest, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->FulfilmentOrderLineRequest[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\FulfilmentOrderLineRequest|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string FulfilmentOrderLineRequest
     */
    public function getAttributeName()
    {
        return 'FulfilmentOrderLineRequest';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfFulfilmentOrderLineRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
