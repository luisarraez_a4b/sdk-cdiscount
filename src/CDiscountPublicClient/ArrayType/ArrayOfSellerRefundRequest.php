<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfSellerRefundRequest ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfSellerRefundRequest
 * @subpackage Arrays
 */
class ArrayOfSellerRefundRequest extends AbstractStructArrayBase
{
    /**
     * The SellerRefundRequest
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest[]
     */
    public $SellerRefundRequest;
    /**
     * Constructor method for ArrayOfSellerRefundRequest
     * @uses ArrayOfSellerRefundRequest::setSellerRefundRequest()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest[] $sellerRefundRequest
     */
    public function __construct(array $sellerRefundRequest = array())
    {
        $this
            ->setSellerRefundRequest($sellerRefundRequest);
    }
    /**
     * Get SellerRefundRequest value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest[]|null
     */
    public function getSellerRefundRequest()
    {
        return isset($this->SellerRefundRequest) ? $this->SellerRefundRequest : null;
    }
    /**
     * This method is responsible for validating the values passed to the setSellerRefundRequest method
     * This method is willingly generated in order to preserve the one-line inline validation within the setSellerRefundRequest method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateSellerRefundRequestForArrayConstraintsFromSetSellerRefundRequest(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfSellerRefundRequestSellerRefundRequestItem) {
            // validation for constraint: itemType
            if (!$arrayOfSellerRefundRequestSellerRefundRequestItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest) {
                $invalidValues[] = is_object($arrayOfSellerRefundRequestSellerRefundRequestItem) ? get_class($arrayOfSellerRefundRequestSellerRefundRequestItem) : sprintf('%s(%s)', gettype($arrayOfSellerRefundRequestSellerRefundRequestItem), var_export($arrayOfSellerRefundRequestSellerRefundRequestItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The SellerRefundRequest property can only contain items of type \A4BGroup\Client\CDiscount\StructType\SellerRefundRequest, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set SellerRefundRequest value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest[] $sellerRefundRequest
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest
     */
    public function setSellerRefundRequest(array $sellerRefundRequest = array())
    {
        // validation for constraint: array
        if ('' !== ($sellerRefundRequestArrayErrorMessage = self::validateSellerRefundRequestForArrayConstraintsFromSetSellerRefundRequest($sellerRefundRequest))) {
            throw new \InvalidArgumentException($sellerRefundRequestArrayErrorMessage, __LINE__);
        }
        if (is_null($sellerRefundRequest) || (is_array($sellerRefundRequest) && empty($sellerRefundRequest))) {
            unset($this->SellerRefundRequest);
        } else {
            $this->SellerRefundRequest = $sellerRefundRequest;
        }
        return $this;
    }
    /**
     * Add item to SellerRefundRequest value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest
     */
    public function addToSellerRefundRequest(\A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest) {
            throw new \InvalidArgumentException(sprintf('The SellerRefundRequest property can only contain items of type \A4BGroup\Client\CDiscount\StructType\SellerRefundRequest, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->SellerRefundRequest[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\SellerRefundRequest|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string SellerRefundRequest
     */
    public function getAttributeName()
    {
        return 'SellerRefundRequest';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfSellerRefundRequest
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
