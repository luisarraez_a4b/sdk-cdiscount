<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfOfferProduct ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfOfferProduct
 * @subpackage Arrays
 */
class ArrayOfOfferProduct extends AbstractStructArrayBase
{
    /**
     * The OfferProduct
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct[]
     */
    public $OfferProduct;
    /**
     * Constructor method for ArrayOfOfferProduct
     * @uses ArrayOfOfferProduct::setOfferProduct()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct[] $offerProduct
     */
    public function __construct(array $offerProduct = array())
    {
        $this
            ->setOfferProduct($offerProduct);
    }
    /**
     * Get OfferProduct value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct[]|null
     */
    public function getOfferProduct()
    {
        return isset($this->OfferProduct) ? $this->OfferProduct : null;
    }
    /**
     * This method is responsible for validating the values passed to the setOfferProduct method
     * This method is willingly generated in order to preserve the one-line inline validation within the setOfferProduct method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateOfferProductForArrayConstraintsFromSetOfferProduct(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfOfferProductOfferProductItem) {
            // validation for constraint: itemType
            if (!$arrayOfOfferProductOfferProductItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct) {
                $invalidValues[] = is_object($arrayOfOfferProductOfferProductItem) ? get_class($arrayOfOfferProductOfferProductItem) : sprintf('%s(%s)', gettype($arrayOfOfferProductOfferProductItem), var_export($arrayOfOfferProductOfferProductItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The OfferProduct property can only contain items of type \A4BGroup\Client\CDiscount\StructType\OfferProduct, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set OfferProduct value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct[] $offerProduct
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct
     */
    public function setOfferProduct(array $offerProduct = array())
    {
        // validation for constraint: array
        if ('' !== ($offerProductArrayErrorMessage = self::validateOfferProductForArrayConstraintsFromSetOfferProduct($offerProduct))) {
            throw new \InvalidArgumentException($offerProductArrayErrorMessage, __LINE__);
        }
        if (is_null($offerProduct) || (is_array($offerProduct) && empty($offerProduct))) {
            unset($this->OfferProduct);
        } else {
            $this->OfferProduct = $offerProduct;
        }
        return $this;
    }
    /**
     * Add item to OfferProduct value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct
     */
    public function addToOfferProduct(\A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct) {
            throw new \InvalidArgumentException(sprintf('The OfferProduct property can only contain items of type \A4BGroup\Client\CDiscount\StructType\OfferProduct, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->OfferProduct[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\OfferProduct|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string OfferProduct
     */
    public function getAttributeName()
    {
        return 'OfferProduct';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOfferProduct
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
