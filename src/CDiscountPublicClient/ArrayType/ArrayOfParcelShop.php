<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfParcelShop ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfParcelShop
 * @subpackage Arrays
 */
class ArrayOfParcelShop extends AbstractStructArrayBase
{
    /**
     * The ParcelShop
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop[]
     */
    public $ParcelShop;
    /**
     * Constructor method for ArrayOfParcelShop
     * @uses ArrayOfParcelShop::setParcelShop()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop[] $parcelShop
     */
    public function __construct(array $parcelShop = array())
    {
        $this
            ->setParcelShop($parcelShop);
    }
    /**
     * Get ParcelShop value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop[]|null
     */
    public function getParcelShop()
    {
        return isset($this->ParcelShop) ? $this->ParcelShop : null;
    }
    /**
     * This method is responsible for validating the values passed to the setParcelShop method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParcelShop method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParcelShopForArrayConstraintsFromSetParcelShop(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfParcelShopParcelShopItem) {
            // validation for constraint: itemType
            if (!$arrayOfParcelShopParcelShopItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop) {
                $invalidValues[] = is_object($arrayOfParcelShopParcelShopItem) ? get_class($arrayOfParcelShopParcelShopItem) : sprintf('%s(%s)', gettype($arrayOfParcelShopParcelShopItem), var_export($arrayOfParcelShopParcelShopItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ParcelShop property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ParcelShop, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ParcelShop value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop[] $parcelShop
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop
     */
    public function setParcelShop(array $parcelShop = array())
    {
        // validation for constraint: array
        if ('' !== ($parcelShopArrayErrorMessage = self::validateParcelShopForArrayConstraintsFromSetParcelShop($parcelShop))) {
            throw new \InvalidArgumentException($parcelShopArrayErrorMessage, __LINE__);
        }
        if (is_null($parcelShop) || (is_array($parcelShop) && empty($parcelShop))) {
            unset($this->ParcelShop);
        } else {
            $this->ParcelShop = $parcelShop;
        }
        return $this;
    }
    /**
     * Add item to ParcelShop value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop
     */
    public function addToParcelShop(\A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop) {
            throw new \InvalidArgumentException(sprintf('The ParcelShop property can only contain items of type \A4BGroup\Client\CDiscount\StructType\ParcelShop, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ParcelShop[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\ParcelShop|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string ParcelShop
     */
    public function getAttributeName()
    {
        return 'ParcelShop';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfParcelShop
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
