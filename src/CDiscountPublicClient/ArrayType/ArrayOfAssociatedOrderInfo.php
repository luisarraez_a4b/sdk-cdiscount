<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfAssociatedOrderInfo ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfAssociatedOrderInfo
 * @subpackage Arrays
 */
class ArrayOfAssociatedOrderInfo extends AbstractStructArrayBase
{
    /**
     * The AssociatedOrderInfo
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo[]
     */
    public $AssociatedOrderInfo;
    /**
     * Constructor method for ArrayOfAssociatedOrderInfo
     * @uses ArrayOfAssociatedOrderInfo::setAssociatedOrderInfo()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo[] $associatedOrderInfo
     */
    public function __construct(array $associatedOrderInfo = array())
    {
        $this
            ->setAssociatedOrderInfo($associatedOrderInfo);
    }
    /**
     * Get AssociatedOrderInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo[]|null
     */
    public function getAssociatedOrderInfo()
    {
        return isset($this->AssociatedOrderInfo) ? $this->AssociatedOrderInfo : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAssociatedOrderInfo method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAssociatedOrderInfo method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAssociatedOrderInfoForArrayConstraintsFromSetAssociatedOrderInfo(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfAssociatedOrderInfoAssociatedOrderInfoItem) {
            // validation for constraint: itemType
            if (!$arrayOfAssociatedOrderInfoAssociatedOrderInfoItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo) {
                $invalidValues[] = is_object($arrayOfAssociatedOrderInfoAssociatedOrderInfoItem) ? get_class($arrayOfAssociatedOrderInfoAssociatedOrderInfoItem) : sprintf('%s(%s)', gettype($arrayOfAssociatedOrderInfoAssociatedOrderInfoItem), var_export($arrayOfAssociatedOrderInfoAssociatedOrderInfoItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The AssociatedOrderInfo property can only contain items of type \A4BGroup\Client\CDiscount\StructType\AssociatedOrderInfo, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set AssociatedOrderInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo[] $associatedOrderInfo
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo
     */
    public function setAssociatedOrderInfo(array $associatedOrderInfo = array())
    {
        // validation for constraint: array
        if ('' !== ($associatedOrderInfoArrayErrorMessage = self::validateAssociatedOrderInfoForArrayConstraintsFromSetAssociatedOrderInfo($associatedOrderInfo))) {
            throw new \InvalidArgumentException($associatedOrderInfoArrayErrorMessage, __LINE__);
        }
        if (is_null($associatedOrderInfo) || (is_array($associatedOrderInfo) && empty($associatedOrderInfo))) {
            unset($this->AssociatedOrderInfo);
        } else {
            $this->AssociatedOrderInfo = $associatedOrderInfo;
        }
        return $this;
    }
    /**
     * Add item to AssociatedOrderInfo value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo
     */
    public function addToAssociatedOrderInfo(\A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo) {
            throw new \InvalidArgumentException(sprintf('The AssociatedOrderInfo property can only contain items of type \A4BGroup\Client\CDiscount\StructType\AssociatedOrderInfo, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->AssociatedOrderInfo[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\AssociatedOrderInfo|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string AssociatedOrderInfo
     */
    public function getAttributeName()
    {
        return 'AssociatedOrderInfo';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfAssociatedOrderInfo
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
