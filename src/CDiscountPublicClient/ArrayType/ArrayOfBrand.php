<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfBrand ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfBrand
 * @subpackage Arrays
 */
class ArrayOfBrand extends AbstractStructArrayBase
{
    /**
     * The Brand
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\Brand[]
     */
    public $Brand;
    /**
     * Constructor method for ArrayOfBrand
     * @uses ArrayOfBrand::setBrand()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Brand[] $brand
     */
    public function __construct(array $brand = array())
    {
        $this
            ->setBrand($brand);
    }
    /**
     * Get Brand value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Brand[]|null
     */
    public function getBrand()
    {
        return isset($this->Brand) ? $this->Brand : null;
    }
    /**
     * This method is responsible for validating the values passed to the setBrand method
     * This method is willingly generated in order to preserve the one-line inline validation within the setBrand method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateBrandForArrayConstraintsFromSetBrand(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfBrandBrandItem) {
            // validation for constraint: itemType
            if (!$arrayOfBrandBrandItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\Brand) {
                $invalidValues[] = is_object($arrayOfBrandBrandItem) ? get_class($arrayOfBrandBrandItem) : sprintf('%s(%s)', gettype($arrayOfBrandBrandItem), var_export($arrayOfBrandBrandItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The Brand property can only contain items of type \A4BGroup\Client\CDiscount\StructType\Brand, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set Brand value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Brand[] $brand
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfBrand
     */
    public function setBrand(array $brand = array())
    {
        // validation for constraint: array
        if ('' !== ($brandArrayErrorMessage = self::validateBrandForArrayConstraintsFromSetBrand($brand))) {
            throw new \InvalidArgumentException($brandArrayErrorMessage, __LINE__);
        }
        if (is_null($brand) || (is_array($brand) && empty($brand))) {
            unset($this->Brand);
        } else {
            $this->Brand = $brand;
        }
        return $this;
    }
    /**
     * Add item to Brand value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Brand $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfBrand
     */
    public function addToBrand(\A4BGroup\Client\CDiscountPublicClient\StructType\Brand $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\Brand) {
            throw new \InvalidArgumentException(sprintf('The Brand property can only contain items of type \A4BGroup\Client\CDiscount\StructType\Brand, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->Brand[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Brand|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Brand|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Brand|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Brand|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Brand|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string Brand
     */
    public function getAttributeName()
    {
        return 'Brand';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfBrand
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
