<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfVoucher ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfVoucher
 * @subpackage Arrays
 */
class ArrayOfVoucher extends AbstractStructArrayBase
{
    /**
     * The Voucher
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher[]
     */
    public $Voucher;
    /**
     * Constructor method for ArrayOfVoucher
     * @uses ArrayOfVoucher::setVoucher()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher[] $voucher
     */
    public function __construct(array $voucher = array())
    {
        $this
            ->setVoucher($voucher);
    }
    /**
     * Get Voucher value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher[]|null
     */
    public function getVoucher()
    {
        return isset($this->Voucher) ? $this->Voucher : null;
    }
    /**
     * This method is responsible for validating the values passed to the setVoucher method
     * This method is willingly generated in order to preserve the one-line inline validation within the setVoucher method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateVoucherForArrayConstraintsFromSetVoucher(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfVoucherVoucherItem) {
            // validation for constraint: itemType
            if (!$arrayOfVoucherVoucherItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher) {
                $invalidValues[] = is_object($arrayOfVoucherVoucherItem) ? get_class($arrayOfVoucherVoucherItem) : sprintf('%s(%s)', gettype($arrayOfVoucherVoucherItem), var_export($arrayOfVoucherVoucherItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The Voucher property can only contain items of type \A4BGroup\Client\CDiscount\StructType\Voucher, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set Voucher value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher[] $voucher
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVoucher
     */
    public function setVoucher(array $voucher = array())
    {
        // validation for constraint: array
        if ('' !== ($voucherArrayErrorMessage = self::validateVoucherForArrayConstraintsFromSetVoucher($voucher))) {
            throw new \InvalidArgumentException($voucherArrayErrorMessage, __LINE__);
        }
        if (is_null($voucher) || (is_array($voucher) && empty($voucher))) {
            unset($this->Voucher);
        } else {
            $this->Voucher = $voucher;
        }
        return $this;
    }
    /**
     * Add item to Voucher value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVoucher
     */
    public function addToVoucher(\A4BGroup\Client\CDiscountPublicClient\StructType\Voucher $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher) {
            throw new \InvalidArgumentException(sprintf('The Voucher property can only contain items of type \A4BGroup\Client\CDiscount\StructType\Voucher, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->Voucher[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Voucher|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string Voucher
     */
    public function getAttributeName()
    {
        return 'Voucher';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfVoucher
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
