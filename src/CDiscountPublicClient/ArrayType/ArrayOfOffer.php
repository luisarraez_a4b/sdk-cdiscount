<?php

namespace A4BGroup\Client\CDiscountPublicClient\ArrayType;

use \WsdlToPhp\PackageBase\AbstractStructArrayBase;

/**
 * This class stands for ArrayOfOffer ArrayType
 * Meta informations extracted from the WSDL
 * - nillable: true
 * - type: tns:ArrayOfOffer
 * @subpackage Arrays
 */
class ArrayOfOffer extends AbstractStructArrayBase
{
    /**
     * The Offer
     * Meta informations extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \A4BGroup\Client\CDiscountPublicClient\StructType\Offer[]
     */
    public $Offer;
    /**
     * Constructor method for ArrayOfOffer
     * @uses ArrayOfOffer::setOffer()
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Offer[] $offer
     */
    public function __construct(array $offer = array())
    {
        $this
            ->setOffer($offer);
    }
    /**
     * Get Offer value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer[]|null
     */
    public function getOffer()
    {
        return isset($this->Offer) ? $this->Offer : null;
    }
    /**
     * This method is responsible for validating the values passed to the setOffer method
     * This method is willingly generated in order to preserve the one-line inline validation within the setOffer method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateOfferForArrayConstraintsFromSetOffer(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $arrayOfOfferOfferItem) {
            // validation for constraint: itemType
            if (!$arrayOfOfferOfferItem instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\Offer) {
                $invalidValues[] = is_object($arrayOfOfferOfferItem) ? get_class($arrayOfOfferOfferItem) : sprintf('%s(%s)', gettype($arrayOfOfferOfferItem), var_export($arrayOfOfferOfferItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The Offer property can only contain items of type \A4BGroup\Client\CDiscount\StructType\Offer, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set Offer value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Offer[] $offer
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer
     */
    public function setOffer(array $offer = array())
    {
        // validation for constraint: array
        if ('' !== ($offerArrayErrorMessage = self::validateOfferForArrayConstraintsFromSetOffer($offer))) {
            throw new \InvalidArgumentException($offerArrayErrorMessage, __LINE__);
        }
        if (is_null($offer) || (is_array($offer) && empty($offer))) {
            unset($this->Offer);
        } else {
            $this->Offer = $offer;
        }
        return $this;
    }
    /**
     * Add item to Offer value
     * @throws \InvalidArgumentException
     * @param \A4BGroup\Client\CDiscountPublicClient\StructType\Offer $item
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer
     */
    public function addToOffer(\A4BGroup\Client\CDiscountPublicClient\StructType\Offer $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \A4BGroup\Client\CDiscountPublicClient\StructType\Offer) {
            throw new \InvalidArgumentException(sprintf('The Offer property can only contain items of type \A4BGroup\Client\CDiscount\StructType\Offer, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->Offer[] = $item;
        return $this;
    }
    /**
     * Returns the current element
     * @see AbstractStructArrayBase::current()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer|null
     */
    public function current()
    {
        return parent::current();
    }
    /**
     * Returns the indexed element
     * @see AbstractStructArrayBase::item()
     * @param int $index
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer|null
     */
    public function item($index)
    {
        return parent::item($index);
    }
    /**
     * Returns the first element
     * @see AbstractStructArrayBase::first()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer|null
     */
    public function first()
    {
        return parent::first();
    }
    /**
     * Returns the last element
     * @see AbstractStructArrayBase::last()
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer|null
     */
    public function last()
    {
        return parent::last();
    }
    /**
     * Returns the element at the offset
     * @see AbstractStructArrayBase::offsetGet()
     * @param int $offset
     * @return \A4BGroup\Client\CDiscountPublicClient\StructType\Offer|null
     */
    public function offsetGet($offset)
    {
        return parent::offsetGet($offset);
    }
    /**
     * Returns the attribute name
     * @see AbstractStructArrayBase::getAttributeName()
     * @return string Offer
     */
    public function getAttributeName()
    {
        return 'Offer';
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see AbstractStructArrayBase::__set_state()
     * @uses AbstractStructArrayBase::__set_state()
     * @param array $array the exported values
     * @return \A4BGroup\Client\CDiscountPublicClient\ArrayType\ArrayOfOffer
     */
    public static function __set_state(array $array)
    {
        return parent::__set_state($array);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
